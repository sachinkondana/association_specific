(function() {
    var socreCardContent, socreCardUpdatedContent;
    var editscore = 0;
    var scores;
    var markers = null;
    var frontPar = 0;
    var backPar = 0;
    var frontPlayerScores = [];
    var backPlayerScores = [];
    var timer;
    var scorecardSubmitted = false;
    var modifiedCells = {};
    var autoRefresh = null;
    var enableDebugMode = 0;
    var viewType = 1; // Default HoleView. 0 --> TableView
    var swiper = null;
    "use strict";

    /**
     * Get Scorecard already added by the user
     */
    getScoreCard = function() {
        var roundId = localStorage.getItem("round");
        var actions = {
            "roundId": roundId
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            socreCardContent = data;
            if (data.status == "success") {
                var playerlist = {
                    "created_by": localStorage.getItem("memberid"),

                    "tee_time_slot": localStorage.getItem("tee_slot"),

                    "tee_time": localStorage.getItem("tee_time"),

                    "players": []
                };
                localStorage.setItem("playerlist", JSON.stringify(playerlist));
                swal.close();
                displayInitialScoreCard(data);
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golf/roundHoleSets", actions, successCallback, errorCallback);
    }


    disms = function() {
        $("#float-text").val("");
        $("#handicap").val("0");
        $("#user_id").val("");
        $("#catgory").val("").change();
        $("#nameerror").css("display", "none");
        $("#handierror").css("display", "none");
        $("#categoryerror").css("display", "none");
    }


    /**
     * Add a player to the scorecard
     * @name Player Name
     * @handicap Player Handicap
     * @Type Tee
     * @userid Player Id
     * @gender Player Gender
     */
    addPlayer = function(name, handicap, type, userid) {
        if ((name.replace(/\s+/, "") != "") && (handicap.replace(/\s+/, "") != "0") &&
            (handicap.replace(/\s+/, "") != "") && (type != "")) {
            var playerlist = JSON.parse(localStorage.getItem("playerlist"));
            if (playerlist.players.length < 4) // Only 4 players per score card
            {
                var tempjson = {
                    "player": playerlist.players.length + 1,
                    "name": name,
                    "handicap": handicap,
                    "category": type,
                    "id": userid
                };
                var obj = playerlist;
                obj['players'].push(tempjson);
                var jsonStr = JSON.stringify(obj);
                localStorage.setItem("playerlist", jsonStr);
                showPlayer(obj);
                if (obj.players.length >= 1)
                    $('#Golf_Scorecard_Create_button').css("display", "inline");
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: localizeString("Alert_Text_scorecard"),
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            $("#float-text").val("");
            $("#handicap").val("");
            $("#user_id").val("");
            $('#category').val('');
            $("#nameerror").css("display", "none");
            $("#handierror").css("display", "none");
            $("#categoryerror").css("display", "none");
        } else {
            if ((name.replace(/\s+/, "") == "") && (handicap.replace(/\s+/, "") != "0") && (handicap.replace(/\s+/, "") != "")) {
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
            } else if ((name.replace(/\s+/, "") != "") && (handicap.replace(/\s+/, "") == "0") && (handicap.replace(/\s+/, "") != "")) {
                $("#handierror").css("display", "block");
                $("#handierror").html(localizeString("ValidationError_Handicap0") + '<i class="form-help-icon icon">error</i>');
            } else if ((name.replace(/\s+/, "") != "") && (handicap.replace(/\s+/, "") != "0") && (handicap.replace(/\s+/, "") == "")) {
                $("#handierror").css("display", "block");
                $("#handierror").html(localizeString("ValidationError_HandicapBlank") + '<i class="form-help-icon icon">error</i>');
            } else if ((name.replace(/\s+/, "") == "") && (handicap.replace(/\s+/, "") == "0")) {
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
                $("#handierror").css("display", "block");
                $("#handierror").html(localizeString("ValidationError_Handicap0") + '<i class="form-help-icon icon">error</i>');
            } else if ((name.replace(/\s+/, "") == "") && (handicap.replace(/\s+/, "") == "")) {
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
                $("#handierror").css("display", "block");
                $("#handierror").html(localizeString("ValidationError_HandicapBlank") + '<i class="form-help-icon icon">error</i>');
            }
            if (type == "") {
                $("#categoryerror").css("display", "block");
                $("#categoryerror").html(localizeString("ValidationError_CategoryNotSelected") + '<i class="form-help-icon icon">error</i>');
            }
        }
    }


    /**
     * Show player in the player list.
     */
    showPlayer = function(JSONVal) {
        $('#user-list').html("");
        var name, handicap, teeboxes;
        for (i = 0; i < JSONVal.players.length; i++) {
            name = JSONVal.players[i].name;
            handicap = JSONVal.players[i].handicap.trim();
            teeboxes = JSONVal.players[i].category;
            var maindiv = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
            var maindiv1 = $('<div class="card"></div>');
            var maindiv2 = $('<div class="card-main"> </div>');
            var carddiv = $('<div class="card-header"></div>');
            //var div1 = $('<div class="card-header-side pull-left"><div class="avatar"> <img alt="No Image" src="images/avatar-001.jpg"> </div></div>');
            var div2 = $('<div class="card-inner" style="line-height:16px!important;"> <span class="bold-match-color pnfont" style="line-height:20px;">' + name + '</span><a href="#" class="close-btn" onclick="removePlayer(\'' + i + '\');"><i class="fa fa-close"></i></a> </br> <div class="text" style="line-height:24px;"><span style="float:left;">Handicap:' + handicap + '</span> <span style="float:right;">Tee:' + teeboxes + '</span></div></div>');
            var td = $('<td></td>');
            var tr = $('<tr></tr>');
            carddiv.append( /*div1,*/ div2);
            maindiv2.append(carddiv /*, div3*/ );
            maindiv1.append(maindiv2)
            maindiv.append(maindiv1)
            $('#user-list').append(maindiv);
        }
        $('#user-list').css('display', 'block');

        if (JSONVal.players.length == 4) {
            $('#Scorecard_Label_HoleView').css('display', 'none');
            $('#Tournament_Button_AddPlayer').css('display', 'none');
        } else {
            $('#Tournament_Button_AddPlayer').css('display', 'inline');
        }
    }


    keysTableView = function(i) {
        viewType = 0;
        var selId = localStorage.getItem("selectid"); //p_pid_h_hid
        var prevselId = localStorage.getItem("prevselectid");

        /* Update modified cell list to avoid updating them on refresh */
        if ((localStorage.modifiedCells == null) || (localStorage.modifiedCells == undefined)) {
            localStorage.setItem("modifiedCells", JSON.stringify(modifiedCells));
        }
        var modCells = JSON.parse(localStorage.getItem("modifiedCells"));

        if ((selId == prevselId) && (document.getElementById(selId).getElementsByTagName("a")[0].innerHTML == "1")) {
            modCells[selId] = modCells[selId] + '' + i;
        } else {
            modCells[selId] = i; // Update ModifiedCells
        }
        localStorage.setItem("modifiedCells", JSON.stringify(modCells));
        var ids = selId.split("_");
        pid = parseInt(ids[1]);
        hid = parseInt(ids[3]) - 1;

        if (i == "") {
            document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = "";
            scores[hid].user_stroke[0]['player_' + pid] = document.getElementById(selId).getElementsByTagName("a")[0].innerHTML;
            markers[pid - 1][hid] = i;
        } else {
            if ((selId == prevselId) && (document.getElementById(selId).getElementsByTagName("a")[0].innerHTML == "1")) {
                document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = document.getElementById(selId).getElementsByTagName("a")[0].innerHTML + '' + i;
                markers[pid - 1][hid] = parseInt(markers[pid - 1][hid] + '' + i);
            } else {
                document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = i;
                markers[pid - 1][hid] = parseInt(i);
            }
            scores[hid].user_stroke[0]['player_' + pid] = document.getElementById(localStorage.getItem("selectid")).getElementsByTagName("a")[0].innerHTML;
        }
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        scoreCardData.scores = scores;
        localStorage.setItem("UserScoreCardData", JSON.stringify(scoreCardData));
        localStorage.setItem("prevselectid", selId);
        sumup();
        if ("1" != document.getElementById(selId).getElementsByTagName("a")[0].innerHTML) {
            gotoNextTableView();
        }
    }


    /**
     * Incrementally add the user scores
     */
    sumup = function() {
        var allHolesPlayed = 1;
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        for (var k = 0; k < scoreCardData.players.length; k++) {
            frontPlayerScores[k] = 0;
            backPlayerScores[k] = 0;
        }
        if (scoreCardData.scores.length >= 9) {
            for (var i = 0; i < 9; i++) {
                for (var k = 0; k < scoreCardData.players.length; k++) {
                    var blk_id_front = "p_" + (k + 1) + "_h_" + (i + 1) + "";
                    if ((markers[k][i] == 0 || markers[k][i] == "0") &&
                        (document.getElementById(blk_id_front).getElementsByTagName("a")[0].innerHTML != "0"))
                        allHolesPlayed = 0;
                    if (markers[k][i] != "X" && markers[k][i] != "")
                        frontPlayerScores[k] = parseInt(frontPlayerScores[k]) + parseInt(markers[k][i]);
                    if (scoreCardData.round_info.type == 2) {
                        var blk_id_back = "p_" + (k + 1) + "_h_" + (9 + i + 1); // player_1_hole_1
                        if ((markers[k][i + 9] == 0 || markers[k][i + 9] == "0") &&
                            (document.getElementById(blk_id_back).getElementsByTagName("a")[0].innerHTML != "0"))
                            allHolesPlayed = 0;
                        if (markers[k][i + 9] != "X" && markers[k][i + 9] != "")
                            backPlayerScores[k] = parseInt(backPlayerScores[k]) + parseInt(markers[k][i + 9]);
                    }
                }
            }

            /* Required only for table view */
            if (viewType == 0) { // TableView
                for (var k = 0; k < frontPlayerScores.length; k++) {
                    if ((scoreCardData.round_info.type == 1) && allHolesPlayed) // Set total only when all 9 holes have been played
                        document.getElementById("front_" + k + "").getElementsByTagName("a")[0].innerHTML = frontPlayerScores[k];
                    else
                        document.getElementById("front_" + k + "").getElementsByTagName("a")[0].innerHTML = frontPlayerScores[k];
                    if (scoreCardData.round_info.type >= 2) {
                        document.getElementById("in_" + k + "").getElementsByTagName("a")[0].innerHTML = frontPlayerScores[k];
                        if (allHolesPlayed) // Set total only when all 18 holes have been played
                            document.getElementById("back_" + k + "").getElementsByTagName("a")[0].innerHTML = (frontPlayerScores[k] + backPlayerScores[k]);
                    }
                }
            }
        }
    }


    /**
     * Create new scorecard with tee-time, round and player info
     */
    createScorecard = function() {
        if (localStorage.getItem("isLoggedIn") != "true") {
            swal({
                title: localizeString("Alert_Title_AddPromo"),
                text: localizeString("Alert_Text_AddScorecard"),
                type: "info",
                showCancelButton: false,
                closeOnConfirm: true,
                showLoaderOnConfirm: false,
                confirmButtonText: localizeString("Alert_Button_Ok")
            });
            return;
        }

        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var tim = h + ":" + m;
        editscore = 1;
        var playerlist = JSON.parse(localStorage.getItem("playerlist"));
        var tee_date = localStorage.getItem("tee_date");
        var tee_time = localStorage.getItem("tee_time");
        var tee_slot = localStorage.getItem("tee_slot");
        var round = localStorage.getItem("round");
        var d = new Date(tee_time);

        if (playerlist['players'].length > 0) {
            var actions = {
                "created_by": "",
                "tee_time_slot": tee_slot,
                "tee_date": tee_date,
                "tee_time": tee_time,
                "roundId": round,
                "players": playerlist['players'],
                "club_id": localStorage.getItem("GOLF_CLUB_ID")
            };
            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                socreCardUpdatedContent = data.data;
                if (data.status == "success") {
                    swal.close();
                    localStorage.setItem("score_card_id", data.data.content.id);
                    displayUserScoreCardHoleView(data);
                    $('#userlistdiv').html("<ul></ul>");
                } else {
                    $('#userlistdiv').html("<ul></ul>");
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
                window.plugins.spinnerDialog.hide();
            };

            errorCallback = function(data) {
                $('#userlistdiv').html("<ul></ul>");
            };
            postMessageToServerWithAuth("/golf/scorecard", actions, successCallback, errorCallback);

        } else {
            swal({
                title: localizeString("Alert_Title_NoPlayers"),
                text: localizeString("Alert_Text_NoPlayers"),
                type: "info",
                showCancelButton: false,
                closeOnConfirm: true,
                showLoaderOnConfirm: false,
                confirmButtonText: localizeString("Alert_Button_Ok")
            });
        }
    }


    displayUserScoreCardTableView = function(data) {
        localStorage.setItem("UserScoreCardData", JSON.stringify(data.data.content));
        localStorage.setItem("ImageBasePath", data.data.imageBasePath);
        slideview('score-card-table-view.html', 'script', 'left');
    }

    displayUserScoreCardHoleView = function(data) {
        localStorage.setItem("UserScoreCardData", JSON.stringify(data.data.content));
        localStorage.setItem("ImageBasePath", data.data.imageBasePath);
        slideview('score-card-hole-view.html', 'script', 'left');
    }

    setAddPlayerDefaults = function() {
        $('#category').val('');
    }

    /**
     * Display Initial Scorecard.
     * @side scorecard side
     * @thedata Data to be rendered on the scorecard.
     */
    displayInitialScoreCard = function(scorecardData) {
        var tee_boxes_array = [];
        var i = 0;
        var selcttee = -1;
        if (scorecardData.data.content.length <= 0) {
            $('#scoredetails').html('<p style="text-align:center"> Scorecard deails unavailable. Please try again later</p>');
            $('#scoredetails').css('display', 'block');
            return;
        } else {
            var p0 = $('<p><strong>' + localizeString("Account_Label_Date") + ':</strong> ' + localStorage.getItem("tee_date") + ', <strong>' + localizeString("Scorecard_Text_TeeTime") + ':</strong> ' + localStorage.getItem("tee_time") + '</p>');
            var p1 = $('<p id="startf"><strong>' + localizeString("Scorecard_Text_Start_From") + ':</strong> ' + localStorage.getItem("round_name") + '</p>');
            $('#scoredetails').append(p0, p1);
            $('#scoredetails').css("display", "block");
        }
        for (var j = 0; j < scorecardData.data.content.tee_boxes.length; j++) {
            tee_boxes_array[j] = scorecardData.data.content.tee_boxes[j].master_id;
            var option = document.createElement("option");
            option.setAttribute('value', scorecardData.data.content.tee_boxes[j].master_id);
            option.appendChild(document.createTextNode(scorecardData.data.content.tee_boxes[j].name.toUpperCase()));
            $("#category").append(option);
        }
    }


    stripSecondsField = function(dateTime) {
        dateTimeArray = dateTime.split(" ");
        timeArray = dateTimeArray[1].split(':');
        formatedTime = [timeArray[0], ':', timeArray[1]].join('');
        return [dateTimeArray[0], ' ', formatedTime].join('');
    }


    /**
     * Displays the user score card in table view
     */
    scoreCardTableView = function() {
        var scorecardDetails = JSON.parse(localStorage.getItem("UserScoreCardData"));
        var sDate = null;
        var sTime = null;
        var eDate = null;
        var eTime = null;
        viewType = 0;
        var allHolesPlayed = 1;

        scorecardSubmitted = (scorecardDetails.status == 1) ? true : false;
        $('#scoredetails').html("");
        var startDiv = $('<div style="width:100%"></div>');
        var p = $('<p><b>' + localizeString("Label_CreatedBy") + ': ' + scorecardDetails.created_by + '</b></p>');
        localStorage.setItem("Scorecard_updated_on", scorecardDetails.updated_on);
        localStorage.setItem("ScorecardPlayers", scorecardDetails.players.length);
        /* Fixed garbage selId issue in new scorecards */
        localStorage.setItem("selectid", "");
        if (!scorecardSubmitted) {
            var location = "Glof Course";
            var notes = " Created By: " + scorecardDetails.created_by + ", Tee-Time Slot: " + scorecardDetails.tee_time_slot;
            var eventName = "Golf Round";
            var tee_date_time = scorecardDetails.tee_time.split(" ");
            sDate = tee_date_time[0];
            sTime = tee_date_time[1];
            eDate = tee_date_time[0];
            var endTimeArr = tee_date_time[1].split(":");
            if (scorecardDetails.round_info.type == 2) { // 18 holes so end time Around 3 hrs later
                eTime = [parseInt(endTimeArr[0]) + 3, endTimeArr[1], endTimeArr[2]].join(":");
            } else { // end time Around 1.5 hrs later
                if ((parseInt(endTimeArr[1]) + 30) > 60)
                    eTime = [parseInt(endTimeArr[0]) + 2, parseInt(endTimeArr[1]) + 30 - 60, endTimeArr[2]].join(":");
                else
                    eTime = [parseInt(endTimeArr[0]) + 1, parseInt(endTimeArr[1]) + 30, endTimeArr[2]].join(":");
            }
            notes = notes.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + eventName + '\', \'' + location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
            startDiv.append(p, reminder);
        } else {
            startDiv.append(p);
        }
        var dispDat2 = getDisplayableDate(scorecardDetails.created_on.split(' ')[0]);
        var dispTim2 = getDisplayableTime(scorecardDetails.created_on.split(' ')[1]);
        var p0 = $('<p><b>' + localizeString("Label_CreatedOn") + ': ' + [dispDat2, dispTim2].join(' ') + '</b></p>');
        var datestr;
        if ((scorecardDetails.tee_time == "0000-00-00 00:00:00") || (scorecardDetails.tee_time == "00-00-0000 00:00:00")) {
            datestr = "Not Available";
        } else {
            var dispDat3 = getDisplayableDate(scorecardDetails.tee_time.split(' ')[0]);
            var dispTim3 = getDisplayableTime(scorecardDetails.tee_time.split(' ')[1]);
            datestr = [dispDat3, dispTim3].join(' ');
        }
        var p1 = $('<p><strong>' + localizeString("Label_TeeTime") + ':</strong> ' + datestr + '</p>');
        if (scorecardDetails.updated_on != null) {
            var datestr;
            if ((scorecardDetails.updated_on == "0000-00-00 00:00:00") || (scorecardDetails.updated_on == "00-00-0000 00:00:00")) {
                datestr = "Not Available";
            } else {
                var dispDat4 = getDisplayableDate(scorecardDetails.updated_on.split(' ')[0]);
                var dispTim4 = getDisplayableTime(scorecardDetails.updated_on.split(' ')[1]);
                datestr = [dispDat4, dispTim4].join(' ');
            }
            var p2 = $('<p><strong>' + localizeString("Label_LastUpdatedOn") + ':</strong> ' + datestr + '</p>');
            var p3 = $('<p id="updateby"><strong>' + localizeString("Label_LastUpdatedBy") + ':</strong> ' + scorecardDetails.updated_by + '</p>');
            $('#scoredetails').append(startDiv, p0, p1, p2, p3);
        } else {
            $('#scoredetails').append(startDiv, p0, p1);
        }

        $('#scoredetails').css("display", "block");

        $("#swipecont").html('');

        markers = new Array(scorecardDetails.players.length);
        /* Front side of the score card */
        if (scorecardDetails.round_info.type >= 1) {
            frontPar = 0;
            var swipe = $('<div class="swiper-slide" id=' + scorecardDetails.round_info.hole_set_1.id + '></div>');
            var first = $('<div class="head-hole" style="padding-top:17px!important;"><table width="96%" border="0" cellspacing="5" style="margin:0px auto;"><tbody><tr><td width="100%" align="center"><div class="hole-par-no" style="width:100%!important;"><div> ' + scorecardDetails.round_info.hole_set_1.name + ' </div></div></td></tr></tbody></table>');

            swipe.append(first);
            var topTable = $('<table cellspacing="0" class="table table-bordered box box-solid" id="table-front"></table>');
            var main = $('<tr style="color:#fff;"></tr>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Holes") + '</th>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Par") + '</th>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Index") + '</th>');

            scores = scorecardDetails.scores;
            for (var i = 0; i < scorecardDetails.players.length; i++) {
                var res = scorecardDetails.players[i].name.trim().split(" ");
                var name;
                if (res.length > 1) {
                    name = res[0].charAt(0).toUpperCase() + "" + res[1].charAt(0).toUpperCase();
                } else {
                    name = res[0].charAt(0).toUpperCase() + "" + res[0].charAt(res[0].length - 1).toUpperCase();
                }
                main.append('<th width="14%" bgcolor="#3a3a3a">' + name + '</th>');
                frontPlayerScores[i] = 0;
                /* First time init only */
                markers[i] = new Array(parseInt(scorecardDetails.round_info.type) * 9);
            }
            topTable.append(main);

            for (var i = 0; i < scorecardDetails.round_info.hole_set_1.holes.length; i++) {
                var main = $('<tr style="color:#fff;"></tr>');
                if (scorecardDetails.round_info.hole_set_1.holes[i].par != null &&
                    scorecardDetails.round_info.hole_set_1.holes[i].par != undefined &&
                    scorecardDetails.round_info.hole_set_1.holes[i].par.trim() != "")
                    frontPar = frontPar + parseInt(scorecardDetails.round_info.hole_set_1.holes[i].par);
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + (i + 1) + '</a></td>');
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + scorecardDetails.round_info.hole_set_1.holes[i].par + '</a></td>');
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + scorecardDetails.round_info.hole_set_1.holes[i].index + '</a></td>');
                for (var j = 0; j < scorecardDetails.players.length; j++) {
                    var blk_id = "p_" + (j + 1) + "_h_" + (i + 1) + ""; // player_1_hole_1
                    var scr = scorecardDetails.scores[i].user_stroke[0]['player_' + scorecardDetails.players[j].player];
                    if (scr != null && scr != undefined && scr != "") {
                        if (scr != "X" && scr != " ") {
                            markers[j][i] = parseInt(scr); //scorecardDetails.scores[i].user_stroke[0]['player_' + scorecardDetails.players[j].player]
                            frontPlayerScores[j] = frontPlayerScores[j] + parseInt(scr);
                        } else {
                            markers[j][i] = 0;
                            if (scr == " " || scr == "")
                                allHolesPlayed = 0;
                        }
                    } else {
                        markers[j][i] = 0;
                        allHolesPlayed = 0;
                    }
                    if (scr != null && scr != undefined) {
                        main.append('<td id=' + blk_id + ' style="background-color:#8598A1; font-size:22px!important;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits"  class="toggle">' + scr + '</a></td>');
                    } else {
                        main.append('<td id=' + blk_id + ' style="background-color:#8598A1; font-size:22px!important;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits"  class="toggle"></a></td>');
                    }
                }
                topTable.append(main);
            }

            var main = $('<tr style="color:#fff;" id="total"></tr>');
            if (scorecardDetails.round_info.type == 2) {
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + localizeString("GolfCourse_Label_Out") + '</a></td>');
            } else {
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + localizeString("GolfCourse_Label_Total") + '</a></td>');
            }

            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + frontPar + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle"></a></td>');
            for (var k = 0; k < frontPlayerScores.length; k++) {
                if (allHolesPlayed || scorecardDetails.round_info.type >= 2) {
                    main.append('<td bgcolor="#8598A1" height="40" style="font-size:22px!important" id="front_' + k + '"><a href="#credits" class="toggle">' + frontPlayerScores[k] + '</a></td>');
                } else {
                    main.append('<td bgcolor="#8598A1" height="40" style="font-size:22px!important" id="front_' + k + '"><a href="#credits" class="toggle"></a></td>');
                }
            }

            topTable.append(main);
            swipe.append(topTable);
            $("#swipecont").append(swipe);
        }
        /* Back side of score card */
        if (scorecardDetails.round_info.type >= 2) {
            backPar = 0;
            var swipe = $('<div class="swiper-slide" id=' + scorecardDetails.round_info.hole_set_2.id + '></div>');
            var first = $('<div class="head-hole" style="padding-top:17px!important;"><table width="96%" border="0" cellspacing="5" style="margin:0px auto;"><tbody><tr><td width="100%" align="center"><div class="hole-par-no" style="width:100%!important;"><div> ' + scorecardDetails.round_info.hole_set_2.name + ' </div></div></td></tr></tbody></table>');

            swipe.append(first);
            var topTable = $('<table cellspacing="0" class="table table-bordered box box-solid" id="table-front"></table>');
            var main = $('<tr style="color:#fff;"></tr>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Holes") + '</th>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Par") + '</th>');
            main.append('<th width="14%" bgcolor="#3a3a3a">' + localizeString("GolfCourse_Label_Index") + '</th>');

            for (var i = 0; i < scorecardDetails.players.length; i++) {
                var res = scorecardDetails.players[i].name.split(" ");
                var name;
                if (res.length > 1) {
                    name = res[0].charAt(0) + "" + res[1].charAt(0);
                } else {
                    name = res[0].charAt(0) + "" + res[0].charAt(1);
                }
                main.append('<th width="14%" bgcolor="#3a3a3a">' + name + '</th>');
                backPlayerScores[i] = 0;
            }
            topTable.append(main);

            for (var i = 0; i < scorecardDetails.round_info.hole_set_2.holes.length; i++) {
                var main = $('<tr style="color:#fff;"></tr>');
                if (scorecardDetails.round_info.hole_set_2.holes[i].par != null &&
                    scorecardDetails.round_info.hole_set_2.holes[i].par != undefined &&
                    scorecardDetails.round_info.hole_set_2.holes[i].par.trim() != "")
                    backPar = backPar + parseInt(scorecardDetails.round_info.hole_set_2.holes[i].par);
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + (i + 9 + 1) + '</a></td>');
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + scorecardDetails.round_info.hole_set_2.holes[i].par + '</a></td>');
                main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + scorecardDetails.round_info.hole_set_2.holes[i].index + '</a></td>');

                for (var j = 0; j < scorecardDetails.players.length; j++) {
                    var blk_id = "p_" + (j + 1) + "_h_" + (9 + i + 1); // player_1_hole_1
                    var scr = scorecardDetails.scores[i + 9].user_stroke[0]['player_' + scorecardDetails.players[j].player];
                    if (scr != null && scr != undefined && scr != "") {
                        if (scr != "X" && scr != " ") {
                            markers[j][i + 9] = parseInt(scr); //scorecardDetails.players[j].player
                            backPlayerScores[j] = backPlayerScores[j] + parseInt(scr);
                        } else {
                            markers[j][i] = 0;
                            if (scr == " " || scr == "")
                                allHolesPlayed = 0;
                        }
                    } else {
                        markers[j][i + 9] = 0;
                        allHolesPlayed = 0;
                    }
                    if (scr != null && scr != undefined) {
                        main.append('<td id=' + blk_id + ' style="background-color:#8598A1; font-size:22px!important;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits"  class="toggle">' + scr + '</a></td>'); // player, hole
                    } else {
                        main.append('<td id=' + blk_id + ' style="background-color:#8598A1; font-size:22px!important;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits"  class="toggle"></a></td>');
                    }
                }
                topTable.append(main);
            }

            var main = $('<tr style="color:#fff;" id="total_in"></tr>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + localizeString("GolfCourse_Label_In") + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + frontPar + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle"></a></td>');
            for (var k = 0; k < frontPlayerScores.length; k++) {
                main.append('<td bgcolor="#8598A1" height="40" style="font-size:22px!important" id="in_' + k + '"><a href="#credits" class="toggle">' + frontPlayerScores[k] + '</a></td>');
            }

            topTable.append(main);

            var main = $('<tr style="color:#fff;" id="bigtotal"></tr>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + localizeString("GolfCourse_Label_Total") + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle">' + (frontPar + backPar) + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40" style="font-size:20px!important"><a href="#credits" class="toggle"></a></td>');
            for (var l = 0; l < backPlayerScores.length; l++) {
                if (allHolesPlayed) {
                    var wholetotal = frontPlayerScores[l] + backPlayerScores[l];
                    main.append('<td bgcolor="#8598A1" height="40" style="font-size:22px!important" id="back_' + l + '"><a href="#credits" class="toggle">' + wholetotal + '</a></td>');
                } else {
                    main.append('<td bgcolor="#8598A1" height="40" style="font-size:22px!important" id="back_' + l + '"><a href="#credits" class="toggle"></a></td>');
                }
            }
            topTable.append(main);

            swipe.append(topTable);
            $("#swipecont").append(swipe);
        }

        swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClickable: true,
            spaceBetween: 30,
        });

        $('#holeviewbutton').css('display', 'block');
        $('#Scorecard_Label_HoleView').css('display', 'block');

        if (!scorecardSubmitted) {
            $('#savesubmit').css('display', 'block');
            $('#Scorecard_Button_Save').css('display', 'block');
            $('#Scorecard_Button_Submit').css('display', 'block');
            if (autoRefresh != null) {
                clearInterval(autoRefresh);
                autoRefresh = null;
            }
            autoRefresh = setInterval(refreshScores, 5000);
        }
    }

    refreshScorecard = function(updatedScores) {
        var plid, hoid;
        var selBlkId;
        var curscor;
        localStorage.setItem("Scorecard_updated_on", updatedScores.updated_on);
        scorecardSubmitted = (updatedScores.status == 1) ? true : false;
        for (var sindx = 0; sindx < updatedScores.scores.length; sindx++) {
            for (pindx = 0; pindx < parseInt(localStorage.getItem("ScorecardPlayers")); pindx++) {
                hoid = sindx + 1;
                plid = pindx + 1;
                scor = updatedScores.scores[sindx].user_stroke[0]['player_' + plid];
                selBlkId = 'p_' + plid + '_h_' + hoid;
                curscor = document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML;

                if ((localStorage.modifiedCells == null) || (localStorage.modifiedCells == undefined)) {
                    localStorage.setItem("modifiedCells", JSON.stringify(modifiedCells));
                }
                var modCells = JSON.parse(localStorage.getItem("modifiedCells"));
                // if curent & updated scores are not same & if the selBlkId is not in the modified cell list, then update
                if ((scor != curscor) && !(modCells.hasOwnProperty(selBlkId))) {
                    if (scor == "") {
                        document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML = "";
                        scores[sindx].user_stroke[0]['player_' + plid] = document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML;
                        markers[pindx][sindx] = scor;
                    } else {
                        if (document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML.length == 2) {
                            document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML = "";
                            markers[pindx][sindx] = 0;
                        }
                        document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML = scor;
                        if (scor != "X")
                            markers[pindx][sindx] = parseInt(scor);
                        else
                            markers[pindx][sindx] = scor;
                        scores[sindx].user_stroke[0]['player_' + plid] = document.getElementById(selBlkId).getElementsByTagName("a")[0].innerHTML;
                    }
                } // else no need to update
            }
        }

        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        scoreCardData.scores = scores;
        localStorage.setItem("UserScoreCardData", JSON.stringify(scoreCardData));
        sumup();
        $("#updateby").html('<strong>' + localizeString("Label_LastUpdatedBy") + ':</strong> ' + updatedScores.updated_by);
        if (scorecardSubmitted) {
            $('#Scorecard_Button_Save').css('display', 'none');
            $('#Scorecard_Button_Submit').css('display', 'none');
            $('#savesubmit').css('display', 'none');
            // TODO:show submitted message to user
        }
    }

    refreshScores = function() {
        // Fetch if there are any updates
        var lastUpdate = localStorage.getItem("Scorecard_updated_on");
        var actions = {
            "score_card_id": localStorage.getItem("score_card_id"),
            "lastUpdated": lastUpdate
        };
        //window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            //window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (data.data.isUpdated == true) {
                    window.plugins.spinnerDialog.show(localizeString("SpinDialog_Update"), localizeString("SpinDialog_Wait"), true);
                    refreshScorecard(data.data.content);
                    window.plugins.spinnerDialog.hide();
                }
                if (autoRefresh != null) {
                    clearInterval(autoRefresh);
                    autoRefresh = null;
                }
                if (!scorecardSubmitted)
                    autoRefresh = setInterval(refreshScores, 15000);
            } else {
                console.log("Error while refreshing the scorecard. Message = " + JSON.stringify(data))
            }
            //window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golf/scorecardRefresh", actions, successCallback, errorCallback);
    }

    /**
     * Search user by user name
     */
    searchusers = function(name_by) {
        var actions = {
            "page_number": "1",
            "needle": name_by
        };

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                var thelist = "";
                var obj = removeItem(data.data, 'id', JSON.parse(localStorage.getItem("playerlist")).players);
                $('#userlistdiv').html("<ul>" + thelist + "</ul>");
                for (var i = 0; i < obj.length; i++) {
                    thelist += '<li><a href="javascript:void();" onclick="selectuser(\'' + obj[i].id + '\',\'' + obj[i].first_name + ' ' + obj[i].last_name + '\',\'' + obj[i].handicap + '\',\'' + obj[i].tee + '\')">' + obj[i].first_name + ' ' + obj[i].last_name + '(' + obj[i].membership_number + ')</a></li>';
                }
                if (obj.length > 0) {
                    $('#userlistdiv').show(250);
                } else {
                    $('#userlistdiv').hide(50);
                }
                $('#userlistdiv').html("<ul>" + thelist + "</ul>");
            } else {
                $('#userlistdiv').html("<ul></ul>");
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnCancel: true,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {
            $('#userlistdiv').html("<ul></ul>");
        };

        postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
    }


    selectuser = function(the_id, the_name, the_handicap, the_tee) {
        if (the_name != null && the_name != undefined) {
            document.getElementById("float-text").value = the_name;
            $("#nameerror").css("display", "none");
        }
        if (the_id != null && the_id != undefined) {
            $("#user_id").val(the_id);
        }
        if (the_handicap != null && the_handicap != "null" &&
            the_handicap != undefined && the_handicap != "undefined" &&
            the_handicap != "") {
            document.getElementById("handicap").value = the_handicap;
            $("#handierror").css("display", "none");
        }
        if (the_tee != null && the_tee != undefined && the_tee != "") {
            $("select#category option")
                .each(function() {
                    this.selected = (this.text.toUpperCase() == the_tee.toUpperCase()); //"selected"
                });
            $("#categoryerror").css("display", "none");
        }
    }


    /**
     * Close the credits view
     */
    closeCredits = function() {
        $('#credits').addClass("hidden");
    }

    selectNumPad = function(id) {
        if (scorecardSubmitted) // if scorecard already submitted don't show numpad
            return;
        if (localStorage.getItem("selectid") != "" && localStorage.getItem("selectid") != null &&
            localStorage.getItem("selectid") != undefined) {
            document.getElementById(localStorage.getItem("selectid")).style.backgroundColor = "#8598A1";
        }
        document.getElementById(id).style.backgroundColor = "#0A3043";
        localStorage.setItem("selectid", id);
        $('#copyright_div').css('display', 'none');
        $('#credits').removeClass("hidden", "", "");
        clearTimeout(timer);
        timer = setTimeout(function() {
            //your code to be executed after 10 seconds
            $('#credits').addClass("hidden");
            $('#copyright_div').css('display', 'block');
        }, 10000);
    }


    removeItem = function(obj, prop, list) {
        var c;
        var found = false;
        var newobj;
        newobj = obj;

        for (var i = 0; i < list.length; i++) {
            for (c in obj) {
                if (list[i]['name'].startsWith(obj[c]['first_name'])) {
                    found = true;
                    break;
                }
            }
            if (found) {
                delete newobj[c];
                found = false;
            }
        }
        return newobj.filter(function(x) {
            return x !== null
        });;
    }


    String.prototype.startsWith = function(str) {
        return (this.match("^" + str) == str)
    }


    /**
     * Submit final socrecard to the server.
     */
    submitFinalScorecard = function() {
        swal({
                title: localizeString("Alert_Title_SubmitScore"),
                text: localizeString("Alert_Text_SubmitScore"),
                type: "info",
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: false,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                cancelButtonText: localizeString("Alert_Button_Cancel")
            },
            function() {
                saveScoreCard("1");
                return;
            });
    }

    atleastNinePlayed = function() {
            var totalHolesPlayed = 0;
            var scorecardDetails = JSON.parse(localStorage.getItem("UserScoreCardData"));
            if (scorecardDetails.round_info.type >= 1) {
                for (var i = 0; i < scorecardDetails.round_info.hole_set_1.holes.length; i++) {
                    for (var j = 0; j < scorecardDetails.players.length; j++) {
                        var blk_id = "p_" + (j + 1) + "_h_" + (i + 1) + ""; // player_1_hole_1
                        var scr = scorecardDetails.scores[i].user_stroke[0]['player_' + scorecardDetails.players[j].player];
                        if (scr != null && scr != undefined && scr != "" && scr != "X" && scr != " ") {
                            totalHolesPlayed++;
                        }
                    }
                }
            }
            if (scorecardDetails.round_info.type >= 2) {
                for (var i = 0; i < scorecardDetails.round_info.hole_set_2.holes.length; i++) {
                    for (var j = 0; j < scorecardDetails.players.length; j++) {
                        var blk_id = "p_" + (j + 1) + "_h_" + (9 + i + 1); // player_1_hole_1
                        var scr = scorecardDetails.scores[i + 9].user_stroke[0]['player_' + scorecardDetails.players[j].player];
                        if (scr != null && scr != undefined && scr != "" && scr != "X" && scr != " ") {
                            totalHolesPlayed++;
                        }
                    }
                }
            }

            if (totalHolesPlayed >= 9)
                return true;
            else
                return false;
        }
        /**
         * Save the updated score card to the server.
         */
    saveScoreCard = function(doSubmit) {
        if (autoRefresh != null) {
            clearInterval(autoRefresh);
            autoRefresh = null;
        }
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        var lastUpdate = localStorage.getItem("Scorecard_updated_on");
        var actions = {
            "score_card_id": localStorage.getItem("score_card_id"),
            "scores": scoreCardData.scores,
            "lastUpdated": lastUpdate
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                modifiedCells = {}; // reset modifiedCells once scorecard save is success
                localStorage.setItem("modifiedCells", JSON.stringify(modifiedCells));
                localStorage.setItem("Scorecard_updated_on", data.data.content.updated_on);
                scorecardSubmitted = (data.data.content.status == 1) ? true : false;
                $("#updateby").html('<strong>' + localizeString("Label_LastUpdatedBy") + ':</strong> ' + data.data.content.updated_by);
                if (doSubmit == 1) {
                    if (atleastNinePlayed()) {
                        submitScorecard();
                    } else {
                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: localizeString("Alert_Text_SubmitScore_HoleCheck"),
                                type: "warning",
                                closeOnConfirm: true,
                                showLoaderOnConfirm: false,
                                confirmButtonText: localizeString("Alert_Button_Ok"),
                            },
                            function() {
                                return;
                            });
                    }
                } else {
                    if (!scorecardSubmitted)
                        autoRefresh = setInterval(refreshScores, 15000);
                    swal({
                        title: localizeString("Alert_Title_Done"),
                        text: localizeString("Alert_Text_ScoreCardSaved"),
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    });
                }
            } else if (data.status == "stale_write_error") {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Update"), localizeString("SpinDialog_Wait"), true);
                        refreshScorecard(data.data.content);
                        if (!scorecardSubmitted)
                            autoRefresh = setInterval(refreshScores, 15000);
                        window.plugins.spinnerDialog.hide();
                        return;
                    });
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        if (!scorecardSubmitted)
                            autoRefresh = setInterval(refreshScores, 15000);
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golf/updatescorecardall", actions, successCallback, errorCallback);
    }


    /**
     * Submit the scorecard.
     */
    submitScorecard = function() {
        var actions = {
            "score_card_id": localStorage.getItem("score_card_id")
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                if (autoRefresh != null) {
                    clearInterval(autoRefresh);
                    autoRefresh = null;
                }
                swal({
                        title: localizeString("Alert_Title_Done"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: localizeString("Alert_Button_Close"),
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        allowOutsideClick: false,
                        allowEscapeKey: false
                    },
                    function() {
                        slideview("score-card-history.html", "script", "right");
                    });
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golf/submitscorecard", actions, successCallback, errorCallback);
    }


    /**
     * Remove player from the player list
     */
    removePlayer = function(index) {
        var playerlist = JSON.parse(localStorage.getItem("playerlist"));
        var obj = playerlist.players
        obj.splice(index, 1);
        /* Fix for deleting player. Aligning ids */
        for (var i = 0; i < obj.length; i++) {
            obj[i].player = (i + 1);
        }
        playerlist.players = obj;
        if (playerlist.players.length < 1)
            $('#Golf_Scorecard_Create_button').css("display", "none");
        var jsonStr = JSON.stringify(playerlist);
        localStorage.setItem("playerlist", jsonStr);
        showPlayer(playerlist);
    }


    /**
     * On Document Ready
     */
    documentReadyScoreCard = function() {
        jQuery('.switch_options').each(function() {
            //This object
            var obj = jQuery(this);

            var enb = obj.children('.switch_enable'); //cache first element, this is equal to ON
            var dsb = obj.children('.switch_disable'); //cache first element, this is equal to OFF
            var input = obj.children('input'); //cache the element where we must set the value
            var input_val = obj.children('input').val(); //cache the element where we must set the value

            /* Check selected */
            if (0 == input_val) {
                dsb.addClass('selected');
            } else if (1 == input_val) {
                enb.addClass('selected');
            }

            //Action on user's click(ON)
            enb.on('click', function() {
                $(dsb).removeClass('selected'); //remove "selected" from other elements in this object class(OFF)
                $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(ON)
                $(input).val(1).change(); //Finally change the value to 1
            });

            //Action on user's click(OFF)
            dsb.on('click', function() {
                $(enb).removeClass('selected'); //remove "selected" from other elements in this object class(ON)
                $(this).addClass('selected'); //add "selected" to the element which was just clicked in this object class(OFF)
                $(input).val(0).change(); // //Finally change the value to 0
            });
        });
    }


    $("#float-text").on('keyup', function() {
        if ($("#float-text").val().length > 0) {
            $("#nameerror").css("display", "none");
            $("#nameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#nameerror").css("display", "block");
            $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });

    $("#handicap").on('keyup', function() {
        if ($("#handicap").val().length > 0) {
            $("#handierror").css("display", "none");
            $("#handierror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#handierror").css("display", "block");
            $("#handierror").html(localizeString("ValidationError_HandicapBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });

    updateCategoryError = function() {
        if ($("#category").val().length > 0) {
            $("#categoryerror").css("display", "none");
            $("#categoryerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#categoryerror").css("display", "block");
            $("#categoryerror").html(localizeString("ValidationError_TeeNotSelected") + '<i class="form-help-icon icon">error</i>');
        }
    }

    $("#float-text").on('keyup', function() {
        $('#userlistdiv').show(250);
        if ($(this).val().trim() != "") {
            searchusers($(this).val());
        } else {
            $("#float-text").val("");
            $("#handicap").val("");
            $("#user_id").val("");
            $("#nameerror").css("display", "none");
            $("#handierror").css("display", "none");
            $("#categoryerror").css("display", "none");
            $('#userlistdiv').html("<ul></ul>");
            $('#userlistdiv').hide(250);
        }
    });


    $('html').click(function() {
        if ($('#userlistdiv').html() != "<ul></ul>") {
            $('#userlistdiv').html("<ul></ul>");
            $('#userlistdiv').hide(250);
        }
    });


    isNumberKey = function(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
        /************************** Score-Card.html ends ************************/


    /**
     * Populate the tee-time select box with time
     * in interval of 10 min.
     */
    populateTeeTime = function() {
        var d = new Date();
        var interval = 10;
        d.setHours(5);
        d.setMinutes(0);
        for (var i = 0; i < 85; i++) {
            option = document.createElement('option');
            option.setAttribute('value', i + 1);
            option.appendChild(document.createTextNode(("0" + d.getHours()).slice(-2) + ':' + ("0" + d.getMinutes()).slice(-2)));
            $("#teetime").append(option);
            d.setMinutes(d.getMinutes() + interval);
        }
    }

    dismissAddWindow = function() {
        $("#dateerror").css("display", "none");
        $("#datepicker-adv-2").val("");
        $("#timeerror").css("display", "none");
        $("#teetime").val("");
        $("#rounderror").css("display", "none");
        $("#rounds").val("");
    }

    validateTime = function(tee_time) {
        if (tee_time == "") {
            $("#timeerror").css("display", "block");
            $("#timeerror").html(localizeString("ValidationError_InvalidTeeTime") + '<i class="form-help-icon icon">error</i>');
        } else {
            $("#timeerror").css("display", "none");
        }
    }

    validateDate = function(tee_date) {
        if (tee_date == "") {
            $("#dateerror").css("display", "block");
            $("#dateerror").html(localizeString("ValidationError_DateBlank") + '<i class="form-help-icon icon">error</i>');
        } else {
            $("#dateerror").css("display", "none");
        }
    }

    validateRound = function(round) {
        if (round == "") {
            $("#rounderror").css("display", "block");
            $("#rounderror").html(localizeString("ValidationError_RoundBlank") + '<i class="form-help-icon icon">error</i>');
        } else {
            $("#rounderror").css("display", "none");
        }
    }

    /**
     * Create a new user scorecard for choosen tee time and round
     * @tee_date    Tee-Time
     * @tee_slot    Tee-Time Slot
     * @round       Selected round for the play
     */
    newScoreCard = function(tee_date, tee_slot, tee_time, round, round_name) {
        if (localStorage.getItem("isLoggedIn") != "true") {
            swal({
                    title: localizeString("Alert_Title_AddPromo"),
                    text: localizeString("Alert_Text_AddScorecard"),
                    type: "info",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    showLoaderOnConfirm: false,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function() {
                    $("#Scorecard_Button_Close").click();
                    return;
                });
            return false;
        }
        if ((tee_date != "") && (tee_time != "") && (round != "")) {
            localStorage.setItem("tee_date", tee_date);
            localStorage.setItem("tee_time", tee_time);
            localStorage.setItem("tee_slot", tee_slot);
            localStorage.setItem("round", round);
            localStorage.setItem("round_name", round_name);
            localStorage.setItem("scorecard_data_individual", "");
            $("#Scorecard_Button_Close").click();
            slideview('score-card.html', 'script', 'left');
            return false;
        } else {
            if (tee_date == "") {
                $("#dateerror").css("display", "inline");
                $("#dateerror").html(localizeString("ValidationError_DateBlank") + '<i class="form-help-icon icon">error</i>');
            } else {
                $("#dateerror").css("display", "none");
            }
            if (tee_time == "") {
                $("#timeerror").css("display", "inline");
                $("#timeerror").html(localizeString("ValidationError_InvalidTeeTime") + '<i class="form-help-icon icon">error</i>');
            } else {
                $("#timeerror").css("display", "none");
            }
            if (round == "") {
                $("#rounderror").css("display", "inline");
                $("#rounderror").html(localizeString("ValidationError_RoundBlank") + '<i class="form-help-icon icon">error</i>');
            } else {
                $("#rounderror").css("display", "none");
            }
        }
    }

    /**
     * Populate the possible round details for the game play
     */
    populateRoundDetails = function(data) {
        if (data.data != null && data.data != undefined && data.data != "") {
            if (data.data.golf_round_info == null && data.data.golf_round_info == undefined &&
                data.data.golf_round_info == "")
                return;
            var roundDetails = data.data.golf_round_info;
            if (roundDetails.round_setup != null && roundDetails.round_setup != undefined &&
                roundDetails.round_setup.length >= 1) {
                for (var i = 0; i < roundDetails.round_setup.length; i++) {
                    option = document.createElement('option');
                    option.setAttribute('value', roundDetails.round_setup[i].master_id);
                    option.appendChild(document.createTextNode(roundDetails.round_setup[i].name));
                    $("#rounds").append(option);
                }
                $("#add_modal").css("display", "block");
            } else {
                $("#add_modal").css("display", "none");
            }
        }
    }

    /**
     * Get the list of Users Score Card from Server
     */
    getUsersScoreCardList = function() {
        var actions = {
            "club_id": localStorage.getItem("GOLF_CLUB_ID")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            populateRoundDetails(data);
            if (data.status == "success") {
                swal.close();
                displayUsersScoreCardList(data); // History
            } else {
                $('#user-list').html('<p style="text-align:center">' + data.message + '</p>');
            }
            $("#Golf_Scorecard_Add_button").css("display", "block"); //add_scorecard_button
            $("#separation").css("display", "block");
        };
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/golf/userscorecard", actions, successCallback, errorCallback);
    }

    getLeaderboardList = function() {
        var actions = {
            "club_id": localStorage.getItem("GOLF_CLUB_ID")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayLeaderboardList(data); // History
            } else {
                $('#user-list').html('<p style="text-align:center">' + data.message + '</p>');
            }
        };
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/golf/userscorecard", actions, successCallback, errorCallback);
    }

    function getPlayerNames(iData) {
        var rDom = "";
        for (var j = 0, n = iData.length; j < n; j++) {
            rDom += iData[j]['name'] + ", ";
        }
        return rDom.substring(0, rDom.length - 2);
    }

    function getPlayerStatus(iStatus, iId) {
        var iStatus = parseInt(iStatus);

        var rtDom = '';
        if (iStatus === 1) {
            rtDom = '<div class = "gt-scorelist-status scoreState' + iStatus + '" onclick="gotoLeaderboard(\'' + iId + '\')"> ' + localizeString("Menu_Button_Leaderboard") + ' </div>';
        } else {
            rtDom = '<div class = "gt-scorelist-status scoreState' + iStatus + '" > ' + localizeString("Golf_Scorecard_Label_Progress") + ' </div>';
            // rtDom = '<div onclick="getLeaderBoardView("' + iId + '")" class = "gt-scorelist-status scoreState' + iStatus + '" > ' + localizeString("Golf_Scorecard_Label_Progress") + ' </div>';
        }
        return rtDom;
    }

    function getScorecardListDom(iData, iClickFn) {
        var hostName = iData['created_by'],
            playDate = moment(iData['tee_time'], "YYYY-MM-DD h:mm:ss"),
            listId = iData['id'],
            players = getPlayerNames(iData['players']),
            playStatus = getPlayerStatus(iData.status, listId);

        return '<div class="col-lg-4 col-md-6 course-row my-bks-row animated fadeIn statusBorder' + iData.status + '" onclick="' + iClickFn + '(' + listId + ')">\
                        <div class="gr-scorelist-left">\
                        <div class="gr-score-cont">\
                            <div class="title bkclr1">BY</div>\
                            <div class="name">' + hostName + '</div>\
                        </div>\
                        <div class="gr-score-cont mT15">\
                            <div class="title bkclr1">PLAYERS</div>\
                            <div class="name">' + players + '</div>\
                        </div>\
                    </div>\
                    <div class="gr-scorelist-right">\
                        <div class="gt-scorelist-dt title bkclr1">' + playDate.format("lll") + '</div>' +
            playStatus + '</div>\
                </div>';
    }

    displayLeaderboard = function(iId) {
        slideview('score-card-leaderboard-view.html', 'script', 'left');
    }

    gotoLeaderboard = function(iID) {
        displayUsersScoreCardDetail(iID, 1);
        window.event.stopPropagation(iID);
    }

    /**
     * Display list of all user score cards
     */
    displayUsersScoreCardList = function(socreCardList) {
        //$('#user-list').empty("");

        var lUserList = socreCardList.data.scorecardlist;

        for (var i = 0, n = lUserList.length; i < n; i++) {
            $('#user-list').append(getScorecardListDom(lUserList[i], "displayUsersScoreCardDetail"));
        };
    }

    displayLeaderboardList = function(socreCardList) {
        //$('#user-list').empty("");

        var lUserList = socreCardList.data.scorecardlist;

        for (var i = 0, n = lUserList.length; i < n; i++) {
            if (parseInt(lUserList[i]['status']) === 1) {
                $('#user-list').append(getScorecardListDom(lUserList[i], "displayLeaderboard"));
            }
        };
    }


    /**
     * Display details corresponding to the specific user scorecard
     */
    displayUsersScoreCardDetail = function(id, iLeaderboard) {
        localStorage.setItem("score_card_id", id);
        var actions = {
            "score_card_id": id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close("History Data = " + JSON.stringify(data));
                localStorage.setItem("UserScoreCardData", JSON.stringify(data.data.content));
                localStorage.setItem("ImageBasePath", data.data.imageBasePath);
                localStorage.setItem("prevselectid", ""); //Fix for carrying edits to next scorecard

                if (iLeaderboard) {
                    slideview('score-card-leaderboard-view.html', 'script', 'left');
                } else {
                    slideview('score-card-hole-view.html', 'script', 'left');
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function(data) {};

        postMessageToServerWithAuth("/golf/scorecarddetails", actions, successCallback, errorCallback);
    }

    /************************** Score-Card-history.html ends ************************/

    $(function() {
        $('.toggle').click(function(event) {
            event.preventDefault();
            var target = $(this).attr('href');
            $(target).toggleClass('hidden');
        });
    });


    scoreCardHoleView = function() {
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        var sDate = null;
        var sTime = null;
        var eDate = null;
        var eTime = null;

        scorecardSubmitted = (scoreCardData.status == 1) ? true : false;
        $('#scoredetails').html("");
        var startDiv = $('<div style="width:100%"></div>');
        var p = $('<p><b>' + localizeString("Label_CreatedBy") + ': ' + scoreCardData.created_by + '</b></p>');
        localStorage.setItem("Scorecard_updated_on", scoreCardData.updated_on);
        localStorage.setItem("ScorecardPlayers", scoreCardData.players.length);
        /* Fixed garbage selId issue in new scorecards */
        localStorage.setItem("selectid", "");
        if (!scorecardSubmitted) {
            var location = "Glof Course";
            var notes = " Created By: " + scoreCardData.created_by + ", Tee-Time Slot: " + scoreCardData.tee_time_slot;
            var eventName = "Golf Round";
            var tee_date_time = scoreCardData.tee_time.split(" ");
            sDate = tee_date_time[0];
            sTime = tee_date_time[1];
            eDate = tee_date_time[0];
            var endTimeArr = tee_date_time[1].split(":");
            if (scoreCardData.round_info.type == 2) { // 18 holes so end time Around 3 hrs later
                eTime = [parseInt(endTimeArr[0]) + 3, endTimeArr[1], endTimeArr[2]].join(":");
            } else { // end time Around 1.5 hrs later
                if ((parseInt(endTimeArr[1]) + 30) > 60)
                    eTime = [parseInt(endTimeArr[0]) + 2, parseInt(endTimeArr[1]) + 30 - 60, endTimeArr[2]].join(":");
                else
                    eTime = [parseInt(endTimeArr[0]) + 1, parseInt(endTimeArr[1]) + 30, endTimeArr[2]].join(":");
            }
            notes = notes.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + eventName + '\', \'' + location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
            startDiv.append(p, reminder);
        } else {
            startDiv.append(p);
        }
        var dispDat2 = getDisplayableDate(scoreCardData.created_on.split(' ')[0]);
        var dispTim2 = getDisplayableTime(scoreCardData.created_on.split(' ')[1]);
        var p0 = $('<p><b>' + localizeString("Label_CreatedOn") + ': ' + [dispDat2, dispTim2].join(' ') + '</b></p>');
        var datestr;
        if ((scoreCardData.tee_time == "0000-00-00 00:00:00") || (scoreCardData.tee_time == "00-00-0000 00:00:00")) {
            datestr = "Not Available";
        } else {
            var dispDat3 = getDisplayableDate(scoreCardData.tee_time.split(' ')[0]);
            var dispTim3 = getDisplayableTime(scoreCardData.tee_time.split(' ')[1]);
            datestr = [dispDat3, dispTim3].join(' ');
        }
        var p1 = $('<p><strong>' + localizeString("Label_TeeTime") + ':</strong> ' + datestr + '</p>');
        //localizeString("Label_TeeTimeSlot") + ': ' + scorecardDetails.tee_time_slot + '&nbsp; &nbsp;' +
        if (scoreCardData.updated_on != null) {
            var datestr;
            if ((scoreCardData.updated_on == "0000-00-00 00:00:00") || (scoreCardData.updated_on == "00-00-0000 00:00:00")) {
                datestr = "Not Available";
            } else {
                var dispDat4 = getDisplayableDate(scoreCardData.updated_on.split(' ')[0]);
                var dispTim4 = getDisplayableTime(scoreCardData.updated_on.split(' ')[1]);
                datestr = [dispDat4, dispTim4].join(' ');
            }
            var p2 = $('<p><strong>' + localizeString("Label_LastUpdatedOn") + ':</strong> ' + datestr + '</p>');
            var p3 = $('<p id="updateby"><strong>' + localizeString("Label_LastUpdatedBy") + ':</strong> ' + scoreCardData.updated_by + '</p>');
            $('#scoredetails').append(startDiv, p0, p1, p2, p3);
        } else {
            $('#scoredetails').append(startDiv, p0, p1);
        }

        $('#scoredetails').css("display", "block");

        $("#swipecont").html('');

        //if (markers != null && markers != undefined) // TODO: Check this issue
        markers = new Array(scoreCardData.players.length);

        scores = scoreCardData.scores;

        var roundName = scoreCardData.round_info.name;
        var startFrom = 0;
        if (roundName.indexOf("Front_9") > -1) {
            startFrom = 0;
        } else if (roundName.indexOf("Back_9") > -1) {
            startFrom = 9;
        } else {
            startFrom = 0;
        }
        if (scoreCardData.round_info.type >= 1) {
            for (var i = 0; i < scoreCardData.players.length; i++) {
                frontPlayerScores[i] = 0;
                /* First time init only */
                markers[i] = new Array(parseInt(scoreCardData.round_info.type) * 9);
            }
            for (var i = 0; i < scoreCardData.round_info.hole_set_1.holes.length; i++) {
                var swipe = $('<div class="swiper-slide" id=' + i + '></div>');
                var hdrtxt = localizeString("GolfCourse_Label_Holes") + ' ' + (i + 1) + ', ' + localizeString("GolfCourse_Label_Par");
                hdrtxt = hdrtxt + ' ' + scoreCardData.round_info.hole_set_1.holes[i].par + ', ' + localizeString("GolfCourse_Label_Index");
                hdrtxt = hdrtxt + ' ' + scoreCardData.round_info.hole_set_1.holes[i].index;

                var first = $('<div class="head-hole" style="padding-top:10px!important;"><table width="96%" border="0" cellspacing="5" style="margin:0px auto;"><tbody><tr><td width="100%" align="center"><div class="hole-par-no" style="width:100%!important;"><p class="text_p" style="font-size:18px!important; margin-top:9px!important; font-weight:500; margin-top:6px!immportant;">' + hdrtxt + '</p></div></td></tr></tbody></table>');

                swipe.append(first);
                var topDiv = $("<div class='wysiwyg-img'></div>");
                var table = $('<table cellspacing="0" class="table table-bordered box box-solid"></table>');
                var main = $('<tr style="color:#fff;"></tr>');
                main.append('<th width="80%" bgcolor="#3a3a3a">' + localizeString("Label_Player") + '</th>');
                main.append('<th width="20%" bgcolor="#3a3a3a">' + localizeString("Label_Score") + '</th>');
                table.append(main);

                for (var j = 0; j < scoreCardData.players.length; j++) {
                    var blk_id = "p_" + (j + 1) + "_h_" + (i + 1) + ""; // player_1_hole_1
                    var scr = scoreCardData.scores[i].user_stroke[0]['player_' + scoreCardData.players[j].player];
                    if (scr != null && scr != undefined && scr != "") {
                        if (scr != "X" && scr != " ") {
                            markers[j][i] = parseInt(scr); //scorecardDetails.scores[i].user_stroke[0]['player_' + scorecardDetails.players[j].player]
                            frontPlayerScores[j] = frontPlayerScores[j] + parseInt(scr);
                        } else {
                            markers[j][i] = 0;
                        }
                    } else {
                        markers[j][i] = 0;
                    }
                    var main1 = $('<tr></tr>');
                    main1.append('<td class="namfont" bgcolor="#8598A1" height="40"><a href="#credits" class="toggle">' + scoreCardData.players[j].name.toUpperCase() + '</a></td>');
                    if (scr != null && scr != undefined) {
                        main1.append('<td class="scrfont" id=' + blk_id + ' style="background-color:#FFFFFF; color:#2F2F2F;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits" class="toggle">' + scr + '</a></td>');
                    } else {
                        main1.append('<td class="scrfont" id=' + blk_id + ' style="background-color:#FFFFFF; color:#2F2F2F;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits" class="toggle"></a></td>');
                    }
                    table.append(main1);
                }

                topDiv.append(table);
                swipe.append(topDiv);
                $("#swipecont").append(swipe);
            }
        }

        if (scoreCardData.round_info.type >= 2) {
            for (var i = 0; i < scoreCardData.players.length; i++) {
                backPlayerScores[i] = 0;
            }
            for (var i = 0; i < scoreCardData.round_info.hole_set_2.holes.length; i++) {
                var swipe = $('<div class="swiper-slide" id=' + (i + 9) + '></div>');

                var hdrtxt = localizeString("GolfCourse_Label_Holes") + ' ' + (i + 10) + ', ' + localizeString("GolfCourse_Label_Par");
                hdrtxt = hdrtxt + ' ' + scoreCardData.round_info.hole_set_2.holes[i].par + ', ' + localizeString("GolfCourse_Label_Index");
                hdrtxt = hdrtxt + ' ' + scoreCardData.round_info.hole_set_2.holes[i].index;
                var first = $('<div class="head-hole" style="padding-top:10px!important;">\
                <table width="96%" border="0" cellspacing="5" style="margin:0px auto;"><tbody>\
                <tr>\
                \
                <td width="100%" align="center">\
                <div class="hole-par-no" style="width:100%!important;">\
                <p class="text_p" style="font-size:18px!important; margin-top:9px!important; font-weight:500; margin-top:6px!immportant;">' + hdrtxt + '</p>\
                </div>\
                </td>\
                \
                </tr>\
                </tbody></table>');

                swipe.append(first);
                var topDiv = $("<div class='wysiwyg-img'></div>");
                var table = $('<table cellspacing="0" class="table table-bordered box box-solid"></table>');
                var main = $('<tr style="color:#fff;"></tr>');
                main.append('<th width="80%" bgcolor="#3a3a3a">' + localizeString("Label_Player") + '</th>');
                main.append('<th width="20%" bgcolor="#3a3a3a">' + localizeString("Label_Score") + '</th>');
                table.append(main);

                for (var j = 0; j < scoreCardData.players.length; j++) {
                    var blk_id = "p_" + (j + 1) + "_h_" + (9 + i + 1); // player_1_hole_1
                    var scr = scoreCardData.scores[i + 9].user_stroke[0]['player_' + scoreCardData.players[j].player];
                    if (scr != null && scr != undefined && scr != "") {
                        if (scr != "X" && scr != " ") {
                            markers[j][i + 9] = parseInt(scr); //scorecardDetails.players[j].player
                            backPlayerScores[j] = backPlayerScores[j] + parseInt(scr);
                        } else {
                            markers[j][i] = 0;
                        }
                    } else {
                        markers[j][i + 9] = 0;
                    }
                    var main1 = $('<tr></tr>');
                    main1.append('<td class="namfont" bgcolor="#8598A1" height="40"><a href="#credits" class="toggle">' + scoreCardData.players[j].name.toUpperCase() + '</a></td>');
                    if (scr != null && scr != undefined) {
                        main1.append('<td class="scrfont" id=' + blk_id + ' style="background-color:#FFFFFF; color:#2F2F2F;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits" class="toggle">' + scr + '</a></td>'); // player, hole
                    } else {
                        main1.append('<td class="scrfont" id=' + blk_id + ' style="background-color:#FFFFFF; color:#2F2F2F;" height="40" onclick="selectNumPad(\'' + blk_id + '\');" ><a href="#credits"  class="toggle"></a></td>');
                    }
                    table.append(main1);
                }

                topDiv.append(table);
                swipe.append(topDiv);
                $("#swipecont").append(swipe);
            }
        }

        if (!scorecardSubmitted) {
            $('#savesubmit').css('display', 'block');
            $('#Scorecard_Button_Save').css('display', 'block');
            $('#Scorecard_Button_Submit').css('display', 'block');
            if (autoRefresh != null) {
                clearInterval(autoRefresh);
                autoRefresh = null;
            }
            autoRefresh = setInterval(refreshScores, 5000);
        } else {
            $('#Menu_Button_Leaderboard').css('display', 'block');
        }

        $('#tableviewbutton').css('display', 'block');
        $('#Golf_Scorecard_Label_TableView').css('display', 'block');


        var appendNumber = 1;
        var prependNumber = 1;
        swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            paginationClickable: true,
            initialSlide: startFrom
        });
        document.querySelector('.prepend-2-slides').addEventListener('click', function(e) {
            e.preventDefault();
            swiper.prependSlide([
                '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>',
                '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>'
            ]);
        });
        document.querySelector('.prepend-slide').addEventListener('click', function(e) {
            e.preventDefault();
            swiper.prependSlide('<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>');
        });
        document.querySelector('.append-slide').addEventListener('click', function(e) {
            e.preventDefault();
            swiper.appendSlide('<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>');
        });
        document.querySelector('.append-2-slides').addEventListener('click', function(e) {
            e.preventDefault();
            swiper.appendSlide([
                '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>',
                '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>'
            ]);
        });
    }

    /**
     * Handels Holeview scoring
     */
    keysHoleView = function(i) {
        viewType = 1;
        var selId = localStorage.getItem("selectid"); //p_pid_h_hid
        var prevselId = localStorage.getItem("prevselectid");
        /* Update modified cell list to avoid updating them on refresh */
        if ((localStorage.modifiedCells == null) || (localStorage.modifiedCells == undefined)) {
            localStorage.setItem("modifiedCells", JSON.stringify(modifiedCells));
        }
        var modCells = JSON.parse(localStorage.getItem("modifiedCells"));
        if ((selId == prevselId) && (document.getElementById(selId).getElementsByTagName("a")[0].innerHTML == "1")) {
            modCells[selId] = modCells[selId] + '' + i;
        } else {
            modCells[selId] = i; // Update ModifiedCells
        }
        localStorage.setItem("modifiedCells", JSON.stringify(modCells));
        var ids = selId.split("_");
        pid = parseInt(ids[1]);
        hid = parseInt(ids[3]) - 1;
        if (i == "") {
            document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = "";
            scores[hid].user_stroke[0]['player_' + pid] = document.getElementById(selId).getElementsByTagName("a")[0].innerHTML;
            markers[pid - 1][hid] = i;
        } else {
            if ((selId == prevselId) && (document.getElementById(selId).getElementsByTagName("a")[0].innerHTML == "1")) {
                document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = document.getElementById(selId).getElementsByTagName("a")[0].innerHTML + '' + i;
                markers[pid - 1][hid] = parseInt(markers[pid - 1][hid] + '' + i);
            } else {
                document.getElementById(selId).getElementsByTagName("a")[0].innerHTML = i;
                markers[pid - 1][hid] = parseInt(i);
            }
            scores[hid].user_stroke[0]['player_' + pid] = document.getElementById(selId).getElementsByTagName("a")[0].innerHTML;
        }
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        scoreCardData.scores = scores;
        localStorage.setItem("UserScoreCardData", JSON.stringify(scoreCardData));
        localStorage.setItem("prevselectid", selId);
        sumup();
        if ("1" != document.getElementById(selId).getElementsByTagName("a")[0].innerHTML) {
            gotoNext();
        }
    }

    gotoPrev = function() {
        var selId = localStorage.getItem("selectid");
        if (selId != null && selId != undefined && selId != "") {
            var ids = selId.split("_");
            var pid = parseInt(ids[1]);
            if (parseInt(ids[3]) > 1) {
                var hid = parseInt(ids[3]) - 1;
                var blk_id = "p_" + pid + "_h_" + hid + ""; // player_1_hole_1
                $("#" + blk_id).click();
            }
        }
    }

    /**
     * Auto Jump to next scorable unit
     */
    gotoNext = function() {
        var selId = localStorage.getItem("selectid");
        if (selId != null && selId != undefined && selId != "") {
            var ids = selId.split("_");
            var pid = parseInt(ids[1]);
            var hid = parseInt(ids[3]);
            var plLen = parseInt(localStorage.getItem("ScorecardPlayers"));
            if (pid < plLen) {
                pid++;
            } else {
                pid = 1;
                if (hid >= 0 && hid <= 17) {
                    hid++;
                    // scroll to next view
                    swiper.slideNext(false);
                }
            }

            var blk_id = "p_" + pid + "_h_" + hid + ""; // player_1_hole_1
            $("#" + blk_id).click();
        }
    }

    /**
     * Auto Jump to next scorable unit in Table View
     */
    gotoNextTableView = function() {
        var selId = localStorage.getItem("selectid");
        if (selId != null && selId != undefined && selId != "") {
            var ids = selId.split("_");
            var pid = parseInt(ids[1]);
            var hid = parseInt(ids[3]);
            var plLen = parseInt(localStorage.getItem("ScorecardPlayers"));
            if (pid < plLen) {
                pid++;
            } else {
                pid = 1;
                if (hid >= 0 && hid <= 17) {
                    hid++;
                    if (hid == 10)
                        swiper.slideNext(false); // scroll to next view
                }
            }

            var blk_id = "p_" + pid + "_h_" + hid + ""; // player_1_hole_1
            $("#" + blk_id).click();
        }
    }

    saveScoreCardOnBack = function() {
        var scoreCardData = JSON.parse(localStorage.getItem("UserScoreCardData"));
        var lastUpdate = localStorage.getItem("Scorecard_updated_on");
        var actions = {
            "score_card_id": localStorage.getItem("score_card_id"),
            "scores": scoreCardData.scores,
            "lastUpdated": lastUpdate
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Save"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (autoRefresh != null) {
                    clearInterval(autoRefresh);
                    autoRefresh = null;
                }
                modifiedCells = {}; // reset modifiedCells once scorecard save is success
                localStorage.setItem("modifiedCells", JSON.stringify(modifiedCells));
                localStorage.setItem("Scorecard_updated_on", data.data.content.updated_on);
                $("#updateby").html('<strong>' + localizeString("Label_LastUpdatedBy") + ':</strong> ' + data.data.content.updated_by);
                scorecardSubmitted = (data.data.content.status == 1) ? true : false;
                var currentPage = (window.location.pathname).split("/").pop();
                if (currentPage == "score-card-hole-view.html") {
                    window.location.href = "score-card-history.html";
                } else {
                    window.location.href = "javascript:history.back()";
                }
            } else if (data.status == "stale_write_error") {
                console.log("conflict while saving...just refresh & move out");
                window.plugins.spinnerDialog.show(localizeString("SpinDialog_Update"), localizeString("SpinDialog_Wait"), true);
                refreshScorecard(data.data.content);
                if (autoRefresh != null) {
                    clearInterval(autoRefresh);
                    autoRefresh = null;
                }
                window.plugins.spinnerDialog.hide();
                saveScoreCardOnBack();
                window.location.href = "javascript:history.back()";
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golf/updatescorecardall", actions, successCallback, errorCallback);
    }

    function onBackPressed() {
        var currentPage = (window.location.pathname).split("/").pop();

        if (!$("#credits").hasClass("hidden")) {
            $('#credits').addClass("hidden");
        } else if ((currentPage == "score-card-table-view.html") ||
            (currentPage == "score-card-hole-view.html")) {
            if (!scorecardSubmitted) { // if scorecard not submitted then save & move
                saveScoreCardOnBack();
            } else {
                if (autoRefresh != null) {
                    clearInterval(autoRefresh);
                    autoRefresh = null;
                }
                window.location.href = "javascript:history.back()";
            }
        }
    }

    /* android backbutton event starts*/
    document.addEventListener("backbutton", onBackPressed, false);
}());