(function() {
    var enableDebugMode = 0;
    "use strict";

    onLoad = function() {
        if (localStorage.getItem("isLoggedIn") == "true") {

            showHomeScreenShortcuts();
            updateprofile();
            var clearfix = $("<div class='clearfix'></div>");
            clearfix.insertAfter("#top-carousel");
            if (/Android/i.test(navigator.userAgent)) {
                navigator.splashscreen.hide();
            }
        } else {

            $("#homeButtons").css({ "display": "block" });
            $("#content-launch").css({ "overflow-y": "hidden" });
            if (/Android/i.test(navigator.userAgent)) {
                navigator.splashscreen.hide();
            }
        }
    }

    showHomeScreenShortcuts = function() {
        // check here
        if (localStorage.getItem("memberverified") == "0") {
            if (localStorage.getItem("email_verified") == "1") {
                if (localStorage.getItem("admin_to_verify") == "1" ||
                    localStorage.getItem("admin_to_verify") == 1) {
                    $("#notify").css("display", "block");
                    $("#cardheading").html("Approval Pending !");
                    $("#cardcontent").html("Due to a mismatch of details submitted with club records, your account is pending club administrators approval. Please contact the club management for clarifications. Once verified, you will be able to access all member specific features of the MobileApp.");
                }
            } else {
                $("#notify").css("display", "block");
                $("#cardheading").html(localizeString("Alert_Title_ActionRequired"));
                $("#cardcontent").html(localizeString("Account_Label_VerificationMessage") + "</br> </br> <div style='text-align:center; width:50%; border-bottom:1px solid white;' onclick='resendEmail();'>" + localizeString("Label_Resend_Email") + "</div></br>");
            }
            $("#homeActivationAlert").css("display", "block");
        } else {
            if (localStorage.getItem("eventCalendarBookingMenuEnabled") == "true") {
                $("#homeEvents").css({ "display": "block" });
            }
            if (localStorage.getItem("newsEnabled") == "true") {
                $("#homeNews").css({ "display": "block" });
            }
            if (localStorage.getItem("transactionsEnabled") == "true") {
                $("#homeTrans").css({ "display": "block" });
            }
            if (localStorage.getItem("notificationEnabled") == "true") {
                $("#homeNotify").css({ "display": "block" });
            }
            /*if (localStorage.getItem("tournamentRegistrationEnabled") == "true") {
                $("#homeTournment").css({"display": "block"});
            }*/
            if (localStorage.getItem("userChatEnabled") == "true") {
                $("#homeConnect").css({ "display": "block" });
                $("#homeMessages").css({ "display": "block" });
            }
            if (localStorage.getItem("offersEnabled") == "true") {
                $("#homeOffers").css({ "display": "block" });
            }
            if (localStorage.getItem("faciBookingEnabled") == "true") {
                $("#homeBook").css({ "display": "block" });
            }
        }
    }

    updateprofile = function() {
        var actions = null;

        successCallback = function(data) {
            if (data.status == "success") {
                localStorage.setItem("server_ts", data.user_status.data.server_ts);
                localStorage.setItem("memberid", data.user_status.data.member_id);
                localStorage.setItem("membership_number", data.user_status.data.membership_number);
                localStorage.setItem("membership_type", data.user_status.data.member_type);
                localStorage.setItem("memberverified", data.user_status.status);
                localStorage.setItem("email", data.user_status.data.email);
                localStorage.setItem("phone", data.user_status.data.phone);
                localStorage.setItem("first_name", data.user_status.data.first_name);
                localStorage.setItem("last_name", data.user_status.data.last_name);
                localStorage.setItem("avatar", data.user_status.data.avatar);
                localStorage.setItem("gender", data.user_status.data.gender);
                localStorage.setItem("dob", data.user_status.data.dob);
                localStorage.setItem("isPrimary", data.user_status.data.is_primary);
                localStorage.setItem("handicap", data.user_status.data.handicap);
                localStorage.setItem("admin_to_verify", data.user_status.data.admin_to_verify);

                if (data.user_status.data.photo !== "" && data.user_status.data.photo !== "null" &&
                    data.user_status.data.photo !== null) {
                    localStorage.setItem("photo", data.user_status.data.photo);
                } else {
                    localStorage.setItem("photo", "");
                }
                localStorage.setItem("email_verified", data.user_status.data.email_verified);

                /* Process the user updates */
                processUserUpdates(data.badge_count);
            } else if (data.status == "notify") {
                swal({
                        title: "Info",
                        text: data.message,
                        type: "info",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        confirmButtonText: "Try Again"
                    },
                    function() {
                        return;
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {
            console.log("Index: ERROR: " + JSON.stringify(data));
        };
        postMessageToServerWithAuth("/user/details/index", actions, successCallback, errorCallback);
    }

    /**
     * Process the User Updates for badging menu items
     */
    processUserUpdates = function(updates) {
        var totalOutsideBadgeCount = 0;
        var bagdeArray = {
            "accountscount": 0,
            "blogcount": 0,
            "aboutcount": 0,
            "clubmgmtcount": 0,
            "classifiedcount": 0,
            "allclasicount": 0,
            "myclasicount": 0,
            "connectcount": 0,
            "blogcount": 0,
            "chatcount": 0,
            "mediacount": 0,
            "newscount": 0,
            "eventscount": 0,
            "allevntcount": 0,
            "myevntcount": 0,
            "facilitycount": 0,
            "banquetcount": 0,
            "fitnesscount": 0,
            "hnspacount": 0,
            "recreationalcount": 0,
            "roomcount": 0,
            "saloncount": 0,
            "sportcount": 0,
            "othercount": 0,
            "fbookingcount": 0,
            "fnbcount": 0,
            "golfcount": 0,
            "galltourcount": 0,
            "gmytourcount": 0,
            "gtourrescount": 0,
            "gscorecardcount": 0,
            "livescorecount": 0,
            "opinioncount": 0,
            "pollcount": 0,
            "surveycount": 0,
            "offercount": 0,
            "sponsorcount": 0,
            "tournamentcount": 0,
            "alltourcount": 0,
            "mytourcount": 0,
            "tourresultcount": 0,
            "3000": 0,
            "3001": 0,
            "3002": 0,
            "1100": 0,
            "1101": 0,
            "1102": 0,
            "1300": 0,
            "1301": 0,
            "1302": 0,
            "1400": 0,
            "1500": 0,
            "1501": 0,
            "1502": 0,
            "1503": 0,
            "1600": 0,
            "1700": 0,
            "1701": 0,
            "1702": 0,
            "1703": 0,
            "1704": 0,
            "1800": 0,
            "1801": 0,
            "1802": 0,
            "1803": 0,
            "1804": 0,
            "1910": 0,
            "1911": 0,
            "1920": 0,
            "1921": 0,
            "1930": 0,
            "1931": 0,
            "1940": 0,
            "1941": 0,
            "1950": 0,
            "1951": 0,
            "1960": 0,
            "1961": 0,
            "1970": 0,
            "1971": 0,
            "3101": 0,
            "3102": 0,
            "1901": 0,
            "1902": 0,
            "2000": 0,
            "2001": 0,
            "2002": 0,
            "2101": 0,
            "2102": 0,
            "2103": 0,
            "2104": 0,
            "2105": 0,
            "2106": 0,
            "2107": 0,
            "2108": 0,
            "2109": 0,
            "2110": 0,
            "2111": 0,
            "2200": 0,
            "2201": 0,
            "2202": 0,
            "2203": 0,
            "2204": 0,
            "2300": 0,
            "2301": 0,
            "2302": 0,
            "2400": 0,
            "2401": 0,
            "2402": 0,
            "2500": 0,
            "2501": 0,
            "2502": 0,
            "2600": 0,
            "2601": 0,
            "2602": 0,
            "2700": 0,
            "2701": 0,
            "2702": 0,
            "2703": 0,
            "2801": 0,
            "2802": 0,
            "2803": 0,
            "2804": 0,
            "2805": 0,
            "2806": 0,
            "2807": 0,
            "2808": 0,
            "2809": 0,
            "2810": 0,
            "2901": 0,
            "2902": 0,
            "2903": 0,
            "2904": 0,
            "1200": 0
        };

        for (var key in updates) {
            if (updates.hasOwnProperty(key)) {
                var value = updates[key];
                totalOutsideBadgeCount += parseInt(value);
                switch (parseInt(key)) {
                    case 1100:
                        bagdeArray["1100"] += parseInt(value);
                        bagdeArray["blogcount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1101:
                        bagdeArray["1101"] += parseInt(value);
                        bagdeArray["blogcount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1102:
                        bagdeArray["1102"] += parseInt(value); //blogcount
                        bagdeArray["blogcount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1200:
                        bagdeArray["1200"] += parseInt(value); //mediacount
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1300:
                        bagdeArray["1300"] += parseInt(value); //newscount
                        bagdeArray["newscount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1301:
                        bagdeArray["1301"] += parseInt(value); //newscount
                        bagdeArray["newscount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1302:
                        bagdeArray["1302"] += parseInt(value); //newscount
                        bagdeArray["newscount"] += parseInt(value);
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1400:
                        bagdeArray["1400"] += parseInt(value); //noticount
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1500:
                        bagdeArray["1500"] += parseInt(value); //chatcount
                        bagdeArray["chatcount"] += parseInt(value); // different chat counts
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1501: //invite
                        bagdeArray["1501"] += parseInt(value); //chatcount
                        bagdeArray["chatcount"] += parseInt(value); // different chat counts
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1502: //acepted/rejected
                        bagdeArray["1502"] += parseInt(value); //chatcount
                        bagdeArray["chatcount"] += parseInt(value); // different chat counts
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1503: //new message
                        bagdeArray["1503"] += parseInt(value); //chatcount
                        bagdeArray["chatcount"] += parseInt(value); // different chat counts
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1600: // members
                        bagdeArray["1600"] += parseInt(value); //chatcount
                        bagdeArray["chatcount"] += parseInt(value); //should be member updates
                        bagdeArray["connectcount"] += parseInt(value);
                        break;
                    case 1700:
                        bagdeArray["1700"] += parseInt(value); //allclasicount
                        bagdeArray["allclasicount"] += parseInt(value);
                        bagdeArray["classifiedcount"] += parseInt(value);
                        break;
                    case 1701:
                        bagdeArray["1701"] += parseInt(value); //allclasicount
                        bagdeArray["allclasicount"] += parseInt(value);
                        bagdeArray["classifiedcount"] += parseInt(value);
                        break;
                    case 1702:
                        bagdeArray["1702"] += parseInt(value); //allclasicount
                        bagdeArray["allclasicount"] += parseInt(value);
                        bagdeArray["classifiedcount"] += parseInt(value);
                        break;
                    case 1703:
                        bagdeArray["1703"] += parseInt(value); //myclasicount
                        bagdeArray["myclasicount"] += parseInt(value);
                        bagdeArray["classifiedcount"] += parseInt(value);
                        break;
                    case 1704:
                        bagdeArray["1704"] += parseInt(value); //myclasicount
                        bagdeArray["myclasicount"] += parseInt(value);
                        bagdeArray["classifiedcount"] += parseInt(value);
                        break;
                    case 1800:
                        bagdeArray["1800"] += parseInt(value); //allevntcount
                        bagdeArray["allevntcount"] += parseInt(value);
                        bagdeArray["eventscount"] += parseInt(value);
                        break;
                    case 1801:
                        bagdeArray["1801"] += parseInt(value); //allevntcount
                        bagdeArray["allevntcount"] += parseInt(value);
                        bagdeArray["eventscount"] += parseInt(value);
                        break;
                    case 1802:
                        bagdeArray["1802"] += parseInt(value); //allevntcount
                        bagdeArray["allevntcount"] += parseInt(value);
                        bagdeArray["eventscount"] += parseInt(value);
                        break;
                    case 1803:
                        bagdeArray["1803"] += parseInt(value); //myevntcount
                        bagdeArray["myevntcount"] = parseInt(value);
                        bagdeArray["eventscount"] += parseInt(value);
                        break;
                    case 1804:
                        bagdeArray["1804"] += parseInt(value); //myevntcount
                        bagdeArray["myevntcount"] = parseInt(value);
                        bagdeArray["eventscount"] += parseInt(value);
                        break;
                    case 1900:
                    case 1910:
                        bagdeArray["1910"] += parseInt(value); //fitnesscount
                        bagdeArray["fitnesscount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1911:
                        bagdeArray["1911"] += parseInt(value); //fitnesscount
                        bagdeArray["fitnesscount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1920:
                        bagdeArray["1920"] += parseInt(value); //sportcount
                        bagdeArray["sportcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1921:
                        bagdeArray["1921"] += parseInt(value); //sportcount
                        bagdeArray["sportcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1930:
                        bagdeArray["1930"] += parseInt(value); //hnspacount
                        bagdeArray["hnspacount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1931:
                        bagdeArray["1931"] += parseInt(value); //hnspacount
                        bagdeArray["hnspacount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1940:
                        bagdeArray["1940"] += parseInt(value); //recreationalcount
                        bagdeArray["recreationalcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1941:
                        bagdeArray["1941"] += parseInt(value); //recreationalcount
                        bagdeArray["recreationalcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1950:
                        bagdeArray["1950"] += parseInt(value); //saloncount
                        bagdeArray["saloncount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1951:
                        bagdeArray["1951"] += parseInt(value); //saloncount
                        bagdeArray["saloncount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1960:
                        bagdeArray["1960"] += parseInt(value); //othercount
                        bagdeArray["othercount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1961:
                        bagdeArray["1961"] += parseInt(value); //othercount
                        bagdeArray["othercount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1970:
                        bagdeArray["1970"] += parseInt(value); //banquetcount
                        bagdeArray["banquetcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1971:
                        bagdeArray["1971"] += parseInt(value); //banquetcount
                        bagdeArray["banquetcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1901:
                        bagdeArray["1901"] += parseInt(value); //fbookingcount
                        bagdeArray["fbookingcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 1902:
                        bagdeArray["1902"] += parseInt(value); //fbookingcount
                        bagdeArray["fbookingcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 2000:
                        bagdeArray["2000"] += parseInt(value); //offercount
                        bagdeArray["offercount"] += parseInt(value);
                        break;
                    case 2001:
                        bagdeArray["2001"] += parseInt(value); //offercount
                        bagdeArray["offercount"] += parseInt(value);
                        break;
                    case 2002:
                        bagdeArray["2002"] += parseInt(value); //offercount
                        bagdeArray["offercount"] += parseInt(value);
                        break;
                    case 2100:
                    case 2101:
                        bagdeArray["2101"] += parseInt(value); //gcoursecount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2102:
                        bagdeArray["2102"] += parseInt(value); //ghandicapcount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2103:
                        bagdeArray["2103"] += parseInt(value); //galltourcount
                        bagdeArray["galltourcount"] += parseInt(value);
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2104:
                        bagdeArray["2104"] += parseInt(value); //galltourcount
                        bagdeArray["galltourcount"] += parseInt(value);
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2105:
                        bagdeArray["2105"] += parseInt(value); //gmytourcount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2106:
                        bagdeArray["2106"] += parseInt(value); //gtourrescount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2107:
                        bagdeArray["2107"] += parseInt(value); //gscorecardcount
                        bagdeArray["gscorecardcount"] += parseInt(value);
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2108:
                        bagdeArray["2108"] += parseInt(value); //gscorecardcount
                        bagdeArray["gscorecardcount"] += parseInt(value);
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2109:
                        bagdeArray["2109"] += parseInt(value); //gscorecardcount
                        bagdeArray["gscorecardcount"] += parseInt(value);
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2110:
                        bagdeArray["2110"] += parseInt(value); //gcoachcount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2111:
                        bagdeArray["2111"] += parseInt(value); //gteetimecount
                        bagdeArray["golfcount"] += parseInt(value);
                        break;
                    case 2200:
                        bagdeArray["2200"] += parseInt(value);
                        bagdeArray["alltourcount"] += parseInt(value);
                        bagdeArray["tournamentcount"] += parseInt(value);
                        break;
                    case 2201:
                        bagdeArray["2201"] += parseInt(value);
                        bagdeArray["alltourcount"] += parseInt(value);
                        bagdeArray["tournamentcount"] += parseInt(value);
                        break;
                    case 2202:
                        bagdeArray["2202"] += parseInt(value);
                        bagdeArray["alltourcount"] += parseInt(value);
                        bagdeArray["tournamentcount"] += parseInt(value);
                        break;
                    case 2203:
                        bagdeArray["2203"] += parseInt(value);
                        bagdeArray["mytourcount"] += parseInt(value);
                        bagdeArray["tournamentcount"] += parseInt(value);
                    case 2204:
                        bagdeArray["2204"] += parseInt(value);
                        bagdeArray["tourresultcount"] += parseInt(value);
                        bagdeArray["tournamentcount"] += parseInt(value);
                        break;
                    case 2300:
                        bagdeArray["2300"] += parseInt(value);
                        bagdeArray["livescorecount"] += parseInt(value);
                        break;
                    case 2301:
                        bagdeArray["2301"] += parseInt(value);
                        bagdeArray["livescorecount"] += parseInt(value);
                        break;
                    case 2302:
                        bagdeArray["2302"] += parseInt(value);
                        bagdeArray["livescorecount"] += parseInt(value);
                        break;
                    case 2400:
                        bagdeArray["2400"] += parseInt(value);
                        bagdeArray["pollcount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2401:
                        bagdeArray["2401"] += parseInt(value);
                        bagdeArray["pollcount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2402:
                        bagdeArray["2402"] += parseInt(value);
                        bagdeArray["pollcount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2500:
                        bagdeArray["2500"] += parseInt(value);
                        bagdeArray["surveycount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2501:
                        bagdeArray["2501"] += parseInt(value);
                        bagdeArray["surveycount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2502:
                        bagdeArray["2502"] += parseInt(value);
                        bagdeArray["surveycount"] += parseInt(value);
                        bagdeArray["opinioncount"] += parseInt(value);
                        break;
                    case 2600:
                        bagdeArray["2600"] += parseInt(value);
                        bagdeArray["sponsorcount"] += parseInt(value);
                        break;
                    case 2601:
                        bagdeArray["2601"] += parseInt(value);
                        bagdeArray["sponsorcount"] += parseInt(value);
                        break;
                    case 2602:
                        bagdeArray["2602"] += parseInt(value);
                        bagdeArray["sponsorcount"] += parseInt(value);
                        break;
                    case 2700:
                        bagdeArray["2700"] += parseInt(value);
                        bagdeArray["accountscount"] += parseInt(value);
                        break;
                    case 2701: // New Transaction
                        bagdeArray["2701"] += parseInt(value);
                        bagdeArray["accountscount"] += parseInt(value);
                        break;
                    case 2702: // Query Response
                        bagdeArray["2702"] += parseInt(value);
                        bagdeArray["accountscount"] += parseInt(value);
                        break;
                    case 2703: // New Monthly Bill
                        bagdeArray["2703"] += parseInt(value);
                        bagdeArray["accountscount"] += parseInt(value);
                        break;
                    case 2800:
                    case 2801: // Updated Affiliation
                        bagdeArray["2801"] += parseInt(value); //affiliationcount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2802: // Updated Club Rules
                        bagdeArray["2802"] += parseInt(value); //clubrulecount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2803: // Updated Dress Code
                        bagdeArray["2803"] += parseInt(value); //dresscount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2804: // Updated Membership Options
                        bagdeArray["2804"] += parseInt(value); //memoptcount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2805: // Updated Membership Criteria
                        bagdeArray["2805"] += parseInt(value); //memcricount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2806: // Updated Gallery
                        bagdeArray["2806"] += parseInt(value); //galcount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2807: // Updated About
                        bagdeArray["2807"] += parseInt(value); //uscount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2808: // Updated History
                        bagdeArray["2808"] += parseInt(value); //historycount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2809: // Updated President Message
                        bagdeArray["2809"] += parseInt(value); //presimsgcount
                        bagdeArray["aboutcount"] += parseInt(value);
                        break;
                    case 2810: // Updated Contact us
                        bagdeArray["2810"] += parseInt(value); //contactcount
                        break;
                    case 2900:
                    case 2901: // Updated Administration
                        bagdeArray["2901"] += parseInt(value); //admincount
                        bagdeArray["clubmgmtcount"] += parseInt(value);
                        break;
                    case 2902: // Updated Present Committee
                        bagdeArray["2902"] += parseInt(value); //presentcomcount
                        bagdeArray["clubmgmtcount"] += parseInt(value);
                        break;
                    case 2903: // Updated Past Committee
                        bagdeArray["2903"] += parseInt(value); //pastcomcount
                        bagdeArray["clubmgmtcount"] += parseInt(value);
                        break;
                    case 2904: // Updated Club Staff
                        bagdeArray["2904"] += parseInt(value); //staffcount
                        bagdeArray["clubmgmtcount"] += parseInt(value);
                        break;
                    case 3000:
                        bagdeArray["3000"] += parseInt(value);
                        bagdeArray["fnbcount"] += parseInt(value);
                        break;
                    case 3001: // Updated Restaurant List
                        bagdeArray["3001"] += parseInt(value);
                        bagdeArray["fnbcount"] += parseInt(value);
                        break;
                    case 3002: // Updated Menu
                        bagdeArray["3002"] += parseInt(value);
                        bagdeArray["fnbcount"] += parseInt(value);
                        break;
                    case 3100:
                        break;
                    case 3101:
                        bagdeArray["3101"] += parseInt(value); //roomcount
                        bagdeArray["roomcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    case 3102:
                        bagdeArray["3102"] += parseInt(value); //roomcount
                        bagdeArray["roomcount"] += parseInt(value);
                        bagdeArray["facilitycount"] += parseInt(value);
                        break;
                    default:
                        console.log("Type Id = " + data.notificationType);
                        console.log("Additional Data = " + data.additionalData);
                        break;
                }
            }
        }

        /* Put menu badges into storage for later use */
        localStorage.setItem("MENUBADGES", JSON.stringify(bagdeArray));

        for (var item in bagdeArray) {
            if (bagdeArray.hasOwnProperty(item)) {
                if (bagdeArray[item] > 0) {
                    var span = document.getElementById(item);
                    if (span == null || span == undefined)
                        continue;
                    txt = document.createTextNode(bagdeArray[item]);
                    span.innerText = txt.textContent;
                    $("#" + item).css("display", "block");
                }
            }
        }

        /* Update Home Screen Badges */
        if (bagdeArray["myevntcount"] > 0) {
            $("#homeEventsBadge").css({ "visibility": "visible" });
            $("#homeEventsBadge").html(bagdeArray["myevntcount"]);
        }
        if (bagdeArray["newscount"] > 0) {
            $("#homeNewsBadge").css({ "visibility": "visible" });
            $("#homeNewsBadge").html(bagdeArray["newscount"]);
        }
        if (bagdeArray["2701"] > 0) {
            $("#homeTransBadge").css({ "visibility": "visible" });
            $("#homeTransBadge").html(bagdeArray["2701"]);
        }
        if (bagdeArray["1400"] > 0) {
            $("#homeNotifyBadge").css({ "visibility": "visible" });
            $("#homeNotifyBadge").html(bagdeArray["1400"]);
        }
        if (bagdeArray["chatcount"] > 0) {
            var invcount = bagdeArray["1501"] + bagdeArray["1502"];
            if (invcount > 0) {
                $("#homeConnectBadge").css({ "visibility": "visible" });
                $("#homeConnectBadge").html(invcount);
            }
            if (bagdeArray["1503"] > 0) {
                $("#homeMessageBadge").css({ "visibility": "visible" });
                $("#homeMessageBadge").html(bagdeArray["1503"]);
            }
        }
        if (bagdeArray["offercount"] > 0) {
            $("#homeOffersBadge").css({ "visibility": "visible" });
            $("#homeOffersBadge").html(bagdeArray["offercount"]);
        }
        if (bagdeArray["fbookingcount"] > 0) {
            $("#homeBookBadge").css({ "visibility": "visible" });
            $("#homeBookBadge").html(bagdeArray["fbookingcount"]);
        }

        localStorage.setItem("TotalOutsideBadgeCount", totalOutsideBadgeCount);
        if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            if (totalOutsideBadgeCount > 0)
                window.plugins.PushbotsPlugin.setBadge(totalOutsideBadgeCount);
            else
                window.plugins.PushbotsPlugin.resetBadge();
        }
    }

    gotoMyTransactionHistory = function() {
        localStorage.setItem("from_page", "home");
        slideview("my-account.html", "script", "left");
    }

    gotoInviteHistory = function() {
        localStorage.setItem("from_page", "home-invite");
        slideview("connect-people.html", "script", "left");
    }

    gotoMessageHistory = function() {
        localStorage.setItem("from_page", "home-message");
        slideview("connect-people.html", "script", "left");
    }

    resendEmail = function() {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Success"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/email", actions, successCallback, errorCallback);
    }
}());