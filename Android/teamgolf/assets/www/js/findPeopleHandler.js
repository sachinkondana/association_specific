(function () {
    var enableDebugMode = 0;

    /* Extend this as needed */
    var chatStatusArr = ["INVITATION_NOT_SENT", "INVITATION_SENT", "INVITATION_RECEIVED", "INVITATION_ACCEPTED", "INVITATION_REJECTED"];

    var numofUsers, remove = false,
            scroll = false;
    "use strict";
    var page_number = 1;
    localStorage["numofUsers"] = 0;

    $(".content").scroll(function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            remove = false;
            scroll = true;
            if ((parseInt(localStorage.getItem("numofUsers")) / 15 ) == page_number) {
                page_number++;
                if ($("#search").val() == '') {
                    getUserList(page_number);
                } else {
                    searchUser(page_number);
                }
            }
        }
    });

    $("#search").keyup(function () {
        localStorage.setItem('numofUsers', '0');
        var keyword = $(this).val();
        scroll = false;
        if (keyword == '') {
            remove = true;
        } else {
            remove = false;
        }
        page_number = 1;
        searchUser(page_number);
    });

    /**
     * Search user in the user list
     * @pg_no page number to begin with
     */
    searchUser = function (pg_no) {
        //localStorage.setItem('numofUsers', '0');
        var actions = {
            "page_number": pg_no,
            "needle": $('#search').val().trim()
        };

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                if (data.data.length > 0)
                    localStorage.setItem('numofUsers', String(parseInt(localStorage["numofUsers"]) + parseInt(JSON.stringify(data.data.length))));
                displayUserList(data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofUsers', 0);
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
    }


    /**
     * Get list of members from server
     * @page_number page number to begin with
     */
    getUserList = function (page_number) {
        /* Handle comming back scenario */
        if ($('#search').val().trim() != "") {
            searchUser(page_number);
            return;
        }

        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                console.log("length = " + data.data.length);
                if ((1 == page_number) || ('1' == page_number)) {
                    page_number = 1; //important for refresh case
                    $('#user-list').html('');
                    localStorage.setItem('numofUsers', 0);
                }
                localStorage.setItem('numofUsers', String(parseInt(localStorage["numofUsers"]) + parseInt(JSON.stringify(data.data.length))));

                displayUserList(data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofUsers', 0);
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
    }


    /**
     * Display list of members
     * @memberList member list
     */
    displayUserList = function (memberList) {
        if (($("#search").val().trim() != "" && !scroll) || remove)
            $('#user-list').html("");

        var fname, lname, id, avatar = "images/avatar-001.jpg";

        if ((memberList.data.length <= 0) && ((1 == page_number) || ('1' == page_number)))
                $('#user-list').html('<div style="width:100%; margin:0 auto; text-align:center"><p style="font-weight:bold;" align="center">' + localizeString("Coaches_Label_Nodata") + '</p></div>');

        for (i = 0; i < memberList.data.length; i++) {
            fname = memberList.data[i].first_name;
            lname = memberList.data[i].last_name;
            photos = memberList.data[i].photo;
            var id = memberList.data[i].id;
            if (fname != '' && fname != null && fname != undefined) {
                var li = $('<li></li>').attr({
                    'Onclick': "gotoDetail('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                });
                var div1 = $('<div></div>').attr({
                    'class': 'user-img'
                });
                var img;
                if (photos != null && photos != undefined && photos != "") {
                    img = $('<img></img>').attr({
                        'src': 'data:image/jpeg;base64,' + photos
                    });
                } else {
                    img = $('<img></img>').attr({
                        'src': 'images/avatar-001.jpg'
                    });
                }

                div1.append(img);
                var div2 = $('<div></div>').attr({
                    'class': 'five_sixth bold-match-color',
                    'style': 'line-height:46px;'
                }).text(fname + " " + lname);

                li.append(div1, div2);
                $('#user-list').append(li);
            }
        }
        window.plugins.spinnerDialog.hide();
    }


    /**
     * Go to member details page
     * @id member id
     * @fname member First Name
     * @lname member Last Name
     */
    gotoDetail = function (id, fname, lname) {
        if (enableDebugMode)
            alert("gotoDetail");
        localStorage.setItem("UserIdenifier", id);
        localStorage.setItem("Userfirst_name", fname);
        localStorage.setItem("Userlast_name", lname);
        slideview("find-people-details.html", 'script', 'left');
    }


    /**
     * Get individual member detail from server
     */
    getPersonalDetails = function () {
        var actions = {
            "user_id": localStorage.getItem("UserIdenifier")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayUserDetails(data);
            } else {
                $('#details').html('');
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user/view", actions, successCallback, errorCallback);
    }


    /**
     * Display Member Details
     * @memberDetail member Detail
     */
    displayUserDetails = function (memberDetail) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(memberDetail));
        $('.user-list').html('');
        var li = $('<li></li>').attr({
            'style': 'border-bottom:none; margin-bottom:0px;'
        });
        var div1 = $('<div></div>').attr({
            'class': 'user-img-01'
        });

        var div2 = $('<div></div>').attr({
            'class': 'four_sixth bold-match-color',
            'style': 'margin-left:3%'
        });
        var p = $('<p></p>').attr({
            'style': 'margin-top:8px; margin-bottom:3px'
        }).text(localStorage.getItem('Userfirst_name') + ' ' + localStorage.getItem('Userlast_name'));
        $("#title").html(localStorage.getItem('Userfirst_name'));
        var img;
        img = $('<img></img>').attr({
            'src': 'images/avatar-001.jpg'
        });
        if (memberDetail == '') {
            var span = $('<span></span>').attr({
                'class': 'text'
            }).text('');
        } else {
            if (memberDetail.data.avatar != null && memberDetail.data.avatar != '' && memberDetail.data.avatar != undefined)
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text(memberDetail.data.avatar);
            else
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text('Not Availbale');

            if (memberDetail.data.photo != null && memberDetail.data.photo != "" && memberDetail.data.photo != undefined) {
                img = $('<img></img>').attr({
                    'src': 'data:image/jpeg;base64,' + memberDetail.data.photo
                });
            }
        }

        div1.append(img);
        p.append($('</br>'), span);
        var div3 = $('<div></div>').attr({
            'class': 'user-arrow'
        });
        var a = $('<a></a>').attr({
            'href': 'javascript:void()'
        });
        var ival = $('<i></i>').attr({
            'class': 'mdi mdi-table-edit',
            'data-name': 'mdi-table-edit'
        });
        a.append(ival);
        div3.append(a);
        div2.append(p, div3);
        li.append(div1, div2);
        $('.user-list').append(li);
        $('#details').html('');

        for (var j = 0; j < memberDetail.data.display_ctrl.length ; j++) {
            if ((memberDetail.data.display_ctrl[j].selected == "1") && (memberDetail.data.display_ctrl[j].name != "first_name")
                && (memberDetail.data.display_ctrl[j].name != "last_name") && (memberDetail.data.display_ctrl[j].name != "avatar")
                && (memberDetail.data.display_ctrl[j].name != "photo") && (memberDetail.data.display_ctrl[j].name != "status")) {
                var tr1 = $('<tr></tr>');
                var td1 = $('<td></td>').attr({
                    'height': '25'
                });
                var strong2 = $('<strong></strong>').text(memberDetail.data.display_ctrl[j].display_name + ':');
                td1.append(strong2);
                if (memberDetail.data[memberDetail.data.display_ctrl[j].name] != null
                    && memberDetail.data[memberDetail.data.display_ctrl[j].name] != undefined
                    && memberDetail.data[memberDetail.data.display_ctrl[j].name] != "") {
                    if (memberDetail.data.display_ctrl[j].name == "email") {
                        var td2 = $('<td onclick="gotomail(\'' + memberDetail.data.email + '\');"></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    } else if (memberDetail.data.display_ctrl[j].name == "phone") {
                        var td2 = $('<td onclick="gotocall(' + memberDetail.data.phone + ');"></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    } else {
                        var td2 = $('<td ></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    }
                } else {
                    var td2 = $('<td></td>').text(localizeString("Account_Label_NotAvailable"));
                }

                tr1.append(td1, td2);
                $('#details').append(tr1);
            }
        }

        $('#content img').simplebox({
            fadeSpeed: 350
        });
    }


    gotomail = function (mailadd) {
        swal({
            title: localizeString("Alert_Title_ConfirmAction"),
            text: localizeString("Alert_Text_SendEmail"),
            type: "info",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("mailto:" + mailadd + "", "_system");
        });
    }


    gotocall = function (phno) {
        swal({
            title: localizeString("Alert_Title_ConfirmAction"),
            text: localizeString("Alert_Text_MakeCall"),
            type: "info",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText :localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("tel:" + phno + "", "_system");
        });
    }
}());