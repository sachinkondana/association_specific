(function() {
    var pageflag = 1;
    var page_number = 1;
    var enableDebugMode = 0;
    "use strict";
    localStorage["numofPolls"] = 0;

    /**
     * scrolling
     */
    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofPolls")) / 15) == page_number) {
                page_number++;
                getPollList(page_number);
            }
        }
    });

    /**
     *  Fetches the list of Polls based on page_number
     *  @page_number current viewable page
     */
    getPollList = function(page_number) {
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length == 0) {
                    blankData = true;
                    if (pageflag == 1)
                        $('#content').html('<p style="text-align:center">' + localizeString("Polls_Label_NoData") + '</p>');
                } else {
                    localStorage.setItem('numofPolls', String(parseInt(localStorage["numofPolls"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayPollList(data.data);
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            pageflag++;
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/polls", actions, successCallback, errorCallback);
    }

    /**
     *  Displays the List of all the active Polls
     *  @pollList Poll List received from the Server
     */
    displayPollList = function(pollList) {
        for (i = 0; i < pollList.content.length; i++) {
            // var div1 = $('<div class="col-lg-4 col-sm-12" width="100%"></div>').attr('onClick', "gotoPollDetail('" + pollList.content[i].id + "')");
            // var div2 = $('<div class="card-01"></div>');
            // var img;
            // var d = new Date(pollList.content[i].startFrom); // Should be start date
            // if (pollList.content[i].image !== null && pollList.content[i].image != ""
            //     && pollList.content[i].image != undefined) {
            //     img = pollList. imageBasePath + pollList.content[i].image;
            // } else {
            //     img = "images/default-listing.png";
            // }
            // var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            // var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + img + '"></div>');
            // aside.append(imgDiv);
            // var div4 = $('<div class="card-main card-width"></div>');
            // var div5 = $('<div class="card-inner-01 card-inner-width"></div>');
            // var p = $('<p class="card-heading card-heading-flow"></p>').text(pollList.content[i].title); //poll-desc-01

            // var div6 = $('<div class="blog-post-meta"></div>')
            // var span3 = $('<span></span>');
            // var dispDat = getDisplayableDate(pollList.content[i].startFrom);
            // var dispDat2 = getDisplayableDate(pollList.content[i].validUpto);
            // var iel = $('<i class="fa fa-clock-o"> ' + localizeString("Label_Started")+':' +  dispDat + ' </i></br>');
            // var iel2 = $('<i class="fa fa-clock-o"> ' + localizeString("Label_EndsOn")+':' +  dispDat2 + ' </i>');
            // span3.append(iel, iel2);
            // div6.append(span3);
            // div5.append(p, div6);
            // div4.append(div5);
            // div2.append(aside, div4);
            // div1.append(div2);
            // $('#content').append(div1);
            $('#content').append(getListDom(pollList.content[i], pollList.imageBasePath, "gotoPollDetail"));
        }
        lazyLoadListImages(page_number, pollList.content.length);
    }

    function getListDom(listData, iImgUrl, iClickCall) {
        var name = listData['title'],
            cStart = moment(listData['startFrom'], "YYYY-MM-DD"),
            cEnd = moment(listData['validUpto'], "YYYY-MM-DD"),
            id = listData['id'],
            logo = (listData['image']) ? (iImgUrl + listData['image']) : "./images/placeholder.png",
            commentsCount = listData['commentsCount'];

        cStart = (cStart.isValid()) ? cStart.format("lll") : "--";
        cEnd = (cEnd.isValid()) ? cEnd.format("lll") : "--";

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="' + iClickCall + '(\'' + id + '\')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + name + '</div>\
                        <div class="course-row-country clamp2 mT10">' + localizeString("Label_Started") + ': ' + cStart + '</div>\
                        <div class="course-row-country clamp2">' + localizeString("Label_EndsOn") + ': ' + cEnd + '</div>\
                    </div>\
                </div>';
    }


    /**
     *  Displays selected Poll details
     *  @id id of the selected Poll
     */
    gotoPollDetail = function(id) {
        localStorage.setItem('PollId', id);
        slideview("poll-details.html", 'script', 'left');
    }

    /**
     *  Fetches details of a specific Poll
     */
    getPollDetail = function() {
        var poll_id = localStorage.PollId;
        var actions = {
            "poll_id": poll_id
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayPoll(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/polls", actions, successCallback, errorCallback);
    }


    /**
     * Displays the contents of a Poll
     * @poll Poll data received from the Server
     */
    displayPoll = function(poll) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(poll));
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;"></p>').text(poll.content[0].title);
        var span1 = $('<span style=" font-size:12px; float:right"></span>');
        var dispDat = getDisplayableDate(poll.content[0].validUpto);
        var iElem2 = $('<i class="fa fa-clock-o"> ' + localizeString("Label_EndsOn") + ': ' + dispDat + '</i>');
        span1.append(iElem2); /*, time*/

        var legend = $('<legend id="polllegend" style="font-size: 1.4em; padding-left:10px; word-wrap:break-word; word-break:break-word;">' + poll.content[0].description + '</legend>');
        if (poll.content[0].image != null && poll.content[0].image != undefined && poll.content[0].image != "") {
            var img = $('<img alt="No Image" src="' + poll.imageBasePath + poll.content[0].image + '" id="image1" style="width:100%;">');
            $('#pollData').append(p1, img, legend, span1);
        } else {
            $('#pollData').append(p1, legend, span1);
        }
        var divform = $('<div class="form-group form-group-label" id="vote-options-form" style="width:90%; padding-left:15px; margin-top:-5px;"> </div>');
        for (var j = 0; j < poll.content[0].options.length; j++) {
            var label = $('<label style="display:block; padding:5px; font-size:1.4em; align=center"></label>');
            if (1 == parseInt(poll.content[0].type)) { // only single select
                var radio = $('<input type="radio" name="Poll" value="' + poll.content[0].options[j].master_id + '" onclick="validateSelection(' + parseInt(poll.content[0].type) + ')" id="Poll_' + j + '"> ' + poll.content[0].options[j].details + ' </input></br>');
                label.append(radio);
            } else if (2 == parseInt(poll.content[0].type)) { // Multi select
                var chkbox = $('<input type="checkbox" name="Poll" value="' + poll.content[0].options[j].master_id + '" onclick="validateSelection(' + parseInt(poll.content[0].type) + ')" id="Poll_' + j + '"> ' + poll.content[0].options[j].details + ' </input></br>');
                label.append(chkbox);
            }
            divform.append(label);
        }
        var spanSuccess = $('<span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="radiosuccess"><i class="form-help-icon icon">check</i></span>');
        var spanFailure = $('<span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="radioerror"><i class="form-help-icon icon">error</i></span>');
        var spanSubmitted = $('<span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="radiosubmit"><i class="form-help-icon icon">check</i></span>');
        if (localStorage.getItem("isLoggedIn") == "true") {
            var submit = $('<input class="btn btn-alt" type="submit" name="submit" id="submit" style="margin-left:auto;margin-right:auto;display:block;" value="Vote" onclick="return submitMySelection(' + poll.content[0].options.length + ', ' + parseInt(poll.content[0].type) + ');" />');
            divform.append(spanSuccess, spanFailure, spanSubmitted, '</br>', '</br>', submit);
        } else {
            divform.append(spanSuccess, spanFailure, spanSubmitted, '</br>');
        }
        $('#pollfieldset').append(divform); /*legend*/

        if (localStorage.getItem("isLoggedIn") == "true") {
            if (true == poll.content[0].alreadySubmitted || "true" == poll.content[0].alreadySubmitted) {
                populateResponse(parseInt(poll.content[0].type), poll.content[0].response, poll.content[0].options.length);
                for (var j = 0; j < poll.content[0].options.length; j++) {
                    document.getElementById('Poll_' + j).disabled = true;
                }
                $("#radiosubmit").css("display", "inline");
                $("#radiosubmit").html('<i class="form-help-icon icon">check</i> ' + localizeString("Poll_Label_Message"));
                document.getElementById('submit').disabled = true;
            }
        }
    }


    /**
     * Removes error messages when any of the option is selected
     */
    validateSelection = function(type) {
        if (atLeastOneRadio(type))
            $("#radioerror").css("display", "none");
    }


    /**
     * Checks if atlease one of the option is selected based on type
     * @type 1 = Radio Button, 2 = Check Box
     */
    atLeastOneRadio = function(type) {
        if (1 == type)
            return ($('input[type=radio]:checked').size() > 0);
        else if (2 == type)
            return ($('input[type=checkbox]:checked').size() > 0);
        else
            return false;
    }


    /**
     * Clears the previous selection if any
     */
    clearSelection = function() {
        var slength = parseInt(localStorage.getItem('SelectedRadioListLength'));
        for (var i = 0; i < slength; i++) {
            document.getElementById('Poll_' + i).checked = false;
        }
    }

    /**
     * Populate already submited user response for a poll
     * @type 1 = Radio Button, 2 = Check Box
     * @response array of user responses
     */
    populateResponse = function(type, response, length) {
        var j = 0;
        for (var i = 0; i < length; i++) {
            if (response[j] == document.getElementById('Poll_' + i).value) {
                document.getElementById('Poll_' + i).checked = true;
                j++;
            }
            if (j >= response.length)
                return;
        }
    }


    /**
     * Submit users response for a poll to the server
     * @len number of poll options
     * @type 1 = Radio Button, 2 = Check Box
     */
    submitMySelection = function(len, type) {
        if (false == atLeastOneRadio(type)) {
            $("#radioerror").css("display", "inline");
            if (1 == type)
                $("#radioerror").html(localizeString("ValidationError_SelectOption") + '<i class="form-help-icon icon">error</i>');
            else
                $("#radioerror").html(localizeString("ValidationError_SelectOneOption") + '<i class="form-help-icon icon">error</i>');
            return;
        } else {
            $("#radioerror").css("display", "none");
        }

        var selectedArr = [];
        for (var i = 0; i < len; i++) {
            if (document.getElementById('Poll_' + i).checked) {
                selectedArr.push(document.getElementById('Poll_' + i).value);
            }
        }

        if (selectedArr.length > 0) {
            var actions = {
                "poll_id": localStorage.PollId,
                "response": selectedArr
            };

            localStorage.setItem('SelectedRadioListLength', selectedArr.length);

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                if (data.status == "success") {
                    //swal.close();
                    clearSelection();
                    swal({
                            title: localizeString("Alert_Title_ThankYou"),
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            slideview("polls.html", 'script', 'left');
                        });
                } else {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
            };
            errorCallback = function(data) {

            };
            postMessageToServerWithAuth("/pollsubmit", actions, successCallback, errorCallback);
        } else {
            swal({
                    title: localizeString("Alert_Title_NoResponse"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function() {
                    return;
                });
        }
    }
}());