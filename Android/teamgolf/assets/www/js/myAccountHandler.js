(function () {
    var enableDebugMode = 0;
    "use strict";

    userAccountBadgeHandler = function () {
        if (localStorage.getItem("MENUBADGES") != null
            && localStorage.getItem("MENUBADGES") != undefined) {
            var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
            if (menuBadgeArray["2701"] > 0) {
                var span = document.getElementById("transactioncount");
                if (span != null && span != undefined) {
                    txt = document.createTextNode(menuBadgeArray["2701"]);
                    span.innerText = txt.textContent;
                    $("#transactioncount").css("display", "block");
                }
            }

            if (menuBadgeArray["2702"] > 0) {
                var span = document.getElementById("querycount");
                if (span != null && span != undefined) {
                    txt = document.createTextNode(menuBadgeArray["2702"]);
                    span.innerText = txt.textContent;
                    $("#querycount").css("display", "block");
                }
            }

            if (menuBadgeArray["2703"] > 0) {
                var span = document.getElementById("billcount");
                if (span != null && span != undefined) {
                    txt = document.createTextNode(menuBadgeArray["2703"]);
                    span.innerText = txt.textContent;
                    $("#billcount").css("display", "block");
                }
            }
            updateMenuBadgeStatus();
        }
    }

    clearFilters = function () {
        $('#date-from').val('');
        $('#date-to').val('');
        hideFilters();
        return getUserTransactionList(null, null);
    }

    showFilters = function () {
        $("#date-picker-form").css('display', 'inline-block');
        $("#date-picker-to").css('display', 'inline-block');
        $("#spacingdiv").css('display', 'inline-block');
        $("#MyAccount_Button_Go").css('display', 'inline-block');
        $("#MyAccount_Button_Clear").css('display', 'none');
        $("#Account_Button_PayNow").css('display', 'none');
        $("#showFiterBtn").attr("onclick","clearFilters()");

        if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            $("#date-from").flatpickr({
                disableMobile: false,
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#date-from").val = dateStr;
                }
            });

            $("#date-to").flatpickr({
                disableMobile: false,
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#date-to").val = dateStr;
                }
            });
        } else {
            $("#date-from").flatpickr({
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#date-from").val = dateStr;
                }
            });

            $("#date-to").flatpickr({
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#date-to").val = dateStr;
                }
            });
        }
    }

    hideFilters = function () {
        $("#date-picker-form").css('display', 'none');
        $("#date-picker-to").css('display', 'none');
        $("#spacingdiv").css('display', 'none');
        $("#MyAccount_Button_Go").css('display', 'none');
        $("#MyAccount_Button_Clear").css('display', 'none');
        if (localStorage.getItem("isLoggedIn") == "true") {
            $("#showFiterBtn").attr("onclick","showFilters()");
            //$("#Account_Button_PayNow").css('display', 'inline-block');
            $("#Account_Button_PayNow").css('display', 'none');
        }
    }

    getTimedTransactionList = function () {
        var retVal = 1;
        var sDate = $("#date-from").val().trim();
        var eDate = $("#date-to").val().trim();

        $("#dferror").html("");
        $("#dterror").html("");

        if (sDate == "") {
            $("#dferror").css("display", "inline");
            $("#dferror").html(localizeString("ValidationError_StartDate") + '<i class="form-help-icon icon">error</i>');
            retVal = 0;
        }

        if (eDate == "") {
            $("#dterror").css("display", "inline");
            $("#dterror").html(localizeString("ValidationError_EndDate") + '<i class="form-help-icon icon">error</i>');
            retVal = 0;
        }

        var result = dates.compare(eDate, sDate);

        if (result < 1) {
            $("#dterror").css("display", "inline");
            $("#dterror").html(localizeString("ValidationError_EndDateLessThanStartDate") + '<i class="form-help-icon icon">error</i>');
            retVal = 0;
        }

        if (retVal == 1) {
            $("#dferror").html("");
            $("#dterror").html("");
            getUserTransactionList(sDate, eDate);
        }
    }

    getUserTransactionList = function (sDate, eDate) {
        var actions = null;

        if (sDate != null && eDate != null) {
            actions = {
                "start_date" : sDate,
                "end_date" : eDate
            };
        }
        if (actions != null) {
            $("#MyAccount_Button_Go").css("display", "none");
            $("#MyAccount_Button_Clear").css("display", "inline");
        }

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (enableDebugMode)
                    alert("Records = " + JSON.stringify(data.data));
                if (data.data.length > 0) {
                    $("#showFiterBtn").css('display', 'inline-block');
                    showUserTranscationList(data);
                } else {
                    $("#showFiterBtn").css('display', 'none');
                    if (localStorage.getItem("isLoggedIn") == "true") {
                        var order_contents = "<p style='text-align:center'>" + localizeString("AccountTransaction_Label_NoData") + "</p>";
                        $("#orderstrans").html(order_contents);
                        $("#orderstrans").css("padding-top", "20px");
                    }
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user_billing", actions, successCallback, errorCallback);
    }

    showUserTranscationList = function (transdata) {
        var transactionList = transdata.data;
        $("#orderstrans").html("");
        if (localStorage.getItem("isLoggedIn") == "true") {
            var index;
            var order_contents = "";
            if (transdata.outstanding != null && transdata.outstanding != undefined
                && parseFloat(transdata.outstanding).toFixed(2) != 0.00) {
                order_contents = order_contents + "<div><p id='outstanding' class='cur_out_p'>Current Outstanding: <b>" + transdata.currency + " " + parseFloat(transdata.outstanding).toFixed(2) + "</b></p></div>";
            }
            order_contents = order_contents + "<div id='transactionTable' style='overflow-x:scroll; width:100%;'>";
            order_contents = order_contents +   "<table class='table table-bordered box box-solid' style='margin-bottom:0px!important; margin-top:10px!important; word-wrap:break-word!important;' id='mainTransactionTable' cellspacing='0'>";
            order_contents = order_contents +       "<tbody id='transactionTableBody'>";
            order_contents = order_contents +           "<tr style='color:#fff'>";
            order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:25%;'>" + localizeString("MyAccount_Label_DocDate") + "</th>";
            order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:45%;'>" + localizeString("MyAccount_Label_Particular") + "</th>";
            order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:30%;'>" + localizeString("MyAccount_Label_DrCr") + "</th>";
            order_contents = order_contents +           "</tr>";
            for (index = 0; index < transactionList.length; ++index) {
                var damount = "";
                var camount = "";
                var tamt = "";
                var color = "gray";
                var sufx = "";
                var type = "Dr/Cr";
                if (transactionList[index].debit == 0 && transactionList[index].credit > 0) {
                    camount = transactionList[index].credit;
                    tamt = transactionList[index].credit;
                    damount = "";
                    color = "Green";
                    sufx = "Cr. ";
                    //type = "Credit";
                } else if (transactionList[index].credit == 0 && transactionList[index].debit > 0) {
                    damount = transactionList[index].debit;
                    tamt = transactionList[index].debit;
                    camount = "";
                    color = "Maroon";
                    sufx = "Dr. ";
                    //type = "Debit";
                }

                var doc_num = encodeURIComponent(transactionList[index].doc_number);
                if (transactionList[index].debit == 0 && transactionList[index].credit > 0) {
                    order_contents = order_contents + "<tr class='colcrdt' id='"+ index + "' data-toggle='modal' data-target='#query-modal' onclick='viewTransactionDetails(\"" + doc_num + "\",\"" + transactionList[index].doc_date + "\");'>";
                } else if (transactionList[index].credit == 0 && transactionList[index].debit > 0) {
                    order_contents = order_contents + "<tr class='coldbt' id='"+ index + "' data-toggle='modal' data-target='#query-modal' onclick='viewTransactionDetails(\"" + doc_num + "\",\"" + transactionList[index].doc_date + "\");'>";
                } else {
                    order_contents = order_contents + "<tr class='coldef' id='"+ index + "' data-toggle='modal' data-target='#query-modal' onclick='viewTransactionDetails(\"" + doc_num + "\",\"" + transactionList[index].doc_date + "\");'>";
                }

                var dispDat = getDisplayableDate(transactionList[index].doc_date);
                order_contents = order_contents +      "<td class='col-sm-4 trans_det'><span style='color:black; width:25%; font-size:12px;'>" + dispDat + "</span></td>";
                order_contents = order_contents +      "<td class='col-sm-4 trans_det'><span style='color:black; width:45%; font-size:12px;'>" + transactionList[index].particular + "</span></td>";
                order_contents = order_contents +      "<td class='col-sm-4 trans_det'><span style='color:black; width:30%; font-size:12px;'>" + sufx + tamt + "</span></td>";
                order_contents = order_contents + "</tr>";
            }
            order_contents = order_contents +       "</tbody>";
            order_contents = order_contents +   "</table>";
            order_contents = order_contents + "</div>";
            $("#orderstrans").html(order_contents);

            // Update the transaction & menu badge count
            var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
            if (menuBadgeArray["2701"] > 0) {
                menuBadgeArray["accountscount"] = menuBadgeArray["accountscount"] - menuBadgeArray["2701"];
                menuBadgeArray["2701"] = 0;
                localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
                updateMenuBadgeStatus();
                updateNotificationState("", ["2701"], 4); // mark them as read
                var span = document.getElementById("transactioncount");
                if (span != null && span != undefined) {
                    txt = document.createTextNode(menuBadgeArray["2701"]);
                    span.innerText = txt.textContent;
                    $("#transactioncount").css("display", "none");
                }
            }
        }
    }

    viewTransactionDetails = function (transactionNumber, transactionDate) {
        localStorage.setItem("currentTransactionId", transactionNumber);
        localStorage.setItem("currentTransactionDate", transactionDate);
        if (localStorage.getItem("currentTransactionId") !== null && localStorage.getItem("currentTransactionId").trim() !== ""
            && localStorage.getItem("currentTransactionId") !== undefined) {
            $("#transrefid").val(decodeURIComponent(transactionNumber));
            $("#trasrefdate").val(transactionDate);
            $("#transrefid").disabled = "disabled";
            $("#transrefid").prop("readonly", true);
            $("#trasrefdate").disabled = "disabled";
            $("#trasrefdate").prop("readonly", true);
        }
    }


    /**
     * Get Monthly bills and there PDFs to show user
     */
    getUserMonthlyBill = function () {
        var actions = null;

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (enableDebugMode)
                    alert("Records = " + JSON.stringify(data.data));
                showUserMonthlyBill(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
        };

        postMessageToServerWithAuth("/getMonthlyBillList", actions, successCallback, errorCallback);
    }

     function formateCurrency(transData, key){
        return (transData.content[key].bill_amount)?transData.currency + ' ' + parseFloat(transData.content[key].bill_amount).toFixed(2):"";
    }


    showUserMonthlyBill = function (transData) {
        var transfound = false;
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];
        $("#orders").html("");
        for (var key in transData.content) {
            if (transData.content.hasOwnProperty(key)) {
                if (transData.content[key] != null) {
                    var monyr = key.split("_");
                    if (monyr == null || monyr.length < 2)
                        continue;
                    var outcont = $('<div class="row bill_items"></div>');
                    var monyr = $('<div class="col-sm-4" style="float:left;"><p>' + monthNames[monyr[0] - 1] + ' ' +  monyr[1] + '</p></div>');
                   // var outstVal = $('<div class="col-sm-4" style="float:left;"><p>' + transData.currency + ' ' + parseFloat(transData.content[key].bill_amount).toFixed(2) + '</p></div>');

                   var outstVal = $('<div class="col-sm-4" style="float:left;"><p>' + formateCurrency(transData, key) + '</p></div>');
                    if (transData.content[key].bill_file != null
                        && transData.content[key].bill_file != undefined
                        && transData.content[key].bill_file.trim() != "") {
                        var billpdf = transData.pdfBasePath + transData.content[key].bill_file.trim();
                        var pdfbut = $("<div class='col-sm-4 btn btn-alt bill_pdf_but' id='Accounts_Show' onclick='openPDF(\"" + billpdf + "\");' name=''> SHOW</div>");
                        outcont.append(monyr, outstVal, pdfbut);
                    } else {
                        outcont.append(monyr, outstVal);
                    }
                    $("#orders").append(outcont);
                    transfound = true;
                }
            }
        }

        if (localStorage.getItem("isLoggedIn") == "true")
            $("#paynowcontainer").css("display", "block");

        if (!transfound && (localStorage.getItem("isLoggedIn") == "true")) {
            var order_contents = $("<p style='text-align:center'> " + localizeString("AccountTransaction_Label_NoData") + "</p>");
            $("#orders").append(order_contents);
        }
    }

    openPDF = function (pdfUrl) {
        window.open("https://docs.google.com/viewer?url=" + pdfUrl, '_blank', 'location=no');
    }

    dismissQueryWindow = function () {
        $("#transrefid").val("");
        $("#trasrefdate").val("");
        $("#msgarea").val("");
        $("#transiderr").css("display", "none");
        $("#tdateerr").css("display", "none");
        $("#msgareaerr").css("display", "none");

    }

    submitQuery = function () {
        if (($("#transrefid").val().trim() == "") && ($("#trasrefdate").val() == "")) {
            $("#transiderr").css("display", "block");
            $("#transiderr").html(localizeString("ValidationError_TitleBlank")+'<i class="form-help-icon icon">error</i>');
            $("#tdateerr").css("display", "block");
            $("#tdateerr").html(localizeString("ValidationError_DateBlank")+'<i class="form-help-icon icon">error</i>');
            return;
        }
        if ($("#msgarea").val().trim() == "") {
            $("#msgareaerr").css("display", "block");
            $("#msgareaerr").html(localizeString("ValidationError_MessageBlank")+'<i class="form-help-icon icon">error</i>');
            return;
        }

        var actions = {
            "subject" : encodeURIComponent($("#transrefid").val() + "__" + $("#trasrefdate").val()) ,
            "comment" : $("#msgarea").val().trim()
            };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: localizeString("Alert_Text_Query"),
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    $("#transrefid").val('');
                    $("#trasrefdate").val('');
                    $("#msgarea").val('');
                    $("#transiderr").css("display", "none");
                    $("#tdateerr").css("display", "none");
                    $("#msgareaerr").css("display", "none");
                    $('#query-modal').modal('toggle');
                    $("#Account_Button_Queries")[0].click();
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/query/add", actions, successCallback, errorCallback);
    }


    getUserQueryList = function () {
        if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
            $("#queryDate").flatpickr({
                disableMobile: false,
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#queryDate").val = dateStr;
                }
            });
        } else {
            $("#queryDate").flatpickr({
                maxDate: "today",
                shorthandCurrentMonth: true,
                onChange: function(selectedDates, dateStr, instance) {
                    //...
                    $("#queryDate").val = dateStr;
                }
            });
        }
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (enableDebugMode)
                    alert("Records = " + JSON.stringify(data.data));
                showUserQueryList(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/myqueries", actions, successCallback, errorCallback);
    }


    showUserQueryList = function (queryList) {
        if (localStorage.getItem("isLoggedIn") == "true") {
            var order_contents = '<div class="btn btn-alt" style="float:right" data-toggle="modal" href="#query-modal1"><i class="fa fa-plus"></i></div></br></br>';
            var index;
            if (queryList.content.length > 0) {
                order_contents = order_contents + "<div id='transactionTable' class='box-body' style='overflow-x:scroll; width:100%;'>";
                order_contents = order_contents +   "<table class='table table-bordered box box-solid' style='margin-top:0px!important; margin-bottom:0px!important; word-wrap:break-word!important;' id='userquery-main' cellspacing='0'>";
                order_contents = order_contents +       "<tbody id='transactionTableBody'>";
                order_contents = order_contents +           "<tr style='color:#fff;'>";
                order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:25%; background-color:#3a3a3a;'>" + localizeString("Account_Label_Date") + "</th>";
                order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:45%; background-color:#3a3a3a;'>" + localizeString("Account_Label_Reference") + "#</th>";
                order_contents = order_contents +               "<th class='col-sm-4 trans_hdr' style='width:30%; background-color:#3a3a3a;'>" + localizeString("Account_Label_Response") + "</th>";
                order_contents = order_contents +           "</tr>";
                for (var i = 0; i < queryList.content.length; i++) {
                    var createdDate = queryList.content[i].createdOn.split(' ');
                    var dispDat = getDisplayableDate(createdDate[0]);
                    order_contents = order_contents + "<tr id='" + i + "'>";
                    order_contents = order_contents +      "<td class='col-sm-4 trans_det' style='width:25%; background-color:#eaeaea;'><span style='color:black; font-size:12px;'>" + dispDat + "</span></td>";
                    order_contents = order_contents +      "<td class='col-sm-4 trans_det' style='width:45%; background-color:#eaeaea;'><span style='color:black; word-wrap:break-word!important; word-break: break-all!important; font-size:12px;'>" + decodeURIComponent(queryList.content[i].subject) + "</span></td>";
                    order_contents = order_contents +      "<td class='col-sm-4 trans_det' style='width:30%; background-color:#eaeaea;'><div class='btn btn-alt' data-toggle='modal' href='#query-response-modal' onclick='getQueryResponse(" + queryList.content[i].id + ");'>" + localizeString("Alert_Button_ShowNow") + "</div></td>";
                    order_contents = order_contents + "</tr>";
                }
                order_contents = order_contents +       "</tbody>";
                order_contents = order_contents +   "</table>";
                order_contents = order_contents + "</div>";
            } else {
                order_contents = order_contents + '<p style="text-align:center"> ' + localizeString("AccountQuery_Label_NoData") + ' </p>';
            }
            $("#querylists").html(order_contents);

            // Update the badge count
            var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
            if (menuBadgeArray["2702"] > 0) {
                menuBadgeArray["accountscount"] = menuBadgeArray["accountscount"] - menuBadgeArray["2702"];
                menuBadgeArray["2702"] = 0;
                localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
                updateMenuBadgeStatus();
                updateNotificationState("", ["2702"], 4); // mark them as read
                var span = document.getElementById("querycount");
                if (span != null && span != undefined) {
                    txt = document.createTextNode(menuBadgeArray["2702"]);
                    span.innerText = txt.textContent;
                    $("#querycount").css("display", "none");
                }
            }
        }
    }


    getQueryResponse = function (queryId) {
        var actions = {
            "queryId" : queryId
            };

        localStorage.setItem("transactionQueryId", queryId);
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                if (enableDebugMode)
                    alert("Response = " + JSON.stringify(data.data));
                showUserQueryResponse(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/myqueries", actions, successCallback, errorCallback);
    }

    mMsgAreaClk = function () {
        $("#query-modal").animate({ scrollTop: $('#msgarea').offset().top - 10 }, 500);
    }

    queryMsgAreaClk = function () {
        $("#query-modal1").animate({ scrollTop: $('#querymsgarea').offset().top - 10 }, 500);
    }

    respMsgAreaClk = function () {
        $("#query-response-modal").animate({ scrollTop: $('#resp-msgarea').offset().top - 10 }, 500);
    }

    showUserQueryResponse = function (response) {
        $("#resp-transrefid").val(decodeURIComponent(response.content.subject));
        $("#resp-transrefid").disabled = "disabled";
        $("#resp-transrefid").prop("readonly", true);
        var createdOn = response.content.createdOn.split(' ');
        $("#resp-trasrefdate").val(createdOn[0]);
        $("#resp-trasrefdate").disabled = "disabled";
        $("#resp-trasrefdate").prop("readonly", true);
        var comments = response.content.comment.replace(/<br\s*\/?>/mg, "\n");
        comments = comments.replace(/<\/br\s*?>/mg, "");
        document.getElementById('queryresponse').innerHTML = "";
        document.getElementById('queryresponse').innerHTML = comments;
    }


    /**
     * Submit new user query. This can be without any existing transaction number;
     */
    submitNewUserQuery = function () {
        if (($("#queryTitle").val().trim() == "") && ($("#queryDate").val() == "")) {
            $("#titleerr").css("display", "block");
            $("#titleerr").html(localizeString("ValidationError_TitleBlank")+'<i class="form-help-icon icon">error</i>');
            $("#dateerr").css("display", "block");
            $("#dateerr").html(localizeString("ValidationError_DateBlank")+'<i class="form-help-icon icon">error</i>');
            return;
        }
        if ($("#querymsgarea").val().trim() == "") {
            $("#querymsgareaerr").css("display", "block");
            $("#querymsgareaerr").html(localizeString("ValidationError_MessageBlank")+'<i class="form-help-icon icon">error</i>');
            return;
        }

        var actions = {
            "subject" : encodeURIComponent($("#queryTitle").val() + '__' + $("#queryDate").val()) ,
            "comment" : $("#querymsgarea").val().trim()
            };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: localizeString("Alert_Text_Query"),
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    $("#queryTitle").val('');
                    $("#queryDate").val('');
                    $("#querymsgarea").val('');
                    $("#titleerr").css("display", "none");
                    $("#dateerr").css("display", "none");
                    $("#querymsgareaerr").css("display", "none");
                    $('#query-modal1').modal('toggle');
                    $("#Account_Button_Queries")[0].click();
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/query/add", actions, successCallback, errorCallback);
    }


    submitUpdatedQuery = function () {
        if ($("#resp-msgarea").val().trim() == "") {
            $("#resp-msgareaerr").css("display", "block");
            $("#resp-msgareaerr").html(localizeString("ValidationError_MessageBlank")+'<i class="form-help-icon icon">error</i>');
            return;
        }
        var actions = {
            "queryId" : localStorage.getItem("transactionQueryId"),
            "comment" : $("#resp-msgarea").val().trim()
            };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: localizeString("Alert_Text_Query"),
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    $("#resp-transrefid").val('');
                    $("#resp-trasrefdate").val('');
                    $("#resp-msgarea").val('');
                    $("#resp-msgareaerr").css("display", "none");
                    $('#query-response-modal').modal('toggle');
                    $("#queryresponse").html('');
                    $("#Account_Button_Queries")[0].click();
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/query/update", actions, successCallback, errorCallback);
    }


    dismissQueryResponseWindow = function () {
        $("#resp-transrefid").val("");
        $("#resp-trasrefdate").val("");
        $("#resp-msgarea").val("");
        $("#queryresponse").html('');
        $("#resp-msgareaerr").css("display", "none");
    }


    dismissNewQueryWindow = function () {
        $("#queryTitle").val("");
        $("#queryDate").val("");
        $("#querymsgarea").val("");
        $("#titleerr").css("display", "none");
        $("#dateerr").css("display", "none");
        $("#querymsgareaerr").css("display", "none");
    }


    paynow = function () {
        swal({
            title: localizeString("Alert_Title_Pay"),
            text: localizeString("Alert_Text_Pay"),
            type: "input",
            inputType: "number",
            showCancelButton: true,
            closeOnConfirm: false,
            animation: "slide-from-top",
            inputPlaceholder: localizeString("Alert_Placeholder_Pay"),
            confirmButtonText: localizeString("Alert_Button_Ok"),
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function (inputValue) {
            if (inputValue === false) return false;

            if (inputValue === "") {
                swal.showInputError(localizeString("Alert_Error_Pay"));
                return false;
            }
            localStorage.setItem("charge_amount", inputValue);
            var actions = {
                "amount": inputValue
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
            successCallback = function (data) {
                window.plugins.spinnerDialog.hide();
                if (data.status == "success") {
                    swal.close();
                    localStorage.setItem("trasaction_order_id", data.order_id);
                    /*if (/Android/i.test(navigator.userAgent)) {
                        getClubPaymentInfo();
                    } else if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {*/
                    slideview("payu-credit.html", 'script', 'left');
                    //}
                } else {
                    var thisone = this;
                    swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function () {
                        return;
                    });
                }
            };
            errorCallback = function (data) {

            };
            postMessageToServerWithAuth("/user/bill/pay", actions, successCallback, errorCallback);
        });
    }


    /**
     * Android Stripe Integration Function
     */
    getClubPaymentInfo = function () {
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_PaymentServerFetching"), localizeString("Please wait"));
        var actions = null;
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                var paymentInfo = data.data;
                if (data.data.content.length > 0) {
                    for (index = 0; index < data.data.content.length; index++) {
                        if (data.data.content[index]["keyName"] == "pg_public_key") {
                            localStorage.setItem("pg_public_key", data.data.content[index]["keyValue"]);
                        }
                        if (data.data.content[index]["keyName"] == "pg_currency") {
                            localStorage.setItem("pg_currency", data.data.content[index]["keyValue"]);
                        }
                        if (data.data.content[index]["keyName"] == "pg_user_id") {
                            localStorage.setItem("pg_user_id", data.data.content[index]["keyValue"]);
                        }
                    }
                    slideview("payu-credit-android.html", 'script', 'left');
                }
            }
        };

        errorCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            var thisone = this;
            swal({
                title: localizeString("Alert_Title_Sorry"),
                text: localizeString("Alert_Text_SomethingWithPayment"),
                type: "error",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function () {
                return;
            });
        };

        postMessageToServerWithAuth("/clubPaymentDetails", actions, successCallback, errorCallback);
    }
}());
