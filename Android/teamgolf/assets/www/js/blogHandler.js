(function() {
    var pageflag = 1,
        blankData = false;
    var datacount = 0;
    var page_number = 1;
    var comment_page_number = 1;
    var enableDebugMode = 0;
    "use strict";
    localStorage["numofBlogs"] = 0;

    /**
     *  Fetches the next page of blogs
     */
    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofBlogs")) / 15) == page_number) {
                page_number++;
                getBlogList(page_number);
            }
        }
    });

    getNextCommentList = function() {
        if (parseInt(localStorage.getItem('numOfBlogComments')) > (15 * comment_page_number)) {
            comment_page_number++;
            getComments(page_number);
        }
    }

    /**
     *  Fetches the list of blogs based on page_number
     *  @page_number current viewable page
     */
    getBlogList = function(page_number) {
        comment_page_number = 0;
        var actions = {
            "page_number": page_number
        };

        if (!blankData)
            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                datacount = data.data.length;
                if (data.data.length == 0) {
                    blankData = true;
                    if (pageflag == 1)
                        $('#content').html('<p style="text-align:center">' + localizeString("Blog_Label_NoData") + '</p>');
                } else {
                    localStorage.setItem('numofBlogs', String(parseInt(localStorage["numofBlogs"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayBlogList(data.data);
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            pageflag++;
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/blogs_new", actions, successCallback, errorCallback);
    }

    /**
     *  Displays fetched blog list
     *  @data data received from server with List of blogs
     */
    function displayBlogList(blogList) {
        var newdIdArray = [];
        if (enableDebugMode)
            alert("data = " + JSON.stringify(blogList) + ", length = " + blogList.content.length);

        for (i = 0; i < blogList.content.length; i++) {
            if (1 == blogList.content[i].status) {
                // newdIdArray[i] = blogList.content[i].id;
                // var div1 = $('<div class="col-lg-4 col-sm-12"></div>').attr('onClick', "gotoBlogsDetail('" + blogList.content[i].id + "', '" + blogList.content[i].blog_id + "', '" + blogList.content[i].commentsCount + "')");
                // var div2 = $('<div class="card-01"></div>');
                // var blgImg = "images/default-listing.png";
                // var aside1 = $('<aside class="card-side pull-left"></aside>'); //card-side-img
                // if (blogList.content[i].image !== null && blogList.content[i].image != undefined && blogList.content[i].image != "") {
                //     blgImg = blogList.imageBasePath + blogList.content[i].image;
                // }
                // var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + blgImg + '"></div>');
                // aside1.append(imgDiv);

                // var div4 = $('<div class="card-main card-width"></div>');
                // var div5 = $('<div class="card-inner-01 card-inner-width"></div>');
                // var p = $('<p class="card-heading card-heading-flow"></p>').text(blogList.content[i].title);

                // var div6 = $('<div class="blog-post-meta"></div>');
                // var span3 = $('<span></span>');

                // var lastUpdatedString = localizeString("Blogs_Label_LastUpdated");
                // var commentsString = localizeString("Blogs_Label_comments");
                // var dispDat = getDisplayableDate(blogList.content[i].last_updated.split(" ")[0]);
                // var dispTim = getDisplayableTime(blogList.content[i].last_updated.split(" ")[1]);

                // var ielem1 = $('<i class="fa fa-clock-o"> ' + lastUpdatedString + ' ' + [dispDat, dispTim].join(' ') + '</i></br>');

                // var ielem2 = $('<i class="fa fa-comments"> ' + blogList.content[i].commentsCount + commentsString + ' </i>');

                // span3.append(ielem1, ielem2);
                // div6.append("</br>", span3);
                // div5.append(p, div6); //p2
                // div4.append(div5);
                // div2.append(aside1, div4);
                // div1.append(div2);
                // $('.row').append(div1);
                $('#content').append(getListDom(blogList.content[i], blogList.imageBasePath, "gotoBlogsDetail"));
            }
        }
        lazyLoadListImages(page_number, blogList.content.length);
    }

    function getListDom(listData, iImgUrl, iClickCall) {
        var name = listData['title'],
            cDate = moment(listData['last_updated'], "YYYY-MM-DD"),
            id = listData['id'],
            logo = (listData['image']) ? (iImgUrl + listData['image']) : "./images/placeholder.png",
            commentsCount = listData['commentsCount'];

        cDate = (cDate.isValid()) ? cDate.format("lll") : "--";

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="' + iClickCall + '(\'' + id + '\')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + name + '</div>\
                        <div class="course-row-country clamp2">' + cDate + '</div>\
                        <div class="course-row-country clamp2">\
                        <i class="fa fa-comments"></i>\
                        ' + commentsCount + ' ' + localizeString("Label_Comments") + '</div>\
                    </div>\
                </div>';
    }


    /**
     *  Displays selected blog details. The same blog can be available in 5 different
     *  locales. Hence master id represent actual blog id while blog id represent
     *  the specific blog in required locale.
     *  @blogId id of the selected Blog
     *  @blogMasterId master id of the selected blogs.
     */
    gotoBlogsDetail = function(blogId, blogMasterId, commentCount) {
        localStorage.setItem('BlogId', blogId);
        localStorage.setItem('BlogMasterId', blogMasterId);
        localStorage.setItem('numOfBlogComments', commentCount);
        slideview("blog-details.html", 'script', 'left');
    }


    /************** Blog Listing Function Completed *********************/
    /**
     *  Add user comment to a specific blog
     *  @comment user provided comment on the blog
     */
    addComment = function(comment) {
        var blog_id = localStorage.BlogId;
        var blog_master_id = localStorage.BlogMasterId;

        $("#detailerror").css("display", "none");
        $("#detailerror").html('<i class="form-help-icon icon">error</i>');

        if (comment.trim() == "") {
            $("#detailerror").css("display", "block");
            $("#detailerror").html(localizeString("ValidationError_Comment") + '<i class="form-help-icon icon">error</i>');
            return false;
        }

        $("#BlogDetail_Button_Close").click();
        actions = {
            "blogId": blog_id,
            "blogMasterId": blog_master_id,
            "comment": comment
        };

        $('#float-textarea').val('');
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Send"), localizeString("SpinDialog_Wait"));

        successCallback = function(data) {
            if (data.status == "success") {
                //getComments();
                swal({
                    title: localizeString("Alert_Title_Success"),
                    type: "success",
                    text: data.message,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };

        errorCallback = function(data) {

        }
        postMessageToServerWithAuth("/blog/comment", actions, successCallback, errorCallback);
    }


    /**
     *  Fetches user comments on a specific blog
     */
    getComments = function() {
        var blog_id = localStorage.BlogId;
        var blog_master_id = localStorage.BlogMasterId;
        actions = {
            "blog_id": localStorage.BlogMasterId,
            "blog_master_id": blog_master_id,
            "page_number": comment_page_number
        };

        successCallback = function(data) {
            if (data.status == "success") {
                displayComments(data.data.comments, data.data.comments_count);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/blog/comments", actions, successCallback, errorCallback);
    }

    /**
     *  Fetches details of a specific blog
     */
    getBlogDetail = function() {
        var actions = {
            "blog_id": localStorage.BlogId
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                displayBlogDetails(data.data);
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/blogs_new", actions, successCallback, errorCallback);
    }


    /**
     *  Displays the contents of a Blog
     *  @blogDetails Blog data received from the Server
     */
    function displayBlogDetails(blogDetails) {
        if (enableDebugMode)
            alert("Blog Details = " + JSON.stringify(blogDetails));

        if (blogDetails.content[0].image !== null && blogDetails.content[0].image != undefined && blogDetails.content[0].image != "") {
            $('#topbanner').attr({
                'src': blogDetails.imageBasePath + blogDetails.content[0].image
            });
            $('#topbanner').css('display', 'block');
        }

        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;"></p>').text(blogDetails.content[0].title);
        var span1 = $('<span style=" font-size:12px;"></span>');
        var iElem2 = $('<i class="fa fa-clock-o"></i>');
        var time = "  " + getDisplayableDate(blogDetails.content[0].last_updated.split(" ")[0]);
        span1.append(iElem2, time);
        var p2 = $('<div class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + blogDetails.content[0].description + '</div>');
        $('#blogData').append(p1, span1, p2);

        if (localStorage.getItem("isLoggedIn") == "true") {
            if (1 == blogDetails.enable_comments) {
                $('#BlogDetail_Button_AddComment').css('display', 'block');
            }

            $('#comments_disp_sec').css('display', 'block');

            displayCommentsCount(blogDetails.comments_count);

            if (blogDetails.comments.length > 0) {
                displayComments(blogDetails.comments, blogDetails.comments_count);
            }
        }
    }


    /**
     *  Displays the user comments count for Blog
     *  @commentsCount new comments count
     */
    displayCommentsCount = function(commentsCount) {
        $('#commentsCount').html('');
        var h3node = $('<h3 style="margin-top:15px;"></h3>');
        var span9 = $('<span class="black-bold" style="font-size:20px; font-weight:500;"></span>').text(commentsCount + ' ' + localizeString("Label_Comments"));
        h3node.append(span9);
        $('#commentsCount').append(h3node);
    }


    /**
     *  Displays the user comments on a Blog
     *  @data User Comments data received from the Server
     */
    displayComments = function(comments, commentsCount) {
        if (enableDebugMode)
            alert("comment len = " + comments.length + ", comments Details = " + JSON.stringify(comments));

        displayCommentsCount(commentsCount);

        for (i = 0; i < comments.length; i++) {
            var div1 = $('<div class="row erow"></div>');
            var div2 = $('<div class="icon-01 col-xx-2">');
            if ((comments[i].photo != null) && (comments[i].photo != "") && (comments[i].photo != NaN) && (comments[i].photo != undefined)) {
                var img1 = $('<img alt="No unread posts" style="max-width:40px">').attr('src', "data:image/jpeg;base64," + comments[i].photo);
                div2.append(img1);
            } else {
                var img1 = $('<img alt="No unread posts" style="max-width:40px">').attr('src', 'images/avatar-001.jpg');
                div2.append(img1);
            }
            var div3 = $('<div class="col-xx-10 descx" title="No unread posts"></div>');
            var a1 = $('<a href="javascript:volid(0);" class="eforumtitle">' + comments[i].avatar + ': </a>'); //.append(comments[i].avatar);
            var p1 = $('<span style="margin-bottom:5px !important;"> &nbsp;&nbsp;' + comments[i].comment + '<span>'); //.attr('style', 'margin-bottom:5px !important;')
            var div4 = $('<div class="blog-post-meta" style="text-align:left;">');
            var iel = $('<i class="fa fa-calendar"></i>');
            var dispDat = getDisplayableDate(comments[i].createdOn.split(" ")[0]);
            var dispTim = getDisplayableTime(comments[i].createdOn.split(" ")[1]);
            div4.append(iel, "&nbsp;&nbsp;" + [dispDat, dispTim].join(' ')); //relative_time(date)
            div3.append(a1, p1, div4);
            div1.append(div2, div3);
            $('#commentsDetail').append(div1);
        }
    }

}());