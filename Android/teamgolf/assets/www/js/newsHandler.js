(function() {

    var enableDebugMode = 0;
    var page_number = 1;
    localStorage["numofNews"] = 0;

    "use strict";

    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofNews")) / 15) == page_number) {
                page_number++;
                getNews(page_number);
            }
        }
    });

    getNews = function(page_number) {
        var actions = {
            "page_number": page_number
        };

        if (1 == page_number)
            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();

            if (data.status == "success") {
                swal.close();
                if (!(data.data.content) || ((data.data.content.length == 0) &&
                        ((1 == page_number) || ('1' == page_number)))) {
                    $('#content').html('<p style="text-align:center">' + localizeString("News_Label_NoNews") + '</p>');
                } else {
                    if ((1 == page_number) || ('1' == page_number)) {
                        page_number = 1;
                        $('#content').html('');
                        localStorage.setItem('numofNews', 0);
                    }
                    localStorage.setItem('numofNews', String(parseInt(localStorage["numofNews"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayNewsList(data.data);
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/news_new", actions, successCallback, errorCallback);
    }


    /**
     * Displays the List of News Items
     * @news News data received from the Server
     */
    displayNewsList = function(news) {
        var newsIdArray = [];

        if (enableDebugMode)
            alert("Data = " + JSON.stringify(news) + ", length = " + news.content.length);

        for (i = 0; i < news.content.length; i++) {
            $('#content').append(getListDom(news.content[i], news.imageBasePath, "gotoNewsDetail"));
        }
        lazyLoadListImages(page_number, news.content.length);
        localStorage["NewsidArray"] = JSON.stringify(JSON.parse(localStorage["NewsidArray"]).concat(newsIdArray));
    }

    function getListDom(listData, iImgUrl, iClickCall) {
        var name = listData['title'],
            lastUpdate = moment(listData['last_updated'], "YYYY-MM-DD"),
            id = listData['id'],
            logo = (listData['image']) ? (iImgUrl + listData['image']) : "./images/placeholder.png";

        lastUpdate = (lastUpdate.isValid()) ? lastUpdate.format("lll") : "--";

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="' + iClickCall + '(\'' + id + '\')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + name + '</div>\
                        <div class="course-row-country clamp2 mT10">' + lastUpdate + '</div>\
                    </div>\
                </div>';
    }


    /**
     * Go To News Detail Page
     * @id news id for which details need to be displayed
     */
    gotoNewsDetail = function(id) {
        localStorage.setItem('NewsId', id);
        slideview("news-details.html", 'script', 'left');
    }


    /**
     * Get the News details for the choosen news id
     */
    getNewsDetails = function() {
        var newsId = localStorage.getItem('NewsId');
        var idArray = JSON.parse(localStorage["NewsidArray"]);
        for (i = 0; i < idArray.length; i++) {
            if (idArray[i] == newsId)
                pos = i;
        }
        var len = idArray.length;

        var actions = {
            "news_id": newsId
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayNewsDetails(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/news_new", actions, successCallback, errorCallback);
    }


    /**
     * Displays the Details of News Item
     * @news News data received from the Server
     */
    displayNewsDetails = function(news) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(news));
        $('#news1').html('');
        var div1 = $('<div class="card" style="width:100%;"></div>');
        var div2 = $('<div class="card-main" style="width:100%;"></div>');
        var div3 = $('<div class="card-img"></div>');
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
        ];

        var dispDat = getDisplayableDate(news.content[0].last_updated.split(" ")[0]);
        var nday = dispDat.split("-");
        var div4 = $('<div class="news-date"></div>').append(nday[0], $('</br>'), monthNames[parseInt(nday[1]) - 1]);
        if (news.content[0].image != null && news.content[0].image != "" && news.content[0].image != undefined) {
            var img1 = $('<img alt="No Image" style="width:100%!important;">').attr({
                'src': news.imageBasePath + news.content[0].image,
                'id': 'image1'
            });
            div3.append(div4, img1);
        } else {
            div3.append(div4);
        }
        var div52 = $('<div class="blog-post-meta" style="padding:3px; text-align:left;font-size: 18px!important;color:#000;font-weight:900;">' + news.content[0].title + '</div>');
        var div5 = $('<div class="blog-post-meta" style="padding:3px; text-align:left;"></div>');
        var span1 = $('<span></span>');
        var iElementl = $('<i class="fa fa-calendar"></i>');
        var str = "&nbsp; " + dispDat + " &nbsp;";
        span1.append(iElementl, str);
        div5.append(span1);
        var div6 = $('<div class="card-inner wysiwyg-img" style="word-wrap:break-word; word-break:break-word;"></div>');
        var temp = news.content[0].description.replace(/\&nbsp;/g, '');
        var contentp = $(temp);
        div6.append(contentp);
        div2.append(div3, div52, div5, div6);
        div1.append(div2);
        $('#news1').append(div1);
    }
}());