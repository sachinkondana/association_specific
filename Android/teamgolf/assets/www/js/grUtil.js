var associationId = localStorage.getItem("GREEN_GOLF_ASSOCIATIONID");

var grUtil = (function() {
    var GREEN_GOLF_TOKEN = localStorage.getItem("GREEN_GOLF_TOKEN"); //"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjIsImlzcyI6Imh0dHA6Ly9sb2NhbGhvc3QvZmFuTHlmZS9wdWJsaWMvaW5kZXgucGhwL2FwaS9hdXRoL2xvZ2luIiwiaWF0IjoxNTA0MjUyOTU4LCJleHAiOjE1MDQyNTY1NTgsIm5iZiI6MTUwNDI1Mjk1OCwianRpIjoiYjFGS1dHYVo5TjVhaW10bSJ9.WCnMikBnsh0IUapB0x33NZcUEM01DG5tV11IoIKct0Q",
    GREEN_GOLF_URL = localStorage.getItem("GREEN_GOLF_URL"); //"https://devres.greengolf.biz";

    function postMessageToServerWithAuth(uri, postParam, successCallback, errorCallback) {
        var actions = {};

        var postParams;
        if (postParam != null)
            postParams = $.extend(actions, postParam);
        else
            postParams = actions;

        if (navigator.onLine) { // Make Ajax call only when device is online
            $.ajax({
                url: GREEN_GOLF_URL + uri,
                type: "POST",
                data: postParams,
                tryCount: 0,
                retryLimit: 3,
                async: true,
                timeout: 15000,
                dataType: "json",
                headers: {
                    // "Content-Type": "application/x-www-form-urlencoded",
                    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                    "token": GREEN_GOLF_TOKEN
                },
                success: function(data) {
                    successCallback(data);
                },
                error: function(jqXHR, exception) {
                    window.plugins.spinnerDialog.hide();
                    if (exception == 'timeout' || jqXHR.statusText == 'timeout') {
                        swal({
                                title: localizeString("Alert_Title_TimeOut"),
                                text: localizeString("Alert_Text_TimeOut"),
                                type: "info",
                                allowOutsideClick: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                errorCallback(jqXHR);
                                return;
                            });
                    }
                    errorCallback(jqXHR);
                    if (jqXHR.status === 0) {
                        var thisone = this;
                        swal({
                                title: localizeString("Alert_Title_Connectivity"),
                                text: localizeString("Alert_Text_Connectivity"),
                                type: "info",
                                showCancelButton: true,
                                closeOnCancel: true,
                                allowOutsideClick: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_TryAgain"),
                                cancelButtonText: localizeString("Alert_Button_Cancel")
                            },
                            function() {
                                $.ajax(thisone);
                                return;
                            });
                    } else if (jqXHR.status === 401) {
                        fetchUpdatedPublicMenu();
                        resignin();
                    } else if (jqXHR.status === 426) {
                        swal({
                                title: localizeString("Alert_Title_Unsupported"),
                                text: jqXHR.responseText,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                allowOutsideClick: false,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    } else {
                        var thisone = this;
                        swal({
                                title: localizeString("Alert_Title_Something"),
                                text: localizeString("Alert_Text_Something"),
                                type: "info",
                                showCancelButton: true,
                                closeOnCancel: true,
                                allowOutsideClick: false,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_TryAgain"),
                                cancelButtonText: localizeString("Alert_Button_Cancel")
                            },
                            function() {
                                $.ajax(thisone);
                                return;
                            });
                    }
                }
            });
        } else {
            window.plugins.spinnerDialog.hide();
            console.log("Device offline");
        }
    }

    function showSwalWarning(iMessage) {
        //window.plugins.spinnerDialog.hide();
        swal({
                title: localizeString("Alert_Title_Sorry"),
                text: iMessage,
                type: "warning",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function() {
                return;
            });
    }

    function populateBookingTime(id) {
        var d = new Date();
        var interval = 10;
        d.setHours(5);
        d.setMinutes(0);
        for (var i = 0; i < 103; i++) {
            option = document.createElement('option');
            option.setAttribute('value', i + 1);
            option.appendChild(document.createTextNode(("0" + d.getHours()).slice(-2) + ':' + ("0" + d.getMinutes()).slice(-2)));
            $("#" + id).append(option);
            d.setMinutes(d.getMinutes() + interval);
        }
    }


    function initDatePicker(bkngType) {
        $("#date-from").change(function() {
            $("#dferror").css("display", "none");
            $("#dferror").html('<i class="form-help-icon icon">error</i>');
        });

        $("#date-to").change(function() {
            $("#dterror").css("display", "none");
            $("#dterror").html('<i class="form-help-icon icon">error</i>');
        });

        $("#time-from").change(function() {
            $("#tferror").css("display", "none");
            $("#tferror").html('<i class="form-help-icon icon">error</i>');
        });

        $("#time-to").change(function() {
            $("#tterror").css("display", "none");
            $("#tterror").html('<i class="form-help-icon icon">error</i>');
        });

        if (1 == bkngType) {
            $("#date-picker-form").css("display", "inline");
        } else if (2 == bkngType) {
            $("#date-picker-form").css("display", "inline");
            populateBookingTime("time-from");
            $("#time-picker-form").css("display", "inline");
        } else if (3 == bkngType) {
            $("#date-picker-form").css("display", "inline");
            $("#date-picker-to").css("display", "inline");
        } else if (4 == bkngType) {
            $("#date-picker-form").css("display", "inline");
            populateBookingTime("time-from");
            $("#time-picker-form").css("display", "inline");
            $("#date-picker-to").css("display", "inline");
        } else if (5 == bkngType) {
            $("#date-picker-form").css("display", "inline");
            populateBookingTime("time-from");
            $("#time-picker-form").css("display", "inline");
            $("#date-picker-to").css("display", "inline");
            populateBookingTime("time-to");
            $("#time-picker-to").css("display", "inline");
        }

        if (0 < bkngType) {
            $("#usrmsg-area-form").css("display", "inline");
            $("#form-req-button").css("display", "inline");
        }

        /* Yesterdays date for the datepicker */
        //var yesterday = new Date(Date.now() - 864e5);
        var day = new Date();
        var myDate = day.getFullYear() + "-" + (1 + day.getMonth()) + "-" + day.getDate();
        if (0 < bkngType) {
            if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                $("#date-from").flatpickr({
                    disableMobile: false,
                    minDate: myDate,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        console.log("dateStr = " + dateStr);
                        $("#date-from").val = dateStr;
                        //yesterday = new Date(new Date(dateStr).getTime() - 864e5);
                        yesterday = new Date(dateStr);
                        console.log("updated yesterday = " + yesterday);
                    }
                });
            } else {
                $("#date-from").flatpickr({
                    minDate: myDate,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        console.log("dateStr = " + dateStr);
                        $("#date-from").val = dateStr;
                        //yesterday = new Date(new Date(dateStr).getTime() - 864e5);
                        yesterday = new Date(dateStr);
                        console.log("updated yesterday = " + yesterday);
                    }
                });
            }
        }

        if (3 <= bkngType) {
            if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                $("#date-to").flatpickr({
                    disableMobile: false,
                    minDate: myDate,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        $("#date-to").val = dateStr;
                    }
                });
            } else {
                $("#date-to").flatpickr({
                    minDate: myDate,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        $("#date-to").val = dateStr;
                    }
                });
            }
        }
    }


    function clearOfflineForm() {
        $('#time-from').prop('selectedIndex', 0);
        $('#time-to').prop('selectedIndex', 0);

        $("#date-from").flatpickr().clear();
        $("#date-to").flatpickr().clear();
        $("#msgarea").val(null);

        $("#time-picker-form").removeClass("control-highlight");
        $("#time-picker-to").removeClass("control-highlight");
        $("#usrmsg-area-to").removeClass("control-highlight");
    }

    function offlineBooking(members_id, iBType, iUrl, iClearForm, booking_id) {
        var retVal = 1;
        $("#dferror").css("display", "none");
        $("#dferror").html('<i class="form-help-icon icon">error</i>');
        $("#msgerror").css("display", "none");
        $("#msgerror").html('<i class="form-help-icon icon">error</i>');
        $("#dterror").css("display", "none");
        $("#dterror").html('<i class="form-help-icon icon">error</i>');
        $("#tferror").css("display", "none");
        $("#tferror").html('<i class="form-help-icon icon">error</i>');
        $("#tterror").css("display", "none");
        $("#tterror").html('<i class="form-help-icon icon">error</i>');

        if ($("#date-picker-form").css('display') != 'none' && $("#date-picker-form").css('display') != undefined) {
            if ($("#date-from").val().trim() == "") {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_StartDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#date-picker-to").css('display') != 'none' && $("#date-picker-to").css('display') != undefined) {
            if ($("#date-to").val().trim() == "") {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_EndDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($('#time-picker-to').css('display') != 'none' && $("#time-picker-to").css('display') != undefined) {
            if ($("#time-to option:selected").text().trim() == "") {
                $("#tterror").css("display", "inline");
                $("#tterror").html(localizeString("ValidationError_ValidEndTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#time-picker-form").css('display') != 'none' && $("#time-picker-form").css('display') != undefined) {
            if ($("#time-from option:selected").text().trim() == "") {
                $("#tferror").css("display", "inline");
                $("#tferror").html(localizeString("ValidationError_ValidStartTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if (0 == retVal)
            return false;

        var request_start_date = $("#date-from").val();
        var request_end_date = $("#date-to").val();
        var request_start_time = $("#time-from option:selected").text();
        var request_end_time = $("#time-to option:selected").text();
        var today = new Date();

        resultStartDate = validateStartDate(request_start_date, getFormatedDate(today));
        resultEndDate = validateEndDate(request_end_date, request_start_date);

        var facilityBookingType = localStorage.getItem("FacilityBookingType");

        if ((0 <= resultStartDate) && (0 <= resultEndDate)) { // Valid date
            if (0 == resultStartDate) { // start dates are same so compare time
                var startTimeResult = validateStartTime(getFormatedTime(today), request_start_time);
                if (!startTimeResult) { // inValid time
                    $("#tferror").css("display", "inline");
                    $("#tferror").html(localizeString("ValidationError_StartTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            }
            if (0 == resultEndDate) { // end dates are same so compare time
                var endTimeResult = validateEndTime(request_end_time, getFormatedTime(today));
                if (!endTimeResult) { // inValid time
                    $("#tterror").css("display", "inline");
                    $("#tterror").html(localizeString("ValidationError_EndTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            }
            if ($("#msgarea").val().trim() == "") {
                $("#msgerror").css("display", "inline");
                $("#msgerror").html(localizeString("ValidationError_Message") + '<i class="form-help-icon icon">error</i>');
                $("#msgarea").focus();
                return false;
            }
            var actions = {
                "booking_type": iBType,
                "master_id": members_id,
                "start_date": request_start_date,
                "start_time": request_start_time,
                "end_date": request_end_date,
                "end_time": request_end_time,
                "user_comment": $("#msgarea").val(),
                "booking_id": booking_id //quick fix for update
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_SendRequest"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                if (data.status == "success") {
                    swal({
                            title: localizeString("Alert_Title_Done"),
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            //slideview('facilities-booking-list.html', 'script', 'left');
                            if (iClearForm) { clearOfflineForm(); }
                            return;
                        });
                } else {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
                window.plugins.spinnerDialog.hide();
            };
            errorCallback = function(data) {

            };

            window.postMessageToServerWithAuth(iUrl, actions, successCallback, errorCallback);
        } else {
            if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
            }
            if (0 > resultEndDate) {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_ValidEndDate") + '<i class="form-help-icon icon">error</i>');
            }
            return false;
        }
    }

    function alertMsg(iMsg) {
        swal({
                title: "",
                text: iMsg,
                type: "error",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function() {
                return;
            });
    }

    function warnMsg(iMsg) {
        swal({
                title: "",
                text: iMsg,
                type: "warning",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function() {
                return;
            });
    }

    function successMsg(iMsg) {
        swal(localizeString("Alert_Title_Success"), iMsg, "success");
    }

    function fetchGOlfCoursePrice(golfCourseId, callBack) {

        var actions = {
            "association_id": associationId,
            "golf_course_id": golfCourseId
        };

        successCallback = function(data) {
            handleLoader(0);
            if (data.result === 200) {
                callBack(data.data[0]);
            } else {
                grUtil.showSwalWarning(data.message);
            }
        }
        errorCallback = function(data) {};

        handleLoader(1);
        if (golfCourseId) {
            grUtil.postMessageToServerWithAuth("api_get_golfcourse_info", actions, successCallback, errorCallback);
        } else {
            grUtil.showSwalWarning(localizeString("Alert_Title_Something"));
        }

    }

    function getGolfCourseList(iPage, callBack) {
        var actions = {
            page: iPage
        };

        successCallback = function(data) {
            handleLoader(0);
            if (data.status === "success") {
                // callBack([], 0, data.imageBasePath);
                callBack(data.data, data.total_rows, data.imageBasePath);
            } else {
                grUtil.showSwalWarning(data.message);
            }
        }
        errorCallback = function(data) {};

        handleLoader(1);
        window.postMessageToServerWithAuth("/golfCourseListing", actions, successCallback, errorCallback);
    }

    var special = ['zeroth', 'first', 'second', 'third', 'fourth', 'fifth', 'sixth', 'seventh', 'eighth', 'ninth', 'tenth', 'eleventh', 'twelfth', 'thirteenth', 'fourteenth', 'fifteenth', 'sixteenth', 'seventeenth', 'eighteenth', 'nineteenth'];
    var deca = ['twent', 'thirt', 'fort', 'fift', 'sixt', 'sevent', 'eight', 'ninet'];

    function getNumInWords(n) {
        if (n < 20) return special[n];
        if (n % 10 === 0) return deca[Math.floor(n / 10) - 2] + 'ieth';
        return deca[Math.floor(n / 10) - 2] + 'y-' + special[n % 10];
    }

    function handleLoader(iShow) {
        try {
            if (iShow) {
                window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"));
            } else {
                window.plugins.spinnerDialog.hide();
            }
        } catch (e) {}
    }


    function paynow(iAmount, iMasterId, iBookingId, iCurrency, iBuggy, iCaddy, iDateTime, iGuestCount) {
        var actions = {
            "amount": iAmount,
            "golf_facility_master_id": iMasterId,
            "booking_id": iBookingId,
            "currency": iCurrency,
            "facility_booking_time": iDateTime,
            "guests": iGuestCount,
            "buggy": iBuggy,
            "caddy": iCaddy
        };

        successCallback = function(data) {
            handleLoader(0);

            if (data.status == "success") {
                localStorage.setItem("trasaction_order_id", data.order_id);

                slideview("payu-credit.html", 'script', 'left');
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {
            handleLoader(0);
        };

        handleLoader(1);
        window.postMessageToServerWithAuth("/golfcourse/bill/pay", actions, successCallback, errorCallback);
    }

    function cancelBookingOrder(iOrederId, callback) {
        var actions = {
            "booking_id": iOrederId
        };

        successCallback = function(data) {
            handleLoader(0);

            if (data.status == "success") {
                callback();
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {
            handleLoader(0);
        };

        handleLoader(1);
        window.postMessageToServerWithAuth("/cancelGolfCourseBooking", actions, successCallback, errorCallback);
    }

    function getBookingStatus(iChoice) {
        var rTxt = null;
        switch (parseInt(iChoice)) {
            case 0:
                rTxt = localizeString("STATUS_Pending");
                break;
            case 1:
                rTxt = localizeString("STATUS_Approved");
                break;
            case 2:
                rTxt = localizeString("STATUS_Rejected");
                break;
            default:
                rTxt = localizeString("STATUS_Cancelled");
                break;
        }
        return '<span class="statusBtn offlineStatus' + iChoice + '">' + rTxt + '</span>';
    }

    function showAskLoginDialog() {
        swal({
                title: localizeString("Alert_Title_Sorry"),
                text: localizeString("Tournament_Need_To_Login"),
                type: "warning",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function() {
                slideview("index.html", 'script', 'left');
            });
    }

    function getStatusBar(iStatus) {
        return '<span class="statusBtn bkStatus' + iStatus + '">' + (parseInt(iStatus) ? "Success" : "Failed") + '</span>';
    }

    function ModifyCopyRight() {
        $("#copyright_div img").attr({ "src": "images/GreenGolf.png" });
    }

    //public members
    return {
        postMessageToServerWithAuth: postMessageToServerWithAuth,
        showSwalWarning: showSwalWarning,
        initDatePicker: initDatePicker,
        alertMsg: alertMsg,
        warnMsg: warnMsg,
        successMsg: successMsg,
        fetchGOlfCoursePrice: fetchGOlfCoursePrice,
        getNumInWords: getNumInWords,
        getGolfCourseList: getGolfCourseList,
        offlineBooking: offlineBooking,
        handleLoader: handleLoader,
        paynow: paynow,
        getBookingStatus: getBookingStatus,
        cancelBookingOrder: cancelBookingOrder,
        showAskLoginDialog: showAskLoginDialog,
        getStatusBar: getStatusBar,
        ModifyCopyRight: ModifyCopyRight
    }
})();