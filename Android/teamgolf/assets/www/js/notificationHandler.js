(function() {
    var enableDebugMode = 0;
    var page_number = 1;
    "use strict";

    localStorage.setItem("from_page", "notifications");

    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            remove = false;
            scroll = true;
            if ((parseInt(localStorage.getItem("numofNoti")) / 15) == page_number) {
                page_number++;
                getPushNotifications(page_number);
            }
        }
    });

    loadNotifications = function() {
        page_number = 1;
        localStorage["numofNoti"] = 0;
        getPushNotifications(1);
        return;
    }

    gotoNotificationAction = function(id, type, payload) {
        var additionalData = JSON.parse(payload);
        var nxtScreen = "";
        switch (parseInt(type)) { //data.notificationType
            case 1100:
                break;
            case 1101:
                // Blog-listing
                nxtScreen = "blogs.html";
                break;
            case 1102:
                // Blog-details
                if (additionalData != null) {
                    nxtScreen = "blog-details.html";
                    localStorage.setItem("BlogId", additionalData.id);
                } else {
                    nxtScreen = "blogs.html";
                }
                break;
            case 1200: //Media
            case 1201:
            case 1202:
                nxtScreen = "media.html";
                break;
            case 1300:
                break;
            case 1301:
                // News-listing
                nxtScreen = "news.html";
                break;
            case 1302:
                // News-details
                if (additionalData != null) {
                    nxtScreen = "news-details.html";
                    localStorage.setItem("NewsId", additionalData.id);
                } else {
                    nxtScreen = "news.html";
                }
                break;
            case 1400: //Notification
                nxtScreen = "notification.html";
                break;
            case 1500:
            case 1501: //invite
            case 1502: //acepted/rejected
                localStorage.setItem("from_page", "notification-invite");
                nxtScreen = "connect-people.html";
                break;
            case 1503: //new message
                //Chat
                localStorage.setItem("from_page", "notification-message");
                nxtScreen = "connect-people.html";
                break;
            case 1700:
                break;
            case 1701:
                // Classified-listing
                nxtScreen = "classified-list.html";
                break;
            case 1702:
                // Classified-details
                if (additionalData != null) {
                    nxtScreen = "classified-details.html";
                    localStorage.setItem("classifiedId", additionalData.id); // All Classified
                } else {
                    nxtScreen = "classified-list.html";
                }
                break;
            case 1703:
                // My-Classified-listing
                nxtScreen = "classified-my.html";
                break;
            case 1704:
                // My-Classified-details
                if (additionalData != null) {
                    nxtScreen = "classified-my-details.html";
                    localStorage.setItem("classifiedId", additionalData.id); // My Classified
                } else {
                    nxtScreen = "classified-my.html";
                }
                break;
            case 1800:
                break;
            case 1801:
                // Event-listing
                nxtScreen = "events.html";
                break;
            case 1802:
                // Event-details
                if (additionalData != null) {
                    nxtScreen = "event-details.html";
                    localStorage.setItem("event_id", additionalData.id);
                } else {
                    nxtScreen = "events.html";
                }
                break;
            case 1803:
                // Event-booking-listing
                nxtScreen = "events-booking-list.html";
                break;
            case 1804:
                // Event-booking-details
                if (additionalData != null) {
                    nxtScreen = "events-booking-details.html";
                    localStorage.setItem("event_booking_id", additionalData.id);
                } else {
                    nxtScreen = "events-booking-list.html";
                }
                break;
            case 1900:
                break;
            case 1910:
                // Fitness-listing
                nxtScreen = "facility-fitness.html";
                break;
            case 1911:
                // Fitness-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-fitness.html";
                }
                break;
            case 1920:
                // Sports-listing
                nxtScreen = "facility-sports.html";
                break;
            case 1921:
                // Sports-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-sports.html";
                }
                break;
            case 1930:
                // H&S-listing
                nxtScreen = "facility-health-spa.html";
                break;
            case 1931:
                // H&S-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-health-spa.html";
                }
                break;
            case 1940:
                // Recreation-listing
                nxtScreen = "facility-recreational.html";
                break;
            case 1941:
                // Recreation-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-recreational.html";
                }
                break;
            case 1950:
                // Saloon-listing
                nxtScreen = "facility-saloon.html";
                break;
            case 1951:
                // Saloon-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-saloon.html";
                }
                break;
            case 1960:
                // Other-listing
                nxtScreen = "facility-other.html";
                break;
            case 1961:
                // Other-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-other.html";
                }
                break;
            case 1970:
                // Banquet-listing
                nxtScreen = "facility-banquate-conference.html";
                break;
            case 1971:
                // Banquet-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-banquate-conference.html";
                }
                break;
            case 1901:
                // Facility-booking-listing
                nxtScreen = "facilities-booking-list.html";
                break;
            case 1902:
                // Facility-booking-details
                if (additionalData != null) {
                    nxtScreen = "facilities-booking-details.html";
                    localStorage.setItem("facility_booking_id", additionalData.id);
                } else {
                    nxtScreen = "facilities-booking-list.html";
                }
                break;
            case 2000:
            case 2001:
                // Offer-listing
                nxtScreen = "offers.html";
                break;
            case 2002:
                // Take to the URL
                if (additionalData != null) {
                    localStorage.setItem("offerURL", additionalData.offerurl);
                }
                nxtScreen = "offers.html";
                break;
            case 2100:
                break;
            case 2101: //Golf Course
                nxtScreen = "golf-course.html";
                break;
            case 2102: // Handicap
                nxtScreen = "handicap-display.html";
                break;
            case 2103:
                // Golf-tournament-listing
                nxtScreen = "golf-tournaments.html";
                break;
            case 2104:
                // Golf-tournament-details
                if (additionalData != null) {
                    nxtScreen = "golf-tournament-details.html";
                    localStorage.setItem("golftournamentid", additionalData.id);
                } else {
                    nxtScreen = "golf-tournaments.html";
                }
                break;
            case 2105:
                // My-Golf-tournament-details
                if (additionalData != null) {
                    nxtScreen = "golf-tournament-my-history-details.html";
                    localStorage.setItem("mygolftournamentid", additionalData.id);
                    localStorage.setItem("mygolfeventid", additionalData.eventId);
                } else {
                    nxtScreen = "golf-tournaments-my-history.html";
                }
                break;
            case 2106:
                // Golf-tournament-result-details
                if (additionalData != null) {
                    nxtScreen = "golf-tournament-result-details.html";
                    localStorage.setItem("golftournamentid", additionalData.id);
                } else {
                    nxtScreen = "golf-tournaments-results.html";
                }
                break;
            case 2107:
                break;
            case 2108:
                if (additionalData != null) {
                    // This is not straight forward. We will have to relook
                    localStorage.setItem("score_card_id", additionalData.id);
                }
                break;
            case 2109:
                break;
            case 2110: // golf coach
                nxtScreen = "golf-coaches.html";
                break;
            case 2111: // tee time
                break;
            case 2200:
                break;
            case 2201:
                // Tournament-listing
                nxtScreen = "tournaments.html";
                break;
            case 2202:
                // Tournament-details
                if (additionalData != null) {
                    nxtScreen = "tournament-details.html";
                    localStorage.setItem("tournament_id", additionalData.id);
                } //else {
                nxtScreen = "tournaments.html";
                //}
                break;
            case 2203:
                // My-Tournament-details
                if (additionalData != null) {
                    nxtScreen = "tournament-my-history-update.html";
                    localStorage.setItem("booking_id", additionalData.id);
                    localStorage.setItem("booking_event_id", additionalData.eventId);
                } //else {
                nxtScreen = "tournaments-my-history.html";
                //}
                break;
            case 2204:
                // Tournament-result-details
                if (additionalData != null) {
                    nxtScreen = "tournament-results-details.html";
                    localStorage.setItem("tournament_id", additionalData.id);
                } else {
                    nxtScreen = "tournaments-results.html";
                }
                break;
            case 2300:
                break;
            case 2301: //Live score URL available
                localStorage.setItem("scorecardUrl", additionalData);
                break;
            case 2302: //Live score details available
                if (additionalData != null) {
                    nxtScreen = "live-socrecard-details.html";
                    localStorage.setItem("facilitieScoreCardId", additionalData.id);
                } else {
                    nxtScreen = "live-scorecard-list.html";
                }
                break;
            case 2400:
                break;
            case 2401:
                // Poll-listing
                nxtScreen = "polls.html";
                break;
            case 2402:
                // Ploo-details
                if (additionalData != null) {
                    nxtScreen = "poll-details.html";
                    localStorage.setItem("PollId", additionalData.id);
                } else {
                    nxtScreen = "polls.html";
                }
                break;
            case 2500:
                break;
            case 2501:
                // Survey-listing
                nxtScreen = "surveys.html";
                break;
            case 2502:
                // Survey-details
                if (additionalData != null) {
                    nxtScreen = "survey-details.html";
                    localStorage.setItem("SurveyId", additionalData.id);
                } else {
                    nxtScreen = "surveys.html";
                }
                break;
            case 2600:
                break;
            case 2601:
                // Sponsors-listing
                nxtScreen = "sponsors.html";
                break;
            case 2602:
                // Sponsors-details
                if (additionalData != null) {
                    nxtScreen = "sponsor-details.html";
                    localStorage.setItem("sponsorId", additionalData.id);
                } else {
                    nxtScreen = "sponsors.html";
                }
                break;
            case 2700:
                break;
            case 2701: // New Transaction
                localStorage.setItem("from_page", "notification-transaction");
                nxtScreen = "my-account.html";
                break;
            case 2702: // Query Response
                localStorage.setItem("from_page", "notification-query");
                nxtScreen = "my-account.html";
                break;
            case 2703: // New Bill
                localStorage.setItem("from_page", "notification-bill");
                nxtScreen = "my-account.html";
                break;
            case 2800:
            case 2801: // Updated Affiliation
                nxtScreen = "affiliation-clubs.html";
                break;
            case 2802: // Updated Club Rules
                nxtScreen = "club-rules.html";
                break;
            case 2803: // Updated Dress Code
                nxtScreen = "dress-code.html";
                break;
            case 2804: // Updated Membership Options
                nxtScreen = "membership-options.html";
                break;
            case 2805: // Updated Membership Criteria
                nxtScreen = "membership-criteria.html";
                break;
            case 2806: // Updated Gallery
                nxtScreen = "gallery.html";
                break;
            case 2807: // Updated About
                nxtScreen = "about-us.html";
                break;
            case 2808: // Updated History
                nxtScreen = "history.html";
                break;
            case 2809: // Updated President Message
                nxtScreen = "message-from-top.html";
                break;
            case 2810: // Updated Contact us
                nxtScreen = "contact.html";
                break;
            case 2900:
            case 2901: // Updated Administration
                nxtScreen = "management.html";
                break;
            case 2902: // Updated Present Committee
                nxtScreen = "present-committee.html";
                break;
            case 2903: // Updated Past Committee
                nxtScreen = "past-committee.html";
                break;
            case 2904: // Updated Club Staff
                nxtScreen = "club-staff.html";
                break;
            case 3000:
            case 3001: // Updated Restaurant List
                nxtScreen = "food-beverage-restaurants.html";
                break;
            case 3002: // Updated Menu
                nxtScreen = "food-beverages-menu.html";
                break;
            case 3100:
                break;
            case 3101: // Room listing
                // Room-listing
                nxtScreen = "facility-room.html";
                break;
            case 3102: // Room booking
                // Room-details
                if (additionalData != null) {
                    nxtScreen = "facility-details.html";
                    localStorage.setItem("facilities_details_id", additionalData.id);
                } else {
                    nxtScreen = "facility-room.html";
                }
                break;
            default:
                console.log("Type Id = " + nxtScreen);
                console.log("Additional Data = " + additionalData);
                break;
        }

        if (additionalData == null || additionalData == undefined || additionalData == "") {
            console.log("Additional data is null/undefined/blank");
            localStorage.removeItem("Notification_Additional_Data");
        } else {
            localStorage.setItem("Notification_Additional_Data", additionalData);
        }

        if (nxtScreen != "")
            slideview(nxtScreen, "script", "right");

        return;
    }


    /**
     * Fetch the list of push notifications delivered for the user
     */
    getPushNotifications = function(pg_no) {
        var actions = {
            "page_number": pg_no
        };;

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                if ((data.data.content.length < 1) && (pg_no == 1)) {
                    localStorage.setItem('numofNoti', 0);
                    $('.row').html('<p style="text-align:center">' + localizeString("Notifications_Label_NoData") + '</p>');
                } else {
                    localStorage.setItem('numofNoti', String(parseInt(localStorage["numofNoti"]) + parseInt(JSON.stringify(data.data.content.length))));
                    if (pg_no == 1)
                        $('.row').html("");
                    displayNotificationList(data.data);
                }
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofNoti', 0);
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/user/notification", actions, successCallback, errorCallback);
    }


    displayNotificationList = function(notificationList) {
        if (enableDebugMode)
            alert("Notification List = " + JSON.stringify(notificationList));
        for (var i = 0; i < notificationList.content.length; i++) {
            var coldiv = $('<div class="col-lg-6 col-sm-12"></div>');
            var outerlink = $('<a href="javascript:void();" style="width:90%!important;">').attr('onClick', "gotoNotificationAction('" + notificationList.content[i].id + "', '" + notificationList.content[i].content.nextActivity + "', '" + JSON.stringify(notificationList.content[i].content.additionalData) + "')");
            var singlethreaddiv = $('<div class="single-thread"></div>');
            var postheaderdiv = $('<div class="post-header"></div>');
            if (notificationList.content[i].status >= 3) {
                singlethreaddiv = $('<div class="single-thread read"></div>');
                postheaderdiv = $('<div class="post-header read"></div>');
            }

            var justdiv = $('<div></div>');
            var topiclink = $('<a class="topic-icon-placeholder link" href="javascript:void();"></a>');
            var avatar = null;
            if (notificationList.content[i].image != null && notificationList.content[i].image != '' &&
                notificationList.content[i].image != undefined) {
                imagePath = notificationList.imageBasePath + notificationList.content[i].image;
                avatar = $('<img width="18" height="20" src="data:image/jpeg;base64,' + imagePath + '" alt=""/>');
            } else {
                avatar = $('<img src="images/logox.png" width="18" height="20"alt=""/>');
            }
            topiclink.append(avatar);
            justdiv.append(topiclink);

            var titleText = "Push Notification";
            if (notificationList.content[i].title != null && notificationList.content[i].title != undefined &&
                notificationList.content[i].title != "" && notificationList.content[i].title.trim() != "Dummy") {
                titleText = notificationList.content[i].title;
            }

            var headerscoldiv = $('<div class="col-xx-10 headers-col" style="width:70%!important"></div>');
            var headertextp = $('<p class="header-text-content"></p>').text(titleText);
            if (notificationList.content[i].status >= 3) {
                headertextp = $('<p class="header-text-content" style="color:#78909C!important;"></p>').text(titleText);
            }
            var deletediv = $('<div style="float:right!important; width:10%!important; padding-left:5px!important; color:#000000" onclick="deleteNotification(' + notificationList.content[i].id + ');"><span><i class="fa fa-trash fa-lg" aria-hidden="true"></i></span></div>');

            var taglinep = $('<p class="tagline-text-content"> <i class="fa fa-calendar"></i> Unknown </p>');
            if (notificationList.content[i].delivered_on != null && notificationList.content[i].delivered_on != undefined) {
                var dispDat = getDisplayableDate(notificationList.content[i].delivered_on.split(' ')[0]);
                var dispTim = getDisplayableTime(notificationList.content[i].delivered_on.split(' ')[1]);
                taglinep = $('<p class="tagline-text-content"> <i class="fa fa-calendar"></i> ' + [dispDat, dispTim].join(' ') + '</p>');
            }

            headerscoldiv.append(headertextp, taglinep);
            outerlink.append(justdiv, headerscoldiv);
            postheaderdiv.append(outerlink, deletediv);

            var row12div = $('<div class="row-12"></div>');
            var header2;
            if (notificationList.content[i].content.bigText != null &&
                notificationList.content[i].content.bigText != undefined &&
                notificationList.content[i].content.bigText.trim() != "" &&
                notificationList.content[i].content.bigText != titleText) {
                header2 = $('<h2 class="notifyTxt">' + notificationList.content[i].content.bigText + '</h2>');
                if (notificationList.content[i].status >= 3) {
                    header2 = $('<h2 class="notifyTxt">' + notificationList.content[i].content.bigText + '</h2>');
                }
            } else {
                header2 = $('<h2 class="notifyTxt"></h2>');
            }
            if ((notificationList.content[i].content.bigPicture != null) &&
                (notificationList.content[i].content.bigPicture != "")) {
                var picture = notificationList.imageBasePath + notificationList.content[i].content.bigPicture;
                var bigImage = $('<img class="no-img-popup" height="154px" width="100%" src="' + picture + '">');
                row12div.append(header2, "</BR>", bigImage);
            } else {
                row12div.append(header2);
            }

            postheaderdiv.append(row12div);
            singlethreaddiv.append(postheaderdiv);
            coldiv.append(singlethreaddiv);
            $('#row').append(coldiv);
        }

        /* Update notification state for all unread notifications */
        for (var i = 0; i < notificationList.content.length; i++) {
            if (notificationList.content[i].status < 3) {
                updateMenuBadgeCounts(notificationList.content[i].content.nextActivity);
                updateNotificationState(notificationList.content[i].id, [], 3); //autoUpdateNotificationStatus();
            }
        }
    }


    /**
     * Update Menu Badge Count on Read
     */
    processMenuBadgeUpdate = function(type) {
        var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
        if (menuBadgeArray[type] > 0) {
            switch (parseInt(type)) {
                case 1100:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1100"];
                    menuBadgeArray["1100"] = 0;
                    break;
                case 1101:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1101"];
                    menuBadgeArray["1101"] = 0;
                    break;
                case 1102:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1102"];
                    menuBadgeArray["1102"] = 0;
                    break;
                case 1200:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1200"];
                    menuBadgeArray["1200"] = 0;
                    break;
                case 1300:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1300"]; //newscount
                    menuBadgeArray["newscount"] -= menuBadgeArray["1300"];
                    menuBadgeArray["1300"] = 0;
                    break;
                case 1301:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1301"]; //newscount
                    menuBadgeArray["newscount"] -= menuBadgeArray["1301"];
                    menuBadgeArray["1301"] = 0;
                    break;
                case 1302:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1302"]; //newscount
                    menuBadgeArray["newscount"] -= menuBadgeArray["1302"];
                    menuBadgeArray["1302"] = 0;
                    break;
                case 1400:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1400"]; //noticount
                    menuBadgeArray["1400"] = 0;
                    break;
                case 1500:
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1500"]; //chatcount
                    menuBadgeArray["chatcount"] -= menuBadgeArray["1500"]; // different chat counts
                    menuBadgeArray["1500"] = 0;
                    break;
                case 1501: //invite
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1501"]; //chatcount
                    menuBadgeArray["chatcount"] -= menuBadgeArray["1501"]; // different chat counts
                    menuBadgeArray["1501"] = 0;
                    break;
                case 1502: //acepted/rejected
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1502"]; //chatcount
                    menuBadgeArray["chatcount"] -= menuBadgeArray["1502"]; // different chat counts
                    menuBadgeArray["1502"] = 0;
                    break;
                case 1503: //new message
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1503"]; //chatcount
                    menuBadgeArray["chatcount"] -= menuBadgeArray["1503"]; // different chat counts
                    menuBadgeArray["1503"] = 0;
                    break;
                case 1600: // members
                    menuBadgeArray["connectcount"] -= menuBadgeArray["1600"]; //chatcount
                    menuBadgeArray["chatcount"] -= menuBadgeArray["1600"]; // should be member updates
                    menuBadgeArray["1600"] = 0;
                    break;
                case 1700:
                    menuBadgeArray["classifiedcount"] -= menuBadgeArray["1700"]; //allclasicount
                    menuBadgeArray["allclasicount"] -= menuBadgeArray["1700"];
                    menuBadgeArray["1700"] = 0;
                    break;
                case 1701:
                    menuBadgeArray["classifiedcount"] -= menuBadgeArray["1701"]; //allclasicount
                    menuBadgeArray["allclasicount"] -= menuBadgeArray["1701"];
                    menuBadgeArray["1701"] = 0;
                    break;
                case 1702:
                    menuBadgeArray["classifiedcount"] -= menuBadgeArray["1702"]; //allclasicount
                    menuBadgeArray["allclasicount"] -= menuBadgeArray["1702"];
                    menuBadgeArray["1702"] = 0;
                    break;
                case 1703:
                    menuBadgeArray["classifiedcount"] -= menuBadgeArray["1703"]; //myclasicount
                    menuBadgeArray["allclasicount"] -= menuBadgeArray["1703"];
                    menuBadgeArray["1703"] = 0;
                    break;
                case 1704:
                    menuBadgeArray["classifiedcount"] -= menuBadgeArray["1704"]; //myclasicount
                    menuBadgeArray["allclasicount"] -= menuBadgeArray["1704"];
                    menuBadgeArray["1704"] = 0;
                    break;
                case 1800:
                    menuBadgeArray["eventscount"] -= menuBadgeArray["1800"]; //allevntcount
                    menuBadgeArray["allevntcount"] -= menuBadgeArray["1800"];
                    menuBadgeArray["1800"] = 0;
                    break;
                case 1801:
                    menuBadgeArray["eventscount"] -= menuBadgeArray["1801"]; //allevntcount
                    menuBadgeArray["allevntcount"] -= menuBadgeArray["1801"];
                    menuBadgeArray["1801"] = 0;
                    break;
                case 1802:
                    menuBadgeArray["eventscount"] -= menuBadgeArray["1802"]; //allevntcount
                    menuBadgeArray["allevntcount"] -= menuBadgeArray["1802"];
                    menuBadgeArray["1802"] = 0;
                    break;
                case 1803:
                    menuBadgeArray["eventscount"] -= menuBadgeArray["1803"]; //myevntcount
                    menuBadgeArray["myevntcount"] -= menuBadgeArray["1803"];
                    menuBadgeArray["1803"] = 0;
                    break;
                case 1804:
                    menuBadgeArray["eventscount"] -= menuBadgeArray["1804"]; //myevntcount
                    menuBadgeArray["myevntcount"] -= menuBadgeArray["1804"];
                    menuBadgeArray["1804"] = 0;
                    break;
                case 1900:
                case 1910:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1910"]; //fitnesscount
                    menuBadgeArray["fitnesscount"] -= menuBadgeArray["1910"];
                    menuBadgeArray["1910"] = 0;
                    break;
                case 1911:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1911"]; //fitnesscount
                    menuBadgeArray["fitnesscount"] -= menuBadgeArray["1911"];
                    menuBadgeArray["1911"] = 0;
                    break;
                case 1920:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1920"]; //sportcount
                    menuBadgeArray["sportcount"] -= menuBadgeArray["1920"];
                    menuBadgeArray["1920"] = 0;
                    break;
                case 1921:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1921"]; //sportcount
                    menuBadgeArray["sportcount"] -= menuBadgeArray["1921"];
                    menuBadgeArray["1921"] = 0;
                    break;
                case 1930:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1930"]; //hnspacount
                    menuBadgeArray["hnspacount"] -= menuBadgeArray["1930"];
                    menuBadgeArray["1930"] = 0;
                    break;
                case 1931:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1931"]; //hnspacount
                    menuBadgeArray["hnspacount"] -= menuBadgeArray["1931"];
                    menuBadgeArray["1931"] = 0;
                    break;
                case 1940:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1940"]; //recreationalcount
                    menuBadgeArray["recreationalcount"] -= menuBadgeArray["1940"];
                    menuBadgeArray["1940"] = 0;
                    break;
                case 1941:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1941"]; //recreationalcount
                    menuBadgeArray["recreationalcount"] -= menuBadgeArray["1941"];
                    menuBadgeArray["1941"] = 0;
                    break;
                case 1950:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1950"]; //saloncount
                    menuBadgeArray["saloncount"] -= menuBadgeArray["1950"];
                    menuBadgeArray["1950"] = 0;
                    break;
                case 1951:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1951"]; //saloncount
                    menuBadgeArray["saloncount"] -= menuBadgeArray["1951"];
                    menuBadgeArray["1951"] = 0;
                    break;
                case 1960:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1960"]; //othercount
                    menuBadgeArray["othercount"] -= menuBadgeArray["1960"];
                    menuBadgeArray["1960"] = 0;
                    break;
                case 1961:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1961"]; //othercount
                    menuBadgeArray["othercount"] -= menuBadgeArray["1961"];
                    menuBadgeArray["1961"] = 0;
                    break;
                case 1970:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1970"]; //banquetcount
                    menuBadgeArray["banquetcount"] -= menuBadgeArray["1970"];
                    menuBadgeArray["1970"] = 0;
                    break;
                case 1971:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1971"]; //banquetcount
                    menuBadgeArray["banquetcount"] -= menuBadgeArray["1971"];
                    menuBadgeArray["1971"] = 0;
                    break;
                case 1901:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1901"]; //fbookingcount
                    menuBadgeArray["fbookingcount"] -= menuBadgeArray["1901"];
                    menuBadgeArray["1901"] = 0;
                    break;
                case 1902:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["1902"]; //fbookingcount
                    menuBadgeArray["fbookingcount"] -= menuBadgeArray["1902"];
                    menuBadgeArray["1902"] = 0;
                    break;
                case 2000:
                    menuBadgeArray["offercount"] -= menuBadgeArray["2000"]; //offercount
                    menuBadgeArray["2000"] = 0;
                    break;
                case 2001:
                    menuBadgeArray["offercount"] -= menuBadgeArray["2001"]; //offercount
                    menuBadgeArray["2001"] = 0;
                    break;
                case 2002:
                    menuBadgeArray["offercount"] -= menuBadgeArray["2002"]; //offercount
                    menuBadgeArray["2002"] = 0;
                    break;
                case 2100:
                case 2101:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2101"]; //gcoursecount
                    menuBadgeArray["2101"] = 0;
                    break;
                case 2102:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2102"]; //ghandicapcount
                    menuBadgeArray["2102"] = 0;
                    break;
                case 2103:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2103"]; //galltourcount
                    menuBadgeArray["galltourcount"] -= menuBadgeArray["2103"];
                    menuBadgeArray["2103"] = 0;
                    break;
                case 2104:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2104"]; //galltourcount
                    menuBadgeArray["galltourcount"] -= menuBadgeArray["2104"];
                    menuBadgeArray["2104"] = 0;
                    break;
                case 2105:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2105"]; //gmytourcount
                    menuBadgeArray["2105"] = 0;
                    break;
                case 2106:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2106"]; //gtourrescount
                    menuBadgeArray["2106"] = 0;
                    break;
                case 2107:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2107"]; //gscorecardcount
                    menuBadgeArray["gscorecardcount"] -= menuBadgeArray["2107"];
                    menuBadgeArray["2107"] = 0;
                    break;
                case 2108:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2108"]; //gscorecardcount
                    menuBadgeArray["gscorecardcount"] -= menuBadgeArray["2108"];
                    menuBadgeArray["2108"] = 0;
                    break;
                case 2109:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2109"]; //gscorecardcount
                    menuBadgeArray["gscorecardcount"] -= menuBadgeArray["2109"];
                    menuBadgeArray["2109"] = 0;
                    break;
                case 2110:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2110"]; //gcoachcount
                    menuBadgeArray["2110"] = 0;
                    break;
                case 2111:
                    menuBadgeArray["golfcount"] -= menuBadgeArray["2111"]; //gcoachcount
                    menuBadgeArray["2111"] = 0;
                    break;
                case 2200:
                    menuBadgeArray["tournamentcount"] -= menuBadgeArray["2200"]; //alltourcount
                    menuBadgeArray["alltourcount"] -= menuBadgeArray["2200"];
                    menuBadgeArray["2200"] = 0;
                    break;
                case 2201:
                    menuBadgeArray["tournamentcount"] -= menuBadgeArray["2201"]; //alltourcount
                    menuBadgeArray["alltourcount"] -= menuBadgeArray["2201"];
                    menuBadgeArray["2201"] = 0;
                    break;
                case 2202:
                    menuBadgeArray["tournamentcount"] -= menuBadgeArray["2202"]; //alltourcount
                    menuBadgeArray["alltourcount"] -= menuBadgeArray["2202"];
                    menuBadgeArray["2202"] = 0;
                    break;
                case 2203:
                    menuBadgeArray["tournamentcount"] -= menuBadgeArray["2203"]; //mytourcount
                    menuBadgeArray["mytourcount"] -= menuBadgeArray["2203"];
                    menuBadgeArray["2203"] = 0;
                case 2204:
                    menuBadgeArray["tournamentcount"] -= menuBadgeArray["2204"]; //mytourcount
                    menuBadgeArray["tourresultcount"] -= menuBadgeArray["2204"];
                    menuBadgeArray["2204"] = 0;
                    break;
                case 2300:
                    menuBadgeArray["livescorecount"] -= menuBadgeArray["2300"];
                    menuBadgeArray["2300"] = 0;
                    break;
                case 2301:
                    menuBadgeArray["livescorecount"] -= menuBadgeArray["2301"];
                    menuBadgeArray["2301"] = 0;
                    break;
                case 2302:
                    menuBadgeArray["livescorecount"] -= menuBadgeArray["2302"];
                    menuBadgeArray["2302"] = 0;
                    break;
                case 2400:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2400"]; //pollcount
                    menuBadgeArray["pollcount"] -= menuBadgeArray["2400"];
                    menuBadgeArray["2400"] = 0;
                    break;
                case 2401:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2401"]; //pollcount
                    menuBadgeArray["pollcount"] -= menuBadgeArray["2401"];
                    menuBadgeArray["2401"] = 0;
                    break;
                case 2402:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2402"]; //pollcount
                    menuBadgeArray["pollcount"] -= menuBadgeArray["2402"];
                    menuBadgeArray["2402"] = 0;
                    break;
                case 2500:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2500"]; //surveycount
                    menuBadgeArray["surveycount"] -= menuBadgeArray["2500"];
                    menuBadgeArray["2500"] = 0;
                    break;
                case 2501:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2501"]; //surveycount
                    menuBadgeArray["surveycount"] -= menuBadgeArray["2501"];
                    menuBadgeArray["2501"] = 0;
                    break;
                case 2502:
                    menuBadgeArray["opinioncount"] -= menuBadgeArray["2502"]; //surveycount
                    menuBadgeArray["surveycount"] -= menuBadgeArray["2502"];
                    menuBadgeArray["2502"] = 0;
                    break;
                case 2600:
                    menuBadgeArray["sponsorcount"] -= menuBadgeArray["2600"];
                    menuBadgeArray["2600"] = 0;
                    break;
                case 2601:
                    menuBadgeArray["sponsorcount"] -= menuBadgeArray["2601"];
                    menuBadgeArray["2601"] = 0;
                    break;
                case 2602:
                    menuBadgeArray["sponsorcount"] -= menuBadgeArray["2602"];
                    menuBadgeArray["2602"] = 0;
                    break;
                case 2700:
                    menuBadgeArray["accountscount"] -= menuBadgeArray["2700"];
                    menuBadgeArray["2700"] = 0;
                    break;
                case 2701: // New Transaction
                    menuBadgeArray["accountscount"] -= menuBadgeArray["2701"];
                    menuBadgeArray["2701"] = 0;
                    break;
                case 2702: // Query Response
                    menuBadgeArray["accountscount"] -= menuBadgeArray["2702"];
                    menuBadgeArray["2702"] = 0;
                    break;
                case 2703: // New Bill
                    menuBadgeArray["accountscount"] -= menuBadgeArray["2703"];
                    menuBadgeArray["2703"] = 0;
                    break;
                case 2800:
                case 2801: // Updated Affiliation
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2801"];
                    menuBadgeArray["2801"] = 0;
                    break;
                case 2802: // Updated Club Rules
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2802"];
                    menuBadgeArray["2802"] = 0;
                    break;
                case 2803: // Updated Dress Code
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2803"];
                    menuBadgeArray["2803"] = 0;
                    break;
                case 2804: // Updated Membership Options
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2804"];
                    menuBadgeArray["2804"] = 0;
                    break;
                case 2805: // Updated Membership Criteria
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2805"];
                    menuBadgeArray["2805"] = 0;
                    break;
                case 2806: // Updated Gallery
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2806"];
                    menuBadgeArray["2806"] = 0;
                    break;
                case 2807: // Updated About
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2807"];
                    menuBadgeArray["2807"] = 0;
                    break;
                case 2808: // Updated History
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2808"];
                    menuBadgeArray["2808"] = 0;
                    break;
                case 2809: // Updated President Message
                    menuBadgeArray["aboutcount"] -= menuBadgeArray["2809"];
                    menuBadgeArray["2809"] = 0;
                    break;
                case 2810: // Updated Contact us
                    menuBadgeArray["2810"] = 0;
                    break;
                case 2900:
                case 2901: // Updated Administration
                    menuBadgeArray["clubmgmtcount"] -= menuBadgeArray["2901"];
                    menuBadgeArray["2901"] = 0;
                    break;
                case 2902: // Updated Present Committee
                    menuBadgeArray["clubmgmtcount"] -= menuBadgeArray["2902"];
                    menuBadgeArray["2902"] = 0;
                    break;
                case 2903: // Updated Past Committee
                    menuBadgeArray["clubmgmtcount"] -= menuBadgeArray["2903"];
                    menuBadgeArray["2903"] = 0;
                    break;
                case 2904: // Updated Club Staff
                    menuBadgeArray["clubmgmtcount"] -= menuBadgeArray["2904"];
                    menuBadgeArray["2904"] = 0;
                    break;
                case 3000:
                    menuBadgeArray["fnbcount"] -= menuBadgeArray["3000"];
                    menuBadgeArray["3000"] = 0;
                    break;
                case 3001: // Updated Restaurant List
                    menuBadgeArray["fnbcount"] -= menuBadgeArray["3001"];
                    menuBadgeArray["3001"] = 0;
                    break;
                case 3002: // Updated Menu
                    menuBadgeArray["fnbcount"] -= menuBadgeArray["3002"];
                    menuBadgeArray["3002"] = 0;
                    break;
                case 3100:
                    break;
                case 3101:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["3101"]; //roomcount
                    menuBadgeArray["roomcount"] -= menuBadgeArray["3101"];
                    menuBadgeArray["3101"] = 0;
                    break;
                case 3102:
                    menuBadgeArray["facilitycount"] -= menuBadgeArray["3102"]; //roomcount
                    menuBadgeArray["roomcount"] -= menuBadgeArray["3102"];
                    menuBadgeArray["3102"] = 0;
                    break;
                default:
                    break;
            }
            localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
        }
        updateMenuBadgeStatus();
    }


    /**
     * Delete Push Notification
     */
    deleteNotification = function(notificationId) {
        swal({
                title: "Confirm Delete?",
                text: "Are you sure you want to delete this notification?",
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                closeOnConfirm: false,
                cancelButtonText: localizeString("Alert_Button_Cancel")
            },
            function() {
                var actions = {
                    "id": notificationId,
                    "status": 5
                };

                window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

                successCallback = function(data) {
                    window.plugins.spinnerDialog.hide();
                    if (data.status == "success") {
                        swal.close();
                        page_number = 1;
                        localStorage["numofNoti"] = 0;
                        getPushNotifications(1);
                    } else {
                        var thisone = this;

                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: data.message,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    }
                    window.plugins.spinnerDialog.hide();
                };
                errorCallback = function(data) {

                };

                postMessageToServerWithAuth("/notificationUpdateStatus", actions, successCallback, errorCallback);
            });
    }
}());