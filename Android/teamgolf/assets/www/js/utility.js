(function () {
    var enableDebugMode = 0;
    var slideIndex = 1;
    var currentIndex = 1;
    var sApp = null;

    /**
     * Dynamically scrolls the photos in the view.
     */
    carousel = function () {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (x.length > 0) {
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";
            }
            slideIndex++;
            if (slideIndex > x.length) {slideIndex = 1}
            x[slideIndex-1].style.display = "block";
            setTimeout(carousel, 3000);
        }
    }

    /**
     * Dynamically generate object from dynamic key and value.
     * Used for storing responses in the response JSON
     */
    getNewObject = function (key, value) {
        var retVal = {};
        retVal[key] = value;
        return retVal;
    }

    getBase64Image = function (img) {
        var canvas = document.createElement("canvas");
        canvas.width = img.width;
        canvas.height = img.height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 512, 512);
        var dataURL = canvas.toDataURL("image/jpeg");
        return dataURL.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
    }

    urlcheck = function (url) {
        if ((url.substring(0,8) != "https://") && (url.substring(0,7) != "http://"))
            url = "http://" + url;
        return url;
    }

    gotourl = function (url) {
        url = urlcheck(url);
        swal({
            title: localizeString("Alert_Title_openUrlAction"),
            text: localizeString("Alert_Text_LeavingAppExt"),
            type: "info",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            var testurl= url.toLowerCase();
            if (testurl.endsWith(".pdf") || testurl.endsWith(".doc") || testurl.endsWith(".docx")
                || testurl.endsWith(".xls") || testurl.endsWith(".xlsx")) {
                window.open("https://docs.google.com/viewer?url=" + url, '_blank', 'location=no');
            } else {
                window.open(url, "_system");
            }
        });
    }

    gotofacebook = function (fbId) {
        if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
            sApp = startApp.set("fb://profile?id=" + fbId);
        }
        if ( /Android/i.test(navigator.userAgent) ) {
            sApp = startApp.set({
                "action": "ACTION_VIEW",
                "package": "com.facebook.katana",
                "uri":"fb://facewebmodal/f?href=https://www.facebook.com/" + fbId
            });
        }
        if (sApp) {
            sApp.check(function(message) { /* success */
                console.log(message);
                setTimeout(function(){if (sApp) sApp.start();}, 50);
            },
            function(error) { /* error */
                console.log(error);
                console.log("App not available");
                setTimeout(function() {
                    if (fbId.toLowerCase().indexOf("facebook") < 0)
                        fbId = "https://facebook.com/" + fbId;
                    fbId = urlcheck(fbId);
                    swal({
                        title: localizeString("Alert_Title_openUrlAction"),
                        text: localizeString("Alert_Text_LeavingAppExt"),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: localizeString("Alert_Button_Proceed"),
                        closeOnConfirm: true,
                        cancelButtonText : localizeString("Alert_Button_Cancel")
                    },
                    function () {
                        window.open(fbId, "_system");
                    });
                }, 50);
            });
        } else {
            if (fbId.toLowerCase().indexOf("facebook") < 0)
                fbId = "https://facebook.com/" + fbId;
            fbId = urlcheck(fbId);
            swal({
                title: localizeString("Alert_Title_openUrlAction"),
                text: localizeString("Alert_Text_LeavingAppExt"),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                closeOnConfirm: true,
                cancelButtonText : localizeString("Alert_Button_Cancel")
            },
            function () {
                window.open(fbId, "_system");
            });
        }
    }

    gototwitter = function (twitterHandle) {
        if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
            sApp = startApp.set("twitter://user?screen_name=" + twitterHandle);
        }
        if ( /Android/i.test(navigator.userAgent) ) {
            sApp = startApp.set({
                "action": "ACTION_VIEW",
                "package": "com.twitter.android",
                "uri":"twitter://user?screen_name=" + twitterHandle
            });
        }

        if (sApp) {
            sApp.check(function(message) { /* success */
                setTimeout(function(){if (sApp) sApp.start();}, 50);
            },
            function(error) { /* error */
                console.log(error);
                console.log("twitter App not available");
                setTimeout(function() {
                    if (twitterHandle.toLowerCase().indexOf("twitter") < 0)
                        twitterHandle = "https://twitter.com/" + twitterHandle;
                    twitterHandle = urlcheck(twitterHandle);
                    swal({
                        title: localizeString("Alert_Title_openUrlAction"),
                        text: localizeString("Alert_Text_LeavingAppExt"),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: localizeString("Alert_Button_Proceed"),
                        closeOnConfirm: true,
                        cancelButtonText : localizeString("Alert_Button_Cancel")
                    },
                    function () {
                        window.open(twitterHandle, "_system");
                    });
                }, 50);
            });
        } else {
            if (twitterHandle.toLowerCase().indexOf("twitter") < 0)
                twitterHandle = "https://twitter.com/" + twitterHandle;
            twitterHandle = urlcheck(twitterHandle);
            swal({
                title: localizeString("Alert_Title_openUrlAction"),
                text: localizeString("Alert_Text_LeavingAppExt"),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                closeOnConfirm: true,
                cancelButtonText : localizeString("Alert_Button_Cancel")
            },
            function () {
                window.open(twitterHandle, "_system");
            });
        }
    }

    gotoinstagram = function (instaHandle) {
        if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
            sApp = startApp.set("instagram://profile?id=" + instaHandle);
            /**
             * For iOS we need to add LSApplicationQueriesScheme for all the apps that
             * we want to query in the plist
             */
        }
        if ( /Android/i.test(navigator.userAgent) ) {
            sApp = startApp.set({
                "action": "ACTION_VIEW",
                "package": "com.instagram.android",
                "uri":"http://instagram.com/_u/" + instaHandle
            });
        }
        if (sApp) {
            sApp.check(function(message) { /* success */
                console.log(message);
                setTimeout(function(){if (sApp) sApp.start();}, 50);
            },
            function(error) { /* error */
                console.log(error);
                console.log("instagram App not available");
                setTimeout(function() {
                    if (instaHandle.toLowerCase().indexOf("instagram") < 0)
                        instaHandle = "https://instagram.com/" + instaHandle;
                    instaHandle = urlcheck(instaHandle);
                    swal({
                        title: localizeString("Alert_Title_openUrlAction"),
                        text: localizeString("Alert_Text_LeavingAppExt"),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonText: localizeString("Alert_Button_Proceed"),
                        closeOnConfirm: true,
                        cancelButtonText : localizeString("Alert_Button_Cancel")
                    },
                    function () {
                        window.open(instaHandle, "_system");
                    });
                }, 50);
            });
        } else {
            if (instaHandle.toLowerCase().indexOf("instagram") < 0)
                instaHandle = "https://instagram.com/" + instaHandle;
            instaHandle = urlcheck(instaHandle);
            swal({
                title: localizeString("Alert_Title_openUrlAction"),
                text: localizeString("Alert_Text_LeavingAppExt"),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                closeOnConfirm: true,
                cancelButtonText : localizeString("Alert_Button_Cancel")
            },
            function () {
                window.open(instaHandle, "_system");
            });
        }
    }

    gotomail = function (emailId) {
        swal({
            title: localizeString("Alert_Title_SendEmailAction"),
            text: localizeString("Alert_Text_LeavingAppExt"),
            type: "warning",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("mailto:" + emailId, "_system");
        });
    }

    gotocall = function (contactNumber) {
        swal({
            title: localizeString("Alert_Title_MakeCallAction"),
            text: localizeString("Alert_Text_LeavingAppPhone"),
            type: "warning",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("tel://"+contactNumber, "_system");
        });
    }

    /**
     * Dynamically scrolls the photos in the view.
     * Also change the image highlight
     */
    modifiedCarousel = function () {
        var i;
        var x = document.getElementsByClassName("mySlides");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
            $("#circle" + i).css("box-shadow", "");
        }
        slideIndex++;
        if (slideIndex > x.length) {slideIndex = 1}
        x[slideIndex-1].style.display = "block";
        $("#circle" + (slideIndex - 1)).css("box-shadow", "0px 0px 9px 4px #ffffff");
        setTimeout(modifiedCarousel, 3000);
    }


    setCurrentIndex = function (index) {
        currentIndex = index;
    }


    /**
     * Email Validation
     */
    emailValidation = function (email) {
        // We need to support + as a valid character in email
        //var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        return re.test(email);
    }


    /**
     * Phone Validation
     */
    phoneValidation = function (phone) {
        return true;
        /*var re = /^\+?([0-9\(\) ]{5,15})$/;
        return re.test(phone);*/
        /*var phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/
        return (phone.match(phoneno))?true:false;*/
    }

    /**
     * Age Validation
     */
    getAge = function (birthDateString1) {
        var today = new Date();
        var birthDateString = "" + birthDateString1;
        var birthDate = new Date(birthDateString);
        var birthDateNew = birthDateString.split(",");

        var age = today.getFullYear() - parseInt(birthDateNew[0]);//.getFullYear();
        var m = today.getMonth() - parseInt(birthDateNew[1]);//birthDate.getMonth();

        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }


    /**
     *  Lazy loads imags
     */
    lazyLoadListImages = function (pno, numberOfImages) {
        if (pno > 0) {
            for (var i = (pno - 1)*15; i < ((pno - 1)*15 + numberOfImages); i++) {
                $("#image-" + i).attr("src", $("#image-" + i).attr("data-src"));
                $("#image-" + i).removeAttr("data-src");
            }
        }
    }


    /**
     * Convert String to Date object for native calendar integration
     */
    dateFromString = function(dateString, timeString, isStart) {
        var dateElements = dateString.split("-");
        var theDate = new Date();

        theDate.setFullYear(dateElements[0]);
        theDate.setMonth(dateElements[1] - 1);
        theDate.setDate(dateElements[2]);

        if (timeString != "null" && timeString != "") {
            var timeElements = timeString.split(":");
            theDate.setHours(timeElements[0]);
            theDate.setMinutes(timeElements[1]);
        } else {
            if (1 == isStart) { // Start date so time starts @ 00:00:00
                theDate.setHours('0');
                theDate.setMinutes('0');
                theDate.setSeconds('0');
            } else { // End date so time ends @ 23:59:59
                theDate.setHours('23');
                theDate.setMinutes('59');
                theDate.setSeconds('59');
            }
        }
        return theDate;
    }


    /**
     * Add an event to the native calendar for reminder
     */
    addEventToNativeCalendar = function (eventTitle, eventLocation, eventNotes, eventStartDate, eventStartTime, eventEndDate, eventEndTime) {
        var success = function(message) { console.log("Success: " + JSON.stringify(message)); };
        var error = function(message) { console.log("Error: " + message); };
        var startDate;
        var endDate;
        if (eventStartDate != "null" && eventStartDate != "") {
            startDate = dateFromString(eventStartDate, eventStartTime, 1);
        } else {
            console.log("Invalid Start Date");
            return;
        }

        if (eventEndDate != "null" && eventEndDate != "") {
            endDate = dateFromString(eventEndDate, eventEndTime, 0);
        } else {
            endDate = dateFromString(eventStartDate, eventStartTime, 0);
        }

        // create an event interactively
        window.plugins.calendar.createEventInteractively(eventTitle, eventLocation, eventNotes, startDate, endDate, success, error);
    }
}());