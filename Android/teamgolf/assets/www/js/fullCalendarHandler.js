(function () {

    var enableDebugMode = 0;
    "use strict";
    loadCalendar = function () {
        //alert("calendarScheduler");
        //days: new Date(2016,3,12),             zones: "fullday",
        scheduler.config.readonly = true;
        scheduler.init('calendarScheduler', new Date(), "month");
        //blocks all events for each Sunday
        scheduler.blockTime(0, "fullday");

        scheduler.addMarkedTimespan({
            days: 1,
            zones: "fullday",
            type:  "dhx_time_block",
            css:   "offtime"
        });
        //blocks events from 0 till 10 hours for 3rd June,2009
        //scheduler.blockTime(new Date(2016,3,12), [13*60,17*60]);
        //makes the same as in examples above, but takes parameters as a config object
        /*scheduler.addMarkedTimespan({
            start_date: new Date(2016, 3, 11),
            end_date: new Date(2016, 3, 12),
            css:"dhx_time_block"
        });*/
        scheduler.updateView();
        //alert("calendarScheduler init done");
        getCalenderEvents();
    }


    getCalenderEvents = function () {
        var actions = null;

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayCalenderEvents(data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/events", actions, successCallback, errorCallback);
    }


    formatCalendarDisplayJson = function (data) {
        var eventJsonData = [];
        for (i = 0; i < data.events.length; i++) {
            var today = new Date(data.events[i].attributes[0].options[0]);
            var month = today.getMonth() + 1;
            var day = today.getDate();
            var day2 = day + 1;
            var year = today.getFullYear();
            var date1 =  [(month<10 ? '0' : '') + month, (day<10 ? '0' : '') + day, year].join('/');
            var date2 =  [(month<10 ? '0' : '') + month, (day<10 ? '0' : '') + day, year].join('/');
            eventJsonData.push({
                "id": i + 1,
                "text": data.events[i].title + ": " + data.events[i].description,
                "start_date":(date1).toString(),
                "end_date":(date2).toString(),
            });
        }
        return eventJsonData;
    }


    displayCalenderEvents = function (data) {
        //alert("displayCalenderEvents");
        var eventsInline = formatCalendarDisplayJson(data);
        scheduler.parse(eventsInline, "json");//takes the name and format of the data source
    }


    /**
     * Communicates with server with Oauth authentication.
     *
     * @uri: Rest API path
     * @postParam: Additional Input Params
     * @successCallback: Callback to be invoked on success
     * @errorCallback: Callback to be invoked on error
     */
    postFullCalenderMessageToServerWithAuth = function (uri, postParam, successCallback, errorCallback) {
        var server_url = localStorage.getItem('SERVER_URL');
        var deviceid = localStorage.getItem("DEVICE_ID");
        var deviceos = localStorage.getItem("DEVICE_OS");
        var appid = localStorage.getItem("APP_ID");
        var username = localStorage.getItem("USER_NAME");
        var password = localStorage.getItem("PASSWORD");
        var communeid = localStorage.getItem("COMMUNE_ID");
        var member_id = localStorage.getItem("memberid");
        var server_ts = localStorage.getItem("server_ts");
        var first_name = localStorage.first_name;
        var last_name = localStorage.last_name;
        var email = localStorage.email;

        var actions = {
            "member_id": member_id,
            "first_name": first_name,
            "last_name": last_name,
            "email": email,
            "deviceid": deviceid,
            "deviceos": deviceos,
            "appid": appid,
            "server_ts": server_ts,
            "locale": localStorage.getItem("APP_LOCALE")
        };

        var postParams;
        if (postParam != null)
            postParams = $.extend(actions, postParam);
        else
            postParams = actions;

        if (enableDebugMode == 1)
        {
            alert(JSON.stringify(postParams));
            alert(server_url + uri);
            alert(localStorage.getItem('token_type') +": "+localStorage.getItem('access_token'));
        }

        $.ajax({
            url: server_url + uri,
            type: "POST",
            data: postParams,
            dataType: "json",
            headers: {
                    "Authorization": localStorage.getItem('token_type') + " " + localStorage.getItem('access_token'),
                    "COMMUNE_ID": "" + communeid
            },
            success: function (data) {
                successCallback(data);
            },
            error: function (jqXHR, exception) {
                window.plugins.spinnerDialog.hide();

                if (jqXHR.status === 0) {
                    var thisone = this;
                    swal({
                        title: localizeString("Alert_Title_Connectivity"),
                        text: localizeString("Alert_Text_Connectivity"),
                        type: "info",
                        showCancelButton: true,
                        closeOnCancel: true,
                        allowOutsideClick: false,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_TryAgain"),
                        cancelButtonText:localizeString("Alert_Button_Cancel")
                    },
                    function () {
                        $.ajax(thisone);
                        return;
                    });
                } else if (jqXHR.status === 401) {
                    resignin();
                } else if (jqXHR.status === 412) {
                    var thisone = this;
                    if (localStorage.getItem('access_token').trim() != "") {
                        reauth(thisone, actions);
                    } else {
                        resignin("auth");
                    }
                }
                else {
                    var thisone = this;
                    swal({
                        title: localizeString("Alert_Title_Something"),
                        text: localizeString("Alert_Text_Something"),
                        type: "info",
                        showCancelButton: true,
                        closeOnCancel: true,
                        allowOutsideClick: true,
                        closeOnConfirm: true,
                        showLoaderOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_TryAgain"),
                        cancelButtonText:localizeString("Alert_Button_Cancel")
                    },
                    function () {
                        $.ajax(thisone);
                        return;
                    });
                }
            }
        });
    }

}());