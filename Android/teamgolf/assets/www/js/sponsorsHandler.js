(function () {
    var enableDebugMode = 0;
    "use strict";

    /**
     * Fetch list of sponsors from the server
     **/
    getSponsors = function (pg_no) {
        var actions = {
            "page_number": pg_no,
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length <= 0) {
                    $('.row').html('<p align="center" style="text-align:center">'+localizeString("Sponsors_Label_NoData")+'</p>');
                } else {
                    if (pg_no == 1)
                        $('.row').html('');
                    displaySponsors(data.data);
                }
            } else {
                window.plugins.spinnerDialog.hide();
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
        };

        postMessageToServerWithAuth("/sponsorsNew", actions, successCallback, errorCallback);
    }


    /**
     * Display the list of Sponosrs
     * @sponsorsData list of sponsors returned from server
     **/
    displaySponsors = function (sponsorsData) {
        if (enableDebugMode)
            alert("Sponsors Data = " + JSON.stringify(sponsorsData));
        for (i = 0; i < sponsorsData.content.length; i++) {
            var div1;
            if (1 == sponsorsData.detailsPageAllowed)
                div1 = $('<div class="col-lg-4 col-sm-6"></div>').attr('onClick', "gotoSponsorsDetail('" + sponsorsData.content[i].id + "')");
            else
                div1 = $('<div class="col-lg-4 col-sm-6"></div>').attr('onClick', "gotoSponsorsUrl('" + sponsorsData.content[i].url + "')");

            var div2 = $('<div class="card-01"></div>');
            var img;
            if (sponsorsData.content[i].image != null && sponsorsData.content[i].image != undefined
                && sponsorsData.content[i].image != "") {
                img = sponsorsData.imageBasePath + sponsorsData.content[i].image;
            } else {
                img = "images/default-listing.png";
            }

            var aside = $('<aside class="card-side card-side-img pull-left" style="background: #eee url(' + img + ') center center;"></aside>');
            var div4 = $('<div class="card-main"></div>');
            var div5 = $('<div class="card-inner-01"></div>');
            var p = $('<p class="card-heading" style="max-width:190px; text-overflow:ellipsis!important; white-space:nowrap!important; overflow:hidden!important;"></p>').text(sponsorsData.content[i].title);

            var p2 = "";
            if (sponsorsData.content[i].shortDescription != null && sponsorsData.content[i].shortDescription != undefined) {
                var html = sponsorsData.content[i].shortDescription;
                var div = document.createElement("div");
                div.innerHTML = html;
                var thecontent = div.innerText;
                p2 = $("<p>" + thecontent+ "</p>").attr({
                    'class': 'blog-desc'
                });
            }

            div5.append(p, p2); /* p2,*/
            div4.append(div5);
            div2.append(aside, div4);
            div1.append(div2);
            $('.row').append(div1);
        }
    }


    /**
     * Goto Sponsors details Page
     **/
    gotoSponsorsDetail = function (sponsorId) {
        localStorage.setItem("sponsorId" , sponsorId);
        slideview("sponsor-details.html", 'script', 'left');
    }


    /**
     * Redirect to the sponsors web portal
     **/
    gotoSponsorsPortal = function () {
        gotourl(localStorage.getItem("SponsorsUrl"));
    }


    /**
     * Redirect to the sponsors web portal
     **/
    gotoSponsorsUrl = function (url) {
        gotourl(url);
    }

    /**
     * Get the details corresponding to a specific Sponsorship
     **/
    getSponsorsDetails = function () {
        var actions = {
            "sponsor_id": localStorage.sponsorId
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displaySponsorDetails(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };

        postMessageToServerWithAuth("/sponsorsNew", actions, successCallback, errorCallback);
    }


    /**
     * Display the specific Sponsorship details
     * @sponsorData Sponsors data recieved from server
     **/
    displaySponsorDetails = function (sponsorsData) {
        if (enableDebugMode)
            alert("displaySponsorDetails: " + JSON.stringify(sponsorsData));

        localStorage.setItem("SponsorsUrl", sponsorsData.content[0].url);

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
            ];

        if (sponsorsData.content[0].image != null && sponsorsData.content[0].image != undefined
             && sponsorsData.content[0].image != "") {
            $('#topbanner').attr({
                'src': sponsorsData.imageBasePath + sponsorsData.content[0].image,
            });
            $("#topbanner").css("display", "block");
        } else {
            $("#topbanner").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(sponsorsData.content[0].title);
        var p2 = $('<p class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + sponsorsData.content[0].Description + '</p>');
        td1.append(p1, p2); /*span1, */
        tr1.append(td1);
        $("#sponsors_body").append(tr1);
        $("#SponsorDetails_Button_ViewMore").css("display", "block");
    }
}());