(function () {
    var enableDebugMode = 0;
    var start_msg_id = 0;
    var autoRefresh = null;
    var newMsgRcvd = 0; /* New message Received but user doesnt presses the new message
                           button and scroll down - @newMsgRcvd maintains this state */

    /* Extend this as needed */
    var chatStatusArr = ["INVITATION_NOT_SENT", "INVITATION_SENT", "INVITATION_RECEIVED", "INVITATION_ACCEPTED", "INVITATION_REJECTED"];

    "use strict";

    $("#msguserold").scroll(function () { //content
        if ($(this).scrollTop() == 0) {
            fetchMessageHistory(parseInt(localStorage.getItem("LastMessageIDFor_" + localStorage.getItem("UserIdenifier"))));
        }

        var diff = $(this)[0].scrollHeight - ($(this).innerHeight() + $(this).scrollTop());
        diff = (diff < 0) ? (diff * -1) : diff;
        if ( diff < 20 ) {
            $('.scrolltolastchat').fadeOut();
            if (newMsgRcvd == 1)
                fetchMessageHistory(start_msg_id);
        }
    });

    /* Set height to enable scroll events */
    $("#msguserold").css("height", Math.max($(document).height(), $(window).height()) - 150);

    /**
     * Listen to user input change listner
     */
    $("#resp-msgarea").on("change keyup", function() { // paste keyup
        if ($(this).val().length > 0) {
            if ($("#sendMessageDiv").hasClass('disabled'))
                $("#sendMessageDiv").removeClass('disabled');
        } else {
            if (!$("#sendMessageDiv").hasClass('disabled'))
                $("#sendMessageDiv").addClass('disabled');
                //$("#sendMessageDiv").prop('disabled', true);
        }
    });

    /**
     * Get individual member detail from server
     */
    getPersonalDetails = function () {
        var actions = {
            "user_id": localStorage.getItem("UserIdenifier")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            //window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayUserDetails(data);
            } else {
                window.plugins.spinnerDialog.hide();
                $('#user-list').html('');
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user/view", actions, successCallback, errorCallback);
    }


    /**
     * Display Member Details
     * @memberDetail member Detail
     */
    displayUserDetails = function (memberDetail) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(memberDetail));
        $('.user-list').html('');
        var li = $('<li></li>').attr({
            'style': 'border-bottom:none; margin-bottom:0px;'
        });
        var div1 = $('<div></div>').attr({
            'class': 'user-img-01',
            'style': 'display:inline;'
        });

        var div2 = $('<div></div>').attr({
            'class': 'four_sixth bold-match-color',
            'style': 'margin-left:3%;'
        });
        var p = $('<p></p>').attr({
            'style': 'margin-top:8px; margin-bottom:3px;'
        }).text(localStorage.getItem('Userfirst_name') + ' ' + localStorage.getItem('Userlast_name'));
        $("#title").html(localStorage.getItem('Userfirst_name'));
        var img;
        img = $('<img></img>').attr({
            'src': 'images/avatar-001.jpg'
        });
        if (memberDetail == '') {
            var span = $('<span></span>').attr({
                'class': 'text'
            }).text('');
        } else {
            if (memberDetail.data.avatar != null && memberDetail.data.avatar != '' && memberDetail.data.avatar != undefined)
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text(memberDetail.data.avatar);
            else
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text('Not Availbale');

            if (memberDetail.data.photo != null && memberDetail.data.photo != "" && memberDetail.data.photo != undefined) {
                img = $('<img></img>').attr({
                    'src': 'data:image/jpeg;base64,' + memberDetail.data.photo
                });
            }
        }

        div1.append(img);
        p.append($('</br>'), span);
        var div3 = $('<div></div>').attr({
            'class': 'user-arrow'
        });
        var a = $('<a></a>').attr({
            'href': 'javascript:void()'
        });
        var ival = $('<i></i>').attr({
            'class': 'mdi mdi-table-edit',
            'data-name': 'mdi-table-edit'
        });
        a.append(ival);
        div3.append(a);
        div2.append(p, div3);
        li.append(div1, div2);
        $('.user-list').append(li);
        $('#details').html('');

        $('#content img').simplebox({
            fadeSpeed: 350
        });

        if (enableDebugMode)
            alert("chat status = " + memberDetail.data.chatStatus);

        localStorage.setItem("numofMessages", "0");
        fetchMessageHistory(start_msg_id);
    }


    sendMessageToUser = function () {
        if (enableDebugMode)
            alert("Sending Message");
        sendChatMessage();
    }


    /**
     * Send Chat Message
     */
    sendChatMessage = function () {
        var message = $("#resp-msgarea").val();

        if (message.trim() == "") {
            $("#msgerror").css("display", "inline");
            $("#msgerror").html(localizeString("ValidationError_Message")+'<i class="form-help-icon icon">error</i>');
            $("#resp-msgarea").focus();
            return;
        }
        var actions = {
            "recipientId": localStorage.getItem("UserIdenifier"),
            "message": message.trim()
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Send"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                $("#resp-msgarea").val("");
                fetchMessageHistory(start_msg_id); //TODO: Check here
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/send_chat_message", actions, successCallback, errorCallback);
    }

    /**
     * Fetch Chat History
     */
    fetchMessageHistory = function (message_id) {
        var actions = {
            "messageId": message_id,
            "recipientId": localStorage.getItem("UserIdenifier")
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        if (autoRefresh != null) {
            clearInterval (autoRefresh);
            autoRefresh = null;
        }

        successCallback = function (data) {
            if (enableDebugMode)
                alert("data = " + JSON.stringify(data));
            if (data.status == "success") {
                swal.close();
                localStorage.setItem('numofMessages', String(parseInt(localStorage["numofMessages"]) + parseInt(JSON.stringify(data.data.messages.length))));
                processMessageHistory(message_id, data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem("numofMessages", "0");
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/get_chat_history", actions, successCallback, errorCallback);
    }

    /**
     * Process the sent messages for UI styling
     * @msg User message
     * @time Message timestamp
     */
    processMyMessage = function (msg, time) {
        var singleMessage = {};
        singleMessage["category"] = "request";
        singleMessage["sender"] = localStorage.first_name + " " + localStorage.last_name + " (" + time + ")";
        singleMessage["message"] = msg;
        return singleMessage;
    }

    /**
     * Process the reveived messages for UI styling
     * @msg User message
     * @time Message timestamp
     */
    processOtherMessage = function (msg, time) {
        var singleMessage = {};
        singleMessage["category"] = "response";
        singleMessage["sender"] = localStorage.getItem("Userfirst_name") + " " + localStorage.getItem("Userlast_name") + " (" + time + ")";
        singleMessage["message"] = msg;
        return singleMessage;
    }

    /**
     * Update Chat History on UI
     * @msgHistory Latest Chat History
     */
    processMessageHistory = function (msg_id, msgHistory) {
        var messages = msgHistory.data.messages;
        var msgUserType = msgHistory.data.mytype;
        var messageItem = null;
        var messageListFormatted = [];
        if (localStorage.getItem("LastUpdatedMessageFor_" + msgHistory.data.chat_id) != msgHistory.data.lastUpdated) {
            localStorage.setItem("LastUpdatedMessageFor_" + msgHistory.data.chat_id, msgHistory.data.lastUpdated); //means new messages have arrived
            // may be cancle the own going thread and then trigger updated fetch here it self
        }
        if (messages.length > 0) {
            localStorage.setItem("LastMessageIDFor_" + localStorage.getItem("UserIdenifier"), (messages[messages.length - 1].id));
            for (var i = messages.length - 1; i >= 0; i--) {  //(var i = 0; i < messages.length; i++)
                messageItem = null;
                var dispDat = getDisplayableDate(messages[i].created_on.split(' ')[0]);
                var dispTim = getDisplayableTime(messages[i].created_on.split(' ')[1]);
                if (messages[i].type == msgUserType)
                    messageItem = processMyMessage(messages[i].message, [dispDat, dispTim].join(' '));
                else
                    messageItem = processOtherMessage(messages[i].message, [dispDat, dispTim].join(' '));

                if (messageItem != null)
                    messageListFormatted.push(messageItem);
            }

            var conversationHistory = renderConversationLayoutContents(messageListFormatted);
            if (enableDebugMode)
                alert("Modified Content: " + conversationHistory);

            var d = $('#msguserold');
            if (msg_id == 0) {
                newMsgRcvd = 0; /* Update new msg count as we are fetching base page */
                $("#msguserold").empty();
                $("#msguserold").append(conversationHistory);
                d.scrollTop(d.prop("scrollHeight") + 200);
                $('.scrolltolastchat').fadeOut();
            } else {
                var old_height = $("#msguserold")[0].scrollHeight; /* Store height before update */
                var old_scroll = d.scrollTop(); /* Store scroll position before update */
                $("#msguserold").prepend(conversationHistory);
                d.scrollTop($("#msguserold")[0].scrollHeight - old_height); /* Keep scroll to current visible position */
                //TODO: To Make the page scroll up the window height : USe the folowing
                //d.scrollTop($("#chatMessageHistory")[0].scrollHeight - old_height - d.height() + 100);
            }
        }
        window.plugins.spinnerDialog.hide(); // Hide this only when the conversation is updated
        if (autoRefresh != null) {
            clearInterval (autoRefresh);
            autoRefresh = null;
        }
        autoRefresh = setInterval(refreshMessageHistory , 5000);
    }

    refreshMessageHistory = function () {
        var actions = {
            "recipientId": localStorage.getItem("UserIdenifier")
        };

        successCallback = function (data) {
            if (enableDebugMode)
                alert("data = " + JSON.stringify(data));
            if (data.status == "success") {
                swal.close();
                if (data.data.unread_count > 0) {
                    // show go down button with badge
                    var diff = $("#msguserold").innerHeight() - $("#msguserold").scrollTop();
                    diff = (diff < 0) ? (diff * -1) : diff;
                    if ( diff < 20 ) {
                         $('.scrolltolastchat').fadeOut();
                         fetchMessageHistory(start_msg_id);
                    } else {
                        $('.scrolltolastchat').fadeIn();
                        newMsgRcvd = 1; /* Update the new message state */
                    }
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/chat_refresh", actions, successCallback, errorCallback);
    }

    backKeyHandler = function () {
        if (autoRefresh != null)
            clearInterval (autoRefresh);
        autoRefresh =  null;
        localStorage.setItem('fromMessagesPage', 'true');
        slideview("javascript:history.back()","link","right");
        return false;
    }

}());