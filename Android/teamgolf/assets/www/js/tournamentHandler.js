(function() {
    var enableDebugMode = 0;
    var team_size = 0;
    var seats_available = 0;
    var event_name = "";
    var slot_name = "";
    var tournament_type = "";
    var booking_id = "";
    var status = 0; // Pending
    var tournament_id;
    var page_number = 1;
    "use strict";

    localStorage.setItem("from_page", "tournaments");

    /**
     * Fetches list of all the tournaments from the server
     */
    getTournamentList = function() {
        var actions = {
            "type": "0"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                displayTournamentList(data.data);
            } else {
                var thisone = this;

                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/tournaments", actions, successCallback, errorCallback);
    }


    /**
     * Renders the list of all the tournaments
     * @data tournameent list data
     */
    displayTournamentList = function(data) {
            if (enableDebugMode)
                alert("getTournamentsList = " + JSON.stringify(data));
            if (data.content.length < 1) {
                $('.row').html('<p style="text-align:center">' + localizeString("GlofTournaments_Label_NoTournament") + '</p>');
                return;
            }
            for (i = 0; i < data.content.length; i++) {
                var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoTournamentDetails(' + data.content[i].id + ')"></div>');
                var div2 = $('<div class="card-01"></div>');
                var imagefortournament = getTournamentImage(data.imageBasePath, data.content[i].image);
                var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
                var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
                aside.append(imgDiv);
                var div3 = $('<div class="card-main card-width"></div>');
                var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
                var tournmanentTitle = data.content[i].title;
                var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

                var div5 = $('<div class="blog-post-meta" style="padding-top:6px!important;"></div>');
                var span1 = $('<span></span>');
                var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + getDisplayableDate(data.content[i].start_date) + '</i></br>');
                var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + getDisplayableDate(data.content[i].end_date) + ' </i></br>');
                if (data.content[i].number_of_events > 1)
                    var ielem3 = $('<i>' + data.content[i].number_of_events + ' Events</i>');
                else
                    var ielem3 = $('<i>' + data.content[i].number_of_events + ' Event</i>');
                span1.append(ielem1, ielem2, ielem3);
                div5.append(span1);
                div4.append(p1, div5);
                div3.append(div4);
                div2.append(aside, div3);
                div1.append(div2);
                $('.row').append(div1);
            }
            lazyLoadListImages(page_number, data.content.length);
        }
        /****************************** tournaments.html ends *****************************/

    /**
     * Go to the details page of specific tournament
     * @tournament_id Id of the tournament whose details are required
     */
    gotoTournamentDetails = function(tournament_id) {
        localStorage.tournament_id = tournament_id;
        slideview('tournament-details.html', 'script', 'left');
    }

    /**
     * fetch the details & slot information of specific tournament
     * @tournament_id Id of the tournament whose details are required
     */
    getTournamentDetails = function() {
        var actions = {
            "type": "0",
            "id": localStorage.tournament_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                displayTournamentDetails(data.data);
            } else {
                var thisone = this;

                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/tournamentDetails", actions, successCallback, errorCallback);
    }


    resetErrorMessage = function() {
        $("#eventerror").css("display", "none");
        var eventId = $("#event-type option:selected").val();
        if (false == validateEventSelection(eventId)) {
            $("#eventerror").css("display", "inline");
            $("#eventerror").html(localizeString("ValidationError_EventType") + '<i class="form-help-icon icon">error</i>');
            //remove slot selection div if it avaliable
            $("#slot-picker-form").css("display", "none");
            return;
        }
        var eventList = JSON.parse(localStorage.getItem("tournamentEventList"));
        var divSlotSelect = $('<div class="form-group form-group-label control-highlight control-focus" id="slot-picker-form" style="height:45px; width:100%; float:left; margin-top:5px; padding-left:10px; padding-right:10px;"><div class="row"><div class="col-lg-6 col-sm-12"> <label class="floating-label" style="width: 150px;" for="slot-picker" id="Tournament_Button_SelectSlot" name="localizeMandatoryElements">Select Slot*</label> <select class="form-control" onchange="resetSlotErrorMessage()" id="slot-picker"> <option value="0" id="Tournament_Button_SelectSlot" name="LocalizeElement">Select Slot</option></select> <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="slotsuccess"><i class="form-help-icon icon">check</i></span> <span class="form-help form-help-msg text-red" style="float:left;margin-top:0px; margin-bottom:0px; display:none;" id="sloterror"><i class="form-help-icon icon">error</i></span></div></div></div>');
        $('#toureventsslots').html(divSlotSelect);
        for (var i = 0; i < eventList.length; i++) {
            if (eventList[i].id == eventId) {
                for (var j = 0; j < eventList[i].slots.length; j++) {
                    option = document.createElement('option');
                    option.setAttribute('value', eventList[i].slots[j].slot_id);
                    option.appendChild(document.createTextNode(eventList[i].slots[j].slot));
                    $("#slot-picker").append(option);
                }
                localStorage.setItem("tournamentEventSelected", JSON.stringify(eventList[i]));
            }
        }
    }

    resetSlotErrorMessage = function() {
        $("#sloterror").css("display", "none");
        var slotId = $("#slot-picker option:selected").val();
        if (false == validateEventSelection(slotId)) {
            $("#sloterror").css("display", "inline");
            $("#sloterror").html(localizeString("ValidationError_SlotSelection") + '<i class="form-help-icon icon">error</i>');
            return;
        }
        localStorage.event_id = $("#event-type option:selected").val();
        localStorage.slot_id = slotId;
        slideview('tournament-details-register.html', 'script', 'left');
    }

    validateEventSelection = function(optionVal) {
        if (optionVal == 0)
            return false;
        return true;
    }

    /**
     * Renders the tournament slot details
     * @details tournament details & slot information for each torunament events
     */
    displayTournamentDetails = function(details) {
            if (enableDebugMode)
                alert("displayTournamentDetails = " + JSON.stringify(details));

            if (details.content.image != undefined && details.content.image != null &&
                details.content.image.trim() != "") {
                $('#topbanner').attr({
                    'src': details.imageBasePath + details.content.image
                });
                $("#topbanner").css("display", "block");
                localStorage.setItem("tournamentBannerImage", details.imageBasePath + details.content.image);
            } else {
                $("#topbanner").css("display", "none");
                localStorage.setItem("tournamentBannerImage", "");
            }
            var tr1 = $('<tr></tr>');
            var td1 = $('<td colspan="2" class="wysiwyg-img"></td>');
            var p1 = $('<p class="text-bold-01" style="margin-left: 5px;font-size:20px; margin-bottom:5px;" id="title"></p>').text(details.content.tournament_title);
            localStorage.setItem("tournamentName", details.content.tournament_title);
            var span1 = $('<span style=" font-size:12px;"></span>');
            var date = localizeString("Label_Starts") + ': ' + getDisplayableDate(details.content.start_date);
            var ielem2 = $('<i class="fa fa-clock-o"></i>');
            var ielem21 = $('<i class="fa fa-clock-o"></i>');
            var date1 = localizeString("Label_Ends") + ': &nbsp;&nbsp;' + getDisplayableDate(details.content.end_date);
            span1.append("&nbsp;&nbsp;", ielem2, "&nbsp;&nbsp;", date, "<br/>");
            span1.append("&nbsp;&nbsp;", ielem21, "&nbsp;&nbsp;", date1);
            var p2 = $('<div class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + details.content.description + '</div>');
            td1.append(p1, span1, p2);
            tr1.append(td1);
            var tr2 = $('<tr></tr>');
            var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
            $("#buttontr").css('display', 'none');
            tr2.append(td2);
            tr1.insertBefore('#buttontr');
            tr2.insertBefore('#buttontr');

            var key, count = 0;
            var eventids;
            if ((localStorage.getItem("isLoggedIn") == "true") &&
                (localStorage.getItem("tournamentRegistrationEnabled") == "true")) {
                if (details.content.number_of_events > 0) {
                    localStorage.setItem("tournamentEventList", JSON.stringify(details.content.event_list));
                    var divEventSelect = $('<div class="form-group form-group-label control-highlight control-focus" id="event-picker-form" style="height:45px; width:100%; float:left; margin-top:5px; padding-left:10px; padding-right:10px;"><div class="row"><div class="col-lg-6 col-sm-12"> <label class="floating-label" style="width: 150px;" for="event-type" id="GolfTournamentsDetails_Label_EventType" name="localizeMandatoryElements">Event Type*</label> <select class="form-control" onchange="resetErrorMessage()" id="event-type"> <option value="0" style="display:none;" id="GolfTournamentsDetails_Label_SelectEvent" name="LocalizeElement">Select Event Type</option></select> <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="eventsuccess"><i class="form-help-icon icon">check</i></span> <span class="form-help form-help-msg text-red" style="float:left;margin-top:0px; margin-bottom:0px; display:none;" id="eventerror"><i class="form-help-icon icon">error</i></span></div></div></div>');
                    $('#tourevents').append(divEventSelect);
                    for (var i = 0; i < details.content.event_list.length; i++) {
                        var tournamenteventid = details.content.event_list[i].id;
                        var tournamenteventname = details.content.event_list[i].name;
                        option = document.createElement('option');
                        option.setAttribute('value', details.content.event_list[i].id);
                        option.appendChild(document.createTextNode(details.content.event_list[i].name));
                        $("#event-type").append(option);
                    }
                } else {
                    //$('#tourevents').html("<p align='center' class='text-bold-01' style='font-size:18px; margin-bottom:5px;'>" + localizeString("Tournament_Registration_Details_Not_Available") + "</p>");
                    $('#tourevents').html("<p align='center' class='text-bold-01' style='font-size:18px; margin-bottom:5px;'></p>");
                }
            } else { // user not logged in case
                $('#tourevents').html("<p align='center' class='text-bold-01' style='font-size:18px; margin-bottom:5px;'>" + localizeString("Tournament_Need_To_Login") + "</p>");
            }
        }
        /****************************** tournament-details.html ends *****************************/


    jQuery.fn.shake = function() {
        this.each(function(i) {
            $(this).css({
                "position": "relative"
            });
            for (var x = 1; x <= 3; x++) {
                $(this).animate({
                    left: -25
                }, 30).animate({
                    left: 0
                }, 30).animate({
                    left: 25
                }, 30).animate({
                    left: 0
                }, 30);
            }
        });
        return this;
    }

    addPlayerPopup = function(tournament_event_id) {
        $("#eventidforpopup").val(tournament_event_id);
    }

    addPlayerToPlayerList = function(name, type, userid, gender, tournamenteventidfrompopup) {
        if ((name.replace(/\s+/, "") != "")) {
            var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
            var tempjson = {
                "player": playerlist.players.length + 1,
                "name": name,
                "category": type,
                "id": userid,
                "gender": gender
            };
            var obj = playerlist;

            obj['players'].push(tempjson);
            var jsonStr = JSON.stringify(obj);
            localStorage.setItem("tournamentplayerlist", jsonStr);
            showLastPlayerAddedtoList(obj, tournamenteventidfrompopup);
            $("#float-text").val("");
            $("#user_id").val("");
            $("#BlogDetail_Button_Close").click();
        } else {
            if ((name.replace(/\s+/, "") == "")) {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
            }
        }
    }


    showLastPlayerAddedtoList = function(playerslist, tournamenteventidfrompopupshow) {
        var name, teeboxes;
        if (playerslist.players.length > 0) {
            var i = (playerslist.players.length - 1);
            name = playerslist.players[i].name;
            teeboxes = playerslist.players[i].category;
            var maindiv = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
            var maindiv1 = $('<div class="card"></div>');
            var maindiv2 = $('<div class="card-main"> </div>');
            var carddiv = $('<div class="card-header"></div>');
            var div1 = $('<div class="card-header-side pull-left"><div class="avatar"> <img alt="John Smith Avatar" src="images/avatar-001.jpg"> </div></div>');
            var div2 = $('<div class="card-inner"> <span class="bold-match-color" style="line-height:55px;">' + name + '</span><a href="#" class="close-btn" onclick="removePlayerFromPlayerList(\'' + i + '\',\'' + tournamenteventidfrompopupshow + '\');"><i class="fa fa-close"></i></a> </div>');
            var div3 = $('<div class="card-inner"><div class="text" style="line-height:13px;"><span style="float:right;"> Category:' + teeboxes + '</span></div></div>');
            var td = $('<td></td>');
            var tr = $('<tr></tr>');
            carddiv.append(div1, div2);
            maindiv2.append(carddiv, div3);
            maindiv1.append(maindiv2)
            maindiv.append(maindiv1)
            $('#user-list' + tournamenteventidfrompopupshow).append(maindiv);
            $('#players' + tournamenteventidfrompopupshow).css('display', 'block');
        }
    }


    removePlayerFromPlayerList = function(index, tournamenteventidfrompopupshow) {
        var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
        var obj = playerlist.players
        obj.splice(index, 1);
        playerlist.players = obj;
        var jsonStr = JSON.stringify(playerlist);
        localStorage.setItem("tournamentplayerlist", jsonStr);
        showAllPlayerAddedtoListRegistration(playerlist, tournamenteventidfrompopupshow);
    }


    showAllPlayerAddedtoList = function(playerslist, tournamenteventidfrompopupshow) {
        $('#user-list' + tournamenteventidfrompopupshow).html("");
        var name, teeboxes;
        for (i = 0; i < JSONVal.players.length; i++) {
            name = JSONVal.players[i].name;
            teeboxes = JSONVal.players[i].category;
            var maindiv = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
            var maindiv1 = $('<div class="card"></div>');
            var maindiv2 = $('<div class="card-main"> </div>');
            var carddiv = $('<div class="card-header"></div>');
            var div1 = $('<div class="card-header-side pull-left"><div class="avatar"> <img alt="John Smith Avatar" src="images/avatar-001.jpg"> </div></div>');
            var div2 = $('<div class="card-inner"> <span class="bold-match-color" style="line-height:55px;">' + name + '</span><a href="#" class="close-btn" onclick="removePlayerFromPlayerList(\'' + i + '\',\'' + tournamenteventidfrompopupshow + '\');"><i class="fa fa-close"></i></a> </div>');
            var div3 = $('<div class="card-inner"><div class="text" style="line-height:13px;"><span style="float:right;"> Category:' + teeboxes + '</span></div></div>');
            var td = $('<td></td>');
            var tr = $('<tr></tr>');
            carddiv.append(div1, div2);
            maindiv2.append(carddiv, div3);
            maindiv1.append(maindiv2)
            maindiv.append(maindiv1)
            $('#user-list' + tournamenteventidfrompopupshow).append(maindiv);
            $('#players' + tournamenteventidfrompopupshow).css('display', 'block');
        }
    }

    disms = function() {
        $("#float-text").val("");
        $("#user_id").val("");
        $("#nameerror").css("display", "none");
    }

    /****************************** tournament-details.html ends *****************************/


    tournamentRegisterLoad = function() {
        localStorage.setItem("tournamentplayerlist", [])
        var playerlist = {
            "players": []
        };
        team_size = 0;
        seats_available = 0;
        event_name = "";
        slot_name = "";
        tournament_type = "";
        localStorage.setItem("tournamentplayerlist", JSON.stringify(playerlist));
        var eventDetails = JSON.parse(localStorage.getItem("tournamentEventSelected"));
        $("#tourname").text(localStorage.getItem("tournamentName"));
        $("#eventname").text("Event : " + eventDetails.name);
        $("#teamsize").html("Team Size : " + eventDetails.team_size + " &nbsp;&nbsp;");
        team_size = eventDetails.team_size;

        for (var i = 0; i < eventDetails.slots.length; i++) {
            if (eventDetails.slots[i].slot_id == localStorage.slot_id) {
                $("#slotname").text("Slot : " + eventDetails.slots[i].slot);
                $("#slotsleft").text("Seats left : " + eventDetails.slots[i].seats_available);
                seats_available = eventDetails.slots[i].seats_available;
                break;
            }
        }
        if (seats_available > 0) {
            if (team_size < 1) {
                $('#addplayerbutton').css('display', 'none');
            } else {
                $('#addplayerbutton').css('display', 'block');
            }
        } else {
            $('#addplayerbutton').css('display', 'none');
            $('#noavailability').html("<p align='center' class='text-bold-01' style='font-size:22px; margin-bottom:5px;'>" + localizeString("Tournament_No_Seats_Left") + "</p>");
            $('#noavailability').css('display', 'block');
        }
        var img = localStorage.getItem("tournamentBannerImage");
        if (img != null && img != "") {
            $('#topbanner').attr({
                'src': img
            });
            $("#topbanner").css("display", "block");
        }
    }


    isNumberKey = function(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }


    $("#float-text").on('keyup', function() {
        $('#userlistdiv').show(250);
        if ($(this).val() != "") {
            searchUsers($(this).val());
        } else {
            $('#userlistdiv').html("<ul></ul>");
        }
    });


    $('html').click(function() {
        if ($('#userlistdiv').html() != "<ul></ul>") {
            $('#userlistdiv').html("<ul></ul>");
            $('#userlistdiv').hide(250);
        }
    });


    $("#float-text").on('keyup', function() {
        if ($("#float-text").val().length > 0) {
            $("#nameerror").css("display", "none");
            $("#nameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#nameerror").css("display", "block");
            $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });


    /**
     * Search users by name
     */
    searchUsers = function(name_by) {
        var actions = {
            "page_number": "1",
            "needle": name_by
        };

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                var thelist = "";
                var obj = removeItem(data.data, 'id', JSON.parse(localStorage.getItem("tournamentplayerlist")).players);
                $('#userlistdiv').html("<ul>" + thelist + "</ul>");
                for (var i = 0; i < obj.length; i++) {
                    thelist += '<li><a href="javascript:void();" onclick="selectUser(\'' + obj[i].id + '\',\'' + obj[i].first_name + ' ' + obj[i].last_name + '\',\'' + obj[i].gender + '\')">' + obj[i].first_name + ' ' + obj[i].last_name + '(' + obj[i].membership_number + ')</a></li>';
                }
                if (obj.length > 0) {
                    $('#userlistdiv').show(250);
                } else {
                    $('#userlistdiv').hide(50);
                }
                $('#userlistdiv').html("<ul>" + thelist + "</ul>");
            } else {
                $('#userlistdiv').html("<ul></ul>");
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            //window.plugins.spinnerDialog.hide();
        }
        errorCallback = function(data) {};

        postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
    }


    selectUser = function(the_id, the_name, the_gender) {
        if (the_name != null) {
            document.getElementById("float-text").value = the_name;
        }
        if (the_id != null) {
            $("#user_id").val(the_id);
        }
        if (the_gender != null) {
            $("#gender").val(the_gender);
        }
    }


    addPlayerToPlayerListRegistration = function(name, userid, calledFrom) {
        if (tournament_type == 1) {
            if ((name.replace(/\s+/, "") != "")) {
                var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
                var tempjson = {
                    "player": playerlist.players.length + 1,
                    "name": name,
                    "category": "",
                    "id": userid,
                    "gender": ""
                };
                var obj = playerlist;

                obj['players'].push(tempjson);
                var jsonStr = JSON.stringify(obj);
                localStorage.setItem("tournamentplayerlist", jsonStr);
                if (calledFrom == 1) {
                    showPlayerAddedtoListRegistration(obj);
                } else if (calledFrom == 2) {
                    showPlayerAddedtoListRegistrationHistory(obj);
                }
                $("#float-text").val("");
                $("#user_id").val("");
                $("#dialog_dismiss").click();
            } else {
                if ((name.replace(/\s+/, "") == "")) {
                    $("#nameerror").css("display", "block");
                    $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
                }
            }
        } else {
            if ((name.replace(/\s+/, "") == "")) {
                $("#nameerror").css("display", "inline");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
            } else {
                var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
                var tempjson = {
                    "player": playerlist.players.length + 1,
                    "name": name,
                    "category": "",
                    "id": userid,
                    "gender": ""
                };
                var obj = playerlist;
                obj['players'].push(tempjson);
                var jsonStr = JSON.stringify(obj);
                localStorage.setItem("tournamentplayerlist", jsonStr);
                if (calledFrom == 1) {
                    showPlayerAddedtoListRegistration(obj);
                } else if (calledFrom == 2) {
                    showPlayerAddedtoListRegistrationHistory(obj);
                } else {
                    //alert("wrong call");
                }
                $("#float-text").val("");
                $("#user_id").val("");
                $("#dialog_dismiss").click();
            }
        }
    }


    removeItem = function(obj, prop, list) {
        var c;
        var found = false;
        var newobj;
        newobj = obj;

        for (var i = 0; i < list.length; i++) {
            for (c in obj) {
                if (list[i]['name'].startsWith(obj[c]['first_name'])) {
                    found = true;
                    break;
                }
            }
            if (found) {
                delete newobj[c];
                found = false;
            }
        }

        return newobj.filter(function(x) {
            return x !== null
        });;
    }


    showPlayerAddedtoListRegistration = function(playerslist) {
        $('#user-list').html("");
        var name, teeboxes;

        for (i = 0; i < playerslist.players.length; i++) {
            name = playerslist.players[i].name;
            teeboxes = playerslist.players[i].category;
            var maindiv = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
            var maindiv1 = $('<div class="card"></div>');
            var maindiv2 = $('<div class="card-main"> </div>');
            var carddiv = $('<div class="card-header"></div>');
            var div1 = $('<div class="card-header-side pull-left"><div class="avatar"> <img alt="John Smith Avatar" src="images/avatar-001.jpg"> </div></div>');
            var div2 = $('<div class="card-inner"> <span class="bold-match-color" style="line-height:55px;">' + name + '</span><a href="#" class="close-btn" onclick="removePlayerFromPlayerListRegistration(\'' + i + '\', \'1\');"><i class="fa fa-close"></i></a> </div>');
            var div3 = $('<div class="card-inner"></div>');
            var td = $('<td></td>');
            var tr = $('<tr></tr>');
            carddiv.append(div1, div2);
            if (tournament_type == 1) {
                maindiv2.append(carddiv, div3);
            } else {
                maindiv2.append(carddiv);
            }
            maindiv1.append(maindiv2)
            maindiv.append(maindiv1)
            $('#user-list').append(maindiv);
            $('#players').css('display', 'block');
        }
        if (playerslist.players.length >= team_size || team_size < 1) {
            $('#addplayerbutton').css('display', 'none');
        } else {
            $('#addplayerbutton').css('display', 'block');
        }
        if (playerslist.players.length == team_size) {
            $('#registerplayer').css('display', 'block');
        } else {
            $('#registerplayer').css('display', 'none');
        }
    }

    removePlayerFromPlayerListRegistration = function(index, calledFrom) {
        var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
        var obj = playerlist.players
        obj.splice(index, 1);
        playerlist.players = obj;
        var jsonStr = JSON.stringify(playerlist);
        localStorage.setItem("tournamentplayerlist", jsonStr);
        if (calledFrom == 1) {
            showPlayerAddedtoListRegistration(playerlist);
        } else if (calledFrom == 2) {
            showPlayerAddedtoListRegistrationHistory(playerlist);
        }
    }

    /**
     * Player Registration for the tournament event
     */
    playerRegistration = function() {
        var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
        var actions = {
            "team": playerlist['players'],
            "tournament_id": localStorage.tournament_id,
            "event_id": localStorage.event_id,
            "slot_id": localStorage.slot_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                document.getElementById('registerplayer').disabled = true;
                swal({
                        title: localizeString("Alert_Title_Done"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: localizeString("Alert_Button_Ok"),
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        closeOnConfirm: false
                    },
                    function() {
                        slideview('tournaments-my-history.html', 'script', 'right');
                    });
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_TryAgain"),
                        cancelButtonText: localizeString("Alert_Button_Cancel")
                    },
                    function() {
                        $.ajax(thisone);
                        return;
                    });
            }
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/tournaments/registeration", actions, successCallback, errorCallback);
    }

    /****************************** tournament-details-register.html ends *****************************/

    tournamentHistoryUpdateLoad = function() {
        localStorage.setItem("tournamentplayerlist", []);
        team_size = 0;
        seats_available = 0;
        event_name = "";
        slot_name = "";
        tournament_type = "";
        booking_id = "";
        status = 0; //Pending
        booking_id = localStorage.booking_id;
        var data = JSON.parse(localStorage.getItem("myhistorydata"));
        jQuery.each(data.data.content, function() {
            if (this.id == localStorage.booking_id) {
                team_size = "";
                team_size = this.team.length;
                if (team_size < 1) {
                    $('#addplayerbutton').css('display', 'none');
                } else {
                    $('#addplayerbutton').css('display', 'block');
                }
                $("#tourname").text(this.title);
                $("#eventname").text(localizeString("Label_Event") + ": " + this.name);
                $("#slotname").text(localizeString("Label_Slot") + ": " + this.slot);
                $("#start_date").html(localizeString("Label_StartDate") + ": " + getDisplayableDate(this.start_date) + "  ");
                $("#teamsize").html(localizeString("Label_TeamSize") + ": " + this.team.length + "  ");
                $("#createdby").html(localizeString("Label_CreatedBy") + ': ' + this.created_by + " ");
                var dispDat = getDisplayableDate(this.created_on.split(' ')[0]);
                var dispTim = getDisplayableTime(this.created_on.split(' ')[1]);
                $("#createdon").html(localizeString("Label_CreatedOn") + ': ' + [dispDat, dispTim].join(' ') + "  ");
                $("#cancelbtn").css("display", "block");
                var currentStatus = "";
                status = this.status;
                if (this.status == 0) {
                    currentStatus = localizeString("GolfTournaments_Label_RegStatusPending");
                } else if (this.status == 1) {
                    $("#updateplayer").css("display", "none");
                    currentStatus = localizeString("GolfTournaments_Label_RegStatusApproved");
                } else if (this.status == 2) {
                    currentStatus = localizeString("GolfTournaments_Label_RegStatusRejected");
                    $("#cancelbtn").css("display", "none");
                    $("#updateplayer").css("display", "none");
                } else if (this.status == 3) {
                    currentStatus = localizeString("GolfTournaments_Label_RegStatusCancelled");
                    $("#cancelbtn").css("display", "none");
                    $("#updateplayer").css("display", "none");
                }
                $("#status").html("Registration status: " + currentStatus);
                tournament_type = this.type;
                var playerlist = {
                    "players": this.team
                };
                localStorage.setItem("tournamentplayerlist", JSON.stringify(playerlist));
                showPlayerAddedtoListRegistrationHistory(playerlist);
                if (this.image !== false && this.image !== null) {
                    $('#topbanner').attr({
                        'src': data.data.imageBasePath + this.image
                    });
                    $("#topbanner").css("display", "block");
                }

                if (this.status == 1) { //Aproved
                    var sDate = null;
                    var sTime = null;
                    var eDate = null;
                    var eTime = null;
                    var tournament_name = this.title + ": " + this.name;
                    var slot_time = this.slot.split(" ");
                    sDate = slot_time[0];
                    eDate = slot_time[0];
                    sTime = slot_time[1];
                    var location = "";
                    var notes = "";
                    tournament_name = tournament_name.replace(/'/g, "\\'").replace(/"/g, "&quot;");
                    var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + tournament_name + '\', \'' + location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
                    $("#reminderspan").append(reminder);
                }
            }
        });
    }

    showPlayerAddedtoListRegistrationHistory = function(playerslist) {
        $('#user-list').html("");
        var name, teeboxes;
        for (i = 0; i < playerslist.players.length; i++) {
            name = playerslist.players[i].name;
            teeboxes = playerslist.players[i].category;
            var maindiv = $('<div class="col-lg-3 col-md-4 col-sm-6"></div>');
            var maindiv1 = $('<div class="card"></div>');
            var maindiv2 = $('<div class="card-main"> </div>');
            var carddiv = $('<div class="card-header"></div>');
            var div1 = $('<div class="card-header-side pull-left"><div class="avatar"> <img alt="No Image" src="images/avatar-001.jpg"> </div></div>');
            var div2;

            if (status == 0) {
                div2 = $('<div class="card-inner"> <span class="bold-match-color" style="line-height:55px;">' + name + '</span><a href="#" class="close-btn" onclick="removePlayerFromPlayerListRegistration(\'' + i + '\', \'2\');"><i class="fa fa-close"></i></a> </div>');
            } else {
                div2 = $('<div class="card-inner"> <span class="bold-match-color" style="line-height:55px;">' + name + '</span> </div>');
            }
            var div3 = $('<div class="card-inner"></div>');
            var td = $('<td></td>');
            var tr = $('<tr></tr>');
            carddiv.append(div1, div2);
            if (tournament_type == 1) {
                maindiv2.append(carddiv, div3);
            } else {
                maindiv2.append(carddiv);
            }
            maindiv1.append(maindiv2)
            maindiv.append(maindiv1)
            $('#user-list').append(maindiv);
            $('#players').css('display', 'block');
        }
        if (playerslist.players.length >= team_size || team_size < 1) {
            $('#addplayerbutton').css('display', 'none');
        } else {
            $('#addplayerbutton').css('display', 'block');
        }

        if (playerslist.players.length == team_size && status == 0) {
            $('#updateplayer').css('display', 'block');
        } else {
            $('#updateplayer').css('display', 'none');
        }
    }

    /**
     * Update player registration for the tournament
     */
    playerRegistrationUpdate = function() {
        var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
        var actions = {
            "team": playerlist['players'],
            "event_id": localStorage.booking_event_id,
            "registration_id": localStorage.booking_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Done"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: localizeString("Alert_Button_Ok"),
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        closeOnConfirm: false
                    },
                    function() {
                        slideview('tournaments-my-history.html', 'script', 'right');
                    });
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_TryAgain"),
                        cancelButtonText: localizeString("Alert_Button_Cancel")
                    },
                    function() {
                        $.ajax(this);
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/tournaments/registration/update", actions, successCallback, errorCallback);
    }

    playerRegistrationCancel = function() {
        swal({
                title: localizeString("Alert_Title_CancelBooking"),
                text: localizeString("Alert_Text_CancelBooking"),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                cancelButtonText: localizeString("Alert_Button_NotNow"),
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    playerRegistrationCancelConfirm();
                } else {
                    swal.close();
                }
            });
    }

    /**
     * Cancel Player registration for the tournament
     */
    playerRegistrationCancelConfirm = function() {
        var playerlist = JSON.parse(localStorage.getItem("tournamentplayerlist"));
        var actions = {
            "registration_id": localStorage.booking_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Cancel"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Done"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonText: localizeString("Alert_Button_Ok"),
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                        closeOnConfirm: false
                    },
                    function() {
                        slideview('tournaments-my-history.html', 'script', 'right');
                    });
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "info",
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_TryAgain"),
                        cancelButtonText: localizeString("Alert_Button_Cancel")
                    },
                    function() {
                        $.ajax(this);
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/tournaments/registration/cancel", actions, successCallback, errorCallback);
    }

    /****************************** tournament-my-history-update.html ends *****************************/

    /**
     * Fetch Users Tournament registration history from the server
     */
    getMyTournamentHistory = function() {
        var actions = null;
        //window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                localStorage.setItem("myhistorydata", JSON.stringify(data));
                displayMyTournamentHistory(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/tournaments/history/user", actions, successCallback, errorCallback);
    }

    /**
     * Display Users tournaments registration history
     * @tournamentHistory History data recieved from server
     */
    displayMyTournamentHistory = function(tournamentHistory) {
        if (tournamentHistory.content.length < 1) {
            $('.row').html('<p style="text-align:center">' + localizeString("Tournaments_Label_NoRegister") + '</p>');
            return;
        }
        for (i = 0; i < tournamentHistory.content.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoMyTournamentHistoryDetail(' + tournamentHistory.content[i].id + ', ' + tournamentHistory.content[i].event_id + ')"></div>');
            var div2 = $('<div class="card-01"></div>');
            var imagefortournament = "images/default-listing.png";
            if (tournamentHistory.content[i].image != null && tournamentHistory.content[i].image != "")
                imagefortournament = tournamentHistory.imageBasePath + tournamentHistory.content[i].image;
            var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
            aside.append(imgDiv);
            var div3 = $('<div class="card-main card-width"></div>');
            var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
            var tournmanentTitle = tournamentHistory.content[i].title;
            var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

            var div5 = $('<div class="blog-post-meta" style="padding-top:6px!important;"></div>');
            var span1 = $('<span></span>');

            var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + getDisplayableDate(tournamentHistory.content[i].start_date) + '</i></br>');
            var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + getDisplayableDate(tournamentHistory.content[i].end_date) + ' </i>');
            span1.append(ielem1, ielem2);
            div5.append(span1);
            div4.append(p1, div5);
            div3.append(div4);
            div2.append(aside, div3);
            div1.append(div2);
            $('.row').append(div1);
        }
        lazyLoadListImages(page_number, tournamentHistory.content.length);
    }

    gotoMyTournamentHistoryDetail = function(booking_id, event_id /*, tournamentHistory*/ ) {
        localStorage.booking_id = booking_id;
        localStorage.booking_event_id = event_id;
        slideview('tournament-my-history-update.html', 'script', 'left');
    }

    getTournamentImage = function(base_path, tournament_img) {
        if (tournament_img !== false && tournament_img !== null &&
            tournament_img !== "") {
            return base_path + tournament_img;
        } else {
            return "images/default-listing.png";
        }
    }

    /****************************** tournament-my-history.html ends *****************************/

    /**
     * Fetch all tournament results list from the server
     */
    getTournamentResults = function() {
        var actions = {
            "club_id": localStorage.getItem("GOLF_CLUB_ID")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                localStorage.setItem("tournament_result_data", JSON.stringify(data.data));
                displayTournamentResult(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/userscorecard", actions, successCallback, errorCallback);
    }


    /**
     * Render the tournament results received from the server
     * @resultData tournament result data
     */
    displayTournamentResult = function(resultData) {
        if (enableDebugMode)
            alert("result Data = " + JSON.stringify(resultData));
        if (resultData.content.length < 1) {
            $('.row').html('<p style="text-align:center">' + localizeString("Tournament_Label_NoData") + '</p>');
            return;
        }
        for (i = 0; i < resultData.content.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoTournamentResultDetail(' + resultData.content[i].id + ')"></div>');
            var div2 = $('<div class="card-01"></div>');
            var imagefortournament = "images/default-listing.png";
            if (resultData.content[i].image != null && resultData.content[i].image != "")
                imagefortournament = resultData.imageBasePath + resultData.content[i].image;
            var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
            aside.append(imgDiv);
            var div3 = $('<div class="card-main card-width"></div>');
            var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
            var tournmanentTitle = resultData.content[i].title;
            var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

            var div5 = $('<div class="blog-post-meta"></div>');
            var span1 = $('<span></span>');

            var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + getDisplayableDate(resultData.content[i].start_date) + '</i></br>');
            var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + getDisplayableDate(resultData.content[i].end_date) + '</i></br>');
            if (resultData.content[i].number_of_events > 1)
                var ielem3 = $('<i>' + resultData.content[i].number_of_events + ' Events</i>');
            else
                var ielem3 = $('<i>' + resultData.content[i].number_of_events + ' Event</i>');
            span1.append(ielem1, ielem2, ielem3);
            div5.append(span1);
            div4.append(p1, div5);
            div3.append(div4);
            div2.append(aside, div3);
            div1.append(div2);
            $('.row').append(div1);
        }
        lazyLoadListImages(page_number, resultData.content.length);
    }

    gotoTournamentResultDetail = function(tournament_id) {
            localStorage.tournament_id = tournament_id;
            slideview('tournament-results-details.html', 'script', 'left');
        }
        /****************************** tournament-result.html ends *****************************/

    /**
     * Fetch all tournament results list from the server
     */
    getTournamentResultDetails = function() {
        var actions = {
            "id": localStorage.tournament_id
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                displayTournamentResultDetails(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/tournamentsForResult", actions, successCallback, errorCallback);
    }

    displayTournamentResultDetails = function(resultData) {
        if (enableDebugMode)
            alert("Tournament Result Data = " + JSON.stringify(resultData));
        $("#tourname").text(resultData.content[0].title);
        $("#start_date").html("<strong>" + localizeString("Label_StartDate") + ": </strong>" + getDisplayableDate(resultData.content[0].start_date) + "  ");
        $("#end_date").html("<strong>" + localizeString("Label_EndDate") + ": </strong>  " + getDisplayableDate(resultData.content[0].end_date) + "  ");
        $("#total_events").html("<strong>" + localizeString("Label_TotalEvents") + ": </strong>" + resultData.content[0].number_of_events + "  ");
        $("#desc").html(resultData.content[0].description);
        if (resultData.content[0].image != undefined && resultData.content[0].image != null &&
            resultData.content[0].image.trim() != "") {
            $('#topbanner').attr({
                'src': resultData.imageBasePath + resultData.content[0].image
            });
            $("#topbanner").css("display", "block");
        } else {
            $("#topbanner").css("display", "none");
        }
        if (resultData.content[0].result != null && resultData.content[0].result != undefined &&
            resultData.content[0].result.trim() != "")
            $("#tournamentResults").html(resultData.content[0].result);
        else
            $("#tournamentResults").css("display", "none");
    }

    getGolfLeaderBoard = function(tournament_id, event_id) {
        var actions = {
            "tournamentId": tournament_id,
            "eventId": event_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_LeaderBoard"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayLeaderBoardForTournament(data.data.content);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/golfLeaderBoard", actions, successCallback, errorCallback);
    }


    displayLeaderBoardForTournament = function(leaderBoardContent) {
        for (var i = 0; i < leaderBoardContent.length; i++) {
            var row = $("<tr> id='" + i + "'" + " style='color:#fff;' onclick=gotoAllHoleScore(" + leaderBoardContent[i].id + ") </tr>");
            $("#leaderboard_table").append(row);
            row.append($("<td>" + leaderBoardContent[i].rank + "</td>"));
            row.append($("<td>" + leaderBoardContent[i].name + "</td>"));
            row.append($("<td>" + leaderBoardContent[i].score + "</td>"));
        }
    }


    gotoAllHoleScore = function(playerId) {}

    /****************************** tournament-result-details.html ends *****************************/

    $("#search").keyup(function() {
        localStorage.setItem('numofUsers', '0');
        var keyword = $(this).val();
        scroll = false;
        if (keyword == '') {
            remove = true;
        } else {
            remove = false;
        }
        page_number = 1;

        $(".row").empty();

        //searchTournament(page_number);
    });

    /**
     * Search user in the user list
     * @pg_no page number to begin with
     */
    searchTournament = function(pg_no) {
        //localStorage.setItem('numofUsers', '0');
        var actions = {
            "page_number": pg_no,
            "needle": $('#search').val().trim()
        };

        successCallback = function(data) {
            if (data.status == "success") {
                swal.close();
                if (data.data.length > 0)
                    localStorage.setItem('numofUsers', String(parseInt(localStorage["numofUsers"]) + parseInt(JSON.stringify(data.data.length))));
                displayTournamentList(data.data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofUsers', 0);
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/tournaments", actions, successCallback, errorCallback);
    }
}());