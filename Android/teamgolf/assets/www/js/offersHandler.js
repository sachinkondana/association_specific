(function () {
    var enableDebugMode = 0;
    var page_number = 1;

    "use strict";

    localStorage.setItem("numofOffers", '0');

    $(".content").scroll(function () {
        if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofOffers")) / 15) == page_number) {
                page_number++;
                getOffer(page_number);
            }
        }
    });

    getOffer = function (page_number) {
        var actions = {
            "page_number": page_number,
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if ((page_number == 1) && (data.data.content.length == 0)) {
                    $('#spcontent').html('<p style="text-align:center">' + localizeString("Offers_Label_NoOffers") +'</p>');
                } else if (data.data.content.length > 0) {
                    localStorage.setItem('numofOffers', String(parseInt(localStorage["numofOffers"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayOffers(data.data);
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/offers", actions, successCallback, errorCallback);
    }

    displayOffers = function (offers) {
        var contul = $('<ul class="accordion_example" id="ofrcont"></ul>');
        console.log("offer = " + JSON.stringify(offers));
        for (i = 0; i < offers.content.length; i++) {
            var tr;
            var li = $('<li id="ofr-' + i + '"></li>');
            var div1 = $('<div onclick="return gotoTitleClickHandler(' + i + ', ' + offers.content[i].id + ');"></div>');
            var img = $('<img alt="No Image">').attr({
                'src': 'images/mobicom_icon.png'
            });
            if (offers.content[i].image != null && offers.content[i].image != undefined && offers.content[i].image != "") {
                img = $('<img alt="No Image" style="object-fit: cover">').attr({
                    'src': offers.imageBasePath + offers.content[i].image
                });
            }
            var p = $('<div class="acc_text_head"></div>').text(offers.content[i].title);
            div1.append(img, p);
            var div2 = $('<div class="wysiwyg-img"></div>');
            var spandesc = $('<h5></h5>').text(offers.content[i].title);
            var pdesc = $('<p></p>').text(offers.content[i].Description);
            if (offers.content[i].url != null && offers.content[i].url != undefined
                && offers.content[i].url.trim() != "") {
                var urllink = $('<a onclick="gotoUrlClickHandler(' + offers.content[i].id + ', \'' + offers.content[i].url + '\');">' + localizeString("Media_Button_ViewDetail")+'</a>');
                div2.append(spandesc, pdesc, urllink);
            } else {
                div2.append(spandesc, pdesc);
            }
            li.append(div1, div2);
            contul.append(li);
        }
        $("#spcontent").append(contul);
        $(".accordion_example").smk_Accordion();
    }

    gotoTitleClickHandler = function (ofrindx, offerid) {
        setTimeout(function(){
            if ($("#ofr-" + ofrindx).hasClass("acc_active")) {
                recordClickStats(offerid, 0);
            }
        } , 300);
    }

    gotoUrlClickHandler = function (offerid, url) {
        recordClickStats(offerid, 1);
        gotourl(url);
    }

    recordClickStats = function (offerid, clicked) {
        if (clicked == 1) {
            var actions = {
                "offer_id": offerid,
                "click": clicked
            };
        } else {
            var actions = {
                "offer_id": offerid
            };
        }
        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                console.log("Stats recorded successfully");
            } else {
                var thisone = this;
            }
        };
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/offerstats", actions, successCallback, errorCallback);
    }

}());