(function () {
    var enableDebugMode = 0;
    var tod = new Date();
    var date13bef = tod.setFullYear( tod.getFullYear() - 14 );
    "use strict";
    initializeUserProfile = function () {
        $("#my-account-full-name").html(localStorage.getItem("first_name") + " " + localStorage.getItem("last_name"));
        //$("#my-account-avatar").html(localStorage.getItem("avatar"));

        if (localStorage.getItem("photo") != null && localStorage.getItem("photo") != undefined
            && localStorage.getItem("photo").trim() != "") {
            var img = document.getElementById("photo");
            img.src = "data:image/jpeg;base64," + localStorage.getItem("photo");
            var img = document.getElementById("navphoto");
            img.src = "data:image/jpeg;base64," + localStorage.getItem("photo");
        }

        $("#fname").val(localStorage.getItem("first_name"));
        $("#lname").val(localStorage.getItem("last_name"));
        $("#membershipno").val(localStorage.getItem("membership_number"));
        $("#email").val(localStorage.getItem("email"));
        $("#phone").val(localStorage.getItem("phone"));
        $("#avatar").val(localStorage.getItem("avatar"));
        $("select#gender option")
            .each(function () {
            this.selected = (this.text == localStorage.getItem("gender")); //"selected"
        });
        $("#dob").val(localStorage.getItem("dob"));

        if ((localStorage.getItem("dob") != null) && (localStorage.getItem("dob") != undefined)
            && (localStorage.getItem("dob").trim() != "")) {
            if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                $("#dob").flatpickr({
                    disableMobile: false,
                    defaultDate: localStorage.getItem("dob"),
                    maxDate: date13bef,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        $("#dob").val = dateStr;
                    }
                });
            } else {
                $("#dob").flatpickr({
                    defaultDate: localStorage.getItem("dob"),
                    maxDate: date13bef,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        $("#dob").val = dateStr;
                    }
                });
            }
        } else {
            if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                $("#dob").flatpickr({
                    disableMobile: false,
                    maxDate: date13bef,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        console.log("dateStr = " + dateStr);
                        $("#dob").val = dateStr;
                    }
                });
            } else {
                $("#dob").flatpickr({
                    maxDate: date13bef,
                    shorthandCurrentMonth: true,
                    onChange: function(selectedDates, dateStr, instance) {
                        //...
                        console.log("dateStr = " + dateStr);
                        $("#dob").val = dateStr;
                    }
                });
            }
        }

        if (localStorage.getItem("isPrimary") == 1 || localStorage.getItem("isPrimary") == "1")
            document.getElementById('isprimary').checked = true;
        else
            document.getElementById('issecondary').checked = true;

        if (localStorage.getItem("admin_to_verify") == 0 || localStorage.getItem("admin_to_verify") == "0") {
            document.getElementById('email').disabled = true;
            document.getElementById('membershipno').disabled = true;
            document.getElementById('isprimary').disabled = true;
            document.getElementById('issecondary').disabled = true;
        }
    }

    $('input').each(function (i) {
        $(this).keyup(function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                $('input').eq(i + 1).focus();
            }
        });
    });

    $('input').unbind('focusout');
    $(document).on('focusout', 'input', function () {
        setTimeout(function () {
            window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
        }, 500);
    });

    // Check First Name
    $("#fname").blur(function () {
        if ($("#fname").val().length > 0) {
            $("#fnameerror").css("display", "none");
            $("#fnameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#fnameerror").css("display", "block");
            $("#fnameerror").html(localizeString("ValidationError_FirstnameBlank")+'<i class="form-help-icon icon">error</i>');
        }
    });

    /*$("#lname").blur(function () {
        if ($("#lname").val().length > 0) {
            $("#lnameerror").css("display", "none");
            $("#lnameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#lnameerror").css("display", "block");
            $("#lnameerror").html('Last Name cannot be left blank.<i class="form-help-icon icon">error</i>');
        }
    });*/

    // Check Membership Number
    $("#membershipno").blur(function () {
        if ($("#membershipno").val().trim().length > 0) {
            $("#membershipnoerror").css("display", "none");
            $("#membershipnoerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#membershipnoerror").css("display", "inline");
            $("#membershipnoerror").html(localizeString("ValidationError_MembershipBlank")+'<i class="form-help-icon icon">error</i>');
            $("#membershipno").val('');
        }
    });

    // Check Email
    $("#email").blur(function () {
        if ($("#email").val().trim().length > 0) {
            $("#emailerror").css("display", "none");
            $("#emailerror").html('<i class="form-help-icon icon">error</i>');
            if (emailValidation($("#email").val().trim()) == false) {
                $("#emailerror").css("display", "inline");
                $("#emailerror").html(localizeString("ValidationError_InvalidEmail")+'<i class="form-help-icon icon">error</i>');
            }
        } else {
            $("#emailerror").css("display", "inline");
            $("#emailerror").html(localizeString("ValidationError_EmailBlank")+'<i class="form-help-icon icon">error</i>');
            $("#email").val('');
            $("#password").val('');
            $("#confirmpassword").val('');
        }
    });

    // Check Phone
    $("#phone").blur(function () {
        if ($("#phone").val().length > 0) {
            $("#phoneerror").css("display", "none");
            $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            if (!phoneValidation($("#phone").val())) {
                $("#phoneerror").css("display", "block");
                $("#phoneerror").html(localizeString("ValidationError_InvalidPhone")+'<i class="form-help-icon icon">error</i>');
            }
        }
    });

    // Check Avatar
    $("#avatar").blur(function () {
        if ($("#avatar").val().length > 4) {
            $("#avatarerror").css("display", "none");
            $("#avatarerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#avatarerror").css("display", "block");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort")+'<i class="form-help-icon icon">error</i>');
        }
    });

    $("#avatar").on('keyup', function () {
        var desiredavatar = $("#avatar").val();
        if (desiredavatar.length > 4) {
            var actions = {
                "avatar": desiredavatar
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Register"), localizeString("SpinDialog_Wait"), true);

            successCallback = function (data) {
                window.plugins.spinnerDialog.hide();
                $("#avatarsuccess").css("display", "none");
                $("#avatarerror").css("display", "none");
                if (data.status == "success") {
                    swal.close();
                    $("#avatarsuccess").css("display", "block");
                    $("#avatarsuccess").html(localizeString("ValidationError_DisplayName")+'<i class="form-help-icon icon">check</i>');
                } else {
                    $("#avatarerror").css("display", "block");
                    $("#avatarerror").html(localizeString("ValidationError_NoDisplayName")+'<i class="form-help-icon icon">error</i>');
                }
            };
            errorCallback = function (data) {
                $("#avatarsuccess").css("display", "none");
            };
            postMessageToServerWithAuth("/user/avatar/check", actions, successCallback, errorCallback);
        } else {
            $("#avatarsuccess").css("display", "none");
            $("#avatarerror").css("display", "block");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort")+'<i class="form-help-icon icon">error</i>');
        }
    });

    // Check Gender
    /*$("#gender").blur(function () {
        if ($("#gender").val().trim().length > 0) {
            $("#gendererror").css("display", "none");
            $("#gendererror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#gendererror").css("display", "inline");
            $("#gendererror").html('Gender cannot be left blank.<i class="form-help-icon icon">error</i>');
            $("#gender").val('');
        }
    });*/

    // Check DOB
    $("#dob").change(function () {
        if ($("#dob").val().trim().length > 4) {
            var sel_date_1 = $("#dob").val().split("-");
            if (getAge(sel_date_1) >= 13) {
                $("#dobsuccess").css("display", "none");
                $("#doberror").css("display", "none");
                $("#doberror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#dob").val("");
                $("#doberror").css("display", "block");
                $("#doberror").html(localizeString("ValidationError_Age") + '<i class="form-help-icon icon">error</i>');
                $("#dob").focus();
            }
        } else {
             $("#dob").val(localStorage.getItem("dob"));
            $("#dobsuccess").css("display", "none");
            $("#doberror").css("display", "none");
            $("#doberror").html('<i class="form-help-icon icon">error</i>');
        }
    });


    /**
     * Update user details in the backend
     */
    updateUserDetails = function () {
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var membershipNumber = $("#membershipno").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var avatar = $("#avatar").val();
        var gender = $("#gender").val();
        var dob = $("#dob").val();
        var isPrimary = 0;
        if (document.getElementById('isprimary').checked)
            isPrimary = document.getElementById('isprimary').value;
        else if (document.getElementById('issecondary').checked)
            isPrimary = document.getElementById('issecondary').value;
        var photo = localStorage.getItem("photo");

        var member_id = localStorage.getItem("memberid");
        if (fname.length > 0) {
            $("#fnameerror").css("display", "none");
            $("#fnameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#fnameerror").css("display", "block");
            $("#fnameerror").html(localizeString("ValidationError_FirstnameBlank") + '<i class="form-help-icon icon">error</i>');
            $("#fname").focus();
            return false;
        }
        /*if (lname.length > 0) {
            $("#lasterror").css("display", "none");
            $("#lasterror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#lnameerror").css("display", "block");
            $("#lnameerror").html('First Name cannot be left blank.<i class="form-help-icon icon">error</i>');
            $("#lname").focus();
            return false;
        }*/
        if (membershipNumber.trim().length > 0) {
            $("#membershipnoerror").css("display", "none");
            $("#membershipnoerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#membershipnoerror").css("display", "inline");
            $("#membershipnoerror").html(localizeString("ValidationError_MembershipBlank") + '<i class="form-help-icon icon">error</i>');
            $("#membershipno").focus();
            return false;
        }
        if (emailValidation(email)==true) {
            $( "#emailerror" ).css("display","none");
            $( "#emailerror" ).html('<i class="form-help-icon icon">error</i>');
        } else {
            $( "#emailerror" ).css("display","inline");
            $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            $( "#email" ).focus();
            return false;
        }
        if (phone.trim().length > 0) {
            if (phoneValidation(phone) == true) {
                $("#phoneerror").css("display", "none");
                $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#phoneerror").css("display", "block");
                $("#phoneerror").html(localizeString("ValidationError_InvalidPhone") + '<i class="form-help-icon icon">error</i>');
                $("#phone").focus();
                return false;
            }
        }
        if (avatar.length > 4) {
            $("#avatarsuccess").css("display", "none");
            $("#avatarerror").css("display", "none");
            $("#avatarerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#avatarerror").css("display", "block");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort") + '<i class="form-help-icon icon">error</i>');
            $("#avatar").focus();
            return false;
        }

        if (dob.length > 0) {
            if (dob.length > 4) {
                $("#dobsuccess").css("display", "none");
                $("#doberror").css("display", "none");
                $("#doberror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#doberror").css("display", "block");
                $("#doberror").html(localizeString("ValidationError_DateOfBirth")+'<i class="form-help-icon icon">error</i>');
                $("#dob").focus();
                return false;
            }
        }
        /*if (gender.length > 0) {
            $("#gendersuccess").css("display", "none");
            $("#gendererror").css("display", "none");
            $("#gendererror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#gendererror").css("display", "inline");
            $("#gendererror").html(localizeString("ValidationError_GenderBlank") + '<i class="form-help-icon icon">error</i>');
            $("#gender").focus();
            return false;
        }*/

        var actions = {
            "firstname": fname,
            "lastname": lname,
            "avatar": avatar,
            "phonenumber": phone,
            "email": email,
            "photo": photo,
            "photo_mime_type": "data:image/jpeg;base64",
            "dob": dob,
            "gender": gender,
            "isPrimary": isPrimary,
            "membershipNumber": membershipNumber
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Save"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                localStorage.setItem("phone", phone);
                localStorage.setItem("first_name", fname);
                localStorage.setItem("last_name", lname);
                localStorage.setItem("avatar", avatar);
                localStorage.setItem("gender", gender);
                localStorage.setItem("dob", dob);
                var dnam = fname + " " + lname;
                if (dnam.length < 15) {
                    $("#navfullname").html(dnam);
                } else {
                    var nam = fname + " " + lname.trim().split(" ")[0];
                    if (nam.length < 15)
                        $("#navfullname").html(nam);
                    else
                        $("#navfullname").html(fname);
                }
                //$("#navfullname").html(localStorage.getItem("first_name") + " " + localStorage.getItem("last_name"));
                $("#navdisplayname").html(localStorage.getItem("avatar"));
                $("#my-account-full-name").html(localStorage.getItem("first_name") + " " + localStorage.getItem("last_name"));
                //$("#my-account-avatar").html(localStorage.getItem("avatar"));

                swal({
                    title: localizeString("Alert_Title_Success"),
                    type: "success",
                    text: data.message,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user/update", actions, successCallback, errorCallback);
    }
}());