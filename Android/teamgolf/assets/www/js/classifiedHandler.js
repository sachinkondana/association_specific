(function() {
    var pageflag = 1;
    var datacount = 0;
    var page_number = 1;
    var enableDebugMode = 0;
    var commentPageNumber = 1;
    var typeofMedia = "";
    var commentsCount = 0;

    /* Extend this as needed */
    var classifiedStatusArr = ["Pending Admin Aproval", "Admin Approved", "Admin Rejected", "User Canceled"];
    "use strict";

    /**
     *  Fetches the list of Classifieds based on page_number
     *  @page_number current viewable page
     */
    getClassifiedList = function(page_number) {
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length == 0) {
                    blankData = true;
                    if (pageflag == 1)
                        $('#content').html('<p style="text-align:center">' + localizeString("MyClassifides_Label_NoClassifides") + '</p>');
                } else {
                    displayClassifiedList(data.data);
                    pageflag++;
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/classifieds", actions, successCallback, errorCallback);
    }


    /**
     *  Displays the List of all the active Classified
     *  @classifiedList List of Classifieds received from the Server
     */
    displayClassifiedList = function(classifiedList) {
        if (enableDebugMode)
            alert("classifiedList = " + JSON.stringify(classifiedList));

        localStorage.setItem('numOfClassifieds', classifiedList.content.length);
        for (i = 0; i < classifiedList.content.length; i++) {
            $('#content').append(getListDom(classifiedList.content[i], classifiedList.imageBasePath, "gotoClassifiedDetail"));
        }

        lazyLoadListImages(page_number, classifiedList.content.length);
    }


    /**
     *  Lazy load image
     */
    lazyLoadImage = function(imageid) {
        $("#" + imageid).attr("src", $("#" + imageid).attr("data-src"));
        $("#" + imageid).removeAttr("data-src");
    }


    /**
     *  Displays selected Classified details
     *  @id id of the selected classified
     */
    gotoClassifiedDetail = function(id) {
        localStorage.setItem('classifiedId', id);
        slideview("classified-details.html", 'script', 'left');
    }

    /**
     *  Fetches details of a specific Classified
     */
    getClassifiedDetails = function() {
        var classified_id = localStorage.classifiedId;
        var actions = {
            "classifiedId": classified_id
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayClassifiedDetails(data.data);
                localStorage.setItem('commentPageNumber', commentPageNumber);
                getClassifiedComments(commentPageNumber);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/classifieds", actions, successCallback, errorCallback);
    }


    /**
     * Displays the contents of a Classified
     * @classified Classified data received from the Server
     */
    displayClassifiedDetails = function(classified) {
        if (enableDebugMode)
            alert("classifiedDetails = " + JSON.stringify(classified));

        /*if (classified.content[0].image !== null && classified.content[0].image != "null" && classified.content[0].image != undefined) {
            if (classified.content[0].image.image1 !== null && classified.content[0].image.image1 != undefined) {
                $('#topbanner').attr({
                    'data-src': classified.imageBasePath + classified.content[0].image.image1
                });
            }
        }*/

        try {
            if ((classified.content[0].image != null) && (classified.content[0].image != undefined) &&
                ((classified.content[0].image.image1 != null) && (classified.content[0].image.image1 != undefined)) ||
                ((classified.content[0].image.image2 != null) && (classified.content[0].image.image2 != undefined))) {
                if (classified.content[0].image.image1 != null && classified.content[0].image.image1 != undefined &&
                    classified.content[0].image.image1 != "") {
                    $('#topbanner').attr({
                        'src': classified.imageBasePath + classified.content[0].image.image1
                    });
                    $("#topbanner").css("display", "block");
                } else {
                    $("#topbanner").removeClass("mySlides");
                    $("#topbanner").css("display", "none");
                }

                if (classified.content[0].image.image2 != null && classified.content[0].image.image2 != undefined &&
                    classified.content[0].image.image2 != "") {
                    $('#topbanner2').attr({
                        'src': classified.imageBasePath + classified.content[0].image.image2
                    });
                } else {
                    $("#topbanner2").removeClass("mySlides");
                }
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner2").removeClass("mySlides");
            }
        } catch (err) {
            $("#topbanner").removeClass("mySlides");
            $("#topbanner2").removeClass("mySlides");
        }

        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;"></p>').text(classified.content[0].title);
        var span1 = $('<span style=" font-size:12px;"></span>');
        var dispDat = getDisplayableDate(classified.content[0].createdOn);
        var iel = $('<i class="fa fa-clock-o"> ' + localizeString("Label_PostedOn") + ': ' + dispDat + ' </i>');
        var iel2 = $('<i class="fa fa-user">  ' + localizeString("Label_PostedBy") + ': ' + classified.content[0].firstName + ' ' + classified.content[0].lastName + ' </i>');
        span1.append(iel, '<br/>', iel2);
        var p2 = $('<p></p>');
        p2.append(classified.content[0].description, '</br>');
        $('#classifiedData').append(p1, span1, p2);

        if (localStorage.getItem("isLoggedIn") == "true") {
            $("#ClassifiedDetail_Button_comment").css("display", "block");
        }
        carousel();
    }


    /**
     * Set selected image as the banner image
     */
    changeBannerImage = function(imageId) {
        $("#topbanner").attr("src", $("#" + imageId).attr("src"));
    }

    /**
     *  Fetches user comments on a specific classified
     */
    getClassifiedComments = function(pageNumber) {
        var classified_id = localStorage.classifiedId;
        actions = {
            "classifiedId": classified_id,
            "pageNumber": pageNumber,
        };

        successCallback = function(data) {
            if (data.status == "success") {
                commentsCount += data.data.length;
                localStorage.setItem('numOfComments', commentsCount);
                if (1 == pageNumber)
                    displayNumberOfComments(data.data.length); // show comment count only when page count is 1
                displayClassifiedComments(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/classified/comments", actions, successCallback, errorCallback);
    }


    /**
     *  Fetches next set of classified commments
     */
    getNextClassifiedComment = function() {
        var pageNum = parseInt(localStorage.getItem('commentPageNumber'));
        if (localStorage.getItem('numOfComments') >= pageNum * 15) {
            pageNum += 1;
            localStorage.setItem('commentPageNumber', pageNum);
            getClassifiedComments(pageNum);
        }
    }


    /**
     *  Display the updated nummber of comments on the top
     *  @commentsCount new comment count
     */
    displayNumberOfComments = function(commentsCount) {
        $('.elitewhite-01').html('');
        /* Display number of comments */
        var h3node = $('<h3 style="margin-top:15px;"></h3>');
        var span9 = $('<span class="black-bold" style="font-size:20px; font-weight:500;"></span>').text(commentsCount + ' ' + localizeString("Label_Comments"));
        h3node.append(span9);
        $('.elitewhite-01').append(h3node);
    }


    /**
     *  Displays the user comments on a Classified
     *  @comments User Comments data received from the Server
     */
    function displayClassifiedComments(comments) {
        for (i = 0; i < comments.length; i++) {
            var div1 = $('<div class="row erow"></div>');
            var div2 = $('<div class="icon-01 col-xx-2">');
            if ((comments[i].userAvatar != null) && (comments[i].userAvatar != "") &&
                (comments[i].userAvatar != NaN) && (comments[i].userAvatar != undefined)) {
                var img1 = $('<img alt="No Image" style="max-width:40px">').attr('src', comments[i].userAvatar);
                div2.append(img1);
            } else {
                var img1 = $('<img alt="No Image" style="max-width:40px">').attr('src', 'images/avatar-001.jpg');
                div2.append(img1);
            }
            var div3 = $('<div class="col-xx-10 descx" title="No unread posts"></div>');
            var a1 = $('<a href="javascript:volid(0);" class="eforumtitle">' + comments[i].firstName + ' ' + comments[i].lastName + ': </a>');
            var div4 = $('<div class="blog-post-meta" style="text-align:left;"></div>');
            var iel = $('<i class="fa fa-calendar"></i>');
            div4.append(iel, "&nbsp;&nbsp;" + comments[i].createdOn);
            div3.append(a1, "<br/>", comments[i].comment, div4); /*comments[i].comment,*/
            div1.append(div2, div3);
            $('.elitewhite-01').append(div1);
        }
    }


    /**
     *  Add user comment to a specific Classified
     *  @comment user provided comment on the classified
     */
    addClassifiedComment = function(comment) {
        var ClassifiedId = localStorage.classifiedId;

        $("#detailerror").css("display", "none");
        $("#detailerror").html('<i class="form-help-icon icon">error</i>');

        if (comment.trim() == "") {
            $("#detailerror").css("display", "block");
            $("#detailerror").html(localizeString("ValidationError_Comment") + '<i class="form-help-icon icon">error</i>');
            return false;
        }

        $("#BlogDetail_Button_Close").click();

        actions = {
            "classifiedId": ClassifiedId,
            "comment": comment
        };

        $('#float-textarea').val('');
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_SendRequest"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                localStorage.setItem('commentPageNumber', 1); // 1 for resetting and fetching all comments again
                getClassifiedComments(1);
                swal({
                    title: localizeString("Alert_Title_Success"),
                    type: "success",
                    text: data.message,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };

        errorCallback = function(data) {

        }
        postMessageToServerWithAuth("/classified/commentadd", actions, successCallback, errorCallback);
    }


    /**
     *  Fetches the list of all the Classifieds posted by User based on page_number
     *  @page_number current viewable page
     */
    getMyClassifiedList = function(page_number) {
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length == 0) {
                    blankData = true;
                    if (pageflag == 1)
                        $('#content').html('<p style="text-align:center">' + localizeString("MyClassifides_Label_NoClassifides") + '</p>');
                } else {
                    displayMyClassifiedList(data.data);
                    pageflag++;
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/classified/user", actions, successCallback, errorCallback);
    }


    /**
     *  Displays the List of all the active Classified posted by user
     *  @classifiedList List of Classifieds received from the Server
     */
    displayMyClassifiedList = function(classifiedList) {
        if (enableDebugMode)
            alert("MyClassifiedList = " + JSON.stringify(classifiedList));
        localStorage.setItem('numOfClassifieds', classifiedList.content.length);
        for (i = 0; i < classifiedList.content.length; i++) {
            $('#content').append(getListDom(classifiedList.content[i], classifiedList.imageBasePath, "gotoMyClassifiedDetail"));
        }
        lazyLoadListImages(page_number, classifiedList.content.length); // make this in some callback
    }

    function getListDom(listData, iImgUrl, iClickCall) {
        var name = listData['title'],
            cDate = moment(listData['createdOn'], "YYYY-MM-DD"),
            id = listData['id'],
            logo = (listData['image']['image1']) ? (iImgUrl + listData['image']['image1']) : "./images/placeholder.png",
            commentsCount = listData['commentsCount'];

        cDate = (cDate.isValid()) ? cDate.format("lll") : "--";

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="' + iClickCall + '(\'' + id + '\')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + name + '</div>\
                        <div class="course-row-country clamp2">' + cDate + '</div>\
                        <div class="course-row-country clamp2">\
                        <i class="fa fa-comments"></i>\
                        ' + commentsCount + ' ' + localizeString("Label_Comments") + '</div>\
                    </div>\
                </div>';
    }

    /**
     *  Displays selected Classified details
     *  @id id of the selected classified
     */
    gotoMyClassifiedDetail = function(id) {
        localStorage.setItem('classifiedId', id);
        slideview("classified-my-details.html", 'script', 'left');
    }


    /**
     * Opens up a form to add new cassified
     */
    addNewClassified = function() {
        if (localStorage.getItem("isLoggedIn") != "true") {
            swal({
                title: localizeString("Alert_Text_AddPromo"),
                text: localizeString("Alert_Text_AddPromo"),
                type: "info",
                showCancelButton: false,
                closeOnConfirm: true,
                showLoaderOnConfirm: false,
                confirmButtonText: localizeString("Alert_Button_Ok")
            });
            return;
        }
        localStorage.setItem('classifiedId', "-1"); // -1 for adding new classified
        /* Check if the image uploads are allowed. And then open the post page*/
        checkImageConfig();
    }


    /**
     * Checks if the image upload is allowed for the user
     */
    checkImageConfig = function() {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                localStorage.setItem('isImageAllowed', data.data.content.isImageAllowed);
                slideview("classified-post.html", 'script', 'left');
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/classified/imageconfig", actions, successCallback, errorCallback);
    }


    /**
     * Hides/displays the Image Upload div's based on the configuration
     * received from the backend.
     */
    updateClassifiedPostUI = function() {
        var imageUploadAllowed = localStorage.getItem('isImageAllowed');
        if (enableDebugMode)
            alert("imageUploadAllowed = " + imageUploadAllowed);

        if (imageUploadAllowed == "true") {
            $('#imageHeader').css("display", "inline");
            $('#imageDiv1').css("display", "block"); // Block in necessary here
            $('#imageDiv2').css("display", "block"); // Block in necessary here
        } else {
            $('#imageHeader').css("display", "none");
            $('#imageDiv1').css("display", "none");
            $('#imageDiv2').css("display", "none");
        }
    }


    /**
     * Submit the classfied for admin approval
     */
    SubmitClassifiedForApproval = function() {
        var title = document.getElementById("float-text").value;
        var description = document.getElementById("float-textarea").value;
        var image1 = localStorage.getItem("photo1");
        var image2 = localStorage.getItem("photo2");

        var actions = {
            "title": title,
            "description": description,
            "image1": image1,
            "mimeType1": "data:image/jpeg;base64",
            "image2": image2,
            "mimeType2": "data:image/jpeg;base64",
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Success"),
                        text: localizeString("Alert_Text_ClassifiedSubmission"),
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        slideview("classified-my.html", 'script', 'left');
                        return;
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/classified/add", actions, successCallback, errorCallback);
    }


    /**
     *  Get image from the cammera/file url
     *  @id type of media capture
     *  @photoId document element id where the captured image needs to be displayed
     */
    getMedia = function(id, photoId) {
        localStorage.setItem("photoId", photoId);
        if (id == "captureImg") {
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 50,
                targetWidth: 512,
                targetHeight: 512,
                correctOrientation: true,
                encodingType: Camera.EncodingType.JPEG,
                allowEdit: true,
                destinationType: Camera.DestinationType.DATA_URL
            });
            typeofMedia = "iMAGE";
        }

        if (id == "selectImg") {
            navigator.camera.getPicture(onSuccess, onFail, {
                quality: 50,
                targetWidth: 512,
                targetHeight: 512,
                correctOrientation: true,
                encodingType: Camera.EncodingType.JPEG,
                allowEdit: true,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                destinationType: Camera.DestinationType.DATA_URL
            });
            typeofMedia = "iMAGE";
        }
    }

    /**
     * Success Callback for base64 encoded image data
     */
    onSuccess = function(imageData) {
        var image = document.getElementById(localStorage.getItem("photoId"));
        localStorage.setItem(image.id, imageData);
        image.src = "data:image/jpeg;base64," + imageData;
    }


    /**
     * Failure callback
     */
    onFail = function(message) {}


    /**
     * Post data callback
     */
    postData = function(caption, fileUrl, call, type, time) {
        var img = document.getElementById(localStorage.getItem("photoId"));
        img.src = fileUrl;
    }


    /**
     * Remove Media
     */
    removeMedia = function(photoId) {
        var img = document.getElementById(photoId);
        img.src = "";
    }


    /**
     * get the details of the selected use classified
     */
    getMyClassifiedDetails = function() {
        var classifiedId = localStorage.getItem('classifiedId');
        var actions = {
            "classifiedId": classifiedId
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayMyClassifiedDetails(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function(data) {

        };

        postMessageToServerWithAuth("/classifieds", actions, successCallback, errorCallback);
    }


    /**
     *  Displays the details of a classified and allows edit if not already canceled
     *  @classified classified data for the selected classified
     */
    displayMyClassifiedDetails = function(classified) {

        classifiedStatusArr[0] = localizeString("GolfTournaments_Label_RegStatusPending");
        classifiedStatusArr[1] = localizeString("GolfTournaments_Label_RegStatusApproved");
        classifiedStatusArr[2] = localizeString("GolfTournaments_Label_RegStatusRejected");
        classifiedStatusArr[3] = localizeString("GolfTournaments_Label_RegStatusCancelled");

        if (enableDebugMode)
            alert("classified = " + JSON.stringify(classified));
        var p1 = $('<p style="font-size:14px; margin-bottom:5px;"> ' + localizeString("MyClassifides_Lable_PublishStatus") + ' <strong>' + classifiedStatusArr[parseInt(classified.content[0].status)] + '</strong> </p>');
        var span1 = $('<span style=" font-size:12px;"></span>');
        var dispDat = getDisplayableDate(classified.content[0].createdOn);
        var iel = $('<i class="fa fa-clock-o"> ' + localizeString("Label_PostedOn") + ': ' + dispDat + ' </i>');
        span1.append(iel, '<br/>');
        $('#classifiedData').append(p1, span1);

        document.getElementById("float-text").value = classified.content[0].title;
        document.getElementById("float-textarea").value = classified.content[0].description;

        if (classified.content[0].image != null && classified.content[0].image != undefined) {
            if (classified.content[0].image.image1 != null && classified.content[0].image.image1 != undefined &&
                classified.content[0].image.image1 != "") {
                document.getElementById("photo1").src = classified.imageBasePath + classified.content[0].image.image1;
                localStorage.setItem("photo1", null); //getBase64Image(document.getElementById("photo1"))
            }
            if (classified.content[0].image.image2 != null && classified.content[0].image.image2 != undefined &&
                classified.content[0].image.image2 != "") {
                document.getElementById("photo2").src = classified.imageBasePath + classified.content[0].image.image2;
                localStorage.setItem("photo2", null); //getBase64Image(document.getElementById("photo2"))
            }
        }

        if (classified.content[0].adminComment != null && classified.content[0].adminComment != undefined) {
            $("#adminmsg-area-form").css("display", "inline");
            var adminresponse = classified.content[0].adminComment.replace(/<br\s*\/?>/mg, "\n");
            adminresponse = adminresponse.replace(/<\/br\s*?>/mg, "");
            $("#adminresponses").append(adminresponse);
        }

        if (3 == parseInt(classified.content[0].status)) {
            $("#float-text").prop('disabled', true); // Disable adding new title text
            $("#float-textarea").prop('disabled', true); // Disable adding new description text
            $("#ClassifiedDetal_Label_AddPhoto").attr('disabled', 'disabled'); // Disable selecting new photo1
            $("#ClassifiedDetal_Label_AddPhoto2").attr('disabled', 'disabled'); // Disable selecting new photo2
            //$("#msgarea").prop('disabled', true); // Disable adding new message
            $("#submitBtn").css("display", "none");
            $("#cancelBtn").css("display", "none");
        }
    }


    /**
     * Upadates an existing classified posted by the user
     */
    updateMyClassified = function() {
        var classifiedId = localStorage.getItem('classifiedId');
        var title = document.getElementById("float-text").value;
        var description = document.getElementById("float-textarea").value;
        var image1 = localStorage.getItem("photo1");
        var image2 = localStorage.getItem("photo2");

        var actions;
        if ((image1 != null && image1 != "null") && (image2 != null && image2 != "null")) {
            actions = {
                "classifiedId": classifiedId,
                "title": title,
                "description": description,
                "image1": image1,
                "mimeType1": "data:image/jpeg;base64",
                "image2": image2,
                "mimeType2": "data:image/jpeg;base64",
            };
        } else if ((image1 != null && image1 != "null") && (image2 == null || image2 == "null")) {
            actions = {
                "classifiedId": classifiedId,
                "title": title,
                "description": description,
                "image1": image1,
                "mimeType1": "data:image/jpeg;base64",
            };
        } else if ((image1 == null || image1 == "null") && (image2 != null && image2 != "null")) {
            actions = {
                "classifiedId": classifiedId,
                "title": title,
                "description": description,
                "image2": image2,
                "mimeType2": "data:image/jpeg;base64",
            };
        } else if ((image1 == null || image1 == "null") && (image2 == null || image2 == "null")) {
            actions = {
                "classifiedId": classifiedId,
                "title": title,
                "description": description,
            };
        }

        // Remove media case
        if ($("#photo1").attr("src") == "") {
            actions.image1 = "noImage";
            actions.mimeType1 = "noImage";
        }

        if ($("#photo2").attr("src") == "") {
            actions.image2 = "noImage";
            actions.mimeType2 = "noImage";
        }

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Success"),
                        text: localizeString("Alert_Text_ClassifiedUpdate"),
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        slideview("classified-my.html", 'script', 'left');
                        return;
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/classified/add", actions, successCallback, errorCallback);
    }


    /**
     * Cancel an existing classified posted by user
     */
    cancelMyClassified = function() {
        var classifiedId = localStorage.getItem('classifiedId');
        var actions = {
            "classifiedId": classifiedId,
        };

        swal({
                title: localizeString("Alert_Title_RemoveClassified"),
                text: localizeString("Alert_Test_RemoveClassified"),
                type: "warning",
                showCancelButton: true,
                closeOnConfirm: false,
                confirmButtonText: localizeString("Alert_Button_Remove"),
                cancelButtonText: localizeString("Alert_Button_Cancel")
            },
            function() {
                window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
                successCallback = function(data) {
                    window.plugins.spinnerDialog.hide();
                    if (data.status == "success") {
                        swal({
                                title: localizeString("Alert_Title_Success"),
                                text: localizeString("Alert_Text_RemovedClassified"),
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: false,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                slideview("classified-my.html", 'script', 'left');
                                return;
                            });
                    } else {
                        var thisone = this;
                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: data.message,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    }
                };
                errorCallback = function(data) {

                };
                postMessageToServerWithAuth("/classified/remove", actions, successCallback, errorCallback);
                return;
            });
    }
}());