(function () {

    var enableDebugMode = 0;
    "use strict";
    /* Default template function name for this page */
    var defaultPresidentMessageTemplateFunction = "renderMultipleParaThemeContents";

    /* Page id for this page */
    var pageId = "1";

    var aboutContent = {
        "imageBase": "images/",
        "templateContent":
        {
            "image" : "",
            "paraContent" : [
                "Hello Everyone,",
                "It gives me great honor to be the General Manager of My MobiCom Club. I welcome all the members and their guests to fully utilize all the resources our club has to offer. It is with great pride that we serve our members and hope to have them return often, after an enjoyable experience at our facility.",
                "Best Wishes,",
                "Pooja Rathan",
                "General Manager"
            ]
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Multiple Para Theme
     *  @param content: Must be as required for Multiple Para Theme
     */
    displayExistingContent = function(content) {

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PresidentMessageTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultPresidentMessageTemplateFunction;
            localStorage.setItem("PresidentMessageTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PresidentMessageTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("PresidentMessageTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {

        var lastUpd = localStorage.getItem("PresidentMessageLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("PresidentMessageLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentPMContent = JSON.parse(localStorage.getItem("CurrentPMContent"));
        if (currentPMContent == null || currentPMContent == undefined) {
            currentPMContent = getStaticContent();
            localStorage.setItem("CurrentPMContent", JSON.stringify(currentPMContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentPMContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchPMSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchPMSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentPMContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("PresidentMessageLastUpdated", lastUpdateOn);
        }
    };

}());
