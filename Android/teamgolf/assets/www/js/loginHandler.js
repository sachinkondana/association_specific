$(window).load(function() {
    $("#email").attr("placeholder", localizeString("Login_Placeholder_Email"));
    $("#password").attr("placeholder", localizeString("Login_Placeholder_Password"));
    $("#emailerror").html(localizeString("ValidationError_Email"));
    $("#passworderror").html(localizeString("ValidationError_Password"));
    preFillCreds();
});

$('#login').addClass('active');

/*$('#content').css('height',($(window).height()-180)+"px");*/
$("#email").focus(function() {
    $("#emaildiv").css("border-bottom", "1px solid green");
    $("#emailerror").css("display", "none");
});
$("#password").focus(function() {
    $("#passworddiv").css("border-bottom", "1px solid green");
    $("#passworderror").css("display", "none");
});

(function() {
    var enableDebugMode = 0;
    "use strict";
    preFillCreds = function() {
        if ((localStorage.getItem("loginUName") != null) &&
            (localStorage.getItem("loginUName") != undefined) &&
            (localStorage.getItem("loginUName").trim() != "")) {

            $("#email").val(localStorage.getItem("loginUName"));

            if ((localStorage.getItem("loginUPwd") != null) &&
                (localStorage.getItem("loginUPwd") != undefined) &&
                (localStorage.getItem("loginUPwd").trim() != ""))
                $("#password").attr('placeholder', 'U N C H A N G E D');
        }
    }

    loginNow = function() {
        var deviceid = localStorage.getItem("DEVICE_ID");
        var deviceos = localStorage.getItem("DEVICE_OS");
        var appid = localStorage.getItem("APP_ID");
        var email = $("#email").val();
        var loginpassword = $("#password").val();
        if ((loginpassword.trim() == "") && ((localStorage.getItem("loginUPwd") != null) &&
                (localStorage.getItem("loginUPwd") != undefined) &&
                (localStorage.getItem("loginUPwd").trim() != ""))) {
            loginpassword = localStorage.getItem("loginUPwd");
        }
        $("#passworddiv").css("border-bottom", "1px solid green");
        $("#emaildiv").css("border-bottom", "1px solid green");
        $("#passworderror").css("display", "none");
        $("#emailerror").css("display", "none");
        if (email.length < 1) {
            $("#emailerror").css("display", "inline");
            $("#emaildiv").css("border-bottom", "1px solid red");
        }
        if (loginpassword.length < 1) {
            $("#passworderror").css("display", "inline");
            $("#passworddiv").css("border-bottom", "1px solid red");
        }
        if (email.length < 1) {
            return false;
        }
        if (loginpassword.length < 1) {
            return false;
        }

        if ($('#remember').is(':checked')) {
            localStorage.setItem("loginUName", email);
            localStorage.setItem("loginUPwd", loginpassword);
        }

        var actions = {
            "email": email,
            "password": loginpassword,
            "deviceid": deviceid,
            "deviceos": deviceos,
            "appid": appid
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            if (data.status == "success") {
                if (enableDebugMode)
                    alert("login params = " + JSON.stringify(data));
                localStorage.setItem("access_token", data.user_status.data.access_token);
                localStorage.setItem("token_type", data.user_status.data.token_type);
                localStorage.setItem("refresh_token", data.user_status.data.refresh_token);
                localStorage.setItem("server_ts", data.user_status.data.server_ts);
                localStorage.setItem("memberid", data.user_status.data.member_id);
                localStorage.setItem("membership_number", data.user_status.data.membership_number);
                localStorage.setItem("membership_type", data.user_status.data.member_type);
                localStorage.setItem("memberverified", data.user_status.status);
                localStorage.setItem("email", data.user_status.data.email);
                localStorage.setItem("phone", data.user_status.data.phone);
                localStorage.setItem("first_name", data.user_status.data.first_name);
                localStorage.setItem("last_name", data.user_status.data.last_name);
                localStorage.setItem("avatar", data.user_status.data.avatar);
                localStorage.setItem("gender", data.user_status.data.gender);
                localStorage.setItem("dob", data.user_status.data.dob);
                localStorage.setItem("isPrimary", data.user_status.data.isPrimary);
                localStorage.setItem("handicap", data.user_status.data.handicap);
                localStorage.setItem("admin_to_verify", data.user_status.data.admin_to_verify);

                if (data.user_status.data.photo !== "" && data.user_status.data.photo !== "null" && data.user_status.data.photo !== null) {
                    localStorage.setItem("photo", data.user_status.data.photo);
                } else {
                    localStorage.setItem("photo", "");
                }
                localStorage.setItem("email_verified", data.user_status.data.email_verified);
                var menuListReceived = data.user_status.data.menu;
                var isFacilitySelected = false;
                var isFacilityBookingSelected = false;
                var facilityBookingEnabledArray = { '1': 'false', '2': 'false', '3': 'false', '4': 'false', '5': 'false', '6': 'false', '7': 'false', '8': 'false' };
                var aboutEnabled = 0,
                    clubMgmtEnabled = 0,
                    isFitnessSelected = 0,
                    isSportsSelected = 0,
                    isHnSSelected = 0,
                    isSaloonSelected = 0;
                var isOtherSelected = 0,
                    isRecreationalSelected = 0,
                    isFnBSelected = 0,
                    isRoomSelected = 0,
                    isBnCSelected = 0,
                    isBlogEnabled = 0;
                var isConnectEnabled = 0,
                    isEventEnabled = 0,
                    isMyAccountEnabled = 0,
                    isGolfEnabled = 0,
                    isClassifiedEnabled = 0;
                var isOpinionEnabled = 0,
                    isTournamentEnabled = 0;

                var menuNameList = {};

                // Fetch backup navigation menu from storage
                var loggedInNavigation = localStorage.getItem("NAVIGATION_BACKUP");
                localStorage.setItem("userChatEnabled", "false");

                for (var i = 0; i < menuListReceived.length; i++) {
                    loggedInNavigation = loggedInNavigation.replace("@" + menuListReceived[i].cfdMappingId + "@", "block");
                    /* Prepare Menu Name list */
                    menuNameList["menunameid_" + menuListReceived[i].cfdMappingId] = menuListReceived[i].featureName;

                    if ((menuListReceived[i].cfdMappingId == '1.1') || (menuListReceived[i].cfdMappingId == '1.1.1') ||
                        (menuListReceived[i].cfdMappingId == '1.2') || (menuListReceived[i].cfdMappingId == '1.2.1') ||
                        (menuListReceived[i].cfdMappingId == '1.3') || (menuListReceived[i].cfdMappingId == '1.4') ||
                        (menuListReceived[i].cfdMappingId == '1.6') || (menuListReceived[i].cfdMappingId == '1.6.1') ||
                        (menuListReceived[i].cfdMappingId == '1.7') || (menuListReceived[i].cfdMappingId == '1.8')) {
                        aboutEnabled = 1;
                    }

                    if (menuListReceived[i].cfdMappingId == '1.5')
                        menuNameList["menunameid_1.5"] = menuListReceived[i].featureName;

                    if ((menuListReceived[i].cfdMappingId == '2.1') || (menuListReceived[i].cfdMappingId == '2.2') ||
                        (menuListReceived[i].cfdMappingId == '2.3') || (menuListReceived[i].cfdMappingId == '2.4')) {
                        clubMgmtEnabled = 1;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.1.1') || (menuListReceived[i].cfdMappingId == '3.1.2') ||
                        (menuListReceived[i].cfdMappingId == '3.1.3')) {
                        isFacilitySelected = true;
                        isFitnessSelected = true; // Fitness Facility
                        if (menuListReceived[i].cfdMappingId == '3.1.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[1] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.1.1')
                            menuNameList["menunameid_3.1.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.2.1') || (menuListReceived[i].cfdMappingId == '3.2.2') ||
                        (menuListReceived[i].cfdMappingId == '3.2.3')) {
                        isFacilitySelected = true;
                        isSportsSelected = true; // Sports Facility
                        if (menuListReceived[i].cfdMappingId == '3.2.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[2] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.2.1')
                            menuNameList["menunameid_3.2.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.3.1') || (menuListReceived[i].cfdMappingId == '3.3.2') ||
                        (menuListReceived[i].cfdMappingId == '3.3.3')) {
                        isFacilitySelected = true;
                        isHnSSelected = true; // Health & Spa Facility
                        if (menuListReceived[i].cfdMappingId == '3.3.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[3] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.3.1')
                            menuNameList["menunameid_3.3.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.4.1') || (menuListReceived[i].cfdMappingId == '3.4.2') ||
                        (menuListReceived[i].cfdMappingId == '3.4.3')) {
                        isFacilitySelected = true;
                        isSaloonSelected = true; // Saloon Facility
                        if (menuListReceived[i].cfdMappingId == '3.4.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[8] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.4.1')
                            menuNameList["menunameid_3.4.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.5.1') || (menuListReceived[i].cfdMappingId == '3.5.2') ||
                        (menuListReceived[i].cfdMappingId == '3.5.3')) {
                        isFacilitySelected = true;
                        isOtherSelected = true; // Other Facility
                        if (menuListReceived[i].cfdMappingId == '3.5.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[4] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.5.1')
                            menuNameList["menunameid_3.5.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '3.6.1') || (menuListReceived[i].cfdMappingId == '3.6.2') ||
                        (menuListReceived[i].cfdMappingId == '3.6.3')) {
                        isFacilitySelected = true;
                        isRecreationalSelected = true; // Recreational Facility
                        if (menuListReceived[i].cfdMappingId == '3.6.3') {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[7] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '3.6.1')
                            menuNameList["menunameid_3.6.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '4.1') || (menuListReceived[i].cfdMappingId == '4.2')) {
                        isFnBSelected = true; // F & B
                        if (menuListReceived[i].cfdMappingId == '4.2')
                            facilityBookingEnabledArray[9] = 'true';
                        if (menuListReceived[i].cfdMappingId == '4.1')
                            menuNameList["menunameid_4.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '5.1') || (menuListReceived[i].cfdMappingId == '5.2') ||
                        (menuListReceived[i].cfdMappingId == '5.3.1') || (menuListReceived[i].cfdMappingId == '5.3.2')) {
                        isFacilitySelected = true;
                        isRoomSelected = true; // Room Facility
                        if ((menuListReceived[i].cfdMappingId == '5.3.1') || (menuListReceived[i].cfdMappingId == '5.3.2')) {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[5] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '5.1')
                            menuNameList["menunameid_5.0"] = menuListReceived[i].featureName;
                    }
                    if ((menuListReceived[i].cfdMappingId == '6.1') || (menuListReceived[i].cfdMappingId == '6.2') ||
                        (menuListReceived[i].cfdMappingId == '6.3')) {
                        isFacilitySelected = true;
                        isBnCSelected = true; // Banquet & Conference
                        if ((menuListReceived[i].cfdMappingId == '6.3')) {
                            isFacilityBookingSelected = true;
                            facilityBookingEnabledArray[6] = 'true';
                        }
                        if (menuListReceived[i].cfdMappingId == '6.1')
                            menuNameList["menunameid_6.0"] = menuListReceived[i].featureName;
                    }

                    if ((menuListReceived[i].cfdMappingId == '7.2.1') || (menuListReceived[i].cfdMappingId == '7.2.2') ||
                        (menuListReceived[i].cfdMappingId == '7.2.3')) {
                        isBlogEnabled = true;
                        if (menuListReceived[i].cfdMappingId == '7.2.1')
                            menuNameList["menunameid_7.2.0"] = menuListReceived[i].featureName;
                    }

                    if ((menuListReceived[i].cfdMappingId == '7.1') || (menuListReceived[i].cfdMappingId == '7.2.1') ||
                        (menuListReceived[i].cfdMappingId == '7.3') || (menuListReceived[i].cfdMappingId == '7.4') ||
                        (menuListReceived[i].cfdMappingId == '7.5') || (menuListReceived[i].cfdMappingId == '7.6') ||
                        (menuListReceived[i].cfdMappingId == '7.7') || (menuListReceived[i].cfdMappingId == '14.1') ||
                        (menuListReceived[i].cfdMappingId == '14.2')) {
                        isConnectEnabled = true;
                        if (menuListReceived[i].cfdMappingId == '7.4') {
                            menuNameList["menunameid_7.4"] = menuListReceived[i].featureName;
                            localStorage.setItem("notificationEnabled", "true");
                        }
                        if (menuListReceived[i].cfdMappingId == '7.5')
                            menuNameList["menunameid_7.5"] = menuListReceived[i].featureName;
                        if (menuListReceived[i].cfdMappingId == '7.6') {
                            menuNameList["menunameid_7.6"] = menuListReceived[i].featureName;
                            localStorage.setItem("newsEnabled", "true");
                        }
                    }

                    if ((menuListReceived[i].cfdMappingId == '8.1') || (menuListReceived[i].cfdMappingId == '8.2') ||
                        (menuListReceived[i].cfdMappingId == '8.3')) {
                        isEventEnabled = true;
                        if (menuListReceived[i].cfdMappingId == '8.3') {
                            //menuListString = menuListString + ",8.4"; // Event Calendar
                            localStorage.setItem("eventCalendarBookingMenuEnabled", "true");
                            menuNameList["menunameid_8.3"] = menuListReceived[i].featureName;
                        }
                        if (menuListReceived[i].cfdMappingId == '8.1')
                            menuNameList["menunameid_8.1"] = menuListReceived[i].featureName;
                    }

                    if ((menuListReceived[i].cfdMappingId == '9.1') || (menuListReceived[i].cfdMappingId == '9.2') ||
                        (menuListReceived[i].cfdMappingId == '9.3') || (menuListReceived[i].cfdMappingId == '9.4') ||
                        (menuListReceived[i].cfdMappingId == '9.5')) {
                        isMyAccountEnabled = true;
                        if (menuListReceived[i].cfdMappingId == '9.2')
                            localStorage.setItem("transactionsEnabled", "true");
                        if (menuListReceived[i].cfdMappingId == '9.3')
                            localStorage.setItem("querriesEnabled", "true");
                        if (menuListReceived[i].cfdMappingId == '9.4')
                            localStorage.setItem("paymentEnabled", "true");
                    }

                    if (menuListReceived[i].cfdMappingId == '11') {
                        localStorage.setItem("offersEnabled", "true");
                    }

                    if ((menuListReceived[i].cfdMappingId == '12.1') || (menuListReceived[i].cfdMappingId == '12.2')) {
                        isClassifiedEnabled = true; //Enable both 12.0 & 12.3
                        if (menuListReceived[i].cfdMappingId == '12.1')
                            menuNameList["menunameid_12.1"] = menuListReceived[i].featureName;
                    }

                    if ((menuListReceived[i].cfdMappingId == '13.1') || (menuListReceived[i].cfdMappingId == '13.2')) {
                        isOpinionEnabled = true; //Enable both 13.0 & 13.3
                        if (menuListReceived[i].cfdMappingId == '13.1')
                            menuNameList["menunameid_13.1"] = menuListReceived[i].featureName;
                        if (menuListReceived[i].cfdMappingId == '13.2')
                            menuNameList["menunameid_13.2"] = menuListReceived[i].featureName;
                    }

                    if (menuListReceived[i].cfdMappingId == '14.2') {
                        localStorage.setItem("userChatEnabled", "true");
                    }

                    if ((menuListReceived[i].cfdMappingId == '15.1') || (menuListReceived[i].cfdMappingId == '15.2') ||
                        (menuListReceived[i].cfdMappingId == '15.3')) {
                        isTournamentEnabled = true; // Enable both 15.0
                        if (menuListReceived[i].cfdMappingId == '15.2') {
                            localStorage.setItem("tournamentRegistrationEnabled", "true");
                        }
                        if (menuListReceived[i].cfdMappingId == '15.1')
                            menuNameList["menunameid_15.1"] = menuListReceived[i].featureName;
                        if (menuListReceived[i].cfdMappingId == '15.3')
                            menuNameList["menunameid_15.3"] = menuListReceived[i].featureName;
                    }

                    if ((menuListReceived[i].cfdMappingId == '1.1:Golf') || (menuListReceived[i].cfdMappingId == '1.2:Golf') ||
                        (menuListReceived[i].cfdMappingId == '1.3:Golf') || (menuListReceived[i].cfdMappingId == '1.4:Golf') ||
                        (menuListReceived[i].cfdMappingId == '1.5.1:Golf') || (menuListReceived[i].cfdMappingId == '1.5.2:Golf') ||
                        (menuListReceived[i].cfdMappingId == '1.6:Golf') || (menuListReceived[i].cfdMappingId == '1.7:Golf') ||
                        (menuListReceived[i].cfdMappingId == '1.8:Golf')) {
                        isGolfEnabled = true;
                        if (menuListReceived[i].cfdMappingId == '1.1:Golf') {
                            localStorage.setItem("golfCourseEnabled", "true");
                            menuNameList["menunameid_1.1:Golf"] = menuListReceived[i].featureName;
                        }
                        if (menuListReceived[i].cfdMappingId == '1.2:Golf')
                            menuNameList["menunameid_1.2:Golf"] = menuListReceived[i].featureName;
                        if (menuListReceived[i].cfdMappingId == '1.3:Golf') {
                            localStorage.setItem("golfTeeTimeEnabled", "true");
                            menuNameList["menunameid_1.3:Golf"] = menuListReceived[i].featureName;
                        }
                        if (menuListReceived[i].cfdMappingId == '1.5.1:Golf') {
                            localStorage.setItem("golfTournamentEnabled", "true");
                            menuNameList["menunameid_1.5.1:Golf"] = menuListReceived[i].featureName;
                        }
                        if (menuListReceived[i].cfdMappingId == '1.5.2:Golf') {
                            menuNameList["menunameid_1.5.2:Golf"] = menuListReceived[i].featureName;
                            localStorage.setItem("golfTournamentRegistrationEnabled", "true");
                        }
                        if (menuListReceived[i].cfdMappingId == '1.6:Golf') {
                            localStorage.setItem("golfScorecardEnabled", "true");
                            menuNameList["menunameid_1.6:Golf"] = menuListReceived[i].featureName;
                        }
                        if (menuListReceived[i].cfdMappingId == '1.7:Golf')
                            menuNameList["menunameid_1.7:Golf"] = menuListReceived[i].featureName;
                        if (menuListReceived[i].cfdMappingId == '1.8:Golf')
                            menuNameList["menunameid_1.8:Golf"] = menuListReceived[i].featureName;
                    }
                }

                localStorage.setItem("FacilityBookingEnabledCategories", JSON.stringify(facilityBookingEnabledArray));
                localStorage.setItem("MenuNameList", JSON.stringify(menuNameList));

                if (aboutEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@1.0@", "block");
                if (clubMgmtEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@2.0@", "block");
                if (isFacilitySelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.0@", "block");
                if (isFitnessSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.1.0@", "block");
                if (isSportsSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.2.0@", "block");
                if (isHnSSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.3.0@", "block");
                if (isSaloonSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.4.0@", "block");
                if (isOtherSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.5.0@", "block");
                if (isRecreationalSelected)
                    loggedInNavigation = loggedInNavigation.replace("@3.6.0@", "block");
                if (isFnBSelected)
                    loggedInNavigation = loggedInNavigation.replace("@4.0@", "block");
                if (isRoomSelected)
                    loggedInNavigation = loggedInNavigation.replace("@5.0@", "block");
                if (isBnCSelected)
                    loggedInNavigation = loggedInNavigation.replace("@6.0@", "block");
                if (isFacilityBookingSelected) {
                    loggedInNavigation = loggedInNavigation.replace("@3.8@", "block");
                    localStorage.setItem("faciBookingEnabled", "true");
                }
                if (isBlogEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@7.2.0@", "block");
                if (isConnectEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@7.0@", "block");
                if (isEventEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@8.0@", "block");
                if (isGolfEnabled)
                    loggedInNavigation = loggedInNavigation.replace("@1.0:Golf@", "block");
                if (isClassifiedEnabled) {
                    loggedInNavigation = loggedInNavigation.replace("@12.0@", "block");
                    loggedInNavigation = loggedInNavigation.replace("@12.3@", "block");
                }
                if (isTournamentEnabled) {
                    loggedInNavigation = loggedInNavigation.replace("@15.0@", "block");
                    //loggedInNavigation = loggedInNavigation.replace("@15.3@", "block");
                }

                loggedInNavigation = loggedInNavigation.replace("@13.0@", "block");
                loggedInNavigation = loggedInNavigation.replace("@13.3@", "block");
                loggedInNavigation = loggedInNavigation.replace("@myacnt@", "block");
                loggedInNavigation = loggedInNavigation.replace("@myacntsetting@", "block");

                var globalMenuIdList = JSON.parse(localStorage.getItem("GLOBALMENUIDARRAY"));
                for (var j = 0; j < globalMenuIdList.length; j++) {
                    loggedInNavigation = loggedInNavigation.replace("@" + globalMenuIdList[j] + "@", "none");
                }
                localStorage.setItem("NAVIGATION", loggedInNavigation); // Final logged in menu
                localStorage.setItem("NAVIGATION_SET", "true"); // Unset this on Logout

                /* Now update the user login status */
                localStorage.setItem("isLoggedIn", "true");

                /* Set this for user menu badge updates */
                localStorage.setItem("from_page", "login");

                window.plugins.spinnerDialog.hide();

                slideview("index.html", "script", "left"); //my-account
            } else {
                window.plugins.spinnerDialog.hide();
                var thisone = this;
                localStorage.setItem("isLoggedIn", "false");
                swal({
                        title: localizeString("Alert_Title_AuthFailed"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/user/login", actions, successCallback, errorCallback);
    }


    $('input').each(function(i) {
        $(this).keyup(function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                if ($(this).attr("id") == 'password') {
                    loginNow();
                } else {
                    $('input').eq(i + 1).focus();
                }
            }
        });
        $('input').unbind('focusout');
        $(document).on('focusout', 'input', function() {
            setTimeout(function() {
                window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
            }, 500);
        });
    });


    $("#showHide").click(function() {
        if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
    });


    /**
     * Prepares the logged in user menu based on the input from the server
     */
    updateUserLoggedInMenu = function() {
        // Prepared on login Success
        var menuItemList = JSON.parse(localStorage.getItem("loggedInMenuSplitArray"));
        // Fetch backup navigation menu from storage
        var loggedInNavigation = localStorage.getItem("NAVIGATION_BACKUP");
        for (var i = 0; i < menuItemList.length; i++) {
            loggedInNavigation = loggedInNavigation.replace("@" + menuItemList[i] + "@", "block");
        }
        var globalMenuIdList = JSON.parse(localStorage.getItem("GLOBALMENUIDARRAY"));
        for (var j = 0; j < globalMenuIdList.length; j++) {
            loggedInNavigation = loggedInNavigation.replace("@" + globalMenuIdList[j] + "@", "none");
        }
        localStorage.setItem("NAVIGATION", loggedInNavigation); // Final logged in menu
        localStorage.setItem("NAVIGATION_SET", "true"); // Unset this on Logout
    }
}());