(function () {

    var enableDebugMode = 0;
    var timer;
    var start = 0;
    var end = 9;
    var totalPar = 0;
    var currentSide = 0;
    var page_number = 1;
    "use strict";
    localStorage.setItem("from_page", "golftournaments");


    /**
     * Fetch the golf course setup information.
     */
    getCourseInfo = function () {
        var actions = {
            "type" : "1"
        };

        successCallback = function (data) {
            if (data.status == "success") {
                if (data.golf_details != null && data.golf_details != undefined && data.message == "success") {
                    displayGolfCourseInfo(data);
                } else {
                    $('#setupUnavailable').html('<p style="text-align:center"> '+localizeString("Golf_Label_Nodata")+' </p>');
                    $('#setupUnavailable').css('display', 'block');
                    window.plugins.spinnerDialog.hide();
                }
            } else {
                window.plugins.spinnerDialog.hide();
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        }
        errorCallback = function (data) {
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"));
        postMessageToServerWithAuth("/golfcourse", actions, successCallback, errorCallback);
    }


    displayGolfCourseInfo = function (courseInfo) {
        var numOfImages = 0;

        if (enableDebugMode)
            alert(JSON.stringify(courseInfo));

        try {
            if (courseInfo.golf_details.images != null && courseInfo.golf_details.images != undefined) {
                if (courseInfo.golf_details.images.image1 != null && courseInfo.golf_details.images.image1 != undefined
                    && courseInfo.golf_details.images.image1 != "") {
                    $('#topbanner').attr({
                        'src': courseInfo.imageBasePath + courseInfo.golf_details.images.image1
                    });
                    $("#topbanner").css("display", "block");
                } else {
                    $("#topbanner").removeClass("mySlides");
                    $("#topbanner").css("display", "none");
                }

                if (courseInfo.golf_details.images.image2 != null && courseInfo.golf_details.images.image2 != undefined
                    && courseInfo.golf_details.images.image2 != "") {
                    $('#topbanner2').attr({
                        'src': courseInfo.imageBasePath + courseInfo.golf_details.images.image2
                    });
                    numOfImages++;
                }  else {
                    $("#topbanner2").removeClass("mySlides");
                }

                if (courseInfo.golf_details.images.image3 != null && courseInfo.golf_details.images.image3 != undefined
                    && courseInfo.golf_details.images.image3 != "") {
                    $('#topbanner3').attr({
                        'src': courseInfo.imageBasePath + courseInfo.golf_details.images.image3
                    });
                    numOfImages++;
                } else {
                    $("#topbanner3").removeClass("mySlides");
                }

                if (numOfImages >= 1)
                    carousel();
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner2").removeClass("mySlides");
                $("#topbanner3").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }
        } catch(err) {
            $("#topbanner").removeClass("mySlides");
            $("#topbanner2").removeClass("mySlides");
            $("#topbanner3").removeClass("mySlides");
            $("#topbanner").css("display", "none");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(localizeString("GolfCourse_Label_Deatails"));
        var p2 = $('<p style="word-wrap:break-word; word-break:break-word;">' + courseInfo.golf_details.description + '</p>');
        if (courseInfo.golf_details.local_rules != null && courseInfo.golf_details.local_rules != undefined
            && courseInfo.golf_details.local_rules.trim() != "") {
            var p3 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="rules"></p>').text(localizeString("GolfCourse_Label_Rules"));
            var p4 = $('<p style="word-wrap:break-word; word-break:break-word;">' + courseInfo.golf_details.local_rules.trim() + '</p>');
            td1.append(p1, p2, p3, p4);
        } else {
            td1.append(p1, p2);
        }
        var p4 = $('<p style="font-size:20px; margin-bottom:5px;"> <span class="text-bold-01">' + localizeString("GolfCourse_Label_NumOfHoles") + ':</span> <strong>' + (courseInfo.golf_details.hole_sets.length * 9) + '</strong> </p>');
        if (courseInfo.golf_details.course_total_par != null && courseInfo.golf_details.course_total_par != undefined) {
            var labtxt = localizeString("GolfCourse_Label_Par");
            labtxt = labtxt.charAt(0).toUpperCase() + labtxt.substr(1).toLowerCase();
            var p5 = $('<p style="font-size:20px; margin-bottom:5px;"> <span class="text-bold-01">' + labtxt + ':</span> <strong>' + courseInfo.golf_details.course_total_par + '</strong> </p>');
            td1.append(p4, p5);
        } else {
            td1.append(p4);
        }

        tr1.append(td1);

        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');

        var tee_box_arr = {};
        // Iterate tee box to build tee boxes object
        if (courseInfo.golf_details.tee_boxes && courseInfo.golf_details.tee_boxes.length > 0) {
            for (var k = 0; k < courseInfo.golf_details.tee_boxes.length; k++) {
                tee_box_arr["" + courseInfo.golf_details.tee_boxes[k].master_id] = courseInfo.golf_details.tee_boxes[k].name;
            }
        }

        if (courseInfo.golf_details.round_setup) {
            for (var i = 0; i < courseInfo.golf_details.round_setup.length; i++) {
                var rtable = $('<table cellspacing="0" class="table table-bordered box box-solid" id="Rounds" align="center"></table>');
                var rmain = $('<tr style="color:#fff;"></tr>');
                var holesets = "";
                for (var j = 0;  j < courseInfo.golf_details.hole_sets.length; j++) {
                    if (courseInfo.golf_details.round_setup[i].hole_set_1 == courseInfo.golf_details.hole_sets[j].master_id)
                        holesets = holesets + " " + courseInfo.golf_details.hole_sets[j].name + ",";
                    if (courseInfo.golf_details.round_setup[i].hole_set_2 == courseInfo.golf_details.hole_sets[j].master_id)
                        holesets = holesets + " " + courseInfo.golf_details.hole_sets[j].name + ",";
                }
                holesets = holesets.replace(/,\s*$/, "");
                rmain.append('<th colspan="3" width="100%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + courseInfo.golf_details.round_setup[i].name + '<p style="font-size:13px!important; line-height: 0px;"> (' + holesets.trim() + ')</p></th>');
                rtable.append(rmain); // Round Name & sets Done
                // Now add tee info within the table
                if (courseInfo.golf_details.tee_boxes.length > 0) {
                    //Check if the CR or SR is available
                    var nonZeroCR = false;
                    var nonZeroSR = false;
                    if (courseInfo.golf_details.round_setup[i].crsr) {
                        for (var j = 0; j < courseInfo.golf_details.round_setup[i].crsr.length; j++) {
                            if (courseInfo.golf_details.round_setup[i].crsr[j].cr
                                && (0.0 != parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].cr).toFixed(1))) {
                                nonZeroCR = true;
                            }
                            if (courseInfo.golf_details.round_setup[i].crsr[j].sr
                                && (0.0 != parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].sr).toFixed(1))) {
                                nonZeroSR = true;
                            }
                        }
                        var teemain = $('<tr style="color:#fff;"></tr>');

                        if (nonZeroCR && nonZeroSR) {
                            teemain.append('<th width="50%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + localizeString("GolfCourse_Label_TeeBox") + '</th>');
                            teemain.append('<th width="25%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + "CR" + '</th>');
                            teemain.append('<th width="25%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + "SR" + '</th>');
                        } else if (nonZeroCR || nonZeroSR) {
                            teemain.append('<th width="60%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + localizeString("GolfCourse_Label_TeeBox") + '</th>');
                            if (nonZeroCR)
                                teemain.append('<th width="40%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + "CR" + '</th>');
                            else
                                teemain.append('<th width="40%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + "SR" + '</th>');
                        } else {
                            teemain.append('<th width="100%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + localizeString("GolfCourse_Label_TeeBox") + '</th>');
                        }
                        rtable.append(teemain);

                        for (var j = 0; j < courseInfo.golf_details.round_setup[i].crsr.length; j++) {
                            var teemain1 = $('<tr style="color:#fff;"></tr>');
                            teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important; font-weight:500!important;">' + tee_box_arr["" + courseInfo.golf_details.round_setup[i].crsr[j].tee].toUpperCase() + '</a></td>');
                            if (nonZeroCR && nonZeroSR) {
                                if ("" != courseInfo.golf_details.round_setup[i].crsr[j].cr)
                                    teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">' + parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].cr).toFixed(1) + '</a></td>');
                                else
                                    teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">0.0</a></td>');
                                if ("" != courseInfo.golf_details.round_setup[i].crsr[j].sr)
                                    teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">' + parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].sr).toFixed(1) + '</a></td>');
                                else
                                    teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">0.0</a></td>');
                            } else if (nonZeroCR || nonZeroSR) {
                                if (nonZeroCR) {
                                    if ("" != courseInfo.golf_details.round_setup[i].crsr[j].cr)
                                        teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">' + parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].cr).toFixed(1) + '</a></td>');
                                    else
                                        teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">0.0</a></td>');
                                } else {
                                    if ("" != courseInfo.golf_details.round_setup[i].crsr[j].sr)
                                        teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">' + parseFloat(courseInfo.golf_details.round_setup[i].crsr[j].sr).toFixed(1) + '</a></td>');
                                    else
                                        teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important;">0.0</a></td>');
                                }
                            }
                            rtable.append(teemain1);
                        }
                    } else { // There is no CR SR So just show the tee boxes
                        var teemain = $('<tr style="color:#fff;"></tr>');
                        teemain.append('<th width="100%" bgcolor="#3a3a3a" style="font-size:16px!important;">' + localizeString("GolfCourse_Label_TeeBox") + '</th>');
                        rtable.append(teemain);
                        for (var k = 0; k < courseInfo.golf_details.tee_boxes.length; k++) {
                            var teemain1 = $('<tr style="color:#fff;"></tr>');
                            teemain1.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle" style="font-size:16px!important; font-weight:500!important;">' + courseInfo.golf_details.tee_boxes[k].name + '</a></td>');
                            rtable.append(teemain1);
                        }
                    }
                }
                rtable.insertBefore('#buttontr');
            }
        }

        var tr2 = $('<tr></tr>');
        var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
        tr2.append(td2);
        tr2.insertBefore('#buttontr');

        window.plugins.spinnerDialog.hide();
        /* Show golf course hole informations */
        showGolfHoleDetails(courseInfo);
    }


    /**
     * Display the per hole information of the golf course.
     */
    showGolfHoleDetails = function (courseInfo) {
        var totalSidePar = 0;
        localStorage.setItem("golfCourseInfo", JSON.stringify(courseInfo));
        $("#swipecont").html('');

        for (var i = 0;  i < courseInfo.golf_details.hole_sets.length; i++) {
            totalSidePar = 0;
            var swipe = $('<div class="swiper-slide" id=' + courseInfo.golf_details.hole_sets[i].id + '></div>');
            var first = $('<div class="head-hole" style="padding-top:17px!important;"><table width="96%" border="0" cellspacing="5" style="margin:0px auto;"><tbody><tr><td width=13%><div class="swiper-button-next" style="position:fixed; float:right; right:12px;"><i class="fa fa-chevron-right"></i></div></td><td width="70%" align="center"><div class="hole-par-no" style="width:100%!important;"><div>' + courseInfo.golf_details.hole_sets[i].name + '</div><div></td><td width="13%"><div class="swiper-button-prev" style="position:fixed; float:left; left:12px;"><i class="fa fa-chevron-left"></i></div></td></tr></tbody></table>');
            swipe.append(first);

            var topDiv = $("<div class='wysiwyg-img'></div>");
            topDiv.append("<p></p>");
            for (var k = 0; k < courseInfo.golf_details.hole_sets[i].holes.length; k++) {
                var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px; padding-left:12px; padding-right:12px;">Hole ' + (i*9 + k + 1) + ' (Par ' + courseInfo.golf_details.hole_sets[i].holes[k].par + ', Index ' + courseInfo.golf_details.hole_sets[i].holes[k].index + ')</p>');
                topDiv.append(p1);
                if (courseInfo.golf_details.hole_sets[i].holes[k].image != null
                    && courseInfo.golf_details.hole_sets[i].holes[k].image != undefined
                    && courseInfo.golf_details.hole_sets[i].holes[k].image != "") {
                    var img = $('<img style="width:100%; padding-left:12px; padding-right:12px;" src="' + courseInfo.imageBasePath + courseInfo.golf_details.hole_sets[i].holes[k].image +'">');
                    topDiv.append(img);
                }
                if (courseInfo.golf_details.hole_sets[i].holes[k].description != null
                    && courseInfo.golf_details.hole_sets[i].holes[k].description != undefined
                    && courseInfo.golf_details.hole_sets[i].holes[k].description != "") {
                    var p2 = $('<p style="padding-left:12px; padding-right:12px;">' + courseInfo.golf_details.hole_sets[i].holes[k].description + '</p>');
                    topDiv.append(p2);
                }
                var tableDiv = $('<div style="margin-left:12px!important; margin-right:12px!important;"></div>');
                var table = $('<table cellspacing="0" class="table table-bordered box box-solid" id="scorecard_main_"' + i +' align="center"></table>');
                var main = $('<tr style="color:#fff;"></tr>');
                var tee_boxes_array = [];
                for (var j = 0; j < courseInfo.golf_details.tee_boxes.length; j++) {
                    if (courseInfo.golf_details.hole_sets[i].holes[k].distance[courseInfo.golf_details.tee_boxes[j].master_id] != null) {
                        tee_boxes_array[j] = courseInfo.golf_details.tee_boxes[j].master_id;
                        main.append('<th width="20%" bgcolor="#3a3a3a">' + courseInfo.golf_details.tee_boxes[j].name + '</th>');
                    }
                }
                table.append(main);
                var main1 = $('<tr style="color:#fff;"></tr>');
                for (var l = 0; l < tee_boxes_array.length; l++) {
                    if (courseInfo.golf_details.hole_sets[i].holes[k].distance[tee_boxes_array[l]] != null
                        && courseInfo.golf_details.hole_sets[i].holes[k].distance[tee_boxes_array[l]] != undefined)
                        main1.append('<td bgcolor="#8598A1" height="40"><a href="#credits" class="toggle">' + courseInfo.golf_details.hole_sets[i].holes[k].distance[tee_boxes_array[l]] + '</a></td>');
                }
                table.append(main1);

                tableDiv.append(table);
                topDiv.append(tableDiv);
                topDiv.append('<p align="right" class="text_p" style="border-top: #babab9 2px solid; padding-top:10px; padding-left:12px; padding-right:12px; padding-bottom:10px;"></p>');
            }

            swipe.append(topDiv);
            $("#swipecont").append(swipe);
        }
        $('#tablediv').css('display', 'inline');

        var appendNumber = 1;
        var prependNumber = 1;
        var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
            nextButton: '.swiper-button-next',
            prevButton: '.swiper-button-prev',
            slidesPerView: 1,
            centeredSlides: true,
            paginationClickable: true,
            spaceBetween: 30,
        });
        document.querySelector('.prepend-2-slides').addEventListener('click', function (e) {
            e.preventDefault();
            swiper.prependSlide([
              '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>',
              '<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>'
              ]);
        });
        document.querySelector('.prepend-slide').addEventListener('click', function (e) {
            e.preventDefault();
            swiper.prependSlide('<div class="swiper-slide">Slide ' + (--prependNumber) + '</div>');
        });
        document.querySelector('.append-slide').addEventListener('click', function (e) {
            e.preventDefault();
            swiper.appendSlide('<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>');
        });
        document.querySelector('.append-2-slides').addEventListener('click', function (e) {
            e.preventDefault();
            swiper.appendSlide([
                '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>',
                '<div class="swiper-slide">Slide ' + (++appendNumber) + '</div>'
            ]);
        });
    }


    /**
     * Show Front (hole 1 to 9) part of the golf course Info
     */
    showFront = function () {
        start = 0;
        end = 9;
        totalPar = 0;
        currentSide = 0;
        document.getElementById('back').style.pointerEvents = 'auto';
        showGolfHoleDetails(JSON.parse(localStorage.getItem("golfCourseInfo")));
    }


    /**
     * Show Back (hole 9 to 18) part of the golf course Info
     */
    showBack = function () {
        var courseInfo = JSON.parse(localStorage.getItem("golfCourseInfo"));
        start = 9;
        end = courseInfo.hole_details.holes.length;
        currentSide = 1;
        document.getElementById('back').style.pointerEvents = 'none';
        showGolfHoleDetails(courseInfo);
    }


    /**
     * Fetches list of all the Golf tournaments from the server
     */
    getGolfTournamentList = function () {
        var actions = {
            "type" : "1"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length > 0)
                    displayGolfTournamentList(data.data);
                else
                    $('.row').html('<p style="text-align:center">' + localizeString("GlofTournaments_Label_NoTournament") + '</p>');
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/tournaments", actions, successCallback, errorCallback);
    }


    getTournamentImage = function (base_path, tournament_img) {
        if (tournament_img !== false && tournament_img !== null && tournament_img !== "") {
            return base_path + tournament_img;
        } else {
            return "images/default-listing.png";
        }
    }


    /**
     * Renders the list of all the tournaments
     * @data tournament list data
     */
    displayGolfTournamentList = function (tournamentList) {
        if (enableDebugMode)
            alert("displayGolfTournamentList = " + JSON.stringify(tournamentList));

        if (tournamentList.content.length < 1) {
            $('.row').html('<p style="text-align:center">' + localizeString("GlofTournaments_Label_NoTournament") + '</p>');
            return;
        }

        for (i = 0; i < tournamentList.content.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoGolfTournamentDetails(' + tournamentList.content[i].id + ')"></div>');
            var div2 = $('<div class="card-01"></div>');
            var imagefortournament = getTournamentImage(tournamentList.imageBasePath, tournamentList.content[i].image);
            var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
            aside.append(imgDiv);
            var div3 = $('<div class="card-main card-width"></div>');
            var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
            var tournmanentTitle = tournamentList.content[i].title;
            var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

            var div5 = $('<div class="blog-post-meta" style="padding-top:6px;"></div>');
            var span1 = $('<span></span>');
            var dispDat = getDisplayableDate(tournamentList.content[i].start_date);
            var dispDat2 = getDisplayableDate(tournamentList.content[i].end_date);
            var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + dispDat + '</i></br>');
            var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + dispDat2 + ' </i>');
            span1.append(ielem1, ielem2);
            div5.append(span1);
            div4.append(p1, div5);
            div3.append(div4);
            div2.append(aside, div3);
            div1.append(div2);
            $('.row').append(div1);
        }
        lazyLoadListImages(page_number, tournamentList.content.length);
    }


    /**
     * Go to Golf Tournament Details
     */
    gotoGolfTournamentDetails = function (tournament_id) {
        localStorage.setItem("golftournamentid", tournament_id);
        slideview('golf-tournament-details.html', 'script', 'left');
    }


    /**
     * Fetch Golf tournament details
     */
    getGolfTournamentDetails = function () {
        var actions = {
            "id": localStorage.getItem("golftournamentid"),
            "type": "1"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayGolfTournamentDetails(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        }
        errorCallback = function (data) {
            window.plugins.spinnerDialog.hide();
        };
        postMessageToServerWithAuth("/tournamentDetails", actions, successCallback, errorCallback);
    }


    /**
     * Render tournament Details on UI
     * @tournamentDetails Tournament Details received from the backend
     */
    displayGolfTournamentDetails = function (tournamentDetails) {
        if (enableDebugMode)
            alert("Tournament Details : " + JSON.stringify(tournamentDetails));

        //var tr1 = $('<tr></tr>');
        var dispDat = getDisplayableDate(tournamentDetails.content.start_date);
        var dispDat2 = getDisplayableDate(tournamentDetails.content.end_date);
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(tournamentDetails.content.tournament_title);
        var span1 = $('<span style=" font-size:12px;"></span>');
        var date = "&nbsp;" + localizeString("Label_StartDate") + "&nbsp;: " + dispDat;
        var ielem2 = $('<i class="fa fa-clock-o"></i>');
        var ielem21 = $('<i class="fa fa-clock-o"></i>');
        var date1 = "&nbsp;" + localizeString("Label_EndDate") + "&nbsp;&nbsp;: " + dispDat2;
        span1.append("&nbsp;&nbsp;", ielem2, date, "<br/>");
        span1.append("&nbsp;&nbsp;", ielem21, date1);
        var p2 = $('<p style="word-wrap:break-word; word-break:break-word;">' + tournamentDetails.content.description + '</p>');
        td1.append(p1, span1, p2);
        //tr1.append(td1);

        $("#buttontr").css('display', 'block');
        //tr1.insertBefore('#buttontr');

        $('#buttontr').append(td1, "<br/>", "<br/>");

        if ((localStorage.getItem("isLoggedIn") == "true")
            && (localStorage.getItem("golfTournamentRegistrationEnabled") == "true")
            && (parseInt(tournamentDetails.content.number_of_events) > 0)) {
            for (var i = 0; i < tournamentDetails.content.event_list.length; i++) {
                option = document.createElement('option');
                option.setAttribute('value', tournamentDetails.content.event_list[i].id);
                option.appendChild(document.createTextNode(tournamentDetails.content.event_list[i].name));
                $("#event-type").append(option);
            }
            $("#event-picker-form").css('display', 'block');
            $("#registrationbuttondiv").css('display', 'block');
        }
    }


    resetErrorMessage = function () {
        $("#eventerror").css("display", "none");
    }

    validateEventSelection = function (optionVal) {
        if (optionVal == 0)
            return false;
        return true;
    }


    /**
     * Register user for the tournament
     */
    registerTournament = function () {
        if (false == validateEventSelection($("#event-type option:selected").val())) {
            $("#eventerror").css("display", "inline");
            $("#eventerror").html(localizeString("ValidationError_EventType") + '<i class="form-help-icon icon">error</i>');
            return;
        }

        var actions = {
            "tournament_id": localStorage.getItem("golftournamentid"),
            "type": "1",
            "event_id": $("#event-type option:selected").val()
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {

                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: false,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    slideview('golf-tournaments-my-history.html', 'script', 'left');
                });
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/tournaments/golfRegistration", actions, successCallback, errorCallback);
    }


    /**
     * Fetch tournament history for the current user
     */
    getMyGolfTournamentHistory = function () {
        var actions = {
            "type": "1"
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length > 0)
                    displayMyGolfTournamentHistory(data.data);
                else
                    $('.row').html('<p style="text-align:center"> ' + localizeString("GlofTournaments_Label_NoRegister") + ' </p>');
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function (data) {
        };

        postMessageToServerWithAuth("/tournaments/history/user", actions, successCallback, errorCallback);
    }


    /**
     * Render Users tournament registration detais
     */
    displayMyGolfTournamentHistory = function (tournamentHistoryList) {
        if (enableDebugMode)
            alert("tournamentHistoryList = " + JSON.stringify(tournamentHistoryList));
        for (i = 0; i < tournamentHistoryList.content.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoMyGolfTournamentHistoryDetail(' + tournamentHistoryList.content[i].tournament_id + ', ' + tournamentHistoryList.content[i].event_id + ')"></div>');
            var div2 = $('<div class="card-01"></div>');
            var imagefortournament = getTournamentImage(tournamentHistoryList.imageBasePath, tournamentHistoryList.content[i].image);
            var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
            aside.append(imgDiv);
            var div3 = $('<div class="card-main card-width"></div>');
            var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
            var tournmanentTitle = tournamentHistoryList.content[i].title;
            var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

            var div5 = $('<div class="blog-post-meta" style="padding-top:6px!important;"></div>');
            var span1 = $('<span></span>');

            var dispDat = getDisplayableDate(tournamentHistoryList.content[i].start_date);
            var dispDat2 = getDisplayableDate(tournamentHistoryList.content[i].end_date);

            var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + dispDat + '</i></br>');
            var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + dispDat2 + ' </i>');
            span1.append(ielem1, ielem2);
            div5.append(span1);
            div4.append(p1, div5);
            div3.append(div4);
            div2.append(aside, div3);
            div1.append(div2);
            $('.row').append(div1);
        }
        lazyLoadListImages(page_number, tournamentHistoryList.content.length);
    }


    /**
     * GoTo Registered event details page
     * @tournamentId Unique tournament Identifier
     * @eventId Unique event identifier
     */
    gotoMyGolfTournamentHistoryDetail = function (tournamentId, eventId) {
        localStorage.setItem("mygolftournamentid", tournamentId);
        localStorage.setItem("mygolfeventid", eventId);
        slideview('golf-tournament-my-history-details.html', 'script', 'left');
    }


    /**
     * Fetch the Registered event History details
     */
    getMyGolfTournamentHistoryDetails = function () {
        var actions = {
            "tournamentId": localStorage.getItem("mygolftournamentid"),
            "eventId": localStorage.getItem("mygolfeventid"),
            "type": "1"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                displayMyGolfTournamentHistoryDetails(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function (data) {
        };

        postMessageToServerWithAuth("/golfTournamentDetails", actions, successCallback, errorCallback);
    }


    /**
     * Display the golf registration detais for the player
     * @tournamentDetails registration details for the player
     */
    displayMyGolfTournamentHistoryDetails = function (tournamentDetails) {
        if (enableDebugMode)
            alert("tournamentEventDetails = " + JSON.stringify(tournamentDetails));
        var dispDat = getDisplayableDate(tournamentDetails.start_date);
        var dispDat2 = getDisplayableDate(tournamentDetails.end_date);
        var dispDat3 = getDisplayableDate(tournamentDetails.created_on.split(' ')[0]);
        var dispTim = getDisplayableTime(tournamentDetails.created_on.split(' ')[1]);
        $("#tournament_title").text(tournamentDetails.tournament_title);
        $("#event_tite").text(localizeString("RegistrationDetail_Label_RegisteredEvent") + " : " + tournamentDetails.event_name);
        $("#start_date").html(localizeString("RegistrationDetail_Label_StartDate") + " : " + dispDat + "  ");
        $("#end_date").html(localizeString("RegistrationDetail_Label_EndDate") + " : " + dispDat2 + "  ");
        $("#registered_on").html(localizeString("RegistrationDetail_Label_RegistrationDate") + " : " + [dispDat3, dispTim].join(' ') + "  ");
        $("#registeration_id").html(localizeString("RegistrationDetail_Label_RegistrationID") + " : " + tournamentDetails.reg_id + "  ");

        localStorage.setItem("mygolfeventregistrationid", tournamentDetails.reg_id);
        localStorage.setItem("mygolftournamentid", tournamentDetails.tournament_id);
        localStorage.setItem("mygolfeventid", tournamentDetails.event_id);

        var sDate = null;
        var sTime = null;
        var eDate = null;
        var eTime = null;
        var title = tournamentDetails.tournament_title + " : " + tournamentDetails.event_name;
        var location = "";
        var notes = "";
        if (tournamentDetails.event_start_status == 0) {
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus") + " : " + localizeString("GolfTournaments_Label_StatusNot"));
            $("#cancle_registration").css("display", "block");
        } else if (tournamentDetails.event_start_status == 1) { // Event is live
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus") + " : " + localizeString("GolfTournaments_Label_StatusLive"));
            if (tournamentDetails.reg_status == "1") { // Registration status approved
                $("#cancle_registration").css("display", "none");
                if (tournamentDetails.already_marked == 0) {
                    if (tournamentDetails.un_marked_player_list.length > 0) {
                        for (var i = 0; i < tournamentDetails.un_marked_player_list.length; i++) {
                            option = document.createElement('option');
                            option.setAttribute('value', tournamentDetails.un_marked_player_list[i].id);
                            option.appendChild(document.createTextNode(tournamentDetails.un_marked_player_list[i].name));
                            $("#marker-name").append(option);
                        }
                        $("#marker-picker-form").css("display", "block");
                        $("#mark_player").css("display", "block");
                    } else {
                        // user has not marked any player and there are no player to mark for
                        // and tournament in progress, so allow user to cancle his registration
                        $("#cancle_registration").css("display", "block");
                    }
                } else { //palyer already marked so now we can show him scorecard
                    $("#marker-name").prop('disabled', true);
                    $("#mark_player").prop('disabled', true);
                    $("#player_scorecard").css("display", "block");
                }
            } else if (tournamentDetails.reg_status == "0") { // Registration status pending approval
                $("#cancle_registration").css("display", "block");
            } else {
                //  Do nothing
            }
        } else {
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus") + " : " + localizeString("GolfTournaments_Label_StatusConcluded"));
            $("#cancle_registration").css("display", "none");
        }

        if (tournamentDetails.reg_status == "0") {
            status = localizeString("GolfTournaments_Label_RegStatusPending");
        } else if (tournamentDetails.reg_status == "1") {
            sDate = tournamentDetails.start_date;
            eDate = tournamentDetails.end_date;
            sTime = tournamentDetails.tee_off_time.split(" ")[1] + ":00";
            title = title.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            location = location.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            notes = notes.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + title + '\', \'' + location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
            reminder.insertAfter("#tournament_title");
            $("#updateplayer").css("display", "none");
            status = localizeString("GolfTournaments_Label_RegStatusApproved");
            /**
             * Show tee off hole and tee off time only when tournament registration is approved
             */
            $("#tee_off_hole").html(localizeString("GolfTournamentsDetails_Label_TeeOffHole") + " : " + tournamentDetails.tee_off_hole_num);
            $("#tee_off_time").html(localizeString("GolfTournamentsDetails_Label_TeeOffTime") + " : " + tournamentDetails.tee_off_time);
            $("#tee_off_hole").css("display", "block");
            $("#tee_off_time").css("display", "block");
        } else if (tournamentDetails.reg_status == "2") {
            status = "Admin Rejected";
            //$("#cancelbtn").css("display", "none");
            $("#cancle_registration").css("display", "none");
            $("#updateplayer").css("display", "none");
        } else if (tournamentDetails.reg_status == "3") {
            status = "User Cancelled";
            //$("#cancelbtn").css("display", "none");
            $("#cancle_registration").css("display", "none");
            $("#updateplayer").css("display", "none");
        }
        $("#registration_status").text(localizeString("RegistrationDetail_Label_RegistrationStatus") + " : " + status);
    }


    /**
     * Reset Marker selection error messages
     */
    resetMarkerErrorMessage = function () {
        $("#markererror").css("display", "none");
    }


    /**
     * Cancel players registration for the tournament
     */
    canclePlayerRegistration = function () {
        swal({
            title: localizeString("Alert_Title_CancelRegistration"),
            text: localizeString ("Alert_Text_CancelRegistration"),
            type: "warning",
            showCancelButton: true,
            closeOnConfirm: false,
            confirmButtonText: localizeString("Alert_Button_PleaseCancel"),
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            var actions = {
                "tournamentId": localStorage.getItem("mygolftournamentid"),
                "eventId": localStorage.getItem("mygolfeventid"),
                "registration_id": localStorage.getItem("mygolfeventregistrationid"),
                "type": "1"
            };

            successCallback = function (data) {
                if (data.status == "success") {
                    //swal.close();
                    swal({
                        title: localizeString("Alert_Title_Success"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function () {
                        gotoMyGolfTournamentHistoryDetail(localStorage.getItem("mygolftournamentid"), localStorage.getItem("mygolfeventid"));
                    });
                } else {
                    var thisone = this;
                    swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function () {
                        return;
                    });
                }
                window.plugins.spinnerDialog.hide();
            }

            errorCallback = function (data) {
            };

            postMessageToServerWithAuth("/tournaments/registration/cancel", actions, successCallback, errorCallback);
        });
    }


    /**
     * Mark Player to add scores for
     */
    setMarkedPlayer = function () {
        if (false == validateEventSelection($("#marker-name option:selected").val())) {
            $("#markererror").css("display", "inline");
            $("#markererror").html(localizeString("ValidationError_SelectPlayer")+'<i class="form-help-icon icon">error</i>');
            return;
        }

        var actions = {
            "tournamentId": localStorage.getItem("golftournamentid"),
            "eventId": localStorage.getItem("mygolfeventid"),
            "type": "1",
            "markedForId": $("#marker-name option:selected").val()
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: false,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    slideview('golf-tournament-my-history-details.html', 'script', 'left');
                });
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/setMarkerForGolfTournament", actions, successCallback, errorCallback);
    }


    showPlayerScoreCard = function () {
        localStorage.setItem("currentSelectedHoleId", "0");
        slideview('golf-score-card.html', 'script', 'left');
    }


    loadPlayerScoreCardDetails = function () {
        var actions = {
            "tournamentId": localStorage.getItem("golftournamentid"),
            "eventId": localStorage.getItem("mygolfeventid"),
            "type": "1"
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                localStorage.setItem("scorecardDetails", JSON.stringify(data.data));
                displayScoreCardView(data.data); // Start off by showing first side only
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/golfTournamentScoreCard", actions, successCallback, errorCallback);
    }


    displayScoreCardView = function (scorecardDetails) {
        if (enableDebugMode)
            alert("Score Card Data = " + JSON.stringify(scorecardDetails));
        $('#scoredetails').html("");
        $("#tournament_title").text(scorecardDetails.tournamentInfo.tournament_title);
        $("#event_tite").text(localizeString("RegistrationDetail_Label_RegisteredEvent")+" : " + scorecardDetails.tournamentInfo.event_name);
        $("#start_date").html(localizeString("RegistrationDetail_Label_StartDate")+" : "+ scorecardDetails.tournamentInfo.start_date + "  ");
        $("#end_date").html(localizeString("RegistrationDetail_Label_EndDate")+" : " + scorecardDetails.tournamentInfo.end_date + "  ");
        $("#registered_on").html(localizeString("RegistrationDetail_Label_RegistrationDate")+" : " + scorecardDetails.tournamentInfo.created_on + "  ");
        $("#registeration_id").html(localizeString("RegistrationDetail_Label_RegistrationID")+" : " + scorecardDetails.tournamentInfo.reg_id + "  ");
        //$('#scoredetails').css("display", "none");
        if (scorecardDetails.tournamentInfo.event_start_status == 0) {
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus")+" : "+ localizeString("GolfTournaments_Label_StatusNot"));
        } else if (scorecardDetails.tournamentInfo.event_start_status == 1) {
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus")+" : "+ localizeString("GolfTournaments_Label_StatusLive"));
        } else {
            $("#event_status").text(localizeString("RegistrationDetail_Label_TournamentStatus")+" : "+ localizeString("GolfTournaments_Label_StatusConcluded"));
        }

        if (scorecardDetails.tournamentInfo.reg_status == 0) {
            status = localizeString("GolfTournaments_Label_RegStatusPending");
        } else if (scorecardDetails.tournamentInfo.reg_status == 1) {
            status = localizeString("GolfTournaments_Label_RegStatusApproved");

        } else if (scorecardDetails.tournamentInfo.reg_status == 2) {
            status = localizeString("GolfTournaments_Label_RegStatusRejected");
        } else if (scorecardDetails.tournamentInfo.reg_status == 3) {
            status = localizeString("GolfTournaments_Label_RegStatusCancelled");
        }
        $("#registration_status").text(localizeString("RegistrationDetail_Label_RegistrationStatus")+" : " + status);

        $("#tee_off_hole").html(localizeString("GolfTournamentsDetails_Label_TeeOffHole")+" : "+ scorecardDetails.tournamentInfo.tee_off_hole_num);
        $("#tee_off_time").html(localizeString("GolfTournamentsDetails_Label_TeeOffTime")+" : "+ scorecardDetails.tournamentInfo.tee_off_time);
        $("#tee_off_hole").css("display", "block");
        $("#tee_off_time").css("display", "block");


        localStorage.setItem("golftournamentid", scorecardDetails.tournamentInfo.tournament_id);
        localStorage.setItem("mygolfeventid", scorecardDetails.tournamentInfo.event_id);
        localStorage.setItem("mygolfmarkedid", scorecardDetails.markingFor[0].markedFor);

        if (scorecardDetails.tournamentInfo.image != null && scorecardDetails.tournamentInfo.image != "") {
            $('#topbanner').attr({
                'src': scorecardDetails.tournamentInfo.image
            });
        }

        var currHoleId = parseInt(localStorage.getItem("currentSelectedHoleId"));
        if (0 == currHoleId) {
            for (var i = 0; i < scorecardDetails.golfCourseDetails.holes.length; i++) {
                option = document.createElement('option');
                option.setAttribute('value', scorecardDetails.golfCourseDetails.holes[i].hole);
                option.appendChild(document.createTextNode(scorecardDetails.golfCourseDetails.holes[i].hole));
                $("#hole-number").append(option);
            }
        }
        if (scorecardDetails.golfCourseDetails.holes.length > 0) {
            $("#hole-picker-form").css("display", "block");
            /* Show the last selected hole information */
            showLastSelectedHoleScore(localStorage.getItem("currentSelectedHoleId"));
        }
    }


    /**
     * Display Update Score for a specific hole
     * @holeId golf hole Id
     * @scorecardDetails complete scorecard json
     */
    displayPlayersUpdatedScoreView = function (holeId, scorecardDetails) {
        for (var j = 0; j < scorecardDetails.playersInMyGroup.length; j++) { // iterate over the players
            if (scorecardDetails.scoreData[holeId-1].scores[j] != null && scorecardDetails.scoreData[holeId-1].scores[j].score != null
                && scorecardDetails.scoreData[holeId-1].scores[j].score != undefined) {
                var divId = "score_" + scorecardDetails.scoreData[holeId-1].scores[j].playerId + "_" + holeId;
                document.getElementById(divId).getElementsByTagName("a")[0].innerHTML = scorecardDetails.scoreData[holeId - 1].scores[j].score;
            }
        }
    }


    /**
     * Fixes UI to show last saved hole information.
     * On saving the score, the UI and scores are updated.
     * @holeId last updated hole Id
     */
    showLastSelectedHoleScore = function (holeId) {
        $("#hole-number").value = holeId;
        handleHoleIdChange();
    }


    /**
     * OnChange Handler for hole select option
     */
    handleHoleIdChange = function () {
        var selectedHoleId = parseInt($("#hole-number option:selected").val());
        localStorage.setItem("currentSelectedHoleId", selectedHoleId);
        var scorecardDetails = JSON.parse(localStorage.getItem("scorecardDetails"));
        if (selectedHoleId != 0) {
            $('#main').html("");
            var main = $('<tr style="color:#fff;"></tr>');
            main.append('<th width="14%" bgcolor="#3a3a3a">Holes</th>');
            main.append('<th width="14%" bgcolor="#3a3a3a">Par</th>');
            for (var i = 0; i < scorecardDetails.playersInMyGroup.length; i++) {
                var res = scorecardDetails.playersInMyGroup[i].userName.split(" ");
                var name;
                if (res.length > 1) {
                    name = res[0].charAt(0) + "" + res[1].charAt(0);
                } else {
                    name = res[0].charAt(0) + "" + res[0].charAt(1);
                }
                if (scorecardDetails.markingFor[0].markedFor == scorecardDetails.playersInMyGroup[i].playerId)
                    name = name + "#";
                main.append('<th width="14%" bgcolor="#3a3a3a">' + name + '</th>');
            }
            $('#main').append(main);
            var main = $('<tr style="color:#fff;"></tr>');
            main.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle">' + scorecardDetails.golfCourseDetails.holes[selectedHoleId - 1].hole + '</a></td>');
            main.append('<td bgcolor="#b2b2b2" height="40"><a href="#credits" class="toggle">' + scorecardDetails.golfCourseDetails.holes[selectedHoleId - 1].par + '</a></td>');
            for (var j = 0; j < scorecardDetails.playersInMyGroup.length; j++) {
                var divId = "score_" + scorecardDetails.playersInMyGroup[j].playerId + "_" + selectedHoleId;
                if (1 == scorecardDetails.playersInMyGroup[j].editable && scorecardDetails.scoreData[selectedHoleId - 1].scores[j].score == "NA") {
                    main.append('<td id=' + divId + ' style="background-color:#8598A1;" height="40" onclick="selectNumPad(\'' + divId + '\');" ><a href="#credits" class="toggle"></a></td>');
                }
                else {
                    main.append('<td id=' + divId + ' style="background-color:#8598A1;" height="40"><a href="#credits" class="toggle"></a></td>');
                }
            }
            $('#main').append(main);
            $("#savesubmit").css("display", "block");
            displayPlayersUpdatedScoreView(selectedHoleId, scorecardDetails);
        } else  {
            $('#main').html("");
            $("#savesubmit").css("display", "none");
        }
    }


    selectNumPad = function (id) {
        $('#credits').removeClass("hidden");
        clearTimeout(timer);
        timer = setTimeout(function () {
            //your code to be executed after 1 seconds
            $('#credits').addClass("hidden");
        }, 4000);

        if (localStorage.getItem("selectPlayerHoleId") != "") {
            if (document.getElementById(localStorage.getItem("selectPlayerHoleId")) != null) {
                document.getElementById(localStorage.getItem("selectPlayerHoleId")).style.backgroundColor = "#8598A1";
            }
        }
        document.getElementById(id).style.backgroundColor = "#0A3043";
        localStorage.setItem("selectPlayerHoleId", id);
    }


    keys = function (i) {
        if (i == "") {
            document.getElementById(localStorage.getItem("selectPlayerHoleId")).getElementsByTagName("a")[0].innerHTML = "";
        } else {

            document.getElementById(localStorage.getItem("selectPlayerHoleId")).getElementsByTagName("a")[0].innerHTML = i;
        }
    }


    /**
     * Save updated scorecard
     */
    saveScoreCard = function () {
        var scorecardDetails = JSON.parse(localStorage.getItem("scorecardDetails"));
        var holeId = $("#hole-number option:selected").val();
        var playerId = scorecardDetails.self.playerId;
        var divId = "score_" + playerId + "_" + holeId;
        var myScore = document.getElementById(divId).getElementsByTagName("a")[0].innerHTML;
        var divId = "score_" + localStorage.getItem("mygolfmarkedid") + "_" + holeId;
        var markedScore = document.getElementById(divId).getElementsByTagName("a")[0].innerHTML;
        localStorage.setItem("currentSelectedHoleId", holeId);
        var actions = {
            "tournamentId": localStorage.getItem("golftournamentid"),
            "eventId": localStorage.getItem("mygolfeventid"),
            "holeId": holeId,
            "selfScore": myScore,
            "markedPlayerId": localStorage.getItem("mygolfmarkedid"),
            "markedPlayerScore": markedScore
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                /*var select = document.getElementById("hole-number");
                for (var i = 1; i <= scorecardDetails.golfCourseDetails.holes.length; i++) {
                    select.options[i] = null;
                }*/
                loadPlayerScoreCardDetails();
            } else {
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/golfTournamentSubmitScore", actions, successCallback, errorCallback);
    }


    /**
     * fetch the List of All the Golf Tournaments from the server for Results
     */
    getGolfTournamentListForResults = function () {
        var actions = {
            "type" : "1"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length > 0)
                    displayGolfTournamentListForResults(data.data);
                else
                    $('.row').html('<p style="text-align:center">' + localizeString("GlofTournaments_Label_NoTournament") + '</p>');
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/tournaments", actions, successCallback, errorCallback);
    }


    /**
     * Renders the list of all the tournaments
     * @tournamentList tournament list data
     */
    displayGolfTournamentListForResults = function (tournamentList) {
        if (enableDebugMode)
            alert("displayGolfTournamentList = " + JSON.stringify(tournamentList));

        if (tournamentList.content.length < 1) {
            $('.row').html('<p style="text-align:center">' + localizeString("GlofTournaments_Label_NoTournament") + '</p>');
            return;
        }

        for (i = 0; i < tournamentList.content.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-12" onclick="gotoGolfTournamentResultDetails(' + tournamentList.content[i].id + ')"></div>');
            var div2 = $('<div class="card-01"></div>');
            var imagefortournament = getTournamentImage(tournamentList.imageBasePath, tournamentList.content[i].image);
            var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
            var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + i) + '" src="images/default-listing.png" data-src="' + imagefortournament + '"></div>');
            aside.append(imgDiv);
            var div3 = $('<div class="card-main card-width"></div>');
            var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
            var tournmanentTitle = tournamentList.content[i].title;
            var p1 = $('<p class="card-heading card-heading-flow">' + tournmanentTitle + '</p>');

            var div5 = $('<div class="blog-post-meta" style="padding-top:6px;"></div>');
            var span1 = $('<span></span>');
            var dispDat = getDisplayableDate(tournamentList.content[i].start_date);
            var dispDat2 = getDisplayableDate(tournamentList.content[i].end_date);
            var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ':' + '&nbsp;' + dispDat + '</i></br>');
            var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ':' + '&nbsp;' + dispDat2 + ' </i></br>');
            if (tournamentList.content[i].number_of_events > 1)
                var ielem3 = $('<i>' + tournamentList.content[i].number_of_events + ' Events</i>');
            else
                var ielem3 = $('<i>' + tournamentList.content[i].number_of_events + ' Event</i>');
            span1.append(ielem1, ielem2, ielem3);
            div5.append(span1);
            div4.append(p1, div5);
            div3.append(div4);
            div2.append(aside, div3);
            div1.append(div2);
            $('.row').append(div1);
        }
        lazyLoadListImages(page_number, tournamentList.content.length);
    }


    /**
     * Go to Golf Tournament Details
     */
    gotoGolfTournamentResultDetails = function (tournament_id) {
        localStorage.setItem("golftournamentid", tournament_id);
        slideview('golf-tournament-result-details.html', 'script', 'left');
    }


    /**
     * Go to Golf Tournament Details
     */
    getGolfTournamentResultDetails = function () {
        var actions = {
            "id": localStorage.getItem("golftournamentid"),
            "type": "1"
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                //localStorage.setItem("tournamentdata", JSON.stringify(data));
                swal.close();
                displayGolfTournamentResultDetails(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/tournamentDetails", actions, successCallback, errorCallback);
    }


    /**
     * Renders the Tournament Details for Results
     */
    displayGolfTournamentResultDetails = function (tournamentDetails) {
        if (enableDebugMode)
            alert("Tournament Result Details : " + JSON.stringify(tournamentDetails));

        //var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(tournamentDetails.content.tournament_title);
        var span1 = $('<span style=" font-size:12px;"></span>');
        var dispDat = getDisplayableDate(tournamentDetails.content.start_date);
        var dispDat2 = getDisplayableDate(tournamentDetails.content.end_date);
        var date = "&nbsp;" + localizeString("Label_StartDate") + "&nbsp;: " + dispDat;
        var ielem2 = $('<i class="fa fa-clock-o"></i>');
        var ielem21 = $('<i class="fa fa-clock-o"></i>');
        var date1 = "&nbsp;" + localizeString("Label_EndDate") + "&nbsp;&nbsp;: " + dispDat2;
        span1.append("&nbsp;&nbsp;", ielem2, date, "<br/>");
        span1.append("&nbsp;&nbsp;", ielem21, date1);
        var p2 = $('<p style="word-wrap:break-word; word-break:break-word;">' + tournamentDetails.content.description + '</p>');
        td1.append(p1, span1, p2);
        //tr1.append(td1);

        $("#buttontr").css('display', 'block');
        //tr1.insertBefore('#buttontr');

        $('#buttontr').append(td1, "<br/>", "<br/>");

        for (var i = 0; i < tournamentDetails.content.event_list.length; i++) {
            option = document.createElement('option');
            option.setAttribute('value', tournamentDetails.content.event_list[i].id);
            option.appendChild(document.createTextNode(tournamentDetails.content.event_list[i].name));
            $("#event-type").append(option);
        }

        $("#event-picker-form").css('display', 'block');
        $("#leaderBoardButton").css('display', 'block');
    }


    /**
     * Fetch the Golf Leader board
     */
    getGolfLeaderBoard = function () {
        var actions = {
            "tournamentId": localStorage.getItem("golftournamentid"),
            "type": "1",
            "eventId": $("#event-type option:selected").val()
        };
        cleanLeaderBoard();

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                displayGolfLeaderBoard(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };

        postMessageToServerWithAuth("/golfTournamentleaderBoard", actions, successCallback, errorCallback);
    }


    /**
     * Generate Leader Board Header Template
     */
    buildLiveBoardTableHeaderTemplate=   function (){
        var template = "";
        template = template + "<div id='leaderboardTable'>";
        template = template +   "<table class='table table-bordered box box-solid' id='main' cellspacing='0'>";
        template = template +       "<tbody id='leaderboardTableBody'>";
        template = template +           "<tr style='color:#fff'>";
        template = template +               "<th width='33%' bgcolor='#3a3a3a'>" + localizeString("GolfCourse_Label_Name") + "</th>";
        template = template +               "<th width='33%' bgcolor='#3a3a3a'>" + localizeString("GolfCourse_Label_Total") + "</th>";
        template = template +               "<th width='33%' bgcolor='#3a3a3a'>" + localizeString("GolfCourse_Label_Rank") + "</th>";
        template = template +           "</tr>";
        template = template +       "</tbody>";
        template = template +   "</table>";
        template = template + "</div>";
        return template;
    }


    /**
     * Generate Leader Board Row Template
     */
    buildLiveBoardRowTemplate= function (data,index) {
        var template = "";
        template = template      + "<tr id='"+ index+"'"+ " style='color:#fff;'>";
        template = template     +     "<td height='40' bgcolor='#b2b2b2'><span>" + data.name + "</span></td>";
        template = template      +      "<td height='40' style='background-color:#b2b2b2'><span>" + data.score + "</span></td>";
        template = template      +      "<td height='40' style='background-color:#b2b2b2'><span>" + data.rank + "</span></td>";
        template = template      + "</tr>";
        return template;
    }


    /**
     * Render tournament Details on UI
     * @tournamentDetails Tournament Details received from the backend
     */
    displayGolfLeaderBoard = function (data) {
        var template= buildLiveBoardTableHeaderTemplate();
        $("#leaderBoardTableData").append(template);

        var index=0;
        for(index=0;index<data.length;index++){
           template = buildLiveBoardRowTemplate(data[index],index);
           $("#leaderboardTableBody").append(template);
        }
    }


    cleanLeaderBoard = function () {
        $("#leaderboardTable").remove();
    }
}());