(function() {
    var enableDebugMode = 0;

    /* Extend this as needed */
    var bookingStatusArr = ["Pending Admin Aproval", "Admin Approved", "Admin Rejected", "User Canceled"];

    /* Extend this as needed */
    var facilityCategoryArr = ["Fitness", "Sports", "Health & Spa", "Recreational", "Room", "Banquet-Conference Room", "Others", "Salon"];

    /* Extend this as needed */
    var bookingTypeArr = ["SD", "SDST", "SDED", "SDSTED", "SDSTEDET"];

    /* Array of facility categories for which booking is allowed */
    var bookingAllowedForCategories = JSON.parse(localStorage.getItem("FacilityBookingEnabledCategories"));

    var page_number = 1;

    "use strict";

    localStorage.setItem("from_page", "facilities");
    localStorage["numofFacilities"] = 0;

    facilityCategoryArr[0] = localizeString("Menu_Button_FitnessFacilities");
    facilityCategoryArr[1] = localizeString("Menu_Button_SportsFacilities");
    facilityCategoryArr[2] = localizeString("HealthnSpa_Title");
    facilityCategoryArr[3] = localizeString("Recreational_Title");
    facilityCategoryArr[4] = localizeString("Menu_Button_Room");
    facilityCategoryArr[5] = localizeString("BanquetsnConference_Title");
    facilityCategoryArr[6] = localizeString("Menu_Button_Others");
    facilityCategoryArr[7] = localizeString("Menu_Button_Salon");

    bookingStatusArr[0] = localizeString("GolfTournaments_Label_RegStatusPending");
    bookingStatusArr[1] = localizeString("GolfTournaments_Label_RegStatusApproved");
    bookingStatusArr[2] = localizeString("GolfTournaments_Label_RegStatusRejected");
    bookingStatusArr[3] = localizeString("GolfTournaments_Label_RegStatusCancelled");

    $("#cancel_booking").html(localizeString("Alert_Title_CancelBooking"));

    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofFacilities")) / 15) == page_number) {
                page_number++;
                getFacilityListing(localStorage["faciCategoryId"], page_number);
            }
        }
    });

    getFacilityListing = function(category_id, page_number) {
        localStorage["faciCategoryId"] = category_id;
        var actions = {
            "category_id": category_id,
            "page_number": page_number
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length <= 0) {
                    $('.row').html('<p style="text-align:center">' + facilityCategoryArr[category_id - 1] + " " + localizeString("Facility_Label_NoData") + '</p>');
                } else {
                    localStorage.setItem('numofFacilities', String(parseInt(localStorage["numofFacilities"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayFacilityListing(data.data);
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/facilities", actions, successCallback, errorCallback);
    }


    displayFacilityListing = function(facilityList) {
        if (enableDebugMode)
            alert("facilityList = " + JSON.stringify(facilityList));
        var isUserLoggedIn = false;
        var facilityDiv = null;
        if (localStorage.getItem("isLoggedIn") != "true") {
            isUserLoggedIn = false;
        } else {
            isUserLoggedIn = true;
            if (localStorage.getItem("memberverified") != 0)
                $("#mybooking").css("display", "inline");
        }

        for (var i = 0; i < facilityList.content.length; i++) {
            facilityDiv = null;
            if ("1" == facilityList.content[i].isPublic) {
                facilityDiv = generateFacilityDiv(i, facilityList.content[i], facilityList.imageBasePath);
            } else // facility is private
            {
                if (isUserLoggedIn) {
                    facilityDiv = generateFacilityDiv(i, facilityList.content[i], facilityList.imageBasePath);
                }
            }
            if (facilityDiv != null)
                $('.row').append(facilityDiv);
        }
        lazyLoadListImages(page_number, facilityList.content.length);
    }

    generateFacilityDiv = function(inx, facility, imageBasePath) {
        var category = facility.categoryId;
        var div1 = $('<div class="col-lg-4 col-sm-12" onclick="goToFacilityDescription(' + category + ', ' + facility.id + ')"></div>');
        var div2 = $('<div class="card-01 col-sm-12"></div>');
        var faciimage;
        if (facility.images != null && ((facility.images.image1 != null && facility.images.image1 != undefined &&
                facility.images.image1 != "") || (facility.images.image2 != "" && facility.images.image2 != null &&
                facility.images.image2 != undefined) || (facility.images.image3 != "" &&
                facility.images.image3 != null && facility.images.image3 != undefined))) {
            if (facility.images.image1 != null && facility.images.image1 != undefined && facility.images.image1 != "") {
                faciimage = imageBasePath + facility.images.image1;
            } else {
                if (facility.images.image2 != null && facility.images.image2 != undefined && facility.images.image2 != "") {
                    faciimage = imageBasePath + facility.images.image2;
                } else {
                    if (facility.images.image3 != null && facility.images.image3 != undefined && facility.images.image3 != "") {
                        faciimage = imageBasePath + facility.images.image3;
                    }
                }
            }
        } else {
            faciimage = "images/default-listing.png";
        }

        var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
        var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + inx + '" src="images/default-listing.png" data-src="' + faciimage + '"></div>');
        aside.append(imgDiv);
        var div3 = $('<div class="card-main card-width"></div>');
        var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
        var p1 = $('<p class="card-heading card-heading-flow">' + facility.name + '</p>');
        var shortDescription = facility.description.replace(/(<([^>]+)>)/ig, "");
        if (shortDescription.length > 30)
            shortDescription = shortDescription.substring(0, 27) + "..."
        var p2 = $('<p>' + shortDescription + '</p>');
        div4.append(p1, p2);
        div3.append(div4);
        div2.append(aside, div3);
        div1.append(div2);
        return div1;
    }


    getFacilityDetails = function() {
        facility_id = localStorage.getItem("facilities_details_id");
        var actions = {
            "facility_id": facility_id,
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayFacilityDetails(data.data.content, data.data.imageBasePath);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/facilities", actions, successCallback, errorCallback);
    }


    /**
     * Displya facility details for the selected facility
     * @facility facility details object
     * @imageBasePath image path for the images
     */
    displayFacilityDetails = function(facility, imageBasePath) {
        if (enableDebugMode)
            alert("Facility = " + JSON.stringify(facility));

        /**
         * Allow booking only in following 2 scenarios:
         * 1. User logged in and is verified.
         * 2. User logged in and menu id available in logged in menu
         */
        if (((localStorage.getItem("isLoggedIn") == "true") &&
                (localStorage.getItem("memberverified") != 0)) ||
            ((localStorage.getItem("isLoggedIn") == "true") &&
                (bookingAllowedForCategories[facility[0].categoryId] == "true"))) // Check for user loggedin
        {
            var bkngType = parseInt(facility[0].bookingType);
            if (1 == bkngType) {
                $("#date-picker-form").css("display", "inline");
            } else if (2 == bkngType) {
                $("#date-picker-form").css("display", "inline");
                populateBookingTime("time-from");
                $("#time-picker-form").css("display", "inline");
            } else if (3 == bkngType) {
                $("#date-picker-form").css("display", "inline");
                $("#date-picker-to").css("display", "inline");
            } else if (4 == bkngType) {
                $("#date-picker-form").css("display", "inline");
                populateBookingTime("time-from");
                $("#time-picker-form").css("display", "inline");
                $("#date-picker-to").css("display", "inline");
            } else if (5 == bkngType) {
                $("#date-picker-form").css("display", "inline");
                populateBookingTime("time-from");
                $("#time-picker-form").css("display", "inline");
                $("#date-picker-to").css("display", "inline");
                populateBookingTime("time-to");
                $("#time-picker-to").css("display", "inline");
            }

            if (0 < bkngType) {
                $("#usrmsg-area-form").css("display", "inline");
                $("#form-req-button").css("display", "inline");
            }

            /* Yesterdays date for the datepicker */
            //var yesterday = new Date(Date.now() - 864e5);
            var day = new Date();
            var myDate = day.getFullYear() + "-" + (1 + day.getMonth()) + "-" + day.getDate();
            if (0 < bkngType) {
                if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                    $("#date-from").flatpickr({
                        disableMobile: false,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            console.log("dateStr = " + dateStr);
                            $("#date-from").val = dateStr;
                            //yesterday = new Date(new Date(dateStr).getTime() - 864e5);
                            yesterday = new Date(dateStr);
                            console.log("updated yesterday = " + yesterday);
                        }
                    });
                } else {
                    $("#date-from").flatpickr({
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            console.log("dateStr = " + dateStr);
                            $("#date-from").val = dateStr;
                            //yesterday = new Date(new Date(dateStr).getTime() - 864e5);
                            yesterday = new Date(dateStr);
                            console.log("updated yesterday = " + yesterday);
                        }
                    });
                }
            }

            if (3 <= bkngType) {
                if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                    $("#date-to").flatpickr({
                        disableMobile: false,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            $("#date-to").val = dateStr;
                        }
                    });
                } else {
                    $("#date-to").flatpickr({
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            $("#date-to").val = dateStr;
                        }
                    });
                }
            }
        }

        var numOfImages = 0;
        if (null != facility[0].images) {
            if (null != facility[0].images.image1 && undefined != facility[0].images.image1 && "" != facility[0].images.image1) {
                $('#topbanner').attr({
                    'src': imageBasePath + facility[0].images.image1
                });
                $("#topbanner").css("display", "block");
                numOfImages++;
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }

            if (null != facility[0].images.image2 && undefined != facility[0].images.image2 && "" != facility[0].images.image2) {
                $('#topbanner2').attr({
                    'src': imageBasePath + facility[0].images.image2
                });
                numOfImages++;
            } else {
                $("#topbanner2").removeClass("mySlides");
            }

            if (null != facility[0].images.image3 && undefined != facility[0].images.image3 && "" != facility[0].images.image3) {
                $('#topbanner3').attr({
                    'src': imageBasePath + facility[0].images.image3
                });
                numOfImages++;
            } else {
                $("#topbanner3").removeClass("mySlides");
            }

            if (numOfImages > 1)
                carousel();
        } else {
            $("#topbanner").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(facility[0].name);
        var p2 = $('<p class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + facility[0].description + '</p>');
        td1.append(p1, p2);
        tr1.append(td1);

        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');

        if (0 < parseInt(facility[0].bookingType)) {
            var tr2 = $('<tr></tr>');
            var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
            tr2.append(td2);
            tr2.insertBefore('#buttontr');

            if (((localStorage.getItem("isLoggedIn") == "true") &&
                    (localStorage.getItem("memberverified") != 0)) ||
                ((localStorage.getItem("isLoggedIn") == "true") &&
                    (bookingAllowedForCategories[facility[0].categoryId] == "true"))) {
                var tr3 = $('<tr></tr>');
                var td3 = $('<td colspan="2" align="center"></td>');
                var p3 = $('<p class="text-bold-01" align="center" style="font-size:20px; margin-bottom:5px;" id="reservtitle"></p>').text("Request Reservation");
                td3.append(p3);
                tr3.append(td3);
                tr3.insertAfter('#buttontr');
            }
        }

        localStorage.setItem("FacilityBookingType", facility[0].bookingType);
        localStorage.setItem("FacilityMasterId", facility[0].master_id);
    }

    stripSecondsField = function(time) {
        timeArray = time.split(':');
        return [timeArray[0], ':', timeArray[1]].join('');
    }


    stripSecondsFieldFromDateTime = function(dateTime) {
        dateTimeArray = dateTime.split(" ");
        timeArray = dateTimeArray[1].split(':');
        formatedTime = [timeArray[0], ':', timeArray[1]].join('');
        return [dateTimeArray[0], ' ', formatedTime].join('');
    }


    /**
     * Populate the bookin-time select box with time
     * in interval of 10 min.
     */
    populateBookingTime = function(id) {
        var d = new Date();
        var interval = 10;
        d.setHours(5);
        d.setMinutes(0);
        for (var i = 0; i < 103; i++) {
            option = document.createElement('option');
            option.setAttribute('value', i + 1);
            option.appendChild(document.createTextNode(("0" + d.getHours()).slice(-2) + ':' + ("0" + d.getMinutes()).slice(-2)));
            $("#" + id).append(option);
            d.setMinutes(d.getMinutes() + interval);
        }
    }


    bookingHistoryDetailLoad = function() {
        var booking_id = localStorage.getItem("facility_booking_id");
        getBookingDetails(booking_id);
    }


    /**
     * Fetch the facilities booking history for a user
     */
    getBookingList = function() {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayBookingHistory(data.data);
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/booking/history", actions, successCallback, errorCallback);
    }


    /**
     * Render Facility booking History list
     * @bookingList list of bookings fetched from server
     */
    displayBookingHistory = function(bookingList) {
        if (enableDebugMode)
            alert("list = " + JSON.stringify(bookingList));
        $('#user-list').html("");
        var slot, createdby, historydata;
        if (bookingList.content.length == 0) {
            var p1 = $('<p style="text-align:center">' + localizeString("MyFacilityBookingDetails_Label_NoData") + '</p>');
            $('#user-list').append(p1);
        } else {
            for (var i = 0; i < bookingList.content.length; i++) {
                bookingStatus = bookingStatusArr[parseInt(bookingList.content[i].status)];
                start_date = getDisplayableDate(bookingList.content[i].start_date);
                facility_name = bookingList.content[i].name;
                booking_id = bookingList.content[i].booking_id;

                var li = $('<li></li>');
                var a = $('<a></a>').attr({
                    'onclick': 'bookingHistoryDetail(' + booking_id + ');',
                    'href': '#',
                    'style': 'float:left; padding:5px 10px; width:100%;'
                });
                var div2 = $('<div></div>').attr({
                    'class': 'five_sixth bold-match-color',
                    'style': 'line-height:40px;'
                }).text(facility_name);
                var div3 = $('<div></div>').attr({
                    'class': 'text',
                    'style': 'line-height:26px;'
                });
                var span1 = $('<span></span>').attr({
                    'style': 'float:left;'
                }).html(localizeString("MyEvent_Label_TicketStatus") + ':' + " <strong>" + bookingStatus + "</strong>");
                var span2 = $('<span></span>').attr({
                    'style': 'float:right;'
                }).html(localizeString("Label_TicketDate") + ':' + start_date);
                div3.append(span1, span2);
                a.append(div2, div3);
                li.append(a);
                $('#user-list').append(li);
            }
        }
    }


    /**
     * Goto facility description page based on category & facility id
     * @category facility category like sports, fitness
     * @i facility id specific to a category.
     */
    goToFacilityDescription = function(category, i) {
        localStorage.setItem("facilities_details_id", i);
        if (category == 7) //Salon
            $('#facility-salon').addClass('active');
        if (category == 6) //Others
            $('#facility-other').addClass('active');
        else if (category == 5) //Banquet
            $('#facility-banquet').addClass('active');
        else if (category == 4) //room
            $('#facility-room').addClass('active');
        else if (category == 3) //recreational
            $('#facility-recreational').addClass('active');
        else if (category == 2) //Health & Spa
            $('#facility-health').addClass('active');
        else if (category == 1) //sports
            $('#facility-sports').addClass('active');
        else if (category == 0) //fitness
            $('#facility-fitness').addClass('active');
        slideview('facility-details.html', 'script', 'left');
    }


    bookingHistoryDetail = function(booking_id) {
        localStorage.setItem("facility_booking_id", booking_id);
        slideview('facilities-booking-details.html', 'script', 'left');
        return false;
    }


    /**
     * Fetch booking details corresponding to the give booking id.
     * @bookin_id  booking id for the selected booking
     */
    getBookingDetails = function(booking_id) {
        var actions = {
            "booking_id": booking_id,
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayExistingBookingDetails(data.data);
            } else {
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/booking/details", actions, successCallback, errorCallback);
    }


    /**
     * Renders existing booking details for the user & allows
     * user to update and/or cancel the booking.
     */
    displayExistingBookingDetails = function(bookingDetail) { //need booking type here
        if (enableDebugMode)
            alert("Booking Details = " + JSON.stringify(bookingDetail));
        $('#msgadminold').focus(function() {
            this.blur();
        });
        $('#msguserold').focus(function() {
            this.blur();
        });

        //localStorage.setItem("facilities_booking_detail", JSON.stringify(bookingDetail));
        var facility_id = bookingDetail.content.facility_id;
        var facility_name = bookingDetail.content.name; // Display this some where
        var isEditable = bookingDetail.content.editable;
        var booking_status = bookingStatusArr[parseInt(bookingDetail.content.status)]; // Display this some where
        var sDate = null;
        var sTime = null;
        var eDate = null;
        var eTime = null;

        if (null != bookingDetail.content.images && undefined != bookingDetail.content.images) {
            if (null != bookingDetail.content.images.image1 && undefined != bookingDetail.content.images.image1 &&
                "" != bookingDetail.content.images.image1) {
                $('#topbanner').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.images.image1
                });
                $("#topbanner").css("display", "block");
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }

            if (null != bookingDetail.content.images.image2 && undefined != bookingDetail.content.images.image2 &&
                "" != bookingDetail.content.images.image2) {
                $('#topbanner2').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.images.image2
                });
            } else {
                $("#topbanner2").removeClass("mySlides");
            }

            if (null != bookingDetail.content.images.image3 && undefined != bookingDetail.content.images.image3 &&
                "" != bookingDetail.content.images.image3) {
                $('#topbanner3').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.images.image3
                });
            } else {
                $("#topbanner3").removeClass("mySlides");
            }

            carousel();
        } else {
            $("#topbanner").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(facility_name);
        var p2 = $('<p> ' + localizeString("Label_BookingStatus") + ':' + ' <strong>' + booking_status + '</strong> </p>');
        td1.append(p1, p2);
        tr1.append(td1);

        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');

        var tr2 = $('<tr></tr>');
        var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
        tr2.append(td2);
        tr2.insertBefore('#buttontr');

        var tr3 = $('<tr></tr>');
        var td3 = $('<td colspan="2" align="center"></td>');
        var p3 = $('<p class="text-bold-01" align="center" style="font-size:20px; margin-bottom:5px;" id="reservtitle">' + localizeString("FacilityBooking_Label_Upadte") + '</p>');
        td3.append(p3);
        tr3.append(td3);
        tr3.insertAfter('#buttontr');

        switch (parseInt(bookingDetail.content.booking_type)) {
            case 5:
                populateBookingTime("time-to");
                $("#time-picker-to").css("display", "inline");
                if (bookingDetail.content.end_time != null) {
                    var etime = bookingDetail.content.end_time;
                    var timerecv = stripSecondsField(etime);
                    eTime = bookingDetail.content.end_time;
                    $("select#time-to option")
                        .each(function() {
                            this.selected = (this.text == timerecv); //"selected"
                        });
                }
            case 4:
                populateBookingTime("time-from");
                $("#time-picker-form").css("display", "inline");
                if (bookingDetail.content.start_time != null) {
                    var sttime = bookingDetail.content.start_time;
                    var timerecv = stripSecondsField(sttime);
                    sTime = bookingDetail.content.start_time;
                    $("select#time-from option")
                        .each(function() {
                            this.selected = (this.text == timerecv); //"selected"
                        });
                }
            case 3:
                $("#date-picker-form").css("display", "inline");
                if (bookingDetail.content.start_date != null) {
                    $("#date-from").val(bookingDetail.content.start_date);
                    sDate = bookingDetail.content.start_date;
                }

                $("#date-picker-to").css("display", "inline");
                if (bookingDetail.content.end_date != null) {
                    $("#date-to").val(bookingDetail.content.end_date);
                    eDate = bookingDetail.content.end_date;
                }
                break;
            case 2:
                populateBookingTime("time-from");
                $("#time-picker-form").css("display", "inline");
                if (bookingDetail.content.start_time != null) {
                    var sttime = bookingDetail.content.start_time;
                    var timerecv = stripSecondsField(sttime);
                    sTime = bookingDetail.content.start_time;
                    $("select#time-from option")
                        .each(function() {
                            this.selected = (this.text == timerecv); //"selected"
                        });
                }
            case 1:
                $("#date-picker-form").css("display", "inline");
                if (bookingDetail.content.start_date != null) {
                    sDate = bookingDetail.content.start_date;
                    $("#date-from").val(bookingDetail.content.start_date);
                }
                break;
        }

        /* Yesterdays date for the datepicker */
        //var yesterday = new Date(Date.now() - 864e5);
        var day = new Date();
        var myDate = day.getFullYear() + "-" + (1 + day.getMonth()) + "-" + day.getDate();
        if ((0 != isEditable) && (parseInt(bookingDetail.content.status) < 2)) {
            if (0 < parseInt(bookingDetail.content.booking_type)) {
                if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                    $("#date-from").flatpickr({
                        disableMobile: false,
                        defaultDate: bookingDetail.content.start_date,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            $("#date-from").val = dateStr;
                        }
                    });
                } else {
                    $("#date-from").flatpickr({
                        defaultDate: bookingDetail.content.start_date,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            $("#date-from").val = dateStr;
                        }
                    });
                }
            }

            if (3 <= parseInt(bookingDetail.content.booking_type)) {
                if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
                    $("#date-to").flatpickr({
                        disableMobile: false,
                        defaultDate: bookingDetail.content.end_date,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            console.log("dateStr = " + dateStr);
                            $("#date-to").val = dateStr;
                        }
                    });
                } else {
                    $("#date-to").flatpickr({
                        defaultDate: bookingDetail.content.end_date,
                        minDate: myDate,
                        shorthandCurrentMonth: true,
                        onChange: function(selectedDates, dateStr, instance) {
                            //...
                            console.log("dateStr = " + dateStr);
                            $("#date-to").val = dateStr;
                        }
                    });
                }
            }
        }

        $("#usrmsg-area-form").css("display", "block");
        $("#usrmsg-area-to").css("display", "block");

        if ((0 == isEditable) || (parseInt(bookingDetail.content.status) >= 2)) {
            $("#date-from").attr('disabled', 'disabled'); // Disable selecting new start date
            $("#time-from").attr('disabled', 'disabled'); // Disable selecting new start time
            $("#date-to").attr('disabled', 'disabled'); // Disable selecting new end date
            $("#time-to").attr('disabled', 'disabled'); // Disable selecting new end time
            $("#msgarea").prop('disabled', true); // Disable adding new message
        } else {
            if (parseInt(bookingDetail.content.status) < 2) { // No cancelation for Admin Rejected and user cancled bookings
                $("#MyFacility_Button_Update").css("display", "inline");
                $("#cancel_booking").css("display", "inline");
            }
        }

        if (parseInt(bookingDetail.content.status) == 1) {
            facility_name = facility_name.replace(/'/g, "\\'").replace(/"/g, "&quot;");
            var location = "";
            var notes = "";
            var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + facility_name + '\', \'' + location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
            reminder.insertAfter("#buttontr");
        }

        var comments = bookingDetail.content.user_comment.replace(/<br\s*\/?>/mg, "\n");
        comments = comments.replace(/<\/br\s*?>/mg, "");
        $("#msguserold").append(comments);
    }


    validateStartDate = function(indate, outdate) {
        return dates.compare(indate, outdate);
    }


    validateEndDate = function(indate, outdate) {
        var result = 1;
        if ($("#date-picker-to").css('display') != 'none') {
            result = dates.compare(indate, outdate);
        }
        return result;
    }


    validateStartTime = function(intime, outtime) {
        var result = true;
        if ($("#time-picker-from").css('display') != 'none') {
            result = compareTime(intime, outtime);
        }
        return result;
    }


    validateEndTime = function(intime, outtime) {
        var result = true;
        if ($("#time-picker-to").css('display') != 'none') {
            result = compareTime(intime, outtime);
        }
        return result;
    }


    /**
     * Cancel an existing user booking.
     */
    cancelBooking = function() {
        swal({
                title: localizeString("Alert_Title_CancelBooking"),
                text: localizeString("Alert_Text_CancelBooking"),
                type: "warning",
                showCancelButton: true,
                confirmButtonText: localizeString("Alert_Button_Proceed"),
                cancelButtonText: localizeString("Alert_Button_NotNow"),
                closeOnConfirm: false,
                showLoaderOnConfirm: true,
                closeOnCancel: true
            },
            function() {
                var booking_id = localStorage.getItem("facility_booking_id");
                var actions = {
                    "booking_id": booking_id,
                };

                window.plugins.spinnerDialog.show(localizeString("SpinDialog_SendRequest"), localizeString("SpinDialog_Wait"), true);

                successCallback = function(data) {
                    window.plugins.spinnerDialog.hide();
                    if (data.status == "success") {
                        swal({
                                title: localizeString("Alert_Title_Done"),
                                text: data.message,
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: false,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                slideview('facilities-booking-list.html', 'script', 'left');
                            });
                    } else {
                        var thisone = this;
                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: data.message,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    }
                    window.plugins.spinnerDialog.hide();
                };
                errorCallback = function(data) {

                };
                postMessageToServerWithAuth("/booking/cancel", actions, successCallback, errorCallback);
            });
    }


    /**
     * Update an existing booking.
     * Reads the updated booking inputs, validates them
     * & plase the request with admin for approval.
     */
    updateBooking = function() {
        var retVal = 1;
        $("#dferror").css("display", "none");
        $("#dferror").html('<i class="form-help-icon icon">error</i>');
        $("#msgerror").css("display", "none");
        $("#msgerror").html('<i class="form-help-icon icon">error</i>');
        $("#dterror").css("display", "none");
        $("#dterror").html('<i class="form-help-icon icon">error</i>');
        $("#tferror").css("display", "none");
        $("#tferror").html('<i class="form-help-icon icon">error</i>');
        $("#tterror").css("display", "none");
        $("#tterror").html('<i class="form-help-icon icon">error</i>');

        if ($("#date-picker-form").css('display') != 'none' && $("#date-picker-form").css('display') != undefined) {
            if ($("#date-from").val().trim() == "") {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_StartDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#date-picker-to").css('display') != 'none' && $("#date-picker-to").css('display') != undefined) {
            if ($("#date-to").val().trim() == "") {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_EndDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($('#time-picker-to').css('display') != 'none' && $("#time-picker-to").css('display') != undefined) {
            if ($("#time-to option:selected").text().trim() == "") {
                $("#tterror").css("display", "inline");
                $("#tterror").html(localizeString("ValidationError_ValidEndTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#time-picker-form").css('display') != 'none' && $("#time-picker-form").css('display') != undefined) {
            if ($("#time-from option:selected").text().trim() == "") {
                $("#tferror").css("display", "inline");
                $("#tferror").html(localizeString("ValidationError_ValidStartTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if (0 == retVal)
            return false;

        var request_start_date = $("#date-from").val();
        var request_end_date = $("#date-to").val();
        var request_start_time = $("#time-from option:selected").text();
        var request_end_time = $("#time-to option:selected").text();
        var today = new Date();

        resultStartDate = validateStartDate(request_start_date, getFormatedDate(today));
        resultEndDate = validateEndDate(request_end_date, request_start_date);

        var facilityBookingType = localStorage.getItem("FacilityBookingType");

        if ((0 <= resultStartDate) && (0 <= resultEndDate)) { // Valid date
            if (0 == resultStartDate) { // start dates are same so compare time
                var startTimeResult = validateStartTime(getFormatedTime(today), request_start_time);
                if (!startTimeResult) { // inValid time
                    $("#tferror").css("display", "inline");
                    $("#tferror").html(localizeString("ValidationError_StartTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            }
            if (0 == resultEndDate) { // end dates are same so compare time
                var endTimeResult = validateEndTime(request_end_time, getFormatedTime(today));
                if (!endTimeResult) { // inValid time
                    $("#tterror").css("display", "inline");
                    $("#tterror").html(localizeString("ValidationError_EndTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            }
            if ($("#msgarea").val().trim() == "") {
                $("#msgerror").css("display", "inline");
                $("#msgerror").html(localizeString("ValidationError_Message") + '<i class="form-help-icon icon">error</i>');
                $("#msgarea").focus();
                return false;
            }
            var booking_id = localStorage.getItem("facility_booking_id");
            var actions = {
                "booking_id": booking_id,
                "start_date": request_start_date,
                "start_time": request_start_time,
                "end_date": request_end_date,
                "end_time": request_end_time,
                "user_comment": $("#msgarea").val()
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_SendRequest"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                if (data.status == "success") {
                    $("#msgarea").val("");
                    swal({
                            title: localizeString("Alert_Title_Done"),
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            slideview('facilities-booking-list.html', 'script', 'left');
                        });
                } else {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
                window.plugins.spinnerDialog.hide();
            };
            errorCallback = function(data) {

            };
            postMessageToServerWithAuth("/booking/update", actions, successCallback, errorCallback);
        } else {
            if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
            }
            if (0 > resultEndDate) {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_ValidEndDate") + '<i class="form-help-icon icon">error</i>');
            }
            return false;
        }
    }


    /**
     * Request reservation for the facility.
     * Validates all the required parameters based on booking type
     * & then places the reservation request.
     */
    requestReservationNow = function() {
        var retVal = 1;
        $("#dferror").css("display", "none");
        $("#dferror").html('<i class="form-help-icon icon">error</i>');
        $("#msgerror").css("display", "none");
        $("#msgerror").html('<i class="form-help-icon icon">error</i>');
        $("#dterror").css("display", "none");
        $("#dterror").html('<i class="form-help-icon icon">error</i>');
        $("#tferror").css("display", "none");
        $("#tferror").html('<i class="form-help-icon icon">error</i>');
        $("#tterror").css("display", "none");
        $("#tterror").html('<i class="form-help-icon icon">error</i>');

        if ($("#date-picker-form").css('display') != 'none' && $("#date-picker-form").css('display') != undefined) {
            if ($("#date-from").val().trim() == "") {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_StartDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#date-picker-to").css('display') != 'none' && $("#date-picker-to").css('display') != undefined) {
            if ($("#date-to").val().trim() == "") {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_EndDate") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($('#time-picker-to').css('display') != 'none' && $("#time-picker-to").css('display') != undefined) {
            if ($("#time-to option:selected").text().trim() == "") {
                $("#tterror").css("display", "inline");
                $("#tterror").html(localizeString("ValidationError_ValidEndTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if ($("#time-picker-form").css('display') != 'none' && $("#time-picker-form").css('display') != undefined) {
            if ($("#time-from option:selected").text().trim() == "") {
                $("#tferror").css("display", "inline");
                $("#tferror").html(localizeString("ValidationError_ValidStartTime") + '<i class="form-help-icon icon">error</i>');
                retVal = 0;
            }
        }

        if (0 == retVal)
            return false;

        var request_start_date = $("#date-from").val();
        var request_end_date = $("#date-to").val();
        var request_start_time = $("#time-from option:selected").text();
        var request_end_time = $("#time-to option:selected").text();
        var today = new Date();
        resultStartDate = validateStartDate(request_start_date, getFormatedDate(today));
        resultEndDate = validateEndDate(request_end_date, request_start_date);

        var facilityBookingType = localStorage.getItem("FacilityBookingType");

        if (1 == parseInt(facilityBookingType)) {
            if (0 == resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_StartdateToday") + '<i class="form-help-icon icon">error</i>');
                return false;
            } else if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
        } else if (2 == parseInt(facilityBookingType)) {
            if (0 == resultStartDate) { // start dates are same so compare time
                var startTimeResult = validateStartTime(getFormatedTime(today), request_start_time);
                if (!startTimeResult) { // inValid time
                    $("#tferror").css("display", "inline");
                    $("#tferror").html(localizeString("ValidationError_StartTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            } else if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
        } else if (3 == parseInt(facilityBookingType)) {
            if (0 == resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_StartdateToday") + '<i class="form-help-icon icon">error</i>');
                return false;
            } else if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }

            if (0 > resultEndDate) {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_EndDateLessThanStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
            /*else if (0 > resultEndDate) {
                           $("#dterror").css("display", "inline");
                           $("#dterror").html(localizeString("ValidationError_ValidEndDate") + '<i class="form-help-icon icon">error</i>');
                           return false;
                       }*/
        } else if (4 == parseInt(facilityBookingType)) {
            if (0 == resultStartDate) {
                var startTimeResult = validateStartTime(getFormatedTime(today), request_start_time);
                if (!startTimeResult) { // inValid time
                    $("#tferror").css("display", "inline");
                    $("#tferror").html(localizeString("ValidationError_StartTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            } else if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }

            if (0 > resultEndDate) {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_EndDateLessThanStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
            /*else if (0 > resultEndDate) {
                           $("#dterror").css("display", "inline");
                           $("#dterror").html(localizeString("ValidationError_ValidEndDate") + '<i class="form-help-icon icon">error</i>');
                           return false;
                       }*/
        } else if (5 == parseInt(facilityBookingType)) {
            if (0 == resultStartDate) {
                var startTimeResult = validateStartTime(getFormatedTime(today), request_start_time);
                if (!startTimeResult) { // inValid time
                    $("#tferror").css("display", "inline");
                    $("#tferror").html(localizeString("ValidationError_StartTimeLess") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            } else if (0 > resultStartDate) {
                $("#dferror").css("display", "inline");
                $("#dferror").html(localizeString("ValidationError_ValidStartDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }

            if (0 == resultEndDate) {
                var endTimeResult = validateEndTime(request_start_time, request_end_time);
                if (!endTimeResult) { // inValid time
                    $("#tterror").css("display", "inline");
                    $("#tterror").html(localizeString("ValidationError_EndTimeLessThanStartTime") + '<i class="form-help-icon icon">error</i>');
                    return false;
                }
            } else if (0 > resultEndDate) {
                $("#dterror").css("display", "inline");
                $("#dterror").html(localizeString("ValidationError_ValidEndDate") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
        }

        if ($("#msgarea").val().trim() == "") {
            $("#msgerror").css("display", "inline");
            $("#msgerror").html(localizeString("ValidationError_Message") + '<i class="form-help-icon icon">error</i>');
            $("#msgarea").focus();
            return false;
        }

        var facility_id = localStorage.getItem("facilities_details_id");
        var master_id = localStorage.getItem("FacilityMasterId");
        var actions = {
            "facility_id": facility_id,
            "master_id": master_id,
            "start_date": request_start_date,
            "start_time": request_start_time,
            "end_date": request_end_date,
            "end_time": request_end_time,
            "user_comment": $("#msgarea").val()
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_SendRequest"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                $("#date-from").val("");
                $("#time-from").val("");
                $("#msgarea").val("");
                $("#date-to").val("");
                $("#time-to").val("");
                $("#msgarea").val("");
                swal({
                        title: localizeString("Alert_Title_Done"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: false,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        slideview('facilities-booking-list.html', 'script', 'left');
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function(data) {

        };
        postMessageToServerWithAuth("/booking", actions, successCallback, errorCallback);
    }
}());