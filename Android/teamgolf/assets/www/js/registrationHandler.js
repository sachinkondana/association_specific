(function() {
    var enableDebugMode = 0;
    var tod = new Date();
    var date13bef = tod.setFullYear(tod.getFullYear() - 14);
    "use strict";
    $("#showHide").click(function() {
        if ($("#password").attr("type") == "password") {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
    });
    $("#showHide2").click(function() {
        if ($("#confirmpassword").attr("type") == "password") {
            $("#confirmpassword").attr("type", "text");
        } else {
            $("#confirmpassword").attr("type", "password");
        }
    });


    $('input').each(function(i) {
        $(this).keyup(function(e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                if ($(this).attr("id") == 'confirmpassword') {
                    $("#register").click();
                } else {
                    $('input').eq(i + 1).focus();
                }
            }
        });
    });

    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        $("#dob").flatpickr({
            disableMobile: false,
            maxDate: date13bef,
            shorthandCurrentMonth: true,
            onChange: function(selectedDates, dateStr, instance) {
                //...
                $("#dob").val = dateStr;
            }
        });
    } else {
        $("#dob").flatpickr({
            maxDate: date13bef,
            shorthandCurrentMonth: true,
            onChange: function(selectedDates, dateStr, instance) {
                //...
                $("#dob").val = dateStr;
            }
        });
    }

    $('input').unbind('focusout');
    $(document).on('focusout', 'input', function() {
        setTimeout(function() {
            window.scrollTo(document.body.scrollLeft, document.body.scrollTop);
        }, 500);
    });


    if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        $("input").on('focus', function(e) {
            $('#head').addClass('fixfixed');
            $('#copyright_div').css("display", "none");
        });

        $("input").on('blur', function(e) {
            $('#head').removeClass('fixfixed');
            $('#copyright_div').css("display", "block");
        });

        $("textarea").on('focus', function(e) {
            $('#head').addClass('fixfixed');
            $('#copyright_div').css("display", "none");
        });

        $("textarea").on('blur', function(e) {
            $('#head').removeClass('fixfixed');
            $('#copyright_div').css("display", "block");
        });
    }


    //suggest display name
    $("#fname").blur(function() {
        var fname = $("#fname").val().trim();
        var actions = {
            "avatar": fname
        };
        if (fname.length > 0) {
            $("#fnameerror").css("display", "none");
            $("#fnameerror").html('<i class="form-help-icon icon">error</i>');

            successCallback = function(data) {
                if ($("#avatar").val() == "") {
                    if (data.status == "success") {
                        $("#dp").addClass("control-highlight");
                        $("#avatar").val(data.suggestions[0]);
                    } else {
                        var thisone = this;
                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: data.message,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    }
                }
            };

            errorCallback = function(data) {
                if (jqXHR.status === 0) {
                    $("#avatarerror").html(localizeString("ValidationError_NoConnectivity") + '<i class="form-help-icon icon">error</i>');
                } else {
                    $("#avatarerror").html(localizeString("ValidationError_SomethingWrong") + '<i class="form-help-icon icon">error</i>');
                }
            };

            postRegistrationMessageToServerWithAuth("/user/avatar/suggest", actions, successCallback, errorCallback);
        } else {
            $("#fnameerror").css("display", "inline");
            $("#fnameerror").html(localizeString("ValidationError_FirstnameBlank") + '<i class="form-help-icon icon">error</i>');
            $("#fname").val('');
        }
    });


    //last name
    /*$("#lname").blur(function () {
        if ($("#lname").val().trim().length > 0) {
            $("#lnameerror").css("display", "none");
            $("#lnameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#lnameerror").css("display", "inline");
            $("#lnameerror").html('Last Name cannot be left blank.<i class="form-help-icon icon">error</i>');
            $("#lname").val('');

        }
    });*/


    //check membershipno
    $("#membershipno").blur(function() {
        if ($("#membershipno").val().trim().length > 0) {
            $("#membershipnoerror").css("display", "none");
            $("#membershipnoerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#membershipnoerror").css("display", "inline");
            $("#membershipnoerror").html(localizeString("ValidationError_MembershipBlank") + '<i class="form-help-icon icon">error</i>');
            $("#membershipno").val('');
        }
    });


    //check membership type
    /*$("#membershiptype").blur(function () {
        if ($("#membershiptype").val().trim().length > 0) {
            $("#membershiptypeerror").css("display", "none");
            $("#membershiptypeerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#membershiptypeerror").css("display", "inline");
            $("#membershiptypeerror").html('Membership Type cannot be left blank.<i class="form-help-icon icon">error</i>');
            $("#membershiptype").val('');
        }
    });*/


    //check phone
    /*$("#phone").blur(function () {
        if ($("#phone").val().trim().length > 0) {
            $("#phoneerror").css("display","none");
            $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            if (phoneValidation(phone) == true) {
                $("#phoneerror").css("display","none");
                $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#phoneerror").css("display","inline");
                $("#phoneerror").html(localizeString("ValidationError_InvalidPhone")+'<i class="form-help-icon icon">error</i>');
                $("#phone").focus();
                return false;
            }
        }
    }*/

    //check email
    $("#email").blur(function() {
        if ($("#email").val().trim().length > 0) {
            $("#emailerror").css("display", "none");
            $("#emailerror").html('<i class="form-help-icon icon">error</i>');
            if (emailValidation($("#email").val().trim()) == false) {
                $("#emailerror").css("display", "inline");
                $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            }
        } else {
            $("#emailerror").css("display", "inline");
            $("#emailerror").html(localizeString("ValidationError_EmailBlank") + '<i class="form-help-icon icon">error</i>');
            $("#email").val('');
            $("#password").val('');
            $("#confirmpassword").val('');
        }
    });


    // Check DOB
    $("#dob").change(function() {
        if ($("#dob").val().trim().length > 4) {
            var sel_date_1 = $("#dob").val().split("-");
            if (getAge(sel_date_1) >= 13) {
                $("#dobsuccess").css("display", "none");
                $("#doberror").css("display", "none");
                $("#doberror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#dob").val("");
                $("#doberror").css("display", "block");
                $("#doberror").html(localizeString("ValidationError_Age") + '<i class="form-help-icon icon">error</i>');
                $("#dob").focus();
            }
        } else {
            $("#dobsuccess").css("display", "none");
            $("#doberror").css("display", "none");
            $("#doberror").html('<i class="form-help-icon icon">error</i>');
        }
    });


    //check display name empty
    $("#avatar").blur(function() {
        if ($("#avatar").val().trim().length > 4) {
            $("#avatarerror").css("display", "none");
            $("#avatarerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#avatarerror").css("display", "inline");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort") + '<i class="form-help-icon icon">error</i>');
            $("#avatar").val('');
        }
    });


    //check display name availablity
    $("#avatar").on('keyup', function() {
        var desiredavatar = $("#avatar").val().trim();
        if (desiredavatar.length > 4) {
            var actions = {
                "avatar": desiredavatar
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                $("#avatarsuccess").css("display", "none");
                $("#avatarerror").css("display", "none");
                if (data.status == "success") {
                    $("#avatarsuccess").css("display", "inline");
                    $("#avatarsuccess").html(localizeString("ValidationError_DisplayName") + '<i class="form-help-icon icon">check</i>');
                } else {
                    $("#avatarerror").css("display", "inline");
                    $("#avatarerror").html(localizeString("ValidationError_NoDisplayName") + '<i class="form-help-icon icon">error</i>');
                }
            };
            errorCallback = function(data) {
                $("#avatarsuccess").css("display", "none");
                if (jqXHR.status === 0) {
                    $("#avatarerror").html(localizeString("ValidationError_NoConnectivity2") + '<i class="form-help-icon icon">error</i>');
                } else {
                    $("#avatarerror").html(localizeString("ValidationError_SomethingWrong") + '<i class="form-help-icon icon">error</i>');
                }
                $("#avatarerror").css("display", "none");
            };
            postRegistrationMessageToServerWithAuth("/user/avatar/check", actions, successCallback, errorCallback);
        } else {
            $("#avatarsuccess").css("display", "none");
            $("#avatarerror").css("display", "inline");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort") + '<i class="form-help-icon icon">error</i>');
            $("#password").val('');
            $("#confirmpassword").val('');
        }
    });


    //password type
    $("#password").on('keyup', function() {

        $("#password").val($("#password").val().trim());
        var passwordstr = $("#password").val();
        $("#passworderror").css("display", "none");
        $("#passworderror").html('<i class="form-help-icon icon">error</i>');
        if (passwordstr.length < 6) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordSixChar") + '<i class="form-help-icon icon">error</i>');
        }
        re = /[0-9]/;
        if (!re.test(passwordstr)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordNumber") + '<i class="form-help-icon icon">error</i>');
        }
        re = /[a-z]/;
        if (!re.test(passwordstr)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordLowercase") + '<i class="form-help-icon icon">error</i>');
        }
        re = /[A-Z]/;
        if (!re.test(passwordstr)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordUppercase") + '<i class="form-help-icon icon">error</i>');
        }
    });


    //check display name empty
    $("#password").blur(function() {
        if ($("#password").val().length > 0) {} else {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });


    $("#confirmpassword").blur(function() {
        if ($("#confirmpassword").val().length > 0) {
            $("#confirmpassworderror").css("display", "none");
            $("#confirmpassworderror").html('<i class="form-help-icon icon">error</i>');
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpasswordsuccess").html('<i class="form-help-icon icon">check</i>');
            if ($("#confirmpassword").val() == $("#password").val()) {
                $("#confirmpasswordsuccess").css("display", "inline");
                $("#confirmpasswordsuccess").html(localizeString("ValidationError_PasswordMatch") + '<i class="form-help-icon icon">check</i>');
            } else {
                $("#confirmpassworderror").css("display", "inline");
                $("#confirmpassworderror").html(localizeString("ValidationError_PasswordNoMatch") + '<i class="form-help-icon icon">error</i>');
            }
        } else {
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpasswordsuccess").html('<i class="form-help-icon icon">check</i>');
            $("#confirmpassworderror").css("display", "inline");
            $("#confirmpassworderror").html(localizeString("ValidationError_PasswordBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });


    $("#confirmpassword").on('keyup', function() {
        $("#confirmpassword").val($("#confirmpassword").val().trim());
        if ($("#confirmpassword").val().length > 0) {
            $("#confirmpassworderror").css("display", "none");
            $("#confirmpassworderror").html('<i class="form-help-icon icon">error</i>');
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpasswordsuccess").html('<i class="form-help-icon icon">check</i>');

            if ($("#confirmpassword").val() == $("#password").val()) {
                $("#confirmpasswordsuccess").css("display", "inline");
                $("#confirmpasswordsuccess").html(localizeString("ValidationError_PasswordMatch") + '<i class="form-help-icon icon">error</i>');
            } else {
                $("#confirmpassworderror").css("display", "inline");
                $("#confirmpassworderror").html(localizeString("ValidationError_PasswordNoMatch") + '<i class="form-help-icon icon">error</i>');
            }
        } else {
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpasswordsuccess").html('<i class="form-help-icon icon">check</i>');
            $("#confirmpassworderror").css("display", "inline");
            $("#confirmpassworderror").html(localizeString("ValidationError_PasswordBlank") + '<i class="form-help-icon icon">error</i>');
        }
    });


    registerUserNow = function() {
        var fname = $("#fname").val();
        var lname = $("#lname").val();
        var membershipno = $("#membershipno").val();
        var email = $("#email").val();
        var phone = $("#phone").val();
        var gender = $("#gender").val();
        var dob = $("#dob").val();
        var avatar = $("#avatar").val();
        var passwordnew = $("#password").val();
        var confirmpassword = $("#confirmpassword").val();
        var isprimary = 0;
        if (document.getElementById('isprimary').checked)
            isprimary = document.getElementById('isprimary').value;
        else if (document.getElementById('issecondary').checked)
            isprimary = document.getElementById('issecondary').value;

        $("#avatarsuccess").css("display", "none");
        $("#confirmpasswordsuccess").css("display", "none");

        if (fname.length > 0) {
            $("#fnameerror").css("display", "none");
            $("#fnameerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#fnameerror").css("display", "inline");
            $("#fnameerror").html(localizeString("ValidationError_FirstnameBlank") + '<i class="form-help-icon icon">error</i>');
            $("#fname").focus();
            return false;
        }

        if (membershipno.trim().length > 0) {
            $("#membershipnoerror").css("display", "none");
            $("#membershipnoerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#membershipnoerror").css("display", "inline");
            $("#membershipnoerror").html(localizeString("ValidationError_MembershipBlank") + '<i class="form-help-icon icon">error</i>');
            $("#membershipno").focus();
            return false;
        }

        if (emailValidation(email) == true) {
            $("#emailerror").css("display", "none");
            $("#emailerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#emailerror").css("display", "inline");
            $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            $("#email").focus();
            return false;
        }

        /*if (phone.trim().length > 0) {
            $("#phoneerror").css("display","none");
            $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            if (phoneValidation(phone) == true) {
                $("#phoneerror").css("display","none");
                $("#phoneerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#phoneerror").css("display","inline");
                $("#phoneerror").html(localizeString("ValidationError_InvalidPhone")+'<i class="form-help-icon icon">error</i>');
                $("#phone").focus();
                return false;
            }
        }*/

        if (dob.length > 0) {
            if (dob.length > 4) {
                $("#dobsuccess").css("display", "none");
                $("#doberror").css("display", "none");
                $("#doberror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#doberror").css("display", "block");
                $("#doberror").html(localizeString("ValidationError_DateOfBirth") + '<i class="form-help-icon icon">error</i>');
                $("#dob").focus();
                return false;
            }
        }

        if (avatar.trim().length > 4) {
            $("#avatarsuccess").css("display", "none");
            $("#avatarerror").css("display", "none");
            $("#avatarerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#avatarerror").css("display", "inline");
            $("#avatarerror").html(localizeString("ValidationError_DisplayNameShort") + '<i class="form-help-icon icon">error</i>');
            $("#avatar").focus();
            $("#password").val("");
            $("#confirmpassword").val("");
            return false;
        }

        $("#passworderror").css("display", "none");
        $("#passworderror").html('<i class="form-help-icon icon">error</i>');

        if (passwordnew.length < 6) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordNumber") + '<i class="form-help-icon icon">error</i>');
            $("#password").focus();
            return false;
        }
        re = /[0-9]/;
        if (!re.test(passwordnew)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordNumber") + '<i class="form-help-icon icon">error</i>');
            $("#password").focus();
            return false;
        }
        re = /[a-z]/;
        if (!re.test(passwordnew)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordLowercase") + '<i class="form-help-icon icon">error</i>');
            $("#password").focus();
            return false;
        }
        re = /[A-Z]/;
        if (!re.test(passwordnew)) {
            $("#passworderror").css("display", "inline");
            $("#passworderror").html(localizeString("ValidationError_PasswordUppercase") + '<i class="form-help-icon icon">error</i>');
            $("#password").focus();
            return false;
        }

        if (confirmpassword.length > 0) {
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpassworderror").css("display", "none");
            $("#confirmpassworderror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#confirmpassworderror").css("display", "inline");
            $("#confirmpassworderror").html(localizeString("ValidationError_PasswordBlank") + '<i class="form-help-icon icon">error</i>');
            $("#confirmpassword").focus();
            return false;
        }

        if (confirmpassword == passwordnew) {
            $("#confirmpasswordsuccess").css("display", "none");
            $("#confirmpassworderror").css("display", "none");
            $("#confirmpassworderror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#confirmpassworderror").css("display", "inline");
            $("#confirmpassworderror").html(localizeString("ValidationError_PasswordNoMatch") + '<i class="form-help-icon icon">error</i>');
            $("#confirmpassword").focus();
            return false;
        }

        if ($('input[name="isprimary"]:checked').size() > 0) {
            $("#radioerror").css("display", "none");
            $("#radiosuccess").css("display", "none");
            $("#radioerror").html('<i class="form-help-icon icon">error</i>');
        } else {
            $("#radioerror").css("display", "inline");
            $("#radioerror").html(localizeString("ValidationError_MembershipOption") + '<i class="form-help-icon icon">error</i>');
            $("#radioerror").focus();
            return false;
        }

        if ($('#older').is(':checked')) {} else {
            swal({
                    title: localizeString("Alert_Title_ActionRequired"),
                    text: localizeString("Alert_Text_RegisterError13"),
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function() {
                    return;
                });
            return false;
        }

        if ($('#terms').is(':checked')) {} else {
            swal({
                    title: localizeString("Alert_Title_ActionRequired"),
                    text: localizeString("Alert_Text_RegisterErrorTerm"),
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function() {
                    return;
                });
            return false;
        }

        localStorage.setItem("RegistrationEmail", email);

        var actions = {
            "firstname": fname,
            "lastname": lname,
            "membershipNumber": membershipno,
            "email": email,
            "dob": dob,
            "gender": gender,
            "isPrimary": isprimary,
            "avatar": avatar,
            "phone": phone,
            "password": passwordnew,
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Register"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                $("#fname").val("");
                $("#lname").val("");
                $("#membershipno").val("");
                $("#email").val("");
                $("#phone").val("");
                $("#avatar").val("");
                $("#password").val("");
                $("#confirmpassword").val("");
                $("#gender").val("");
                $("#dob").val("");
                document.getElementById('isprimary').checked = false;
                document.getElementById('issecondary').checked = false;

                $("#container").html('<br/><br/><div class="card card-green-bg"><div class="card-main"><div class="card-inner"><p class="card-heading">Thank you, <b>' + fname.toUpperCase() + ' ' + lname.toUpperCase() + '</b> !</p><p> We have sent an email to your email address with steps to activate your account.</p><p> <a href="index.html" class="btn-lng"  onClick="slideview(this,\'link\',\'right\');return false;">Login</a></br><div style="text-align:center; width:95px; border-bottom:1px solid white;" onclick="resendEmail();">Resend Email</div></p></div></div></div>');
            } else {
                var thisone = this;
                if (data.error_id == 2) {
                    $("#fname").val("");
                    $("#lname").val("");
                    $("#membershipno").val("");
                    $("#email").val("");
                    $("#phone").val("");
                    $("#avatar").val("");
                    $("#password").val("");
                    $("#confirmpassword").val("");
                    $("#gender").val("");
                    $("#dob").val("");
                    document.getElementById('isprimary').checked = false;
                    document.getElementById('issecondary').checked = false;
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: true,
                            closeOnConfirm: true,
                            confirmButtonText: "Resend Email",
                            cancelButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            resendEmail();
                            return;
                        });
                } else {
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
            }
        };
        errorCallback = function(data) {

        };
        postRegistrationMessageToServerWithAuth("/user/add", actions, successCallback, errorCallback);
    }


    resendEmail = function() {
        var actions = {
            "email": localStorage.getItem("RegistrationEmail")
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                        title: localizeString("Alert_Title_Sucess"),
                        text: data.message,
                        type: "success",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        };
        errorCallback = function(data) {

        };
        postRegistrationMessageToServerWithAuth("/resendRegisteredEmail", actions, successCallback, errorCallback);
    }


    /**
     * Communicates with server with Basic authentication.
     *
     * @uri: Rest API path
     * @postParam: Additional Input Params
     * @successCallback: Callback to be invoked on success
     * @errorCallback: Callback to be invoked on error
     */
    postRegistrationMessageToServerWithAuth = function(uri, postParam, successCallback, errorCallback) {
        var server_url = localStorage.getItem('SERVER_URL');
        var deviceid = localStorage.getItem("DEVICE_ID");
        var deviceos = localStorage.getItem("DEVICE_OS");
        var appid = localStorage.getItem("APP_ID");
        var username = localStorage.getItem("USER_NAME");
        var password = localStorage.getItem("PASSWORD");
        var communeid = localStorage.getItem("COMMUNE_ID");
        var server_ts = localStorage.getItem("server_ts");

        var actions = {
            "deviceid": deviceid,
            "deviceos": deviceos,
            "appid": appid,
            "server_ts": server_ts,
            "locale": localStorage.getItem("APP_LOCALE")
        };

        var postParams;
        if (postParam != null)
            postParams = $.extend(actions, postParam);
        else
            postParams = actions;

        if (enableDebugMode) {
            alert(JSON.stringify(postParams));
            alert(server_url + uri);
            alert(localStorage.getItem('token_type') + ": " + localStorage.getItem('access_token'));
        }

        $.ajax({
            url: server_url + uri,
            type: "POST",
            data: postParams,
            dataType: "json",
            headers: {
                "Authorization": "Basic " + btoa(username + ":" + password),
                "COMMUNE_ID": "" + communeid,
                "compatibility_version": "" + localStorage.getItem("BackendCompatiabilityVersion")
            },
            success: function(data) {
                successCallback(data);
            },
            error: function(jqXHR, exception) {
                window.plugins.spinnerDialog.hide();
                if (jqXHR.status === 0) {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Connectivity"),
                            text: localizeString("Alert_Text_Connectivity"),
                            type: "info",
                            showCancelButton: true,
                            closeOnCancel: true,
                            allowOutsideClick: true,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_TryAgain"),
                            cancelButtonText: localizeString("Alert_Button_Cancel")
                        },
                        function() {
                            $.ajax(thisone);
                            return;
                        });
                } else if (jqXHR.status === 401) {
                    fetchUpdatedPublicMenu();
                    resignin();
                } else if (jqXHR.status === 412) {
                    var thisone = this;
                    if (localStorage.getItem('access_token').trim() != "") {
                        reauth(thisone, actions);
                        $.ajax(thisone);
                    } else {
                        resignin("auth");
                    }
                } else if (jqXHR.status === 426) {
                    swal({
                            title: localizeString("Alert_Title_Unsupported"),
                            text: jqXHR.responseText,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            allowOutsideClick: false,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                } else {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Something"),
                            text: localizeString("Alert_Text_Something"),
                            type: "info",
                            showCancelButton: true,
                            closeOnCancel: true,
                            allowOutsideClick: false,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_TryAgain"),
                            cancelButtonText: localizeString("Alert_Button_Cancel")
                        },
                        function() {
                            $.ajax(thisone);
                            return;
                        });
                }
            }
        });
    }
}());