(function () {
    var enableDebugMode = 0;
    "use strict";

    /**
     * Fetch the profile attributes to be displayed
     */
    getDisplayableAttributes = function () {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                showDisplayableAttributes(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user/atrributeDisplayForMemberProfile", actions, successCallback, errorCallback);
    }


    showDisplayableAttributes = function (memberDetails) {
        if (enableDebugMode)
            alert("memberDetails = " + JSON.stringify(memberDetails));

        var fname = localStorage.getItem("first_name");
        var lname = localStorage.getItem("last_name");
        var usravatar = localStorage.getItem("avatar");
        if (fname == null || fname == undefined)
            fname = "";
        if (lname == null || lname == undefined)
            lname = "";
        if (usravatar == null || usravatar == undefined)
            usravatar = "";

        $("#my-account-full-name").html(fname + " " + lname);
        $("#my-account-head-full-name").html(fname + " " + lname);
        $("#my-account-avatar").html(usravatar);
        for (var i = 0; i < memberDetails.content.length; i++) {
            if (memberDetails.content[i].name == "photo") {
                if (localStorage.getItem("photo") != null && localStorage.getItem("photo").trim() != ""
                    && localStorage.getItem("photo") != undefined) {
                    var img = document.getElementById("myphoto");
                    img.src = "data:image/jpeg;base64," + localStorage.getItem("photo");
                }
            } else {
                if (memberDetails.content[i].name == "avatar") {
                    $("#my-account-avatar").html(localStorage.getItem("avatar"));
                } else if ((memberDetails.content[i].name != "first_name") && (memberDetails.content[i].name != "last_name")
                    && (memberDetails.content[i].name != "status")) {
                    var tr = $("<tr></tr>");
                    if (memberDetails.content[i].name == "email")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("Contact_Text_Email") + ":</strong></td>");
                    else if (memberDetails.content[i].name == "phone")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("Contact_Text_Phone") + ":</strong></td>");
                    else if (memberDetails.content[i].name == "membership_number")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("Register_Text_Membership") + ":</strong></td>");
                    else if (memberDetails.content[i].name == "membership_type")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("MyAccount_Label_MembershipType") + ":</strong></td>");
                    else if (memberDetails.content[i].name == "dob")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("Account_Label_DateOfBirth") + ":</strong></td>");
                    else if (memberDetails.content[i].name == "gender")
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + localizeString("Account_Label_Gender") + ":</strong></td>");
                    else
                        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-" + memberDetails.content[i].id + "'>&nbsp;" + memberDetails.content[i].display_name + ":</strong></td>");
                    var fieldValue = localStorage.getItem(''+ memberDetails.content[i].name);
                    if (fieldValue == null || fieldValue == undefined || fieldValue.trim() == ""
                        || fieldValue == "0000-00-00" || fieldValue == "00-00-0000")
                        fieldValue = localizeString("Account_Label_NotAvailable");
                    // DOB formatting handler
                    if (memberDetails.content[i].name == "dob")
                        fieldValue = getDisplayableDate(fieldValue);
                    var td2 = $("<td class='details-table-wt' width='40%'><span id='" + memberDetails.content[i].name +"'>" + fieldValue + "</span></td>");
                    tr.append(td1, td2);
                    $("#detailsBody").append(tr);
                }
            }
        }

        // Append iSPrimary information
        var tr = $("<tr></tr>");
        var memType = localStorage.getItem('isPrimary');
        var td1 = $("<td class='details-table-wt' height='25' width='60%'><strong id='attribute-primary'>&nbsp;" + localizeString("MyAccount_Label_MemberType") + ":</strong></td>");
        if (memType == null || memType == undefined || memType == "")
            var td2 = $("<td class='details-table-wt' width='40%'><span id='isPrimary'>" + localizeString("Account_Label_NotAvailable") + "</span></td>");
        else if (localStorage.getItem('isPrimary') == "1")
            var td2 = $("<td class='details-table-wt'width='40%'><span id='isPrimary'>" + localizeString("Register_Option_Primary") + "</span></td>");
        else
            var td2 = $("<td class='details-table-wt' width='40%'><span id='isPrimary'>" + localizeString("Register_Option_Secondary") + "</span></td>");
        tr.append(td1, td2);
        $("#detailsBody").append(tr);
        // Append iSPrimary information Done

        if (localStorage.getItem("isLoggedIn") == "true") {
            if (localStorage.getItem("memberverified") != "0") {
                $("#notify").css("display", "none");
            } else { // Some verification is pending
                if (localStorage.getItem("email_verified") == "1") {
                    if (localStorage.getItem("admin_to_verify") == "1" || localStorage.getItem("admin_to_verify") == 1) {
                        $("#notify").css("display", "block");
                        $("#cardheading").html("Approval Pending !");
                        $("#cardcontent").html("Due to a mismatch of details submitted with club records, your account is pending club administrators approval. Please contact the club management for clarifications. Once verified, you will be able to access all member specific features of the MobileApp.");
                    }
                } else {
                    $("#notify").css("display", "block");
                    $("#cardheading").html(localizeString("Alert_Title_ActionRequired"));
                    $("#cardcontent").html(localizeString("Account_Label_VerificationMessage")  + "</br> </br> <div style='text-align:center; width:95px; border-bottom:1px solid white;' onclick='resendEmail();'>" + localizeString("Label_Resend_Email") + "</div></br>");
                }
            }
        } else {
            $("#notify").css("display", "block");
            $("#cardheading").html(localizeString("Alert_Title_ActionRequired"));
            $("#cardcontent").html(localizeString("MyAccount_Label_LoginWarning"));
        }
    }


    resendEmail = function () {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/email", actions, successCallback, errorCallback);
    }
}());