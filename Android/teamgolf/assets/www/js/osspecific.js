(function() {
    if (/Android/i.test(navigator.userAgent)) {
        $("a[data-toggle='menu']").css("style", "padding: 4px 10px;");
        $("#content").css({ "margin-top": "56px", "margin-bottom": "15px" });

        if ($(window.parent).height() > 400) {
            document.getElementsByTagName("body")[0].style.height = ($(window.parent).height() - 170) + "px";
            $("#content").css("height", ($(window.parent).height() - 117) + "px");
        } else {
            document.getElementsByTagName("body")[0].style.height = "100%";
            $("#content").css("height", "100%");
        }
    } else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
        $("body").prepend("<div class='ioshead' id='ioshead'></div>");
        $("a[data-toggle='menu']").css("style", "padding: 8px 10px;");
        $("#content").css({ "padding-top": "56px", "padding-bottom": "53px", "style": "-webkit-overflow-scrolling:touch" });

        document.getElementsByTagName("body")[0].style.height = ($(window.parent).height() - 130) + "px";
        $("#content").css("height", ($(window.parent).height() - 60) + "px");
    } else {
        //Web
        $("a[data-toggle='menu']").css("style", "padding: 4px 10px;");
        $("#content").css({ "padding-top": "56px", "padding-bottom": "15px" });

        if ($(window.parent).height() > 400) {
            document.getElementsByTagName("body")[0].style.height = ($(window.parent).height() - 170) + "px";
            $("#content").css("height", ($(window.parent).height() - 117) + "px");
        } else {
            document.getElementsByTagName("body")[0].style.height = "100%";
            document.getElementById("content").style.height = "100%";
        }
    }
}());