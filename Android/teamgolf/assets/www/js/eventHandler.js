(function () {

    var enableDebugMode = 0;

    // Required in Event details page
    var usrlimit = 0;
    var event_id;
    var MAX_TKTS_PER_PERSON = 10;
    var page_number = 1;
    var my_event_booking_list = 0;

    localStorage.setItem("from_page", "events");

    /* Extend this as needed */
    var bookingStatusArr = ["Pending Admin Aproval", "Admin Approved", "Admin Rejected", "User Cancelled", "Event Cancelled"];

    "use strict";

    localStorage["numofEvents"] = 0;
    localStorage["UserBookedEvents"] = 0;

    $(".content").scroll(function () {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ( !my_event_booking_list &&
                (parseInt(localStorage.getItem("numofEvents")) / 15 ) == page_number) {
                page_number++;
                getEvents(page_number);
            } else {
                if ( my_event_booking_list &&
                    (parseInt(localStorage.getItem("UserBookedEvents")) / 15 ) == page_number) {
                    page_number++;
                    getEventBookingList(page_number);
                }
            }
        }
    });

    getEvents = function (pg_no) {
        var actions = {
            "page_number": pg_no
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if ((pg_no == 1) && (data.data.content.length < 1)) {
                        $('.row').html('<p style="text-align:center">' + localizeString("Events_Label_NoData") + '</p>');
                } else if (data.data.content.length > 0) {
                    localStorage.setItem('numofEvents', String(parseInt(localStorage["numofEvents"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displayEventList(data.data);
                }
            } else {
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };

        postMessageToServerWithAuth("/events", actions, successCallback, errorCallback);
    }


    /**
     * Display the list of all events.
     */
    displayEventList = function (eventData) {
        if (enableDebugMode)
            alert("EventList = " + JSON.stringify(eventData));
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                          "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
                         ];


        /*if ((localStorage.getItem("isLoggedIn") == "true") && (localStorage.getItem("memberverified") != 0))
            $("#myEventbooking").css("display", "block");*/

        if (eventData.content.length < 1) {
            $('.row').html('<p style="text-align:center">' + localizeString("Events_Label_ScheduledNoData") + '</p>');
        } else {
            for (i = 0; i < eventData.content.length; i++) {
                var div1 = $('<div class="col-lg-4 col-sm-12"></div>').attr('onClick', "gotoEventDetail('" + eventData.content[i].master_id + "')");
                var div2 = $('<div class="card-01 col-sm-12"></div>');
                var img = "";
                if (eventData.content[i].eventPhotoPath != null && eventData.content[i].eventPhotoPath != undefined
                    && ((eventData.content[i].eventPhotoPath.image1 != null && eventData.content[i].eventPhotoPath.image1 != "")
                    || (eventData.content[i].eventPhotoPath.image2 != null && eventData.content[i].eventPhotoPath.image2 != "")
                    || (eventData.content[i].eventPhotoPath.image3 != null && eventData.content[i].eventPhotoPath.image3 != ""))) {
                    if (eventData.content[i].eventPhotoPath.image1 != null && eventData.content[i].eventPhotoPath.image1 != undefined
                        && eventData.content[i].eventPhotoPath.image1 != "") {
                        img = eventData.imageBasePath + eventData.content[i].eventPhotoPath.image1;
                    }
                    else {
                        if (eventData.content[i].eventPhotoPath.image2 != null && eventData.content[i].eventPhotoPath.image2 != undefined
                            && eventData.content[i].eventPhotoPath.image2 != "") {
                            img = eventData.imageBasePath + eventData.content[i].eventPhotoPath.image2;
                        }
                        else {
                            if (eventData.content[i].eventPhotoPath.image3 != null && eventData.content[i].eventPhotoPath.image3 != undefined
                                && eventData.content[i].eventPhotoPath.image3 != "") {
                                img = eventData.imageBasePath + eventData.content[i].eventPhotoPath.image3;
                            }
                        }
                    }
                } else {
                    img = "images/default-listing.png";
                }
                var aside1 = $('<aside class="card-side pull-left"></aside>'); //card-side-img
                var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + (((page_number - 1)*15) + i) + '"src="images/default-listing.png" data-src="' + img + '"></div>');
                aside1.append(imgDiv);
                var div4 = $('<div class="card-main card-width"></div>');
                var div5 = $('<div class="card-inner-01 card-inner-width"></div>');
                var p = $('<p class="card-heading card-heading-flow"></p>').text(eventData.content[i].eventTitle);

                var div6 = $('<div class="blog-post-meta"></div>')
                var span3 = $('<span></span>');
                var dateTime = getDateTimeFromString(eventData.content[i].eventStartDate);
                var dispDat = getDisplayableDate(dateTime[0]);
                var ielem1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ': ' + dispDat + '</i> &nbsp; <i class="fa fa-clock-o"> ' + getDisplayableTime(eventData.content[i].eventStartTime) + '</i></br>');

                var dateTime2 = getDateTimeFromString(eventData.content[i].eventEndDate);
                var dispDat2 = getDisplayableDate(dateTime2[0]);
                var ielem2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ': ' + dispDat2 + '</i> &nbsp; <i class="fa fa-clock-o"> ' + getDisplayableTime(eventData.content[i].eventEndTime) + '</i>');
                span3.append(ielem1, ielem2);

                div6.append(span3);
                div5.append(p, div6); /* p2,*/
                div4.append(div5);
                div2.append(aside1, div4);
                div1.append(div2);
                $('.row').append(div1);
            }
            lazyLoadListImages(page_number, eventData.content.length);
        }
    }


    gotoEventDetail = function (event_id) {
        localStorage.setItem("event_id", event_id);
        slideview("event-details.html", 'script', 'left');
    }


    /**
     * Get the details corresponding to a specific Event
     **/
    getEventDetails = function () {
        var actions = {
            "eventId": localStorage.event_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayEventDetails(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };

        postMessageToServerWithAuth("/events/details", actions, successCallback, errorCallback);
    }

    guestQntyChanged = function () {
        var guests = parseInt($("#guest-quantity option:selected").val());
        drawUserInfoFields(guests);
    }

    /**
     * Draw user input fields for guest details
     */
    drawUserInfoFields = function (guests) {
        var rsvpReqFields = JSON.parse(localStorage.getItem("rsvpFieldsArr"));
        $("#usrDataDiv").html("");
        if (rsvpReqFields != undefined && rsvpReqFields != null && rsvpReqFields.length > 0) {
            var topRowdiv = $('<div class="row" style="padding-bottom:20px;"></div>');
            for (var j = 0; j < guests; j++) {
                var rowdiv = $('<div style="padding-bottom:10px;"></div>');
                // Add fields to collect member data
                for (var i = 0; i < rsvpReqFields.length; i++) {
                    var colDiv = $('<div class="col-lg-6 col-sm-12"></div>');
                    var label =  $('<label style="width:35%;" for="person_' + j + '_det_' + i +'">' + rsvpReqFields[i] + '*: </label>'); //'(Person ' + j + ')
                    var inpt =  $('<input id="person_' + j + '_det_' + i +'" type="text" style="width:65%!important; display:inline-block;">');
                    colDiv.append(label, inpt, '</br>');
                    rowdiv.append(colDiv);
                }
                var plable= $('<p style="display:block;"><b> Details for Person ' + (j+1) + ': </b></p>');
                var espan = $('<span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="error_' + j + '"><i class="form-help-icon icon">error</i></span>');
                topRowdiv.append(plable, rowdiv, espan, '</br>');
            }
            $("#usrDataDiv").append(topRowdiv);
        }

        var rsvpSelArr = JSON.parse(localStorage.getItem("rsvpSelArr"));
        rsvpSelArr[localStorage.getItem("rsvpTktId")] = guests;
        localStorage.setItem("rsvpSelArr", JSON.stringify(rsvpSelArr));
        $("#buy-button").css('display', 'block');
    }

    /**
     * Display the specific event details
     * Enable booking if permitted.
     **/
    displayEventDetails = function (eventData) {
        if (enableDebugMode)
            alert("displayEventDetails: " + JSON.stringify(eventData));

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
            ];

        try{
            if (eventData.content.eventPhotoPath != null && eventData.content.eventPhotoPath != undefined
                && (eventData.content.eventPhotoPath.image1.length > 0 || eventData.content.eventPhotoPath.image2.length > 0
                || eventData.content.eventPhotoPath.image3.length > 0)) {
                if (eventData.content.eventPhotoPath.image1 != null && eventData.content.eventPhotoPath.image1 != undefined
                    && eventData.content.eventPhotoPath.image1 != "") {
                    $('#topbanner').attr({
                        'src': eventData.imageBasePath + eventData.content.eventPhotoPath.image1
                    });
                    $("#topbanner").css("display", "block");
                } else {
                    $("#topbanner").removeClass("mySlides");
                    $("#topbanner").css("display", "none");
                }
                if (eventData.content.eventPhotoPath.image2 != null && eventData.content.eventPhotoPath.image2 != undefined
                    && eventData.content.eventPhotoPath.image2 != "") {
                    $('#topbanner2').attr({
                        'src': eventData.imageBasePath + eventData.content.eventPhotoPath.image2
                    });
                } else {
                    $("#topbanner2").removeClass("mySlides");
                }
                if (eventData.content.eventPhotoPath.image3 != null && eventData.content.eventPhotoPath.image3 != undefined
                    && eventData.content.eventPhotoPath.image3 != "") {
                    $('#topbanner3').attr({
                        'src': eventData.imageBasePath + eventData.content.eventPhotoPath.image3
                    });
                } else {
                    $("#topbanner3").removeClass("mySlides");
                }
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner2").removeClass("mySlides");
                $("#topbanner3").removeClass("mySlides");
            }
        } catch(err) {
            $("#topbanner").removeClass("mySlides");
            $("#topbanner2").removeClass("mySlides");
            $("#topbanner3").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(eventData.content.eventTitle);
        var span1 = $('<span style=" font-size:12px;"></span>');

        var dispDat = getDisplayableDate(eventData.content.eventStartDate);
        if (eventData.content.eventStartTime != null && eventData.content.eventStartTime != undefined
            && eventData.content.eventStartTime.trim() != "") {
            var dispTim = getDisplayableTime(eventData.content.eventStartTime);
            var iel1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ': ' + dispDat + ' </i> &nbsp; <i class="fa fa-clock-o"> ' + dispTim + '</i></br>');
        } else {
            var iel1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ': '+ dispDat + '</i></br>');
        }
        var dispDat2 = getDisplayableDate(eventData.content.eventEndDate);
        if (eventData.content.eventEndTime != null && eventData.content.eventEndTime != undefined
            && eventData.content.eventEndTime.trim() != "") {
            var dispTim2 = getDisplayableTime(eventData.content.eventEndTime);
            var iel2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ': ' + dispDat2 + ' </i> &nbsp; <i class="fa fa-clock-o"> ' + dispTim2 + '</i></br>');
        } else {
            var iel2 = $('<i class="fa fa-calendar"> '+localizeString("Label_Ends") + ': ' + dispDat2 + '</i></br>');
        }
        if (eventData.content.location != null && eventData.content.location != undefined
            && eventData.content.location.trim() != "") {
            var iel3 = $('<i class="fa fa-map-marker"> ' + eventData.content.location + '</i></br>');
            span1.append(iel1, iel2, iel3);
        } else {
            span1.append(iel1, iel2);
        }
        var p2 = $('<p class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word; width:100%!important;"></p>');
        p2.html(eventData.content.eventDescription);
        td1.append(p1, span1, p2);
        tr1.append(td1);

        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');
        carousel();

        /**
         * Allow event calendar booking only if one of the below conditions are met:
         * 1. User is loggedin && is verified && booking is enabled.
         * 2. User is loggedin && the event booking cfd id was sent as part of logged in menu
         */
        if ((((localStorage.getItem("isLoggedIn") == "true")
            && (localStorage.getItem("memberverified") != 0))
            || ((localStorage.getItem("isLoggedIn") == "true")
            && (localStorage.getItem("eventCalendarBookingMenuEnabled") == "true")))) {

            localStorage.setItem("isEventBookable", eventData.content.isBookable);
            if (1 == parseInt(eventData.content.status)) { // Visible or active
                if (1 == parseInt(eventData.content.isBookable)) { // Ticketed Event
                    localStorage.setItem("EventTicketCurrency", eventData.currency);
                    localStorage.setItem("EventTicketDetails", JSON.stringify(eventData.content.ticketInfo));
                    var tr2 = $('<tr></tr>');
                    var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
                    tr2.append(td2);
                    tr2.insertBefore('#buttontr');

                    var dropdownDiv = $('<div class="form-group form-group-label control-highlight control-focus" id="ticket-picker-form" style="width:100%; float:left; margin-top:5px;"></div>');

                    for(var i = 0; i < eventData.content.ticketInfo.length; i++) {
                        var rowdiv = $('<div class="row" style="padding-bottom:10px;"></div>');
                        var colDiv = $('<div class="col-lg-6 col-sm-12"></div>');
                        var label =  $('<label style="width:80%;" for="ticket-quantity-' + i + '">' + eventData.content.ticketInfo[i].ticketTypeName
                            + ' (' + eventData.currency + ' ' + eventData.content.ticketInfo[i].price + ') : </label>');
                        var tktdropdown = '<select class="form-control" style="width:20%!important; display:inline-block;" id="ticket-quantity-' + i + '">';
                        tktdropdown += '<option value="' + eventData.content.ticketInfo[i].id + ':0' + '">0</option>'
                        var countdown = '';
                        var usrlimit = parseInt(eventData.content.ticketInfo[i].bookingLimit);
                        var availableLimit = parseInt(eventData.content.ticketInfo[i].limit);
                        var availability = parseInt(eventData.content.ticketInfo[i].availableTicketCount);
                        var allowedTicekts = availableLimit < availability ? availableLimit : availability;
                        for(var j = 1; j <= allowedTicekts; j++) {
                            countdown += '<option value="' + eventData.content.ticketInfo[i].id + ':' + j + '">' + j + '</option>';
                        }
                        countdown += '</select>';
                        tktdropdown += countdown;
                        colDiv.append(label, tktdropdown, '</br>');
                        rowdiv.append(colDiv);
                        dropdownDiv.append(rowdiv);
                    }
                    var errorSpan = $('<span class="form-help form-help-msg text-red" style="float:left; margin-top:0px; margin-bottom:0px; display:none;" id="ticketerror"><i class="form-help-icon icon">error</i></span>');
                    dropdownDiv.append(errorSpan);

                    dropdownDiv.insertAfter('#buttontr');

                    var disabledTkt = 0;
                    /* Disable tickets selection once the limit is complete */
                    for(var i = 0; i < eventData.content.ticketInfo.length; i++) {
                        if (0 >= parseInt(eventData.content.ticketInfo[i].limit)) {
                            $("#ticket-quantity-" + i).attr('disabled', 'disabled');
                            disabledTkt++;
                        }
                    }
                    if (disabledTkt == eventData.content.ticketInfo.length) {
                        var buttonP = $('<div style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;" id="buy-button"> You have already requested maximum tickets for this event </p></div>');
                    } else {
                        var buttonP = $('<p align="center" id="buy-button"></p>');
                        var buttonHref = $('<a href="javascript:void(0);" class="btn btn-alt" id="tickets_buy" onclick="return buytickets();"><i class="fa fa-ticket"></i> ' + localizeString("EventDetail_Button_Request") + '</a>');
                        buttonP.append(buttonHref);
                    }

                    buttonP.insertAfter('#ticket-picker-form');
                    localStorage.setItem("EventMasterId", eventData.content.master_id);
                } else if (2 == parseInt(eventData.content.isBookable)) { // RSVP only
                    var availability = parseInt(eventData.content.ticketInfo[0].availableTicketCount);
                    var totalPersons = 0;
                    if (eventData.content.ticketInfo[0].limit != undefined
                        && eventData.content.ticketInfo[0].limit != null)
                        totalPersons = parseInt(eventData.content.ticketInfo[0].limit);
                    if (0 < availability) {
                        var selectedArr = {};
                        selectedArr[eventData.content.ticketInfo[0].id] = 1;
                        localStorage.setItem("EventMasterId", eventData.content.master_id);
                        var tr2 = $('<tr></tr>');
                        var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
                        tr2.append(td2);
                        tr2.insertBefore('#buttontr');
                        localStorage.setItem("rsvpSelArr", JSON.stringify(selectedArr));
                        var buttonP;
                        if (0 >= totalPersons) {
                            buttonP = $('<div style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;" id="buy-button"> You have already RSVP\'d for this event </p></div>');
                            buttonP.insertAfter('#buttontr');
                        } else {
                            if (eventData.content.ticketInfo[0].ticketTypeName
                                && eventData.content.ticketInfo[0].ticketTypeName.toLowerCase() == "guests") {
                                // give number selection drop down
                                var rowdiv = $('<div class="row" id="seldrpdwn" style="padding-bottom:10px;"></div>');
                                var colDiv = $('<div class="col-lg-6 col-sm-12"></div>');
                                var label =  $('<label style="width:80%;" for="guest-quantity"> Total number of people : </label>');
                                var tktdropdown = '<select class="form-control" style="width:20%!important; display:inline-block;" onchange="return guestQntyChanged();" id="guest-quantity">';
                                tktdropdown += '<option value="0" style="display:none;"></option>'
                                var countdown = '';
                                var availability = parseInt(eventData.content.ticketInfo[0].availableTicketCount);
                                var allowedTicekts = totalPersons < availability ? totalPersons : availability;
                                for(var j = 1; j <= allowedTicekts; j++) {
                                    countdown += '<option value="' + j + '">' + j + '</option>';
                                }
                                countdown += '</select>';
                                tktdropdown += countdown;
                                colDiv.append(label, tktdropdown, '</br>');
                                rowdiv.append(colDiv);
                                rowdiv.insertAfter('#buttontr');
                            }

                            if ((eventData.content.ticketInfo[0].rsvpFieldReq != null)
                                && (eventData.content.ticketInfo[0].rsvpFieldReq != undefined)) {
                                var rowdiv = $('<div class="row" id="rsvp-fields" style="padding-bottom:10px;"></div>');
                                var usrdatadiv = $('<div class="col-lg-6 col-sm-12" id="usrDataDiv"></div>');
                                rowdiv.append(usrdatadiv);
                                if (eventData.content.ticketInfo[0].ticketTypeName.toLowerCase() == "guests") {
                                    localStorage.setItem("tktTypName", "guests");
                                    rowdiv.insertAfter('#seldrpdwn');
                                } else {
                                    localStorage.setItem("tktTypName", "member");
                                    rowdiv.insertAfter('#buttontr');
                                }
                                localStorage.setItem("rsvpFieldsArr", JSON.stringify(eventData.content.ticketInfo[0].rsvpFieldReq));
                                buttonP = $('<p style="text-align:center; display:none;" id="buy-button"> <a href="javascript:void(0);" class="btn btn-alt" id="rsvp-button" onclick="return rsvpForEvent(\'' + localStorage.event_id + '\');">RSVP</a></p>');
                                buttonP.insertAfter('#usrDataDiv');
                                if (eventData.content.ticketInfo[0].ticketTypeName.toLowerCase() != "guests") {
                                    drawUserInfoFields(1);
                                }
                            } else {
                                buttonP = $('<p style="text-align:center; display:none;" id="buy-button"> <a href="javascript:void(0);" class="btn btn-alt" id="rsvp-button" onclick="return rsvpForEvent(\'' + localStorage.event_id + '\');">RSVP</a></p>');
                                if (eventData.content.ticketInfo[0].ticketTypeName.toLowerCase() == "guests") {
                                    localStorage.setItem("tktTypName", "guests");
                                    buttonP.insertAfter('#seldrpdwn');
                                } else {
                                    localStorage.setItem("tktTypName", "member");
                                    buttonP.insertAfter('#buttontr');
                                    $("#buy-button").css('display', 'block');
                                }
                            }
                        }
                    } else {
                        var noavailability = $('<div style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;" id="buy-button"> Sorry, we ran out of the seats for this event.</p></div>');
                        noavailability.insertAfter('#buttontr');
                    }
                }
            } else if (2 == parseInt(eventData.content.status)) { // Event Cancelled
                var cancel_booking = $('<div style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;"> This event has been cancelled. Please check with the club admin for any further queries. </p></div>');
                cancel_booking.insertAfter('#buttontr');
            } else {
                console.log("Un-handled case. Status = " + eventData.content.status);
            }
        }
    }

    rsvpForEvent = function (eventid) {
        var guestDetails = [];
        var selectedArr = JSON.parse(localStorage.getItem("rsvpSelArr"));
        if (2 == parseInt(localStorage.getItem("isEventBookable"))) {
            var guests = 1;
            if (localStorage.getItem("tktTypName") == "guests") {
                guests = parseInt($("#guest-quantity option:selected").val());
            }
            var rsvpReqFields = JSON.parse(localStorage.getItem("rsvpFieldsArr"));
            for (var j = 0; j < guests; j++) {
                $("#error_" + j).html("");
                $("#error_" + j).css("display", "none");
            }
            if (rsvpReqFields != undefined && rsvpReqFields != null && rsvpReqFields.length > 0) {
                for (var j = 0; j < guests; j++) {
                    var ob1 = {};
                    for (var i = 0; i < rsvpReqFields.length; i++) {
                        var curval = $("#person_"+ j + "_det_" + i).val().trim();
                        if (curval.trim() == "") {
                            var emsg = rsvpReqFields[i] + " for Person " + (j + 1) + " cannot be left blank";
                            $("#error_" + j).html(emsg + ' <i class="form-help-icon icon">error</i>');
                            $("#error_" + j).css("display", "inline");
                            ob1 = {};
                            return;
                        } else {
                            curval = curval.replace(/'/g, "\\'");
                            ob1[rsvpReqFields[i]] = curval;
                        }
                    }
                    guestDetails.push(ob1);
                }
            }
        } else {
            guestDetails = null;
        }
        requestTickets(eventid, selectedArr, guestDetails);
    }

    /**
     * Buy event tickets by requesting clubadmin for the specific
     * number and type of tickets.
     */
    buytickets = function () {
        var event_id = localStorage.event_id;
        var continueBooking = 0;
        $("#ticketerror").css("display", "none");
        $("#ticketerror").html('<i class="form-help-icon icon">error</i>');

        var ticketInfo = JSON.parse(localStorage.getItem("EventTicketDetails"));
        var selectedArr = {};
        for (var i = 0; i < ticketInfo.length; i++) {
            var selectedVal = $("#ticket-quantity-" + i + " option:selected").val().split(":");
            selectedArr[selectedVal[0]] = selectedVal[1];
            if (selectedVal[1] > 0)
                continueBooking = 1;
        }
        if (continueBooking == 0) {
            $("#ticketerror").css("display", "inline");
            $("#ticketerror").html(localizeString("ValidationError_ValidTicketQuantity") + '<i class="form-help-icon icon">error</i>');
            return;
        }
        var price = 0;
        var details = "";
        var currency = localStorage.getItem("EventTicketCurrency");
        for (var i = 0; i < ticketInfo.length; i++) {
            var tktPrice = parseInt(selectedArr[ticketInfo[i].id]) * ticketInfo[i].price
            price += tktPrice;
            if (parseInt(selectedArr[ticketInfo[i].id]) > 0)
                details = details + ticketInfo[i].ticketTypeName + "Tickets : " + selectedArr[ticketInfo[i].id] + " * " + ticketInfo[i].price + " = " + currency + " " + tktPrice + " \n";
        }
        details = details + " --------------------- \n Final Price = " + currency + " " + price + ". Same will be debited from your member account";
        swal({
            title: localizeString("Alert_Title_PriceConfirmation"),
            text: details,
            type: "info",
            showCancelButton: true,
            closeOnCancel: true,
            allowOutsideClick: false,
            closeOnConfirm: false,
            confirmButtonText: localizeString("Alert_Button_Continue"),
            cancelButtonText: localizeString("Alert_Button_Cancel")
        },
        function () {
            requestTickets(event_id, selectedArr, null);
            return;
        });
        return;
    }


    /**
     * Complete the ticket reservation process post the user approval
     * @event_id unique event identifier
     * @ticket_id unique ticket identifier
     * @ticket_quantity number of tickets
     */
    requestTickets = function (event_id, selectedTickets, guestDetails) {
        var event_master_id = localStorage.getItem("EventMasterId");
        var isEvntBkbl = localStorage.getItem("isEventBookable");
        var actions = {
            "eventId": event_id,
            "master_id": event_master_id,
            "isBookable": isEvntBkbl,
            "ticketRequested": selectedTickets,
            "guestsDetails": guestDetails
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Done"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    localStorage.setItem("rsvpFieldsArr", JSON.stringify([]));
                    slideview('events-booking-list.html', 'script', 'left');
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };

        postMessageToServerWithAuth("/events/registerEvent", actions, successCallback, errorCallback);
    }


    /**
     * Fetch event booking history for the user
     */
    getEventBookingList = function (pg_no) {
        my_event_booking_list = 1;
        var actions = {
            "page_number": pg_no
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                localStorage.setItem('UserBookedEvents', String(parseInt(localStorage["UserBookedEvents"]) + parseInt(JSON.stringify(data.data.content.length))));
                if (pg_no == 1) {
                    $('#user-list').html("");
                    if (data.data.content.length == 0) {
                        var p1 = $('<p style="text-align:center">'+localizeString("EventTickets_Label_Nodata")+'</p>');
                        $('#user-list').append(p1);
                    } else {
                        displayEventBookingList(data.data);
                    }
                } else {
                    displayEventBookingList(data.data);
                }
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };

        postMessageToServerWithAuth("/events/getBookings", actions, successCallback, errorCallback);
    }


    /**
     * List all the event tickets booked by the user
     */
    displayEventBookingList = function (bookingList) {
        bookingStatusArr[0] = localizeString("GolfTournaments_Label_RegStatusPending");
        bookingStatusArr[1] = localizeString("GolfTournaments_Label_RegStatusApproved");
        bookingStatusArr[2] = localizeString("GolfTournaments_Label_RegStatusRejected");
        bookingStatusArr[3] = localizeString("GolfTournaments_Label_RegStatusCancelled");
        bookingStatusArr[4] = "Event Cancelled"; // localize this

        if (enableDebugMode)
            alert("booking json = " + JSON.stringify(bookingList));
        for (var i = 0; i < bookingList.content.length; i++) {
            var bookingStatus = bookingStatusArr[parseInt(bookingList.content[i].status)];
            var start_date = bookingList.content[i].eventStart;
            var event_name = bookingList.content[i].eventTitle;
            var booking_id = bookingList.content[i].bookingId;
            var booked_on = bookingList.content[i].bookedOn;
            var dispDat = getDisplayableDate(bookingList.content[i].bookedOn.split(" ")[0]);
            var dispTim = getDisplayableTime(bookingList.content[i].bookedOn.split(" ")[1]);
            var event_location = bookingList.content[i].location;
            if (event_location.length > 15)
                event_location = event_location.substring(0, 14) + '...';

            var li = $('<li></li>');
            var a = $('<a></a>').attr({
                'onclick': 'gotoEventBookingDetails(\"' + booking_id + '\");',
                'href': '#',
                'style': 'float:left; padding:5px 10px; width:100%;'
            });
            var div2 = $('<div></div>').attr({
                'class': 'five_sixth bold-match-color',
                'style': 'line-height:40px; width:100%;'
            }).text(event_name);

            var div3 = $('<div></div>').attr({
                'class': 'text',
                'style': 'line-height:26px; width:100%;'
            });
            if ((bookingList.content[i].isBookable == 2) && (1 >= parseInt(bookingList.content[i].status))) {
                var span2 = $('<span></span>').attr({
                    'style': 'float:right;'
                }).html(localizeString("MyEvent_Label_TicketStatus") + ": <strong> RSVP'd </strong>");
            } else {
                var span2 = $('<span></span>').attr({
                    'style': 'float:right;'
                }).html(localizeString("MyEvent_Label_TicketStatus") + ": <strong>" + bookingStatus + "</strong>");
            }
            div3.append(span2);
            var div4 = $('<div></div>').attr({
                'class': 'text',
                'style': 'line-height:26px; float:left; width:100%; white-space:nowrap; overflow:hidden;'
            });
            var span3 = $('<span></span>').attr({
                'style': 'float:right;'
            }).html("Booked On: <strong>" + [dispDat, dispTim].join(" ") + "</strong>");
            var span4 = $('<span></span>').attr({
                'style': 'float:left;'
            }).html("<strong>#" + booking_id + "</strong>");
            div4.append(span4, span3);
            a.append(div2, div3, '</br>', div4);
            li.append(a);
            $('#user-list').append(li);
        }
    }


    /**
     * Load existing event booking details
     * @bookingId Event booking identifier fo which details need to be fetched
     */
    gotoEventBookingDetails = function (bookingId) {
        localStorage.setItem("event_booking_id", bookingId);
        slideview('events-booking-details.html', 'script', 'left');
        return false;
    }


    /**
     * Fetch existing booking details from the server corresponding to the bookingId
     */
    eventBookingDetailsLoad = function () {
        var bookingId = localStorage.getItem("event_booking_id");

        var actions = {
            "bookingId": bookingId
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayExistingEventBookingDetails(data.data);
            } else {
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/events/getBookings", actions, successCallback, errorCallback);
    }

    /**
     * Render the existing booking details on the UI
     */
    displayExistingEventBookingDetails = function (bookingDetail) {
        bookingStatusArr[0] = localizeString("GolfTournaments_Label_RegStatusPending");
        bookingStatusArr[1] = localizeString("GolfTournaments_Label_RegStatusApproved");
        bookingStatusArr[2] = localizeString("GolfTournaments_Label_RegStatusRejected");
        bookingStatusArr[3] = localizeString("GolfTournaments_Label_RegStatusCancelled");
        bookingStatusArr[4] = "Event Cancelled"; // localize this

        if (enableDebugMode)
            alert("Booking Details = " + JSON.stringify(bookingDetail));

        var event_name = bookingDetail.content.eventTitle;
        var event_location = (bookingDetail.content.location != null && bookingDetail.content.location != undefined) ? bookingDetail.content.location : "";
        var booking_status = bookingStatusArr[parseInt(bookingDetail.content.bookingStatus)];
        var notes = bookingDetail.content.eventDescription;

        if (null != bookingDetail.content.eventPhotoPath && undefined != bookingDetail.content.eventPhotoPath)
        {
            if (null != bookingDetail.content.eventPhotoPath.image1 && undefined != bookingDetail.content.eventPhotoPath.image1
                && "" != bookingDetail.content.eventPhotoPath.image1) {
                $('#topbanner').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.eventPhotoPath.image1
                });
                $("#topbanner").css("display", "block");
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }

            if (null != bookingDetail.content.eventPhotoPath.image2 && undefined != bookingDetail.content.eventPhotoPath.image2
                && "" != bookingDetail.content.eventPhotoPath.image2) {
                $('#topbanner2').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.eventPhotoPath.image2
                });
            } else {
                $("#topbanner2").removeClass("mySlides");
            }

            if (null != bookingDetail.content.eventPhotoPath.image3 && undefined != bookingDetail.content.eventPhotoPath.image3
                && "" != bookingDetail.content.eventPhotoPath.image3) {
                $('#topbanner3').attr({
                    'src': bookingDetail.imageBasePath + bookingDetail.content.eventPhotoPath.image3
                });
            } else {
                $("#topbanner3").removeClass("mySlides");
            }

            carousel();
        } else {
            $("#topbanner").removeClass("mySlides");
            $("#topbanner2").removeClass("mySlides");
            $("#topbanner3").removeClass("mySlides");
        }

        var sDate, sTime, eDate, eTime;

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(event_name);
        var p2 = $('<p> Event Location: <strong>' + event_location + '</strong> </p>');
        var p3 = $('<p> '+localizeString("Label_BookingStatus") + ': <strong>' + booking_status + '</strong> </p>');
        var span1 = $('<span style=" font-size:12px;"></span>');

        var dispDat = getDisplayableDate(bookingDetail.content.eventStartDate);
        if (bookingDetail.content.eventStartTime != null && bookingDetail.content.eventStartTime != undefined
            && bookingDetail.content.eventStartTime.trim() != "") {
            var dispTim = getDisplayableTime(bookingDetail.content.eventStartTime);
            var iel1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ': ' + dispDat
                + ' </i> &nbsp; <i class="fa fa-clock-o"> ' + dispTim + '</i></br>');
            sDate = bookingDetail.content.eventStartDate;
            sTime = bookingDetail.content.eventStartTime.trim();
        } else {
            var iel1 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Starts") + ': ' + dispDat + ' </i></br>');
            sDate = bookingDetail.content.eventStartDate;
            sTime = null;
        }
        var dispDat2 = getDisplayableDate(bookingDetail.content.eventEndDate);
        if (bookingDetail.content.eventEndTime != null && bookingDetail.content.eventEndTime != undefined
            && bookingDetail.content.eventEndTime.trim() != "") {
            var dispTim2 = getDisplayableTime(bookingDetail.content.eventEndTime);
            var iel2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ': ' + dispDat2
                + ' </i> &nbsp; <i class="fa fa-clock-o"> ' + dispTim2 + '</i></br>');
            eDate = bookingDetail.content.eventEndDate;
            eTime = bookingDetail.content.eventEndTime.trim();
        } else {
            var iel2 = $('<i class="fa fa-calendar"> ' + localizeString("Label_Ends") + ': ' + dispDat2 + ' </i>');
            eDate = bookingDetail.content.eventEndDate;
            eTime = null;
        }
        span1.append(iel1, iel2);
        var p4 = $('<p class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + bookingDetail.content.eventDescription + '</p>');
        td1.append(p1, p2, p3, span1, p4);
        tr1.append(td1);
        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');

        if (localStorage.getItem("isLoggedIn") == "true") {
            localStorage.setItem("EventTicketCurrency", bookingDetail.currency);
            localStorage.setItem("EventTicketDetails", JSON.stringify(bookingDetail.content.ticketInfo));

            var tr2 = $('<tr></tr>');
            var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
            tr2.append(td2);
            tr2.insertBefore('#buttontr');

            if (parseInt(bookingDetail.content.bookingStatus) == 1) {
                notes = notes.replace(/'/g, "\\'").replace(/"/g, "&quot;");
                event_name = event_name.replace(/'/g, "\\'").replace(/"/g, "&quot;");
                event_location = event_location.replace(/'/g, "\\'").replace(/"/g, "&quot;");
                var reminder = $('<div style="float:right;" onclick="addEventToNativeCalendar(\'' + event_name + '\', \'' + event_location + '\', \'' + notes + '\', \'' + sDate + '\', \'' + sTime + '\', \'' + eDate + '\', \'' + eTime + '\'); return;"><img src="images/reminder.png" width="40" height="40"></div>');
                reminder.insertBefore("#buttontr");
            }

            var dropdownDiv = $('<div class="form-group form-group-label control-highlight control-focus" id="ticket-picker-form" style="width:100%; float:left; margin-top:5px;"></div>');

            if (bookingDetail.content.isBookable == 1) {
                var totalTicketPrice = 0;
                for(var i = 0; i < bookingDetail.content.ticketInfo.length; i++) {
                    var rowdiv = $('<div class="row" style="padding-bottom:10px;"></div>');
                    var colDiv = $('<div class="col-lg-6 col-sm-12"></div>');
                    var label =  $('<label style="width:80%;" for="ticket-quantity-' + i + '">' + bookingDetail.content.ticketInfo[i].ticketTypeName
                        + ' (' + bookingDetail.currency + ' ' + bookingDetail.content.ticketInfo[i].price + ') : </label>');
                    var tktdropdown = '<select class="form-control" style="width:20%!important; display:inline-block;" id="ticket-quantity-' + i + '">';
                    tktdropdown += '<option value="' + bookingDetail.content.ticketInfo[i].id + ':0' + '">0</option>'
                    var countdown = '';
                    var usrlimit = parseInt(bookingDetail.content.ticketInfo[i].bookingLimit);
                    var availability = parseInt(bookingDetail.content.ticketInfo[i].availableTicketCount);
                    var allowedTicekts = usrlimit < availability ? usrlimit : availability;
                    for(var j = 1; j <= allowedTicekts; j++) {
                        countdown += '<option value="' + bookingDetail.content.ticketInfo[i].id + ':' + j + '">' + j + '</option>';
                    }
                    countdown += '</select>';
                    tktdropdown += countdown;
                    colDiv.append(label, tktdropdown, '</br>');
                    rowdiv.append(colDiv);
                    dropdownDiv.append(rowdiv);
                    totalTicketPrice += bookingDetail.content.ticketInfo[i].price * bookingDetail.content.ticketInfo[i].ticketsBooked;
                }

                if (totalTicketPrice > 0) {
                    var ticketPrice = $('<label id="totalTicketPrice" style="width:100%;"> Total amount = <strong>' + bookingDetail.currency + ' ' + totalTicketPrice + '</strong></label>');
                    dropdownDiv.append('</br>', ticketPrice);
                }
                dropdownDiv.insertAfter('#buttontr');

                /* Show Already selected ticket count upfront */
                for(var i = 0; i < bookingDetail.content.ticketInfo.length; i++) {
                    $("select#ticket-quantity-" + i + " option")
                    .each(function () {
                        this.selected = (this.text == bookingDetail.content.ticketInfo[i].ticketsBooked);
                    });

                    $("#ticket-quantity-" + i).attr('disabled', 'disabled');
                }
                /* selected ticket count display done */

                if (parseInt(bookingDetail.content.bookingStatus) < 2) {
                    var buttonP = $('<p align="center"></p>');

                    var cancelButton = $('</br><div class="btn btn-alt" id="cancel_booking" onclick="return eventBookingCancle(\'' + bookingDetail.content.bookingId + '\');">' + localizeString("Alert_Title_CancelTickets") + '</div>');

                    buttonP.append(cancelButton);
                    buttonP.insertAfter('#ticket-picker-form');
                    if (bookingDetail.content.editable != "1") {
                        $("#cancel_booking").attr('disabled', 'disabled');
                        $("#cancel_booking").removeAttr('onclick');
                    }
                }
            } else if (bookingDetail.content.isBookable == 2) {
                var totalGuests = $('<div id="guestCount" style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;"> Total number of seats requested: ' + bookingDetail.content.ticketInfo[0].ticketsBooked + '</p></div>');
                totalGuests.insertAfter('#buttontr');

                if (bookingDetail.content.ticketInfo[0].rsvp
                    && bookingDetail.content.ticketInfo[0].rsvp.length > 0) {
                    var topRowdiv = $('<div class="row" id="detdiv" style="padding-bottom:20px;"></div>');
                    for (var k = 0; k < bookingDetail.content.ticketInfo[0].rsvp.length; k++) {
                        var rowdiv = $('<div style="padding-bottom:10px;"></div>');
                        for (var key in bookingDetail.content.ticketInfo[0].rsvp[k]) {
                            if (bookingDetail.content.ticketInfo[0].rsvp[k].hasOwnProperty(key)) {
                                if (bookingDetail.content.ticketInfo[0].rsvp[k][key] != null) {
                                    var colDiv = $('<div class="col-lg-6 col-sm-12"></div>');
                                    var label =  $('<label style="width:35%;" for="person_' + k + '_det_' + key +'">' + key + '*: </label>'); //'(Person ' + j + ')
                                    var inpt =  $('<input id="person_' + k + '_det_' + key +'" type="text" style="width:65%!important; display:inline-block;" value="' + bookingDetail.content.ticketInfo[0].rsvp[k][key] + '" disabled>');
                                    colDiv.append(label, inpt, '</br>');
                                    rowdiv.append(colDiv);
                                }
                            }
                        }
                        var plable= $('<p style="display:block; padding-left:5px;"><b> Details for Person ' + (k+1) + ': </b></p>');
                        topRowdiv.append(plable, rowdiv, '</br>');
                    }
                    topRowdiv.insertAfter('#guestCount');
                }
                if (parseInt(bookingDetail.content.bookingStatus) < 2) {
                    var buttonP = $('<p align="center"></p>');

                    var cancelButton = $('</br><div class="btn btn-alt" id="cancel_booking" onclick="return eventBookingCancle(\'' + bookingDetail.content.bookingId + '\');">' + localizeString("Alert_Button_Cancel") + '</div>');

                    buttonP.append(cancelButton);
                    if (bookingDetail.content.ticketInfo[0].rsvp != null)
                        buttonP.insertAfter('#detdiv');
                    else
                        buttonP.insertAfter('#guestCount');

                    if (bookingDetail.content.editable != "1") {
                        $("#cancel_booking").attr('disabled', 'disabled');
                        $("#cancel_booking").removeAttr('onclick');
                    }
                }
            }

            if (4 == parseInt(bookingDetail.content.bookingStatus)) {
                var cancel_booking = $('<div style="width:100%; margin:0 auto; text-align:center"> <p align="center" style="font-weight:bold;"> This event has been cancelled. Please check with the club admin for any further queries. </p></div>');
                cancel_booking.insertAfter('#buttontr');
            }
        }
    }


    /**
     * Check if the user wants to cancle the tickets?
     * @booking_id booking identifier for which the boking is to be cancled
     */
    eventBookingCancle = function (booking_id) {
        swal({
            title: localizeString("Alert_Title_CancelTickets"),
            text: localizeString("Alert_Text_CancelTickets"),
            type: "warning",
            showCancelButton: true,
            cancelButtonText: localizeString("Alert_Button_No"),
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            confirmButtonText: localizeString("Alert_Button_Yes")
        },
        function () {
            cancelEventTicket(booking_id);
        });
        return false;
    }


    /**
     * Cancle the tickets post the user approval.
     * @booking_id booking identifier for which the boking is to be cancled
     */
    cancelEventTicket = function (booking_id) {
        var actions = {
            "bookingId": booking_id
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Done"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    slideview('events-booking-list.html', 'script', 'left');
                });
            } else {
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/events/cancelBooking", actions, successCallback, errorCallback);
    }
}());