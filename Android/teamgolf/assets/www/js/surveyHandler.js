(function() {
    var pageflag = 1;
    var datacount = 0;
    var page_number = 1;
    var enableDebugMode = 0;
    "use strict";
    localStorage["numofSurveys"] = 0;

    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            if ((parseInt(localStorage.getItem("numofSurveys")) / 15) == page_number) {
                page_number++;
                getSurveyList(page_number);
            }
        }
    });


    /**
     *  Fetches the list of Surveys based on page_number
     *  @page_number current viewable page
     */
    getSurveyList = function(page_number) {
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                datacount = data.data.content.length;
                if (data.data.content.length == 0) {
                    blankData = true;
                    if (pageflag == 1)
                        $('#content').html('<p style="text-align:center">' + localizeString("Surveys_Label_NoData") + '</p>');
                } else {
                    localStorage.setItem('numofSurveys', String(parseInt(localStorage["numofSurveys"]) + parseInt(JSON.stringify(data.data.content.length))));
                    displaySurveyList(data.data.content, data.data.imageBasePath);
                }
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
            pageflag++;
        };
        errorCallback = function(data) {};
        postMessageToServerWithAuth("/surveys", actions, successCallback, errorCallback);
    }


    /**
     *  Displays the List of all the active Surveys
     *  @surveyList Survey List received from the Server
     */
    displaySurveyList = function(surveyList, imageBasePath) {
        if (enableDebugMode)
            alert("surveyList = " + JSON.stringify(surveyList));
        for (i = 0; i < surveyList.length; i++) {
            $('#content').append(getListDom(surveyList[i], imageBasePath, "gotoSurveyDetail"));
        }
        lazyLoadListImages(page_number, surveyList.length);
    }

    function getListDom(listData, iImgUrl, iClickCall) {
        var name = listData['title'],
            cStart = moment(listData['startFrom'], "YYYY-MM-DD"),
            cEnd = moment(listData['validUpto'], "YYYY-MM-DD"),
            id = listData['id'],
            logo = (listData['image']) ? (iImgUrl + listData['image']) : "./images/placeholder.png",
            commentsCount = listData['commentsCount'];

        cStart = (cStart.isValid()) ? cStart.format("lll") : "--";
        cEnd = (cEnd.isValid()) ? cEnd.format("lll") : "--";

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="' + iClickCall + '(\'' + id + '\')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + name + '</div>\
                        <div class="course-row-country clamp2 mT10">' + localizeString("Label_Started") + ': ' + cStart + '</div>\
                        <div class="course-row-country clamp2">' + localizeString("Label_EndsOn") + ': ' + cEnd + '</div>\
                    </div>\
                </div>';
    }

    /**
     *  Displays selected Survey details
     *  @id id of the selected Survey
     */
    gotoSurveyDetail = function(id) {
        localStorage.setItem('SurveyId', id);
        slideview("survey-details.html", 'script', 'left');
    }

    /**
     *  Fetches details of a specific Survey
     */
    getSurveyDetail = function() {
        var surveyId = localStorage.SurveyId;
        var actions = {
            "surveyId": surveyId
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
        successCallback = function(data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displaySurvey(data.data);
            } else {
                var thisone = this;
                swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function() {
                        return;
                    });
            }
        }

        errorCallback = function(data) {};

        postMessageToServerWithAuth("/surveys", actions, successCallback, errorCallback);
    }


    /**
     * Displays the contents of a Survey
     * @survey Survey data received from the Server
     */
    displaySurvey = function(survey) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(survey));
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;"></p>').text(survey.content[0].title);
        var span1 = $('<span style=" font-size:12px; float:right"></span>');
        var dispDat = getDisplayableDate(survey.content[0].validUpto);
        var iElem2 = $('<i class="fa fa-clock-o"> ' + localizeString("Label_EndsOn") + ': ' + dispDat + '</i>');
        span1.append(iElem2); /*, time*/
        var legend = $('<legend id="surveylegend" style="font-size: 1.4em; padding-left:10px; word-wrap:break-word; word-break:break-word;">' + survey.content[0].description + '</legend>');
        var spanSubmitted = $('<span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="radiosubmit"></span>');
        var iSubmitted = $('<i class="form-help-icon icon">check</i>');
        spanSubmitted.append(iSubmitted);

        if (survey.content[0].image != null && survey.content[0].image != undefined && survey.content[0].image != "") {
            var img = $('<img alt="No Image" src="' + survey.imageBasePath + survey.content[0].image + '" id="image' + i + '" style="width:250px; height:250px display:none">');
            $('#surveyData').append(p1, legend, img, '</br>', spanSubmitted, span1);
        } else {
            $('#surveyData').append(p1, legend, '</br>', spanSubmitted, span1);
        }

        localStorage.setItem('SurveyQuestionCount', survey.content[0].questionList.length);
        var typeLenArr = [];
        for (var i = 0; i < survey.content[0].questionList.length; i++) {
            var legend1 = $('<legend id="surveylegend' + i + '" style="font-size:1.4em; padding-left:10px;">' + survey.content[0].questionList[i].detail + '</legend>');
            if (survey.content[0].questionList[i].image != null && survey.content[0].questionList[i].image != undefined &&
                survey.content[0].questionList[i].image != "") {
                var img = $('<img alt="No Image" src="' + survey.content[0].questionList[i].image + '" id="image' + i + '" style="width:100%;">');
                $('#surveyfieldset').append(img, legend1);
            } else {
                $('#surveyfieldset').append(legend1);
            }

            /* Store the question id, its type and number of options in it. This will later be used while submitting to iterate over and get selected responses */
            typeLenArr.push({ "qId": survey.content[0].questionList[i].master_id, "type": parseInt(survey.content[0].questionList[i].type), "optLen": survey.content[0].questionList[i].options.length });
            var divform = $('<div class="form-group form-group-label" id="vote-options-form' + i + '" style="width:90%; padding-left:15px; margin-top:-5px;"> </div>');
            if (1 == parseInt(survey.content[0].questionList[i].type)) { // only single select
                for (var j = 0; j < survey.content[0].questionList[i].options.length; j++) {
                    var label = $('<label style="display:inline; padding:5px; font-size:1.4em; align=center"></label>');
                    var radio = $('<input type="radio" name="SurveyRadio' + i + '" value="' + survey.content[0].questionList[i].options[j].master_id + '" onclick="validateSelection(' + i + ', Survey' + i + '_' + j + ', ' + parseInt(survey.content[0].questionList[i].type) + ')" id="Survey' + i + '_' + j + '"> ' + survey.content[0].questionList[i].options[j].detail + ' </input></br>');
                    label.append(radio);
                    divform.append(label);
                }
            } else if (2 == parseInt(survey.content[0].questionList[i].type)) { // Multi select
                for (var j = 0; j < survey.content[0].questionList[i].options.length; j++) {
                    var label = $('<label style="display:inline; padding:5px; font-size:1.4em; align=center"></label>');
                    var chkbox = $('<input type="checkbox" name="SurveyCheckbox' + i + '" value="' + survey.content[0].questionList[i].options[j].master_id + '" onclick="validateSelection(' + i + ', Survey' + i + '_' + j + ', ' + parseInt(survey.content[0].questionList[i].type) + ')" id="Survey' + i + '_' + j + '"> ' + survey.content[0].questionList[i].options[j].detail + ' </input></br>');
                    label.append(chkbox);
                    divform.append(label);
                }
            } else if (3 == parseInt(survey.content[0].questionList[i].type)) { // Subjective
                var txtBox = $('<textarea rows="4" style="width:100%; height:auto; text-wrap:normal;" name="SurveyDescription' + i + '" value="" onclick="validateSelection(' + i + ', Survey' + i + '_' + 0 + ', ' + parseInt(survey.content[0].questionList[i].type) + ')" id="Survey' + i + '_' + 0 + '"></textarea></br>');
                divform.append(txtBox);
            }

            var spanSuccess = $('<span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="radiosuccess' + i + '"><i class="form-help-icon icon">check</i></span>');
            var spanFailure = $('<span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="radioerror' + i + '"><i class="form-help-icon icon">error</i></span>');

            divform.append(spanSuccess, spanFailure, '</br>');
            $('#surveyfieldset').append(divform); /*legend*/
        } // End for loop here

        if (survey.content[0].questionList.length == 0) {
            var divform = $('<div class="form-group form-group-label" id="vote-options-form' + i + '" style="width:90%; padding-left:15px; margin-top:-5px;"> </div>');
            var label = $('<label style="display:inline; padding:5px; font-size:1.1em; color: red; align=center">No Questions added for this Survey</label>');

            //label.append(radio);
            divform.append(label);
            $('#surveyfieldset').append(divform); /*legend*/
        }

        localStorage.setItem('SurveyQuestionTypeLen', JSON.stringify(typeLenArr));

        if (localStorage.getItem("isLoggedIn") == "true") {
            var submit = $('<input class="btn btn-alt" type="submit" name="submit" id="submit" value="Submit Response" style="margin-left:auto;margin-right:auto;display:block;" onclick="return submitMySelection(' + survey.content[0].questionList.length + ');" />');
            $('#surveyfieldset').append('</br>', submit);

            if (true == survey.content[0].alreadySubmitted) {
                for (var i = 0; i < survey.content[0].questionList.length; i++) {
                    populateResponse(survey.content[0].questionList[i].master_id, survey.content[0].questionList[i].type, survey.content[0].response, i, survey.content[0].questionList[i].options.length);
                    if (3 != parseInt(survey.content[0].questionList[i].type)) {
                        for (var j = 0; j < survey.content[0].questionList[i].options.length; j++) {
                            document.getElementById('Survey' + i + '_' + j).disabled = true;
                        }
                    } else {
                        document.getElementById('Survey' + i + '_' + 0).disabled = true;
                    }
                }
                $("#radiosubmit").css("display", "inline");
                $("#radiosubmit").html('&nbsp;&nbsp; ' + localizeString("Survey_Label_Message"));
                document.getElementById('submit').disabled = true;
            }

            if (survey.content[0].questionList.length == 0) {
                $("#radiosubmit").css("display", "inline");
                $("#radiosubmit").html('&nbsp;&nbsp; ' + localizeString("Survey_Label_Message"));
                document.getElementById('submit').disabled = true;
            }
        }
    }


    /**
     * Removes error messages when any of the option is selected
     */
    validateSelection = function(index, id, type) {
        if (3 != type) {
            if (atLeastOneRadio(index, type))
                $("#radioerror" + index).css("display", "none");
        } else {
            if ('' != $('#Survey' + index + '_' + 0).val().trim()) {
                $("#radioerror" + index).css("display", "none");
            }
        }
    }


    /**
     * Checks if atlease one of the option is selected based on type
     * @type 1 = Radio Button, 2 = Check Box 3 = Subjective Text
     */
    atLeastOneRadio = function(id, type) {
        if (1 == type)
            return ($('input[name="SurveyRadio' + id + '"]:checked').size() > 0);
        else if (2 == type)
            return ($('input[name="SurveyCheckbox' + id + '"]:checked').size() > 0);
        else
            return ($('#Survey' + id + '_0').val().trim() != "");
    }


    /**
     * Clears the previous selection if any
     */
    clearSelection = function() {
        var typeLenArray = JSON.parse(localStorage.getItem('SurveyQuestionTypeLen'));
        for (var i = 0; i < typeLenArray.length; i++) {
            if ((1 == typeLenArray[i].type) || (2 == typeLenArray[i].type)) {
                for (var j = 0; j < typeLenArray[i].optLen; j++) {
                    document.getElementById('Survey' + i + '_' + j).checked = false;
                }
            } else {
                document.getElementById('Survey' + i + '_0').value = "";
            }
        }
    }


    /**
     * Populate already submited user response for a survey
     * @qid Question Id
     * @type 1 = Radio Button, 2 = Check Box
     * @response array of user responses
     * @index question list index
     * @lenght number of options
     */
    populateResponse = function(qid, type, response, index, length) {
        var j = 0;
        if (3 != parseInt(type)) {
            for (var i = 0; i < length; i++) {
                if (response[qid] == document.getElementById('Survey' + index + '_' + i).value) {
                    document.getElementById('Survey' + index + '_' + i).checked = true;
                }
            }
        } else {
            document.getElementById('Survey' + index + '_0').value = response[qid];
        }
    }


    /**
     * Submit users response for a survey to the server
     * @len number of survey options
     * @type 1 = Radio Button, 2 = Check Box, 3 = Subjective
     */
    submitMySelection = function(len) {
        for (var i = 0; i < len; i++) {
            var queryType = document.getElementById('Survey' + i + '_0').type;
            if (queryType == "radio") {
                type = 1;
            } else if (queryType == "checkbox") {
                type = 2;
            } else if (queryType == "textarea") {
                type = 3;
            }
            if (false == atLeastOneRadio(i, type)) {
                $("#radioerror" + i).css("display", "inline");
                if (1 == type)
                    $("#radioerror" + i).html('Please select an option.<i class="form-help-icon icon">error</i>');
                else if (2 == type)
                    $("#radioerror" + i).html('Please select atleast one option.<i class="form-help-icon icon">error</i>');
                else
                    $("#radioerror" + i).html('Please fill in your response in the text box.<i class="form-help-icon icon">error</i>');
                return;
            } else {
                $("#radioerror" + i).css("display", "none");
            }
        }

        var selectedArr = {};
        var count = 0;
        var typeLenArray = JSON.parse(localStorage.getItem('SurveyQuestionTypeLen'));
        for (var i = 0; i < typeLenArray.length; i++) {
            if ((1 == typeLenArray[i].type) || (2 == typeLenArray[i].type)) {
                var optionsArr = [];
                for (var j = 0; j < typeLenArray[i].optLen; j++) {
                    if (document.getElementById('Survey' + i + '_' + j).checked) {
                        optionsArr.push(document.getElementById('Survey' + i + '_' + j).value);
                    }
                }
                if (optionsArr.length > 0) {
                    selectedArr[typeLenArray[i].qId] = optionsArr;
                    count++;
                }
            } else {
                selectedArr[typeLenArray[i].qId] = document.getElementById('Survey' + i + '_0').value;
                count++;
            }
        }

        if (enableDebugMode)
            alert("Selected options = " + JSON.stringify(selectedArr) + ", count = " + count);

        if (count > 0) {
            var actions = {
                "surveyId": localStorage.SurveyId,
                "response": selectedArr
            };

            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

            successCallback = function(data) {
                window.plugins.spinnerDialog.hide();
                if (data.status == "success") {
                    clearSelection();
                    swal({
                            title: localizeString("Alert_Title_ThankYou"),
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: false,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            slideview("surveys.html", 'script', 'left');
                        });
                } else {
                    var thisone = this;
                    swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function() {
                            return;
                        });
                }
            };
            errorCallback = function(data) {

            };
            postMessageToServerWithAuth("/surveysubmit", actions, successCallback, errorCallback);
        } else {
            swal({
                    title: localizeString("Alert_Title_NoResponse"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function() {
                    return;
                });
        }
    }
}());