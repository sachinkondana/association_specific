(function () {
    var enableDebugMode = 0;
    var page_number = 1;
    "use strict";
    /**
     * Fetch the list of restaurants available.
     */
    getRestaurantList = function (category_id) {
        var actions = {
            "category_id" : category_id,
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length <= 0) {
                    $('.row').html('<p style="text-align:center"> ' + localizeString("RestaurentList_Label_NoData") + '</p>');
                } else {
                    displayRestaurantList(data.data);
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/facilities", actions, successCallback, errorCallback);
    }



    displayRestaurantList = function (restaurantList) {
        if (enableDebugMode)
            alert("Restaurant List = " + JSON.stringify(restaurantList));
        var isUserLoggedIn = false;
        var restaurantDiv = null;
        if (localStorage.getItem("isLoggedIn") != "true")
        {
            isUserLoggedIn = false;
        }
        else
        {
            isUserLoggedIn = true;
        }

        localStorage.setItem("restaurantList", JSON.stringify(restaurantList));

        for (var i = 0; i < restaurantList.content.length; i++)
        {
            restaurantDiv = null;
            if ("1" == restaurantList.content[i].isPublic)
            {
                restaurantDiv = generateRestaurantDiv(restaurantList.content[i], restaurantList.imageBasePath, i);
            }
            else // facility is private
            {
                if (isUserLoggedIn)
                {
                    restaurantDiv = generateRestaurantDiv(restaurantList.content[i], restaurantList.imageBasePath, i);
                }
            }
            if (restaurantDiv != null)
                $('.row').append(restaurantDiv);
        }
        lazyLoadListImages(page_number, restaurantList.content.length);
    }

    generateRestaurantDiv = function (restaurant, imageBasePath, index) { //index for description hack
        var category = restaurant.category_id;
        var div1 = $('<div class="col-lg-4 col-sm-12" onclick="goToRestaurantMenuCard(' + category + ', ' + restaurant.id + ',' + index + ')"></div>');
        var div2 = $('<div class="card-01"></div>');
        var faciimage;

        if (restaurant.images != null && ((restaurant.images.image1 != null && restaurant.images.image1 != undefined
            && restaurant.images.image1 != "") || (restaurant.images.image2 != "" && restaurant.images.image2 != null
            && restaurant.images.image2 != undefined) || (restaurant.images.image3 != ""
            && restaurant.images.image3 != null && restaurant.images.image3 != undefined))) {
            if (restaurant.images.image1 != null && restaurant.images.image1 != undefined && restaurant.images.image1 != "") {
                faciimage = imageBasePath + restaurant.images.image1;
            } else {
                if (restaurant.images.image2 != null && restaurant.images.image2 != undefined && restaurant.images.image2 != "") {
                    faciimage = imageBasePath + restaurant.images.image2;
                } else {
                    if (restaurant.images.image3 != null && restaurant.images.image3 != undefined && restaurant.images.image3 != "") {
                        faciimage = imageBasePath + restaurant.images.image3;
                    }
                }
            }
        } else {
            faciimage = "images/default-listing.png";
        }
        var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
        var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + index) + '" src="images/default-listing.png" data-src="' + faciimage + '"></div>');
        aside.append(imgDiv);
        var div3 = $('<div class="card-main card-width"></div>');
        var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
        var p1 = $('<p class="card-heading card-heading-flow">' + restaurant.name + '</p>');
        var shortDescription = restaurant.description;
        if (shortDescription.length > 30)
            shortDescription = shortDescription.substring(0, 27) + "..."
        var p2 = $('<p>' + shortDescription + '</p>');
        div4.append(p1, p2);
        div3.append(div4);
        div2.append(aside, div3);
        div1.append(div2);
        return div1;
    }


    goToRestaurantMenuCard = function (categoryId, restaurantId, index) {
        localStorage.setItem("restaurantId", restaurantId);
        localStorage.setItem("restaurantIndex", index);
        slideview('food-beverages-menu.html', 'script', 'left');
    }


    getMenuCard = function () {
        var actions = {
            "restId" : localStorage.getItem("restaurantId"),
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayFoodAndBeverageMenu(data.data);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    allowOutsideClick: false,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/menucardByRestaurant", actions, successCallback, errorCallback);
    }


    displayFoodAndBeverageMenu = function (menuDetails) {
        //if there are food items in the list loop through them, and identify the type.
        //based on the type of the food, move them to either the
        //1. Starters Section
        //2. Main Course section
        //3. Beverages section
        //4. Deserts section

        if (enableDebugMode)
            alert("menuDetails = " + JSON.stringify(menuDetails));

        var restaurantDetails = JSON.parse(localStorage.getItem("restaurantList"));
        var index = localStorage.getItem("restaurantIndex");

        var numOfImages = 0;
        if (null != restaurantDetails.content[index].images)
        {
            if (null != restaurantDetails.content[index].images.image1 &&
                undefined != restaurantDetails.content[index].images.image1
                && "" != restaurantDetails.content[index].images.image1) {
                $('#topbanner').attr({
                    'src': restaurantDetails.imageBasePath + restaurantDetails.content[index].images.image1
                });
                $("#topbanner").css("display", "block");
                numOfImages++;
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }

            if (null != restaurantDetails.content[index].images.image2
                && undefined != restaurantDetails.content[index].images.image2
                && "" != restaurantDetails.content[index].images.image2) {
                $('#topbanner2').attr({
                    'src': restaurantDetails.imageBasePath + restaurantDetails.content[index].images.image2
                });
                numOfImages++;
            } else {
                $("#topbanner2").removeClass("mySlides");
            }

            if (null != restaurantDetails.content[index].images.image3
                && undefined != restaurantDetails.content[index].images.image3
                && "" != restaurantDetails.content[index].images.image3) {
                $('#topbanner3').attr({
                    'src': restaurantDetails.imageBasePath + restaurantDetails.content[index].images.image3
                });
                numOfImages++;
            } else {
                $("#topbanner3").removeClass("mySlides");
            }

            if (numOfImages > 1)
                carousel();
        } else {
            $("#topbanner").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(restaurantDetails.content[index].name);
        var p2 = $('<p class="wysiwyg-img" style="word-wrap:break-word;">' + restaurantDetails.content[index].description + '</p>');
        td1.append(p1, p2);
        tr1.append(td1);
        $("#description_body").append(tr1);

        var menuCards = menuDetails.content;
        var menuCardBasePath = menuDetails.imageBase;
        if (0 < menuCards.length) {
            var tr2 = $('<tr></tr>');
            var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
            tr2.append(td2);
            $("#description_body").append(tr2);

            var tr3 = $('<tr></tr>');
            var td3 = $('<td colspan="2" class="text_p"></td>');
            var p3 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px; text-align:center;" id="menu-title">'+localizeString("FoodnBeverages_Label_RestaurantMenu")+'</p>');
            td3.append(p3);
            tr3.append(td3);
            $("#description_body").append(tr3);
        }

        for (var i = 0; i < menuCards.length; i++) {
            var tr4 = $('<tr></tr>');
            var td4 = $('<td width="100%" colspan="2"></td>');
            var div1 = $('<div class="col-lg-4 col-sm-6"></div>');
            var div2 = $('<div class="card-01" style="margin-bottom:2px!important; margin-top:0px!important;"></div>');
            var div4 = $('<div class="card-main"></div>');
            var div5 = $('<div class="card-inner-01 wysiwyg-img"></div>');
            var p4 = $('<p class="card-heading" style="max-width:190px; text-overflow:ellipsis!important; white-space:nowrap!important; overflow:hidden!important;"></p>').text(menuCards[i].name);
            if (menuCards[i].menu !=  "" && menuCards[i].menu != null)
                var p5 = $('<p class="wysiwyg-img" style="word-wrap:break-word; font-size:10px!important;">' + menuCards[i].description + '<div class="btn btn-alt" style="float:right;" onclick="openPDF(\'' + menuCardBasePath + menuCards[i].menu + '\');"><i class="fa fa-download fa-2x" style="font-size:15px!important; float:right;"> Menu</i></div></p>');
            else
                var p5 = $('<p style="font-size:10px!important;">' + menuCards[i].description + '</p>');
            div5.append(p4, p5);
            div4.append(div5);
            div2.append(div4);
            div1.append(div2);
            td4.append(div1);
            tr4.append(td4);
            $("#description_body").append(tr4);
        }
    }

    openPDF = function (pdfUrl) {
        window.open("https://docs.google.com/viewer?url=" + pdfUrl, '_blank', 'location=no');
    }
}());