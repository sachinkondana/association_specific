var currentVersionString = "0.0.1";
var currentMajorVersionNumber = "0";
var currentMinorVersionNumber = "0";

if (/Android/i.test(navigator.userAgent) || /iPhone|iPad|iPod/i.test(navigator.userAgent)) {
    if (localStorage.getItem("appVersion") == null || localStorage.getItem("appVersion") == "" ||
        localStorage.getItem("appVersion") == undefined) {
        var un = localStorage.getItem("loginUName");
        var pw = localStorage.getItem("loginUPwd");
        window.localStorage.clear();
        localStorage.setItem("appVersion", currentVersionString);
        if (un != null && un != undefined && un != "")
            localStorage.setItem("loginUName", un);
        if (pw != null && pw != undefined && pw != "")
            localStorage.setItem("loginUPwd", pw);
    } else if ((localStorage.getItem("isLoggedIn") == null) ||
        (localStorage.getItem("isLoggedIn") == undefined) ||
        (localStorage.getItem("isLoggedIn") == "false")) {
        if ((localStorage.getItem("appVersion").split(".")[0] != currentMajorVersionNumber) ||
            (localStorage.getItem("appVersion").split(".")[1] != currentMinorVersionNumber)) {
            var devid = localStorage.getItem("DEVICE_ID");
            var un = localStorage.getItem("loginUName");
            var pw = localStorage.getItem("loginUPwd");
            window.localStorage.clear();
            localStorage.setItem("appVersion", currentVersionString);
            if (devid != null && devid != undefined && devid != "")
                localStorage.setItem("DEVICE_ID", devid);
            if (un != null && un != undefined && un != "")
                localStorage.setItem("loginUName", un);
            if (pw != null && pw != undefined && pw != "")
                localStorage.setItem("loginUPwd", pw);
        }
        localStorage.setItem("appVersion", currentVersionString);
    } else if (localStorage.getItem("isLoggedIn") == "true") {
        if ((localStorage.getItem("appVersion").split(".")[0] != currentMajorVersionNumber) ||
            (localStorage.getItem("appVersion").split(".")[1] != currentMinorVersionNumber)) {
            swal({
                    title: "Application Updated!",
                    text: "You need to login again to continue!",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonText: "Confirm",
                    closeOnConfirm: false,
                    allowOutsideClick: false,
                },
                function() {
                    var devid = localStorage.getItem("DEVICE_ID");
                    var un = localStorage.getItem("loginUName");
                    var pw = localStorage.getItem("loginUPwd");
                    window.localStorage.clear();
                    localStorage.setItem("appVersion", currentVersionString);
                    if (devid != null && devid != undefined && devid != "")
                        localStorage.setItem("DEVICE_ID", devid);
                    if (un != null && un != undefined && un != "")
                        localStorage.setItem("loginUName", un);
                    if (pw != null && pw != undefined && pw != "")
                        localStorage.setItem("loginUPwd", pw);
                    window.location.href = "index.html";
                    return;
                });
        } else {
            localStorage.setItem("appVersion", currentVersionString);
        }
    } else {
        console.log("loggedIn: " + localStorage.getItem("isLoggedIn"));
        console.log("App version: " + localStorage.getItem("appVersion"));
    }
}

localStorage.setItem("SERVER_URL", "https://demo.onegolfnet.com/mobicom_api_v5");
localStorage.setItem("SERVER_URL_PAYMENT", "https://demo.onegolfnet.com/9de4a97425678c5b1288aa70c1669a64/payment/");
localStorage.setItem("SERVER_URL_BILL_PAYMENT", "https://demo.onegolfnet.com/9de4a97425678c5b1288aa70c1669a64/bill_pay/index.php");
localStorage.setItem("SERVER_URL_EULA", "https://demo.onegolfnet.com/privacy"); // set in regster-eula.html
localStorage.setItem("COMMUNE_ID", "1");
localStorage.setItem("local_limit", 10);

// The backend api version, this app is compatiable with
localStorage.setItem("BackendCompatiabilityVersion", "6.0.0");

//Localization
localStorage.setItem("DEAFULT_LOCALE", "en");
var supportedLanguages = ["en-us", "en", "id", "ms", "th", "zh-cn", "zh-tw", "hi", "ja-jp", "ko-kr", "vi-vn"];

localStorage.setItem("SUPPORTED_LOCALE", JSON.stringify(supportedLanguages));

if (/Android/i.test(navigator.userAgent)) {
    localStorage.setItem("DEVICE_OS", "2"); //1 iOS - 2 Android
}
if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
    localStorage.setItem("DEVICE_OS", "1"); // 1 for ios 2 for android
}

//Time correction in Forums to handle the Wordpress UTC time issue
localStorage.setItem("utcTimeCorrectionInSec", 19800);

localStorage.setItem("ClubDispFormat", "1");
localStorage.setItem("ClubMonFormat", "1");
localStorage.setItem("ClubYearFormat", "2");
localStorage.setItem("ClubTimeFormat", "1")

function stringGen(len) {
    var text = " ";
    var charset = "abcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < len; i++) {
        text += charset.charAt(Math.floor(Math.random() * charset.length));
    }
    return text;
}
if (localStorage.getItem("APP_ID") == null || localStorage.getItem("APP_ID").trim() == "" || localStorage.getItem("APP_ID") == undefined) {
    localStorage.setItem("APP_ID", stringGen(32));
}

if (localStorage.getItem("memberid") == null || localStorage.getItem("memberid").trim() == "" || localStorage.getItem("memberid") == undefined || localStorage.getItem("memberverified") == null || localStorage.getItem("memberverified").trim() == "" || localStorage.getItem("memberverified") == undefined) {
    localStorage.setItem("memberid", "");
    localStorage.setItem("memberverified", "no");
}


var globalMenuMappingArray = { '1.1': 'About_Us', '1.1.1': 'History', '1.2': 'Club_Rules', '1.2.1': 'Dress_Code', '1.3': 'Message_President', '1.4': 'Affiliations', '1.5': 'Contact_Us', '1.6': 'Membership_Options', '1.6.1': 'Enrollment Criteria', '1.7': 'Photo', '1.8': 'Weather', '2.1': 'Management', '2.2': 'Present_Office', '2.3': 'Past_Office', '2.4': 'Club_Staff', '3.1.1': 'Fitness_Description', '3.1.2': 'Fitness_Pictures', '3.1.3': 'Fitness_Request', '3.2.1': 'Sports_Description', '3.2.2': 'Sports_Pictures', '3.2.3': 'Sports_Request', '3.2.4': 'Live_Score', '3.3.1': 'Health_Description', '3.3.2': 'Health_Pictures', '3.3.3': 'Health_Request', '3.4.1': 'Salon_Description', '3.4.2': 'Salon_Pictures', '3.4.3': 'Salon_Request', '3.5.1': 'Other_Description', '3.5.2': 'Other_Pictures', '3.5.3': 'Other_Request', '4.1': 'Food_Beverage', '4.2': 'FB_Ordering', '5.1': 'Rooms_Description', '5.2': 'Rooms_Photos', '5.3.1': 'Room_Request', '5.3.2': 'Room_Booking', '6.1': 'Banquet_Conference', '6.2': 'Banquet_Pictures', '6.3': 'Banquet_Request', '7.1': 'Forums', '7.2.1': 'Blogs', '7.2.2': 'Blogs_Comments_To_Admin', '7.2.3': 'Blogs_Comments', '7.3': 'Email_Notification', '7.4': 'Push_Notification', '7.5': 'Media', '7.6': 'News', '7.7': 'SMS_Integration', '8.1': 'Events_Description', '8.2': 'Events_Pictures', '8.3': 'Event_Request', '9.1': 'Member_Account', '9.2': 'Review_Account', '9.3': 'Query_Specific', '9.4': 'Payment_Gateway', '9.5': 'Live_Accounts', '10': 'Sponsorship', '11': 'Offers', '12.1': 'Classifieds', '12.2': 'Classifieds_Pictures', '13.1': 'Online_Polling', '13.2': 'Online_Survey', '14.1': 'Member_Directory', '14.2': 'Invite', '15.1': 'Tournaments', '15.2': 'Online member registration', '15.3': 'Tournament Results/ Leaderboard', '1.1:Golf': 'Golf_Description', '1.2:Golf': 'Golf_Handicap', '1.3:Golf': 'Golf_Tee_Time', '1.4:Golf': 'Golf_Pictures', '1.5.1:Golf': 'Golf_Tournament', '1.5.2:Golf': 'Golf_Tournament_Registration', '1.6:Golf': 'Golf_Live_Score', '1.7:Golf': 'Golf_Leaderboard', '1.8:Golf': 'Golf_Coaches', '1.9.1:Golf': 'Golf_Courses', '1.9.2:Golf': 'Golf_Courses_Req' };


var globalMenuIdArray = ['myacnt', '0.0', '1.0', '1.1', '1.1.1', '1.2', '1.2.1', '1.3', '1.4', '1.5', '1.6', '1.6.1', '1.7', '1.8', '2.0', '2.1', '2.2', '2.3', '2.4', '3.0', '3.1.0', '3.1.1', '3.1.2', '3.1.3', '3.2.0', '3.2.1', '3.2.2', '3.2.3', '3.2.4', '3.3.0', '3.3.1', '3.3.2', '3.3.3', '3.4.0', '3.4.1', '3.4.2', '3.4.3', '3.5.0', '3.5.1', '3.5.2', '3.5.3', '3.6.0', '3.6.1', '3.6.2', '3.6.3', '3.8', '4.0', '4.1', '4.2', '5.0', '5.1', '5.2', '5.3.1', '5.3.2', '6.0', '6.1', '6.2', '6.3', '7.0', '7.1', '7.2.0', '7.2.1', '7.2.2', '7.2.3', '7.3', '7.4', '7.5', '7.6', '7.7', '8.0', '8.1', '8.2', '8.3', '8.4', '9.0', '9.1', '9.2', '9.3', '9.4', '9.5', '10', '11', '12.0', '12.1', '12.2', '13.0', '13.1', '13.2', '14.0', '14.1', '14.2', '15.0', '15.1', '15.2', '15.3', '1.1:Golf', '1.2:Golf', '1.3:Golf', '1.4:Golf', '1.5.1:Golf', '1.5.2:Golf', '1.6:Golf', '1.7:Golf', '1.8:Golf', '1.9.1:Golf', '1.9.2:Golf', 'myacntlogout', 'myacntsetting'];
localStorage.setItem("GLOBALMENUIDARRAY", JSON.stringify(globalMenuIdArray));

var menuListing = '\
<div class="menu-scroll">                                                                                                                   \
    <div class="menu-content">                                                                                                              \
        <div class="menu-top fixed">                                                                                                        \
            <div class="menu-top-img"></div>                                                     \
            <div class="menu-top-info">                                                                                                     \
                <a class="menu-top-user" style="float:left;" href="javascript:void(0)">                                                     \
                    <span class="avatar pull-left"><img alt="" src="images/avatar-001.jpg" id="navphoto" onclick="javascript:editUserDP();"></span> \
                </a>                                                                                                                        \
                <div class="menu-label menuHeight" style="float:left; line-height:18px; margin-top: 7px; width:55%;" onclick="javascript:editUsrPrfl();">                                  \
                   <div class="mo1">\
                   <div class="mo2">\
                   <div class="mo3">\
                        <strong id="navfullname">TeamGolf</strong><br>                                      \
                        <span style="font-size:12px" id="navdisplayname">v 4.1.0</span>                                                   \
                    </div>\
                    </div>\
                    </div>\
                    </div>                                                                                                                      \
            </div>                                                                                                                          \
        </div>                                                                                                                              \
        <ul class="nav">                                                                                                                    \
            <li id="home">                                                                                                                  \
                <a class="waves-attach" id="Menu_Button_Home" name ="LocalizeMenuElement" href="index.html" onclick="localStorage.setItem(\'homepressed\', \'1\');slideview(this,\'link\',\'left\');return false;">                                                                    \
                    <i class="fa fa-home fa-fw"></i>                                                                                        \
                </a>                                                                                                                        \
            </li>                                                                                                                           \
            <li id="myaccount" style="display:none" menuId="myacnt">                                                                    \
                <a class="waves-attach" id="Menu_Button_MyAccount" name ="LocalizeMenuElement" href="my-account.html" onclick="slideview(this,\'link\',\'left\');return false;">                                                                                                                    \
                    <i class="fa fa-user-plus fa-fw"></i>                                                                                   \
                </a>                                                                                                                        \
                <span id="accountscount" class="menu__badge" style="display:none;"></span>                                                  \
            </li>                                                                                                                           \
            <li id="about" class="menu-with-child" style="display:@1.0@;" menuId="1.0">                                                     \
                <a class="waves-attach" id="Menu_Button_ClubInformation" name="LocalizeMenuElement" href="javascript:void();" data-target="#about-us" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-user fa-fw"></i>                                                                                        \
                </a>                                                                                                                        \
                <span id="aboutcount" class="group__menu__badge" style="display:none;"></span>                                              \
                <span class="menu-collapse-toggle collapsed" data-target="#about-us" data-toggle="collapse">                                \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="about-us">                                                                           \
                    <li id="about-the-club" style="display:@1.1@;" menuId="1.1">                                                            \
                        <a class="waves-attach" id="menunameid_1.1" href="about-us.html" onclick="slideview(this,\'link\',\'left\');return false;">About Us</a>                                                                                                              \
                        <span id="2807" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="affiliation-clubs" style="display:@1.4@;" menuId="1.4">                                                         \
                        <a class="waves-attach" id="menunameid_1.4" href="affiliation-clubs.html" onclick="slideview(this,\'link\',\'left\');return false;">Affiliated Clubs</a>                                                                                                           \
                        <span id="2801" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-club-rules" style="display:@1.2@;" menuId="1.2">                                                          \
                        <a class="waves-attach" id="menunameid_1.2" href="club-rules.html" onclick="slideview(this,\'link\',\'left\');return false;">Club Rules                                                                                                               \
                        </a>                                                                                                                \
                        <span id="2802" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-dress-code" style="display:@1.2.1@;" menuId="1.2.1">                                                      \
                        <a class="waves-attach" id="menunameid_1.2.1" href="dress-code.html" onclick="slideview(this,\'link\',\'left\');return false;">Dress Code                                                                                                                \
                        </a>                                                                                                                \
                        <span id="2803" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-membership-criteria" style="display:@1.6.1@;" menuId="1.6.1">                                             \
                        <a class="waves-attach" id="menunameid_1.6.1" href="membership-criteria.html" onclick="slideview(this,\'link\',\'left\');return false;">Enrollment Criteria                                                                                                            \
                        </a>                                                                                                                \
                        <span id="2805" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-gallery" style="display:@1.7@;" menuId="1.7">                                                             \
                        <a class="waves-attach" id="menunameid_1.7" href="gallery.html" onclick="slideview(this,\'link\',\'left\');return false;">Gallery                                                                                                     \
                        </a>                                                                                                                \
                        <span id="2806" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-history" style="display:@1.1.1@;" menuId="1.1.1">                                                         \
                        <a class="waves-attach" id="menunameid_1.1.1" href="history.html" onclick="slideview(this,\'link\',\'left\');return false;">History                                                                                                     \
                        </a>                                                                                                                \
                        <span id="2808" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-membership-options" style="display:@1.6@;" menuId="1.6">                                                  \
                        <a class="waves-attach" id="menunameid_1.6" href="membership-options.html" onclick="slideview(this,\'link\',\'left\');return false;">Membership Categories                                                                                                          \
                        </a>                                                                                                                \
                        <span id="2804" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-president-message" style="display:@1.3@;" menuId="1.3">                                                   \
                        <a class="waves-attach" id="menunameid_1.3" href="message-from-top.html" onclick="slideview(this,\'link\',\'left\');return false;">Message from the President                                                                                                           \
                        </a>                                                                                                                \
                        <span id="2810" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="about-weather" style="display:@1.8@;" menuId="1.8">                                                             \
                        <a class="waves-attach" id="menunameid_1.8" href="weather.html" onclick="slideview(this,\'link\',\'left\');return false;">Weather                                                                                                     \
                        </a>                                                                                                                \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="officeb" class="menu-with-child" style="display:@2.0@;" menuId="2.0">                                                   \
                <a class="waves-attach" id="Menu_Button_ClubManagement" name ="LocalizeMenuElement" href="javascript:void()"; data-target="#beares" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-users fa-fw"></i>                                                                                       \
                </a>                                                                                                                        \
                <span id="clubmgmtcount" class="group__menu__badge" style="display:none;"></span>                                           \
                <span class="menu-collapse-toggle collapsed" data-target="#beares" data-toggle="collapse">                                  \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="beares">                                                                             \
                    <li id="officeb-club-staff" style="display:@2.4@;" menuId="2.4">                                                        \
                        <a class="waves-attach" id="menunameid_2.4" href="club-staff.html" onclick="slideview(this,\'link\',\'left\');return false;">Club Staff                                                                                                               \
                        </a>                                                                                                                \
                        <span id="2904" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="officeb-mgmt" style="display:@2.1@;" menuId="2.1">                                                              \
                        <a class="waves-attach" id="menunameid_2.1" href="management.html" onclick="slideview(this,\'link\',\'left\');return false;">Management                                                                                                  \
                        </a>                                                                                                                \
                        <span id="2901" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="officeb-past-comm" style="display:@2.3@;" menuId="2.3">                                                         \
                        <a class="waves-attach" id="menunameid_2.3" href="past-committee.html" onclick="slideview(this,\'link\',\'left\');return false;">Past Office Bearers                                                                                                             \
                        </a>                                                                                                                \
                        <span id="2903" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="officeb-present-comm" style="display:@2.2@;" menuId="2.2">                                                      \
                        <a class="waves-attach" id="menunameid_2.2" href="present-committee.html" onclick="slideview(this,\'link\',\'left\');return false;">Present Office Bearers                                                                                                             \
                        </a>                                                                                                                \
                        <span id="2902" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="classifieds" class="menu-with-child" style="display:@12.0@;" menuId="12.0">                                             \
                <a id="Menu_Button_Classifides" name="LocalizeMenuElement" class="waves-attach" href="javascript:void();" data-target="#classified-menus" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-buysellads fa-fw"></i>                                                                                  \
                </a>                                                                                                                        \
                <span id="classifiedcount" class="group__menu__badge" style="display:none;"></span>                                         \
                <span class="menu-collapse-toggle collapsed" data-target="#classified-menus" data-toggle="collapse">                        \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="classified-menus">                                                                   \
                    <li id="classified-list" style="display:@12.1@;" menuId="12.1">                                                         \
                        <a id="menunameid_12.1" class="waves-attach" href="classified-list.html" onclick="slideview(this,\'link\',\'left\');return false;">All Promotions                                                                                                          \
                        </a>                                                                                                                \
                        <span id="allclasicount" class="menu__badge" style="display:none;"></span>                                          \
                    </li>                                                                                                                   \
                    <li id="classified-my" style="display:@12.3@;" menuId="12.3">                                                           \
                        <a id="Menu_Button_MyClassifides" name="LocalizeElement" class="waves-attach" href="classified-my.html" onclick="slideview(this,\'link\',\'left\');return false;">My Classified                                                                                               \
                        </a>                                                                                                                \
                        <span id="myclasicount" class="menu__badge" style="display:none;"></span>                                           \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="connect" class="menu-with-child" style="display:@7.0@;" menuId="7.0">                                                   \
                <a class="waves-attach" id="Menu_Button_Connect" name ="LocalizeMenuElement" href="javascript:void();" data-target="#connect-user" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-connectdevelop fa-fw"></i>                                                                              \
                </a>                                                                                                                        \
                <span id="connectcount" class="group__menu__badge" style="display:none;"></span>                                            \
                <span class="menu-collapse-toggle collapsed" data-target="#connect-user" data-toggle="collapse">                            \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="connect-user">                                                                       \
                    <li id="connect-blogs" style="display:@7.2.0@;" menuId="7.2.0">                                                         \
                        <a class="waves-attach" id="menunameid_7.2.0" href="blogs.html" onclick="slideview(this,\'link\',\'left\');return false;">Blogs</a>                                                                                                   \
                        <span id="blogcount" class="menu__badge" style="display:none;"></span>                                              \
                    </li>                                                                                                                   \
                    <li id="connect-forums" style="display:none;" menuId="7.1">                                                             \
                        <a class="waves-attach" id="menunameid_7.1" href="forums.html" onclick="slideview(this,\'link\',\'left\');return false;">Forums</a>                                                                                                  \
                    </li>                                                                                                                   \
                    <li id="connect-find-people" style="display:@14.1@;" menuId="14.1">                                                     \
                        <a class="waves-attach" id="menunameid_14.1" href="find-people.html" onclick="slideview(this,\'link\',\'left\');return false;">Member Directory                                                                                                           \
                        </a>                                                                                                                \
                        <span id="dircount" class="menu__badge" style="display:none;"></span>                                               \
                    </li>                                                                                                                   \
                    <li id="connect-connect-people" style="display:@14.2@;" menuId="14.2">                                                  \
                        <a class="waves-attach" id="menunameid_14.2" href="connect-people.html"                                             \
                        onclick="slideview(this,\'link\',\'left\');return false;">Connect With Members                                      \
                        </a>                                                                                                                \
                        <span id="chatcount" class="menu__badge" style="display:none;"></span>                                              \
                    </li>                                                                                                                   \
                    <li id="connect-media" style="display:@7.5@;" menuId="7.5">                                                             \
                        <a class="waves-attach" id="menunameid_7.5" href="media.html" onclick="slideview(this,\'link\',\'left\');return false;">Media                                                                                                       \
                        </a>                                                                                                                \
                        <span id="1200" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="connect-news" style="display:@7.6@;" menuId="7.6">                                                              \
                        <a class="waves-attach" id="menunameid_7.6" href="news.html" onclick="slideview(this,\'link\',\'left\');return false;">News                                                                                                        \
                        </a>                                                                                                                \
                        <span id="newscount" class="menu__badge" style="display:none;"></span>                                              \
                    </li>                                                                                                                   \
                    <li id="connect-notification" style="display:@7.4@;" menuId="7.4">                                                      \
                        <a class="waves-attach" id="menunameid_7.4" href="notification.html" onclick="slideview(this,\'link\',\'left\');return false;"> Notifications                                                                                                       \
                        </a>                                                                                                                \
                        <span id="1400" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="about-contact" style="display:@1.5@;" menuId="1.5">                                                                     \
                <a class="waves-attach" id="menu_name_1.5" href="contact.html" onclick="slideview(this,\'link\',\'left\');return false;">   \
                    <i class="fa fa-tty fa-fw"></i> <span id="menunameid_1.5">Contact Us</span>                                             \
                </a>                                                                                                                        \
                <span id="2809" class="menu__badge" style="display:none;"></span>                                                           \
            </li>                                                                                                                           \
            <li id="events" class="menu-with-child" style="display:@8.0@;" menuId="8.0">                                                    \
                <a class="waves-attach" id="Menu_Button_Events" name ="LocalizeMenuElement" href="javascript:void();" data-target="#events-details" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-calendar fa-fw"></i>                                                                                    \
                </a>                                                                                                                        \
                <span id="eventscount" class="group__menu__badge" style="display:none;"></span>                                             \
                <span class="menu-collapse-toggle collapsed" data-target="#events-details" data-toggle="collapse">                          \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="events-details">                                                                     \
                    <li id="events-list" style="display:@8.1@;" menuId="8.1">                                                               \
                        <a class="waves-attach" id="menunameid_8.1" href="events.html" onclick="slideview(this,\'link\',\'left\');return false;">All Events                                                                                                              \
                        </a>                                                                                                                \
                        <span id="allevntcount" class="menu__badge" style="display:none;"></span>                                           \
                    </li>                                                                                                                   \
                    <li id="events-booking" style="display:@8.3@;" menuId="8.3">                                                            \
                        <a class="waves-attach" id="menunameid_8.3" href="events-booking-list.html" onclick="slideview(this,\'link\',\'left\');return false;">My Event Bookings                                                                                                            \
                        </a>                                                                                                                \
                        <span id="myevntcount" class="menu__badge" style="display:none;"></span>                                            \
                    </li>                                                                                                                   \
                    <li id="events-calendar" style="display:@8.4@;" menuId="8.4">                                                           \
                        <a class="waves-attach" id="menunameid_8.4" href="full-calendar.html" onclick="slideview(this,\'link\',\'left\');return false;">Calendar                                                                                                    \
                        </a>                                                                                                                \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="facilities" class="menu-with-child" style="display:@3.0@;" menuId="3.0">                                                \
                <a class="waves-attach" id="Menu_Button_Facilities" name ="LocalizeMenuElement" href="javascript:void();" data-target="#facilities-full" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-building fa-fw"></i>                                                                                    \
                </a>                                                                                                                        \
                <span id="facilitycount" class="group__menu__badge" style="display:none;"></span>                                           \
                <span class="menu-collapse-toggle collapsed" data-target="#facilities-full" data-toggle="collapse">                         \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="facilities-full">                                                                    \
                    <li id="facility-banquet" style="display:@6.0@;" menuId="6.0">                                                          \
                        <a class="waves-attach" id="menunameid_6.0" href="facility-banquate-conference.html" onclick="slideview(this,\'link\',\'left\');return false;">Banquet & Conference                                                                                                          \
                        </a>                                                                                                                \
                        <span id="banquetcount" class="menu__badge" style="display:none;"></span>                                           \
                    </li>                                                                                                                   \
                    <li id="facility-fitness" style="display:@3.1.0@;" menuId="3.1.0">                                                      \
                        <a class="waves-attach" id="menunameid_3.1.0" href="facility-fitness.html" onclick="slideview(this,\'link\',\'left\');return false;">Fitness                                                                                                     \
                        </a>                                                                                                                \
                        <span id="fitnesscount" class="menu__badge" style="display:none;"></span>                                           \
                    </li>                                                                                                                   \
                    <li id="facility-health" style="display:@3.3.0@;" menuId="3.3.0">                                                       \
                        <a class="waves-attach" id="menunameid_3.3.0" href="facility-health-spa.html" onclick="slideview(this,\'link\',\'left\');return false;">Health & Spa                                                                                                                 \
                        </a>                                                                                                                \
                        <span id="hnspacount" class="menu__badge" style="display:none;"></span>                                             \
                    </li>                                                                                                                   \
                    <li id="facility-recreational" style="display:@3.6.0@;" menuId="3.6.0">                                                 \
                        <a class="waves-attach" id="menunameid_3.6.0" href="facility-recreational.html" onclick="slideview(this,\'link\',\'left\');return false;">Recreation                                                                                                  \
                        </a>                                                                                                                \
                        <span id="recreationalcount" class="menu__badge" style="display:none;"></span>                                      \
                    </li>                                                                                                                   \
                    <li id="facility-room" style="display:@5.0@;" menuId="5.0">                                                             \
                        <a class="waves-attach" id="menunameid_5.0" href="facility-room.html" onclick="slideview(this,\'link\',\'left\');return false;" >Rooms                                                                                                              \
                        </a>                                                                                                                \
                        <span id="roomcount" class="menu__badge" style="display:none;"></span>                                              \
                    </li>                                                                                                                   \
                    <li id="facility-salon" style="display:@3.4.0@;" menuId="3.4.0">                                                        \
                        <a class="waves-attach" id="menunameid_3.4.0" href="facility-saloon.html" onclick="slideview(this,\'link\',\'left\');return false;" >Salon                                                                                                              \
                        </a>                                                                                                                \
                        <span id="saloncount" class="menu__badge" style="display:none;"></span>                                             \
                    </li>                                                                                                                   \
                    <li id="facility-sports" style="display:@3.2.0@;" menuId="3.2.0">                                                       \
                        <a class="waves-attach" id="menunameid_3.2.0" href="facility-sports.html" onclick="slideview(this,\'link\',\'left\');return false;">Sports                                                                                                      \
                        </a>                                                                                                                \
                        <span id="sportcount" class="menu__badge" style="display:none;"></span>                                             \
                    </li>                                                                                                                   \
                    <li id="facility-other" style="display:@3.5.0@;" menuId="3.5.0">                                                        \
                        <a class="waves-attach" id="menunameid_3.5.0" href="facility-other.html" onclick="slideview(this,\'link\',\'left\');return false;" >Other                                                                                                              \
                        </a>                                                                                                                \
                        <span id="othercount" class="menu__badge" style="display:none;"></span>                                             \
                    </li>                                                                                                                   \
                    <li id="facility-booking" style="display:@3.8@;" menuId="3.8">                                                          \
                        <a class="waves-attach" id="Menu_Button_MyFacilityBookings" name ="LocalizeElement" href="facilities-booking-list.html" onclick="slideview(this,\'link\',\'left\');return false;">My Facility Bookings                                      \
                        </a>                                                                                                                \
                        <span id="fbookingcount" class="menu__badge" style="display:none;"></span>                                          \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="facility-foodBreverages" style="display:@4.0@;" menuId="4.0">                                                           \
                <a class="waves-attach" id="menu_nameid_4.0" href="food-beverage-restaurants.html" onclick="slideview(this,\'link\',\'left\');return false;">                                                                                                                    \
                    <i class="fa fa-cutlery fa-fw"></i> <span id="menunameid_4.0">Food & Beverages</span>                                   \
                </a>                                                                                                                        \
                <span id="fnbcount" class="menu__badge" style="display:none;"></span>                                                       \
            </li>                                                                                                                           \
            <li id="golf" class="menu-with-child" style="display:@1.0:Golf@;" menuId="1.0:Golf">                                            \
                <a class="waves-attach" id="Menu_Button_Golf" name ="LocalizeMenuElement" href="javascript:void();" data-target="#golf-details" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-flag fa-fw"></i>                                                                                        \
                </a>                                                                                                                        \
                <span id="golfcount" class="group__menu__badge" style="display:none;"></span>                                               \
                <span class="menu-collapse-toggle collapsed" data-target="#golf-details" data-toggle="collapse">                            \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="golf-details">                                                                       \
                    <li id="golf-course" style="display:@1.1:Golf@;" menuId="1.1:Golf">                                                     \
                        <a class="waves-attach" id="menunameid_1.1:Golf" href="golf-course.html" onclick="slideview(this,\'link\',\'left\');return false;">Golf Course                                                                                                              \
                        </a>                                                                                                                \
                        <span id="2101" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-coaches" style="display:@1.8:Golf@;" menuId="1.8:Golf">                                                    \
                        <a class="waves-attach" id="menunameid_1.8:Golf" href="golf-coaches.html" onclick="slideview(this,\'link\',\'left\');return false;">Coaches                                                                                                     \
                        </a>                                                                                                                \
                        <span id="2110" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-handicap" style="display:@1.2:Golf@;" menuId="1.2:Golf">                                                   \
                        <a class="waves-attach" id="menunameid_1.2:Golf" href="handicap-display.html" onclick="slideview(this,\'link\',\'left\');return false;">Handicap                                                                                                    \
                        </a>                                                                                                                \
                        <span id="2102" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-tournament-list" style="display:@1.5.1:Golf@;" menuId="1.5.1:Golf">                                        \
                        <a class="waves-attach" id="menunameid_1.5.1:Golf" href="golf-tournaments.html" onclick="slideview(this,\'link\',\'left\');return false;">Tournament Listing                                                                                                             \
                        </a>                                                                                                                \
                        <span id="galltourcount" class="menu__badge" style="display:none;"></span>                                          \
                    </li>                                                                                                                   \
                    <li id="golf-tournaments-my-history" style="display:@1.5.2:Golf@;" menuId="1.5.2:Golf">                                 \
                        <a class="waves-attach" id="Menu_Button_MyGolfTournaments" name ="LocalizeElement" href="golf-tournaments-my-history.html" onclick="slideview(this,\'link\',\'left\');return false;">My Golf Tournaments                                       \
                        </a>                                                                                                                \
                        <span id="2105" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-tournaments-results" style="display:@1.7:Golf@;" menuId="1.7:Golf">                                        \
                        <a class="waves-attach" id="menunameid_1.7:Golf" href="golf-tournaments-results.html" onclick="slideview(this,\'link\',\'left\');return false;">Results & Leaderboard                                                                                                         \
                        </a>                                                                                                                \
                        <span id="2106" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-scorecard" style="display:@1.6:Golf@;" menuId="1.6:Golf">                                                  \
                        <a class="waves-attach" id="menunameid_1.6:Golf" href="score-card-history.html" onclick="slideview(this,\'link\',\'left\');return false;">Score Cards                                                                                                               \
                        </a>                                                                                                                \
                        <span id="gscorecardcount" class="menu__badge" style="display:none;"></span>                                        \
                    </li>                                                                                                                   \
                    <li id="golf-tee-time" style="display:@1.3:Golf@;" menuId="1.3:Golf">                                                   \
                        <a class="waves-attach" href="javascript:void()" onclick="golfTeeTime();" id="menunameid_1.3:Golf">Tee-Time         \
                        </a>                                                                                                                \
                        <span id="2111" class="menu__badge" style="display:none;"></span>                                                   \
                    </li> \
                    <li id="golf-tee-time" style="display:@1.9.1:Golf@;" menuId="1.9.1:Golf">                                               \
                        <a class="waves-attach" id="menunameid_1.9.1:Golf" href="golf-course-list.html" onclick="slideview(this,\'link\',\'left\');return false;">Golf Courses   \
                        </a>                                                                                                                \
                        <span id="2112" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                    <li id="golf-tee-time" style="display:@1.9.2:Golf@;" menuId="1.9.2:Golf">                                               \
                        <a class="waves-attach" id="menunameid_1.9.2:Golf" href="golf-course-mybookings.html" onclick="slideview(this,\'link\',\'left\');return false;">My Bookings \
                        </a>                                                                                                                \
                        <span id="2113" class="menu__badge" style="display:none;"></span>                                                   \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="liveScorecard" style="display:@3.2.4@;" menuId="3.2.4">                                                                 \
                <a class="waves-attach" id="Menu_Button_LiveScoreCards" name ="LocalizeMenuElement" href="live-scorecard-list.html" onclick="slideview(this,\'link\',\'left\');return false;">                                                                  \
                    <i class="fa fa-gamepad fa-fw"></i>                                                                                     \
                </a>                                                                                                                        \
                <span id="livescorecount" class="menu__badge" style="display:none;"></span>                                                 \
            </li>                                                                                                                           \
            <li id="member-opinion" class="menu-with-child" style="display:@13.0@;" menuId="13.0">                                          \
                <a class="waves-attach" id="Menu_Button_Opinion" name ="LocalizeMenuElement" href="javascript:void();" data-target="#member-opinion-details" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-check-square-o fa-fw"></i>                                                                              \
                </a>                                                                                                                        \
                <span id="opinioncount" class="group__menu__badge" style="display:none;"></span>                                            \
                <span class="menu-collapse-toggle collapsed" data-target="#member-opinion-details" data-toggle="collapse">                  \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="member-opinion-details">                                                             \
                    <li id="member-opinion-poll" style="display:@13.1@;" menuId="13.1">                                                     \
                        <a class="waves-attach" id="menunameid_13.1" href="polls.html" onclick="slideview(this,\'link\',\'left\');return false;">Online Polling                                                                                                             \
                        </a>                                                                                                                \
                        <span id="pollcount" class="menu__badge" style="display:none;"></span>                                              \
                    </li>                                                                                                                   \
                    <li id="member-opinion-surveys" style="display:@13.2@;" menuId="13.2">                                                  \
                        <a class="waves-attach" id="menunameid_13.2" href="surveys.html" onclick="slideview(this,\'link\',\'left\');return false;">Online Survey                                                                                                              \
                        </a>                                                                                                                \
                        <span id="surveycount" class="menu__badge" style="display:none;"></span>                                            \
                    </li>                                                                                                                   \
                    <li id="member-opinion-report" style="display:@13.3@;" menuId="13.3">                                                   \
                        <a class="waves-attach" id="Menu_Button_ReportIssue" name ="LocalizeElement" href="report-issue.html" onclick="slideview(this,\'link\',\'left\');return false;">Member Feedback                                                                                             \
                        </a>                                                                                                                \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="offers" style="display:@11@;" menuId="11">                                                                              \
                <a class="waves-attach" id="Menu_Button_Offers" name ="LocalizeMenuElement" href="offers.html" onclick="slideview(this,\'link\',\'left\');return false;">                                                                                                                    \
                    <i class="fa fa-tags fa-fw"></i>                                                                                        \
                </a>                                                                                                                        \
                <span id="offercount" class="menu__badge" style="display:none;"></span>                                                     \
            </li>                                                                                                                           \
            <li id="sponsors" style="display:@10@;" menuId="10">                                                                            \
                <a class="waves-attach" id="Menu_Button_Sponsors" name ="LocalizeMenuElement" href="sponsors.html" onclick="slideview(this,\'link\',\'left\');return false;">                                                                                                                    \
                    <i class="fa fa-users fa-fw"></i>                                                                                       \
                </a>                                                                                                                        \
                <span id="sponsorcount" class="menu__badge" style="display:none;"></span>                                                   \
            </li>                                                                                                                           \
            <li id="tournaments" class="menu-with-child" style="display:@15.0@;" menuId="15.0">                                             \
                <a class="waves-attach" id="Menu_Button_Tournaments" name ="LocalizeMenuElement" href="javascript:void();" data-target="#tournaments-details" data-toggle="collapse">                                                                                                     \
                    <i class="fa fa-trophy fa-fw"></i>                                                                                      \
                </a>                                                                                                                        \
                <span id="tournamentcount" class="group__menu__badge" style="display:none;"></span>                                         \
                <span class="menu-collapse-toggle collapsed" data-target="#tournaments-details" data-toggle="collapse">                     \
                    <i class="icon menu-collapse-toggle-close">close</i>                                                                    \
                    <i class="icon menu-collapse-toggle-default">add</i>                                                                    \
                </span>                                                                                                                     \
                <ul class="menu-collapse collapse" id="tournaments-details">                                                                \
                    <li id="tournaments-booking-list" style="display:@15.1@;" menuId="15.1">                                                \
                        <a class="waves-attach" id="menunameid_15.1" href="tournaments.html" onclick="slideview(this,\'link\',\'left\');return false;">All Tournaments                                                                                                         \
                        </a>                                                                                                                \
                        <span id="alltourcount" class="menu__badge" style="display:none;"></span>                                           \
                    </li>                                                                                                                   \
                    <li id="tournaments-my-history" style="display:@15.2@;" menuId="15.2">                                                  \
                        <a class="waves-attach" id="Menu_Button_MyTournaments" name ="LocalizeElement" href="tournaments-my-history.html" onclick="slideview(this,\'link\',\'left\');return false;">My Tournaments                                            \
                        </a>                                                                                                                \
                        <span id="mytourcount" class="menu__badge" style="display:none;"></span>                                            \
                    </li>                                                                                                                   \
                    <li id="tournaments-results" style="display:@15.3@;" menuId="15.3">                                                     \
                        <a class="waves-attach" id="menunameid_15.3" href="tournaments-results.html" onclick="slideview(this,\'link\',\'left\');return false;">Results                                                                                                     \
                        </a>                                                                                                                \
                        <span id="tourresultcount" class="menu__badge" style="display:none;"></span>                                        \
                    </li>                                                                                                                   \
                </ul>                                                                                                                       \
            </li>                                                                                                                           \
            <li id="submenu" style="display:@0.0@;" menuId="0.0">                                                                           \
                <a class="waves-attach" href="subdomain-selection.html" onclick="slideview(this,\'link\',\'left\');return false;">          \
                    <i class="fa fa-lock fa-fw"></i><span id="subdomains">Change Subdomain<span>                                            \
                </a>                                                                                                                        \
            </li>                                                                                                                           \
            <li id="nomenu" style="display:block;">                                                                                         \
            </li>                                                                                                                           \
            <li id="myaccountsettings" style="display:@myacntsetting@" menuId="myacntsetting">                                              \
                <a class="waves-attach" href="settings.html" onclick="slideview(this,\'link\',\'left\');return false;">                     \
                    <i class="fa fa-cog fa-fw"></i> <span id="Menu_Button_Settings" name ="LocalizeMenuElement"></span>                     \
                </a>                                                                                                                        \
            </li>                                                                                                                           \
        </ul>                                                                                                                               \
    </div>                                                                                                                                  \
</div> ';

localStorage.setItem("NAVIGATION_BACKUP", menuListing);

var publicMenuList = "1.0,1.1,1.1.1,1.2,1.2.1,1.3,1.4,1.5,1.6,1.6.1,1.7,1.8,2.0,2.1,2.2,2.3,2.4,3.0,3.1.0,3.1.1,3.1.2,3.1.3,3.2.0,3.2.1,3.2.2,3.2.3,3.2.4,3.3.0,3.3.1,3.3.2,3.3.3,3.4.0,3.4.1,3.4.2,3.4.3,3.5.0,3.5.1,3.5.2,3.5.3,3.6.0,3.8,4.0,4.1,4.2,5.0,5.1,5.2,5.3.1,5.3.2,6.0,6.1,6.2,6.3,7.0,7.1,7.2.0,7.2.1,7.2.2,7.2.3,7.3,7.4,7.5,7.6,7.7,8.0,8.1,8.2,8.3,9.0,9.1,9.2,9.3,9.4,9.5,10,11,12.0,12.1,12.2,13.0,13.1,13.2,13.3,14.0,14.1,14.2,15.0,15.1,15.2,15.3,1.0:Golf,1.1:Golf,1.2:Golf,1.3:Golf,1.4:Golf,1.5:Golf,1.6:Golf,1.7:Golf,1.8:Golf,1.9.1:Golf,1.9.2:Golf";

if (localStorage.getItem("publicMenuList") == undefined || localStorage.getItem("publicMenuList") == null) {
    localStorage.setItem("publicMenuList", publicMenuList);
}

/* This should run and set public menu only on the first time app launch */
if (localStorage.getItem("NAVIGATION_SET") != "true") {
    var publicMenuItemArray = publicMenuList.split(",");
    localStorage.setItem("publicMenuItemArray", JSON.stringify(publicMenuItemArray));
    // Fetch backup navigation menu from storage
    var publicNavigation = localStorage.getItem("NAVIGATION_BACKUP");
    for (var i = 0; i < publicMenuItemArray.length; i++) {
        replaceStr = '\@' + publicMenuItemArray[i] + '\@';
        publicNavigation = publicNavigation.replace(replaceStr, "block");
    }

    //var globalMenuIdList = JSON.parse(localStorage.getItem("GLOBALMENUIDARRAY"));
    for (var j = 0; j < globalMenuIdArray.length; j++) {
        publicNavigation = publicNavigation.replace("\@" + globalMenuIdArray[j] + "\@", "none");
    }
    //console.log("Public navigation Menu = " + publicNavigation);
    localStorage.setItem("NAVIGATION", publicNavigation); // Final Public menu
    localStorage.setItem("NAVIGATION_SET", "true");
}