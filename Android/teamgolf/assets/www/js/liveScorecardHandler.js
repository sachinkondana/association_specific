(function () {
    var enableDebugMode = 0;
    var page_number = 1;
    "use strict";
    /**
     * Fetch the list of sports facility for which live scorecard
     * feature is enabled.
     * @category_id facility category id
     */
    getScoreCardFacilityListing = function (category_id) {
        var actions = {
            "category_id" : category_id,
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length <= 0) {
                    $('.row').html('<p style="text-align:center"> ' + localizeString("LiveScorecard_Label_NoList") + '</p>');
                } else {
                    displayFacilityListingForLiveScoreCard(data.data);
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/facilities", actions, successCallback, errorCallback);
    }


    /**
     * Render the live scorecard facility listing
     * @facilitylist facility details recieved from the backend.
     */
    displayFacilityListingForLiveScoreCard = function (facilityList) {
        var isUserLoggedIn = false;
        var facilityDiv = null;
        if (enableDebugMode)
            alert("facilityList = " + JSON.stringify(facilityList));

        if (localStorage.getItem("isLoggedIn") != "true")
        {
            isUserLoggedIn = false;
        }
        else
        {
            isUserLoggedIn = true;
        }

        for (var i = 0; i < facilityList.content.length; i++)
        {
            if (facilityList.content[i].livescoreneeded != 0 && facilityList.content[i].livescoreneeded != "0") {
                facilityDiv = null;
                if ("1" == facilityList.content[i].isPublic)
                {
                    facilityDiv = generateFacilityDiv(i, facilityList.content[i], facilityList.imageBasePath);
                }
                else // facility is private
                {
                    if (isUserLoggedIn)
                    {
                        facilityDiv = generateFacilityDiv(i, facilityList.content[i], facilityList.imageBasePath);
                    }
                }
                if (facilityDiv != null)
                    $('.row').append(facilityDiv);
            }
        }
        lazyLoadListImages(page_number, facilityList.content.length);
    }


    /**
     * Helper function for rendering the facility listing for scorecard
     */
    generateFacilityDiv = function (inx, facility, imageBasePath) {
        if (enableDebugMode)
            alert("Faci Details = " + JSON.stringify(facility));
        var category = facility.categoryId;
        var div1 = null;
        if (1 == facility.scoreCardType) {
            div1 = $('<div class="col-lg-4 col-sm-12" onclick="goToScoreCardUrl(\'' + facility.scoreCardStreamingLink + '\')"></div>');
        } else {
            div1 = $('<div class="col-lg-4 col-sm-12" onclick="goToScoreCardDescription(' + facility.id + ')"></div>');
        }
        var div2 = $('<div class="card-01"></div>');
        var faciimage;
        if (facility.images != null && ((facility.images.image1 != null && facility.images.image1 != undefined
            && facility.images.image1 != "") || (facility.images.image2 != "" && facility.images.image2 != null
            && facility.images.image2 != undefined) || (facility.images.image3 != ""
            && facility.images.image3 != null && facility.images.image3 != undefined))) {
            if (facility.images.image1 != null && facility.images.image1 != undefined && facility.images.image1 != "") {
                faciimage = imageBasePath + facility.images.image1;
            } else {
                if (facility.images.image2 != null && facility.images.image2 != undefined && facility.images.image2 != "") {
                    faciimage = imageBasePath + facility.images.image2;
                } else {
                    if (facility.images.image3 != null && facility.images.image3 != undefined && facility.images.image3 != "") {
                        faciimage = imageBasePath + facility.images.image3;
                    }
                }
            }
        } else {
            faciimage = "images/default-listing.png";
        }
        var aside = $('<aside class="card-side pull-left"></aside>'); //card-side-img
        var imgDiv = $('<div class="imgDivNoBg"><img id="image-' + ((page_number - 1) * 15 + inx) + '" src="images/default-listing.png" data-src="' + faciimage + '"></div>');
        aside.append(imgDiv);
        var div3 = $('<div class="card-main card-width"></div>');
        var div4 = $('<div class="card-inner-01 card-inner-width"></div>');
        var p1 = $('<p class="card-heading card-heading-flow">' + facility.name + '</p>');
        var shortDescription = facility.description;
        if (shortDescription.length > 30)
            shortDescription = shortDescription.substring(0, 27) + "..."
        var p2 = $('<p>' + shortDescription + '</p>');
        div4.append(p1, p2);
        div3.append(div4);
        div2.append(aside, div3);
        div1.append(div2);
        return div1;
    }


    /**
     * Goto the live scorecard description rendered on our app UI.
     * The live scorecard content will be displayed within the pre tag.
     * @facilityId unique facility identifier for which scorecard needs
     *             to be rendered.
     */
    goToScoreCardDescription = function (facilityId) {
        localStorage.setItem("facilitieScoreCardId", facilityId);
        slideview('live-socrecard-details.html', 'script', 'left');
    }


    /**
     * Goto the live scorecard url for the facility.
     * @url the live scorecard url
     */
    goToScoreCardUrl = function (url) {
        if (url != "http://" && url != "https://" && url.trim() != "") {
            gotourl(url);
        } else {
            swal({
                title: localizeString("Alert_Title_Livesocre_Url"),
                text: localizeString("Alert_Message_Livescore_Url"),
                type: "warning",
                showCancelButton: false,
                closeOnConfirm: true,
                confirmButtonText: localizeString("Alert_Button_Ok")
            },
            function () {
                return;
            });
        }
    }


    /**
     * Fetch the facility detail for live scorecard
     */
    getFacilityDetailsForLiveScoreCard = function () {
        facility_id = localStorage.getItem("facilitieScoreCardId");
        var actions = {
            "facility_id": facility_id,
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayFacilityDetailsForLiveScoreCard(data.data.content, data.data.imageBasePath);
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/facilities", actions, successCallback, errorCallback);
    }


    /**
     * Render facility details for the live scorecard. The scorecard details comes
     * pre-formatted from the backend & is just appended within the pre tag.
     * @facility facility details received from the backend.
     * @imageBasePath image path to be prepended to each image.
     */
    displayFacilityDetailsForLiveScoreCard = function (facility, imageBasePath) {
        if (enableDebugMode)
            alert("Facility = " + JSON.stringify(facility));

        if (null != facility[0].images)
        {
            if (null != facility[0].images.image1 && undefined != facility[0].images.image1 && "" != facility[0].images.image1) {
                $('#topbanner').attr({
                    'src': imageBasePath + facility[0].images.image1
                });
                $("#topbanner").css("display", "block");
            } else {
                $("#topbanner").removeClass("mySlides");
                $("#topbanner").css("display", "none");
            }

            if (null != facility[0].images.image2 && undefined != facility[0].images.image2 && "" != facility[0].images.image2) {
                $('#topbanner2').attr({
                    'src': imageBasePath + facility[0].images.image2
                });
            } else {
                $("#topbanner2").removeClass("mySlides");
            }

            if (null != facility[0].images.image3 && undefined != facility[0].images.image3 && "" != facility[0].images.image3) {
                $('#topbanner3').attr({
                    'src': imageBasePath + facility[0].images.image3
                });
            } else {
                $("#topbanner3").removeClass("mySlides");
            }

            carousel();
        } else {
            $("#topbanner").removeClass("mySlides");
        }

        var tr1 = $('<tr></tr>');
        var td1 = $('<td colspan="2" class="text_p wysiwyg-img"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(facility[0].name);
        var p2 = $('<p class="wysiwyg-img" style="word-wrap:break-word; word-break:break-word;">' + facility[0].description + '</p>');
        td1.append(p1, p2);
        tr1.append(td1);

        $("#buttontr").css('display', 'block');
        tr1.insertBefore('#buttontr');

        var tr2 = $('<tr></tr>');
        var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
        tr2.append(td2);
        tr2.insertBefore('#buttontr');

        var scoreCardDetails = facility[0].scoreCardData.replace(/<br\s*\/?>/mg, "\n");
        scoreCardDetails = scoreCardDetails.replace(/<\/br\s*?>/mg, "");

        $("#msguserold").append(scoreCardDetails);
    }
}());