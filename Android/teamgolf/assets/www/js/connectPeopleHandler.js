(function () {
    var enableDebugMode = 0;
    var page_number_invite = 1;
    var page_number_message = 1;
    var page_number_pending = 1;

    /* Extend this as needed */
    var chatStatusArr = ["INVITATION_NOT_SENT", "INVITATION_SENT", "INVITATION_RECEIVED", "INVITATION_ACCEPTED", "INVITATION_REJECTED"];

    var numofUsers, remove = false,
            scroll = false;
    "use strict";

    localStorage["numofUsers"] = 0;
    localStorage["numofMessagingUsers"] = 0;
    localStorage["numofInviteUsers"] = 0;

    $(".content").scroll(function() {
        if ($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
            remove = false;
            scroll = true;
            if ($("#first-tab").hasClass("active")) {
                if ((parseInt(localStorage.getItem("numofUsers")) / 15 ) == page_number_invite) {
                    page_number_invite++;
                    if ($("#search").val() == '') {
                        getUserListForInvites(page_number_invite);
                    } else {
                        searchUser(page_number_invite);
                    }
                }
            } else if ($("#second-tab").hasClass("active")) {
                if ((parseInt(localStorage.getItem("numofMessagingUsers")) / 15 ) == page_number_message) {
                    page_number_message++;
                    if ($("#search").val() == '') {
                        getUserListForMessage(page_number_message);
                    } else {
                        searchUser(page_number_message);
                    }
                }
            } else if ($("#third-tab").hasClass("active")) {
                if ((parseInt(localStorage.getItem("numofInviteUsers")) / 15 ) == page_number_pending) {
                    page_number_pending++;
                    if ($("#search").val() == '') {
                        getUserListForPendingInvites(page_number_pending);
                    } else {
                        searchUser(page_number_pending);
                    }
                }
            }
        }
    });


    updateOptionTabs = function () {
        if ((localStorage.getItem("isLoggedIn") != "true")) { // || (localStorage.getItem("userChatEnabled") != "true")
            $("#messagetab").html("");
            $("#invitetab").html("");
        } else {
            if (localStorage.getItem("MENUBADGES") != null && localStorage.getItem("MENUBADGES") != undefined) {
                var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
                if (menuBadgeArray["1501"] > 0) {
                    var span = document.getElementById("pendinginvitecount");
                    if (span != null && span != undefined) {
                        txt = document.createTextNode(menuBadgeArray["1501"]);
                        span.innerText = txt.textContent;
                        $("#pendinginvitecount").css("display", "block");
                    }
                } else {
                    $("#pendinginvitecount").css("display", "none");
                }

                if (menuBadgeArray["1503"] > 0) {
                    var span = document.getElementById("messagecount");
                    if (span != null && span != undefined) {
                        txt = document.createTextNode(menuBadgeArray["1503"]);
                        span.innerText = txt.textContent;
                        $("#messagecount").css("display", "block");
                    }
                } else {
                    $("#messagecount").css("display", "none");
                }
            }
        }
    }

    $("#search").keyup(function () {
        var keyword = $(this).val();
        scroll = false;
        if (keyword == '') {
            remove = true;
        } else {
            remove = false;
        }
        if ($("#first-tab").hasClass("active")) {
            localStorage.setItem('numofUsers', '0');
            page_number_invite = 1;
            searchUser(page_number_invite);
        } else if ($("#second-tab").hasClass("active")) {
            localStorage.setItem('numofMessagingUsers', '0');
            page_number_message = 1;
            searchUser(page_number_message);
        } else if ($("#third-tab").hasClass("active")) {
            localStorage.setItem('numofInviteUsers', '0');
            page_number_pending = 1;
            searchUser(page_number_pending);
        }

    });

    /**
     * Search user in the user list
     * @pg_no page number to begin with
     */
    searchUser = function (pg_no) {
        /*if ($("#first-tab").hasClass("active"))
            localStorage.setItem('numofUsers', '0');
        if ($("#second-tab").hasClass("active"))
            localStorage.setItem('numofMessagingUsers', '0');
        if ($("#third-tab").hasClass("active"))
            localStorage.setItem('numofInviteUsers', '0');*/

        var actions = {
            "page_number": pg_no,
            "needle": $('#search').val().trim()
        };

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                if ($("#first-tab").hasClass("active")) {
                    localStorage.setItem('numofUsers', String(parseInt(localStorage["numofUsers"]) + parseInt(JSON.stringify(data.data.length))));
                    displayUserListForInvites(data);
                } else if ($("#second-tab").hasClass("active")) {
                    localStorage.setItem('numofMessagingUsers', String(parseInt(localStorage["numofMessagingUsers"]) + parseInt(JSON.stringify(data.data.length))));
                    displayUserListForMessage(data);
                } else if ($("#third-tab").hasClass("active")) {
                    localStorage.setItem('numofInviteUsers', String(parseInt(localStorage["numofInviteUsers"]) + parseInt(JSON.stringify(data.data.length))));
                    displayUserListForPendingInvites(data);
                }
            } else {
                window.plugins.spinnerDialog.hide();
                if ($("#first-tab").hasClass("active"))
                    localStorage.setItem('numofUsers', 0);
                else if ($("#second-tab").hasClass("active"))
                    localStorage.setItem('numofMessagingUsers', '0');
                else if ($("#third-tab").hasClass("active"))
                    localStorage.setItem('numofInviteUsers', '0');
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            if ($("#first-tab").hasClass("active"))
                localStorage.setItem('numofUsers', 0);
            else if ($("#second-tab").hasClass("active"))
                localStorage.setItem('numofMessagingUsers', '0');
            else if ($("#third-tab").hasClass("active"))
                localStorage.setItem('numofInviteUsers', '0');
        };

        if ($("#first-tab").hasClass("active"))
            postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
        else if ($("#second-tab").hasClass("active"))
            postMessageToServerWithAuth("/users/list/connect", actions, successCallback, errorCallback);
        else if ($("#third-tab").hasClass("active"))
            postMessageToServerWithAuth("/users/list/invite", actions, successCallback, errorCallback);
    }


    /**
     * Get list of members from server
     * @page_number page number to begin with
     */
    getUserListForInvites = function (page_number) {
        page_number_invite = page_number;
        /* Handle comming back scenario */
        if ($('#search').val().trim() != "") {
            searchUser(page_number);
            return;
        }

        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                localStorage.setItem('numofUsers', String(parseInt(localStorage["numofUsers"]) + parseInt(JSON.stringify(data.data.length))));
                if (1 == page_number)
                    $('#user-list-invite').html('');
                displayUserListForInvites(data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofUsers', 0);
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/users/list", actions, successCallback, errorCallback);
    }


    /**
     * Display list of members
     * @memberList member list
     */
    displayUserListForInvites = function (memberList) {
        if (($("#search").val().trim() != "" && !scroll) || remove)
            $('#user-list-invite').html("");

        var fname, lname, id, avatar = "images/avatar-001.jpg";

        localStorage.from_page = "MemberListDirectory";
        if (memberList.data.length < 1 && (page_number_invite == 1)) {
            var nothingDiv = $('<div style="width:100%; margin:0 auto; text-align:center"><p style="font-weight:bold;" align="center" id="Connect_Label_NoData" name="LocalizeElement">No one to connect with at the moment</p></div>')
            $('#user-list-invite').append(nothingDiv);
            return;
        }

        for (i = 0; i < memberList.data.length; i++) {
            if ((memberList.data[i].chatStatus == "INVITATION_SENT")
                || (memberList.data[i].chatStatus == "INVITATION_NOT_SENT")
                || (memberList.data[i].chatStatus == "INVITATION_REJECTED")
                || (memberList.data[i].chatStatus == "INVITATION_ACCEPTED")) {
                fname = memberList.data[i].first_name;
                lname = memberList.data[i].last_name;
                photos = memberList.data[i].photo;
                var id = memberList.data[i].id;
                if (fname != '' && fname != null && fname != undefined && memberList.data[i].email != localStorage.email) {
                    var li = $('<li></li>');
                    var div1 = $('<div></div>').attr({
                        'class': 'user-img'
                    });
                    var img;
                    if (photos != null && photos != undefined && photos != "") {
                        img = $('<img></img>').attr({
                            'src': 'data:image/jpeg;base64,' + photos
                        });
                    } else {
                        img = $('<img></img>').attr({
                            'src': 'images/avatar-001.jpg'
                        });
                    }
                    div1.append(img);

                    if (memberList.data[i].chatStatus == "INVITATION_SENT") {
                        var div2 = $('<div></div>');
                        var div2_1 = $('<div></div>').attr({
                            'class': 'five_sixth bold-match-color',
                            'style': 'line-height:46px; display:inline;',
                            'Onclick': "gotoDetail('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "', 'sinv')"
                        }).text(fname + " " + lname);
                        var div3 = $('<div class="user-arrow btn-lng-02 disabled" style="width:20%;">Sent</div>');
                        div2.append(div2_1, div3);
                    } else if ((memberList.data[i].chatStatus == "INVITATION_NOT_SENT") || (memberList.data[i].chatStatus == "INVITATION_REJECTED")) {
                        var div2 = $('<div></div>');
                        var div2_1 = $('<div></div>').attr({ //five_sixth
                            'class': ' bold-match-color',
                            'style': 'line-height:46px; width:75%; display:inline;',
                            'Onclick': "gotoDetail('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                        }).text(fname + " " + lname);

                        var div2_2 = $("<div></div>").attr({
                            'id': 'msg_invite',
                            'class': 'user-arrow btn-lng-02',
                            'style': 'width:20%;',
                            'Onclick': "sendMessagingInvite('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                        }).text("Invite"); //btn btn-alt

                        div2.append(div2_1, div2_2);
                    } else if (memberList.data[i].chatStatus == "INVITATION_ACCEPTED") {
                        var div2 = $('<div></div>');
                        var div2_1 = $('<div></div>').attr({
                            'class': 'five_sixth bold-match-color',
                            'style': 'line-height:46px; display:inline;',
                            'Onclick': "gotoDetail('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                        }).text(fname + " " + lname);
                        var div3 = $('<div class="user-arrow btn-lng-02 disabled" style="width:20%;">Linked</div>');
                        div2.append(div2_1, div3);
                    }

                    li.append(div1, div2);
                    $('#user-list-invite').append(li);
                }
            }
        }
        window.plugins.spinnerDialog.hide();
    }


    /**
     * Go to member details page
     * @id member id
     * @fname member First Name
     * @lname member Last Name
     */
    gotoDetail = function (id, fname, lname, frm) {
        if (enableDebugMode)
            alert("gotoDetail");
        if ('pinv' == frm)
            localStorage.from_page = "MemberListInvite";
        else if ('sinv' == frm)
            localStorage.from_page = "MemberListDirectory";
        localStorage.setItem("UserIdenifier", id);
        localStorage.setItem("Userfirst_name", fname);
        localStorage.setItem("Userlast_name", lname);
        slideview("connect-people-details.html", 'script', 'left');
    }


    /**
     * Go to member details page
     * @id member id
     * @fname member First Name
     * @lname member Last Name
     */
    gotoChat = function (id, fname, lname, unreadcount) {
        if (enableDebugMode)
            alert("gotoChat");
        localStorage.setItem("UserIdenifier", id);
        localStorage.setItem("Userfirst_name", fname);
        localStorage.setItem("Userlast_name", lname);
        if (parseInt(unreadcount) > 0) {
            if (localStorage.getItem("MENUBADGES") != null && localStorage.getItem("MENUBADGES") != undefined) {
                updateOutSideBadgeCount(parseInt(unreadcount));
                var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
                menuBadgeArray["1503"] = menuBadgeArray["1503"] - parseInt(unreadcount);
                localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
                updateOptionTabs();
                updateMenuBadgeStatus();
            }
        }
        slideview("user-chat.html", 'script', 'left');
    }


    /**
     * Get list of members for messaging from server
     * @page_number page number to begin with
     */
    getUserListForMessage = function (page_number) {
        page_number_message = page_number;
        /* Handle comming back scenario */
        if ($('#search').val().trim() != "") {
            searchUser(page_number);
            return;
        }

        /* Send "type": "chat" to indicate sorted list based on unread messages */
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                localStorage.setItem('numofMessagingUsers', String(parseInt(localStorage["numofMessagingUsers"]) + parseInt(JSON.stringify(data.data.length))));
                if (1 == page_number)
                    $('#user-list-msg').html('');
                displayUserListForMessage(data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofMessagingUsers', 0);
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/users/list/connect", actions, successCallback, errorCallback);
    }


    /**
     * Display list of members for Messaging tab
     * @memberList member list
     * We donot want member's with chat status "INVITATION_SENT" & "INVITATION_RECEIVED"
     */
    displayUserListForMessage = function (memberList) {
        if (($("#search").val().trim() != "" && !scroll) || remove)
            $('#user-list-msg').html("");

        var fname, lname, id, avatar = "images/avatar-001.jpg";

        localStorage.from_page = "MemberListMessage";

        if (memberList.data.length < 1 && (page_number_message == 1)) {
            var nothingDiv = $('<div style="width:100%; margin:0 auto; text-align:center"><p style="font-weight:bold;" align="center" id="Member_Label_NoData" name="LocalizeElement">Member list not available at the moment</p></div>')
            $('#user-list-msg').append(nothingDiv);
            return;
        }

        var unreadMsgCount = 0;

        for (i = 0; i < memberList.data.length; i++) {
            fname = memberList.data[i].first_name;
            lname = memberList.data[i].last_name;
            photos = memberList.data[i].photo;
            var id = memberList.data[i].id;

            if (fname != '' && fname != null && fname != undefined && (memberList.data[i].email != localStorage.email)) {
                var li = $('<li></li>');
                var div1 = $('<div></div>').attr({
                    'class': 'user-img'
                });
                var img;
                if (photos != null && photos != undefined && photos != "") {
                    img = $('<img></img>').attr({
                        'src': 'data:image/jpeg;base64,' + photos
                    });
                } else {
                    img = $('<img></img>').attr({
                        'src': 'images/avatar-001.jpg'
                    });
                }
                div1.append(img);
                var div2;

                div2 = $('<div></div>').attr({
                    'class': 'five_sixth bold-match-color',
                    'style': 'line-height:46px;',
                    'Onclick': "gotoChat('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "', '" + memberList.data[i].unread_count + "')"
                }).text(fname + " " + lname);

                if (memberList.data[i].unread_count > 0) { //tab__menu__badge
                    unreadMsgCount += parseInt(memberList.data[i].unread_count);
                    var spanElement1 = $('<span id="' + id + 'count" class="chat__item__badge">' + memberList.data[i].unread_count + '</span></a>'); //item badge  style="display:none;"
                    div2.append(spanElement1);
                }
                li.append(div1, div2);
                $('#user-list-msg').append(li)
            }
        }

        if (localStorage.getItem("MENUBADGES") != null && localStorage.getItem("MENUBADGES") != undefined) {
            var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
            menuBadgeArray["1503"] = unreadMsgCount;
            localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
            var span = document.getElementById("messagecount");
            if (span != null && span != undefined) {
                txt = document.createTextNode(unreadMsgCount);
                span.innerText = txt.textContent;
                if (span.innerText != "" && span.innerText != " ") {
                    if (unreadMsgCount > 0) {
                        $("#messagecount").css("display", "block");
                    } else {
                        $("#messagecount").css("display", "none");
                    }
                }
            }
            /* TODO: Need to figure out way to update status for individual messages */
            updateMenuBadgeStatus();
        }
        window.plugins.spinnerDialog.hide();
    }


    /**
     * Get list of members with pending invites from server
     * @page_number page number to begin with
     */
    getUserListForPendingInvites = function (page_number) {
        page_number_pending = page_number;
        /* Handle comming back scenario */
        if ($('#search').val().trim() != "") {
            searchUser(page_number);
            return;
        }

        /* Send "type": "chat" to indicate sorted list based on unread messages */
        var actions = {
            "page_number": page_number
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                localStorage.setItem('numofInviteUsers', String(parseInt(localStorage["numofInviteUsers"]) + parseInt(JSON.stringify(data.data.length))));
                if (1 == page_number)
                    $('#user-list-pending-invite').html('');
                displayUserListForPendingInvites(data);
            } else {
                window.plugins.spinnerDialog.hide();
                localStorage.setItem('numofInviteUsers', 0);
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {
            localStorage.setItem('numofUsers', 0);
        };

        postMessageToServerWithAuth("/users/list/invite", actions, successCallback, errorCallback);
    }


    /**
     * Display list of Pending invites
     * @memberList member list
     * We donot want member's with chat status "INVITATION_SENT" & "INVITATION_RECEIVED"
     */
    displayUserListForPendingInvites = function (memberList) {
        if (($("#search").val().trim() != "" && !scroll) || remove)
            $('#user-list-pending-invite').html("");

        var fname, lname, id, avatar = "images/avatar-001.jpg";

        localStorage.from_page = "MemberListInvite";

        if (memberList.data.length < 1 && (page_number_pending == 1)) {
            var nothingDiv = $('<div style="width:100%; margin:0 auto; text-align:center"><p style="font-weight:bold;" align="center" id="Invite_Label_NoData" name="LocalizeElement">No pending invites at the moment.</p></div>')
            $('#user-list-pending-invite').append(nothingDiv);

            /* Fix for clearing invite notification due to previous error */
            if ((localStorage.getItem("isLoggedIn") == "true") && (localStorage.getItem("MENUBADGES") != null)
                && (localStorage.getItem("MENUBADGES") != undefined)) {
                var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
                if (menuBadgeArray["1501"] > 0 ) {
                    updateOutSideBadgeCount(menuBadgeArray["1501"]);
                    menuBadgeArray["chatcount"] = menuBadgeArray["chatcount"] - menuBadgeArray["1501"];
                    menuBadgeArray["connectcount"] = menuBadgeArray["connectcount"] - menuBadgeArray["1501"];
                    menuBadgeArray["1501"] = 0;
                    localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
                    if (menuBadgeArray["1501"] > 0) {
                        var span = document.getElementById("pendinginvitecount");
                        if (span != null && span != undefined) {
                            txt = document.createTextNode(menuBadgeArray["1501"]);
                            span.innerText = txt.textContent;
                            //$("#pendinginvitecount").css("display", "block");
                        }
                    } else {
                        $("#pendinginvitecount").css("display", "none");
                    }

                    updateMenuBadgeStatus();
                    /* TODO: Need to figure out way to update status for individual invites */
                    updateNotificationState("", ["1501"], 4); // mark them as read
                }
            }
            return;
        }

        for (i = 0; i < memberList.data.length; i++) {
            fname = memberList.data[i].first_name;
            lname = memberList.data[i].last_name;
            photos = memberList.data[i].photo;
            var id = memberList.data[i].id;
            if (fname != '' && fname != null && fname != undefined && memberList.data[i].email != localStorage.email) {
                var li = $('<li></li>');
                var div1 = $('<div></div>').attr({
                    'class': 'user-img'
                });
                var img;
                if (photos != null && photos != undefined && photos != "") {
                    img = $('<img></img>').attr({
                        'src': 'data:image/jpeg;base64,' + photos
                    });
                } else {
                    img = $('<img></img>').attr({
                        'src': 'images/avatar-001.jpg'
                    });
                }
                div1.append(img);

                var div2 = $('<div></div>');
                var div2_1 = $('<div></div>').attr({
                    'class': 'five_sixth bold-match-color',
                    'style': 'line-height:46px; display:inline;',
                    'Onclick': "gotoDetail('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "', 'pinv')"
                }).text(fname + " " + lname);
                var div3 = $("<div></div>").attr({
                    'class': 'user-arrow'
                });
                var aElement1 = $('<a href="javascript:void();" class="btn" id="accept_invite"></a>').attr({
                    'Onclick': "acceptInvite('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                });
                var aElement2 = $('<a href="javascript:void();" class="btn" id="reject_invite"></a>').attr({
                    'Onclick': "rejectInnvite('" + id + "', '" + fname.replace(/'/g, "\\'") + "', '" + lname.replace(/'/g, "\\'") + "')"
                });
                var iElement1 = $('<i class="fa fa-check" aria-hidden="true"></i>');
                var iElement2 = $('<i class="fa fa-times" aria-hidden="true"></i>');
                aElement1.append(iElement1);
                aElement2.append(iElement2);
                div3.append(aElement1, "&nbsp;", aElement2);
                div2.append(div2_1, div3);
                li.append(div1, div2);
                $('#user-list-pending-invite').append(li);
            }
        }
        window.plugins.spinnerDialog.hide();
    }

    /**
     * Get individual member detail from server
     */
    getPersonalDetails = function () {
        var actions = {
            "user_id": localStorage.getItem("UserIdenifier")
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                displayUserDetails(data);
            } else {
                $('#details').html('');
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/user/view", actions, successCallback, errorCallback);
    }


    /**
     * Display Member Details
     * @memberDetail member Detail
     */
    displayUserDetails = function (memberDetail) {
        if (enableDebugMode)
            alert("data = " + JSON.stringify(memberDetail));
        $('.user-list').html('');
        var li = $('<li></li>').attr({
            'style': 'border-bottom:none; margin-bottom:0px;'
        });
        var div1 = $('<div></div>').attr({
            'class': 'user-img-01'
        });

        var div2 = $('<div></div>').attr({
            'class': 'four_sixth bold-match-color',
            'style': 'margin-left:3%'
        });
        var p = $('<p></p>').attr({
            'style': 'margin-top:8px; margin-bottom:3px'
        }).text(localStorage.getItem('Userfirst_name') + ' ' + localStorage.getItem('Userlast_name'));
        $("#title").html(localStorage.getItem('Userfirst_name'));
        var img;
        img = $('<img></img>').attr({
            'src': 'images/avatar-001.jpg'
        });
        if (memberDetail == '') {
            var span = $('<span></span>').attr({
                'class': 'text'
            }).text('');
        } else {
            if (memberDetail.data.avatar != null && memberDetail.data.avatar != '' && memberDetail.data.avatar != undefined)
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text(memberDetail.data.avatar);
            else
                var span = $('<span></span>').attr({
                    'class': 'text'
                }).text('Not Availbale');

            if (memberDetail.data.photo != null && memberDetail.data.photo != "" && memberDetail.data.photo != undefined) {
                img = $('<img></img>').attr({
                    'src': 'data:image/jpeg;base64,' + memberDetail.data.photo
                });
            }
        }

        div1.append(img);
        p.append($('</br>'), span);
        var div3 = $('<div></div>').attr({
            'class': 'user-arrow'
        });
        var a = $('<a></a>').attr({
            'href': 'javascript:void()'
        });
        var ival = $('<i></i>').attr({
            'class': 'mdi mdi-table-edit',
            'data-name': 'mdi-table-edit'
        });
        a.append(ival);
        div3.append(a);
        div2.append(p, div3);
        li.append(div1, div2);
        $('.user-list').append(li);
        $('#details').html('');

        for (var j = 0; j < memberDetail.data.display_ctrl.length ; j++) {
            if ((memberDetail.data.display_ctrl[j].selected == "1")
                && (memberDetail.data.display_ctrl[j].name != "first_name")
                && (memberDetail.data.display_ctrl[j].name != "last_name")
                && (memberDetail.data.display_ctrl[j].name != "avatar")
                && (memberDetail.data.display_ctrl[j].name != "photo")
                && (memberDetail.data.display_ctrl[j].name != "status")) {
                var tr1 = $('<tr></tr>');
                var td1 = $('<td></td>').attr({
                    'height': '25'
                });
                var strong2 = $('<strong></strong>').text(memberDetail.data.display_ctrl[j].display_name + ':');
                td1.append(strong2);
                if (memberDetail.data[memberDetail.data.display_ctrl[j].name] != null
                    && memberDetail.data[memberDetail.data.display_ctrl[j].name] != undefined
                    && memberDetail.data[memberDetail.data.display_ctrl[j].name] != "") {
                    if (memberDetail.data.display_ctrl[j].name == "email") {
                        var td2 = $('<td onclick="gotomail(\'' + memberDetail.data.email + '\');"></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    } else if (memberDetail.data.display_ctrl[j].name == "phone") {
                        var td2 = $('<td onclick="gotocall(' + memberDetail.data.phone + ');"></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    } else {
                        var td2 = $('<td ></td>').attr({
                            'width': '50%'
                        }).text((memberDetail.data[memberDetail.data.display_ctrl[j].name]).trim());
                    }
                } else {
                    var td2 = $('<td></td>').text(localizeString("Account_Label_NotAvailable"));
                }

                tr1.append(td1, td2);
                $('#details').append(tr1);
            }
        }

        $('#content img').simplebox({
            fadeSpeed: 350
        });
    }


    gotomail = function (mailadd) {
        swal({
            title: localizeString("Alert_Title_ConfirmAction"),
            text: localizeString("Alert_Text_SendEmail"),
            type: "info",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText : localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("mailto:" + mailadd + "", "_system");
        });
    }


    gotocall = function (phno) {
        swal({
            title: localizeString("Alert_Title_ConfirmAction"),
            text: localizeString("Alert_Text_MakeCall"),
            type: "info",
            showCancelButton: true,
            confirmButtonText: localizeString("Alert_Button_Proceed"),
            closeOnConfirm: true,
            cancelButtonText :localizeString("Alert_Button_Cancel")
        },
        function () {
            window.open("tel:" + phno + "", "_system");
        });
    }

    sendMessagingInvite = function (id, fname, lname) {
        var actions = {
            "recipientId": id
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal({
                    title: localizeString("Alert_Title_Success"),
                    text: data.message,
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    $('#user-list-invite').html("");
                    $("#Directory_Button_Members")[0].click();
                    //Bug 813 - Fix
                    if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
                        getUserListForInvites(1);
                    }
                    //..
                    return;
                });
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/chatinvite", actions, successCallback, errorCallback);
    }


    acceptInvite = function (id, fname, lname) {
        var actions = {
            "recipientId": id
        };

        successCallback = function (data) {
            if (data.status == "success") {
                swal.close();
                //fetchChatHistory();
                updatePendingInviteBadgeCount(id);
                $('#user-list-pending-invite').html("");
                $("#Directory_Button_Invites")[0].click();
                //Bug 813 - Fix
                if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
                    getUserListForPendingInvites(1);
                }
                //..
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/chat_invite_accept", actions, successCallback, errorCallback);
    }

    rejectInnvite = function (id, fname, lname) {
        swal({
            title: localizeString("Alert_Title_Reject_Invite"),
            text: localizeString("Alert_Message_Reject_Invite"),
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            closeOnCancel: true,
            allowOutsideClick: false,
            confirmButtonText: localizeString("Alert_Button_Ok"),
            cancleButtonText: localizeString("Alert_Button_Cancel")
        },
        function () {
            var actions = {
                "recipientId": id
            };

            successCallback = function (data) {
                if (data.status == "success") {
                    swal.close();
                    updatePendingInviteBadgeCount(id);
                    $('#user-list-pending-invite').html("");
                    $("#Directory_Button_Invites")[0].click();
                    //Bug 813 - Fix
                    if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
                        getUserListForPendingInvites(1);
                    }
                    //..
                } else {
                    var thisone = this;
                    swal({
                        title: localizeString("Alert_Title_Sorry"),
                        text: data.message,
                        type: "warning",
                        showCancelButton: false,
                        closeOnConfirm: true,
                        confirmButtonText: localizeString("Alert_Button_Ok")
                    },
                    function () {
                        return;
                    });
                }
            };
            errorCallback = function (data) {

            };
            postMessageToServerWithAuth("/chat_invite_reject", actions, successCallback, errorCallback);
        });
    }

    updatePendingInviteBadgeCount = function (id) {
        if ((localStorage.getItem("isLoggedIn") == "true") && (localStorage.getItem("MENUBADGES") != null)
            && (localStorage.getItem("MENUBADGES") != undefined)) {
            var menuBadgeArray = JSON.parse(localStorage.getItem("MENUBADGES"));
            if (menuBadgeArray["1501"] > 0 ) {
                updateOutSideBadgeCount(1);
                menuBadgeArray["chatcount"] = menuBadgeArray["chatcount"] - 1;
                menuBadgeArray["connectcount"] = menuBadgeArray["connectcount"] - 1;
                menuBadgeArray["1501"] = menuBadgeArray["1501"] - 1;
                localStorage.setItem("MENUBADGES", JSON.stringify(menuBadgeArray));
                if (menuBadgeArray["1501"] > 0) {
                    var span = document.getElementById("pendinginvitecount");
                    if (span != null && span != undefined) {
                        txt = document.createTextNode(menuBadgeArray["1501"]);
                        span.innerText = txt.textContent;
                    }
                } else {
                    $("#pendinginvitecount").css("display", "none");
                }

                updateMenuBadgeStatus();
                /* TODO: Need to figure out way to update status for individual invites */
                updateNotificationState("", ["1501"], 4); // mark them as read
            }
        }
    }
}());