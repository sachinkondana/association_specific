/**
 * Date Time Javascript.
 * Date Time Format 2016-01-15 17:00:00
 * Date Format 2016-01-15
 * Time Format 17:00:00
 */


/**
 * Get todays date in YYYY-MM-DD format.
 */
function getFormatedDate(today)
{
    var month = today.getMonth() + 1;
    var day = today.getDate();
    var year = today.getFullYear();
    return [year, (month<10 ? '0' : '') + month, (day<10 ? '0' : '') + day].join('-');
}

/**
 * Get current time in HH:MM:SS format.
 */
function getFormatedTime(today)
{
    var hour = today.getHours();
    var minute = today.getMinutes();
    var second = today.getSeconds();
    return [hour, (minute<10 ? '0' : '') + minute, (second<10 ? '0' : '') + second].join(':');
}

/**
 * Get current date-time in YYYY-MM-DD HH:MM:SS format.
 */
function getFormatedDateTime()
{
    var today = new Date();
    return [getFormatedDate(today), ' ', getFormatedTime(today)].join('');
}


/**
 * Date string recieved from server
 */
function getDateTimeFromServer()
{
    var dateString = "2016-01-15 17:00:00"; // received date time string
    processRecievedDateTime(dateString);
}

/**
 * Separate date & time from the dateString
 */
function getDateTimeFromString(dateString)
{
    return dateString.split(" ");
}

/**
 * Process recieved Date & Time Separately
 */
function processRecievedDateTime(dateString)
{
    var dateTime = getDateTimeFromString(dateString);
}


/**
 * Dates object that can convert, compare and check range
 */
var dates = {
    convert:function(d) {
        // Converts the date in d to a date-object. The input can be:
        //   a date object: returned without modification
        //  an array      : Interpreted as [year,month,day]. NOTE: month is 0-11.
        //   a number     : Interpreted as number of milliseconds
        //                  since 1 Jan 1970 (a timestamp)
        //   a string     : Any format supported by the javascript engine, like
        //                  "YYYY/MM/DD", "MM/DD/YYYY", "Jan 31 2009" etc.
        //  an object     : Interpreted as an object with year, month and date
        //                  attributes.  **NOTE** month is 0-11.
        return (
            d.constructor === Date ? d :
            d.constructor === Array ? new Date(d[0],d[1],d[2]) :
            d.constructor === Number ? new Date(d) :
            d.constructor === String ? new Date(d) :
            typeof d === "object" ? new Date(d.year,d.month,d.date) :
            NaN
        );
    },
    compare:function(a,b) {
        // Compare two dates (could be of any type supported by the convert
        // function above) and returns:
        //  -1 : if a < b
        //   0 : if a = b
        //   1 : if a > b
        // NaN : if a or b is an illegal date
        // NOTE: The code inside isFinite does an assignment (=).
        return (
            isFinite(a=this.convert(a).valueOf()) &&
            isFinite(b=this.convert(b).valueOf()) ?
            (a>b)-(a<b) :
            NaN
        );
    },
    inRange:function(d,start,end) {
        // Checks if date in d is between dates in start and end.
        // Returns a boolean or NaN:
        //    true  : if d is between start and end (inclusive)
        //    false : if d is before start or after end
        //    NaN   : if one or more of the dates is illegal.
        // NOTE: The code inside isFinite does an assignment (=).
       return (
            isFinite(d=this.convert(d).valueOf()) &&
            isFinite(start=this.convert(start).valueOf()) &&
            isFinite(end=this.convert(end).valueOf()) ?
            start <= d && d <= end :
            NaN
        );
    }
}

/**
 * Compare dates in yyyy-mm-dd format using above functions
 * returns true if out_date is >= in_date
 * false otherwise
 */
function compareDates(in_date, out_date)
{
    var result = dates.compare(in_date, out_date)

    if ((result != NaN) && (0 >= result))
    {
        return true;
    }
    return false;
}

/**
 * Compare times in HH:MM:SS format
 * returns true if out_time is > in_time
 * false otherwise
 */
function compareTime(in_time, out_time)
{
    var time1 = in_time.split(":");
    var time2 = out_time.split(":");
    if (time1[0] < time2[0]) {
        return true;
    }
    else if (time1[0] == time2[0]) {
        if (time1[1] < time2[1])
            return true;
    }
    return false;
}

function getLongMonthName (mon) {
    return calMonths[parseInt(mon) - 1];
}

function getShortMonthName (mon) {
    return calMonthsShort [parseInt(mon) - 1];
}

/**
 * Possible Date Formats:
 * Input Date Format 2016-01-15
 * 1. DD-MM-YY, DD-MM-YYYY
 * 2. DD-MMM-YY, DD-MMM-YYYY
 * 3. DD-MMMM-YY, DD-MMMM-YYYY
 * 4. MM-DD-YY, MM-DD-YYYY
 * 5. MMM-DD-YY, MMM-DD-YYYY
 * 6. MMMM-DD-YY, MMMM-DD-YYYY
 * 7. YY-MM-DD, YYYY-MM-DD
 * 8. YY-MMM-DD, YYYY-MMM-DD
 * 9. YY-MMMM-DD, YYYY-MMMM-DD
 *
 * Display format field will provide option :  (1) D/M/Y, (2) Y/M/D, (3) M/D/Y
 * Month Format field will provide options: (1) MM, (2) MMM, (3) MMMM
 * Year Format Field will provide options: (1) YY, (2) YYYY
 */
function getDisplayableDate(inp) {
    if (inp != null && inp != "") {
        var items = inp.split("-");
        if (items != null && items.length == 3) {
            var returnDate = "";
            var mon = "";
            var yer = "";
            var dispFormat = localStorage.getItem("ClubDispFormat");
            var monFormat = localStorage.getItem("ClubMonFormat");
            var yearFormat = localStorage.getItem("ClubYearFormat");

            if (monFormat != null && ((0 <= parseInt(monFormat)) && (parseInt(monFormat) <= 3))) {
                switch (parseInt(monFormat)) {
                    case 3:
                        mon = getLongMonthName(items[1]);
                        break;
                    case 2:
                        mon = getShortMonthName(items[1]);
                        break;
                    case 1:
                    default :
                        mon = items[1];
                        break;
                }
            }

            if (yearFormat != null && ((0 <= parseInt(yearFormat)) && (parseInt(yearFormat) <= 2))) {
                switch (parseInt(yearFormat)) {
                    case 1:
                        yer = items[0].slice(-2);
                        break;
                    case 2:
                    default :
                        yer = items[0];
                        break;
                }
            }

            if (dispFormat != null && ((0 <= parseInt(dispFormat)) && (parseInt(dispFormat) <= 3))) {
                switch (parseInt(dispFormat)) {
                    case 1 :
                        returnDate = [items[2], mon, yer].join('-');
                        break;
                    case 2 :
                        returnDate = [yer, mon, items[2]].join('-');
                        break;
                    case 3 :
                        returnDate = [mon, items[2], yer].join('-');
                        break;
                    default :
                        returnDate = inp;
                        break;
                }
            }
            return returnDate; /* Return Displayable Date */
        }
    }
    return inp; /* Invalid case so return what you got */
}

/**
 * Possible Time Formats:
 * Input Time Format 17:00:00
 * 1. 12 hour format
 * 2. 24 hour format
 */
function getDisplayableTime(inp) {
    if (inp != null && inp != "") {
        var timFormat = localStorage.getItem("ClubTimeFormat");
        if (timFormat != null && ((0 <= parseInt(timFormat)) && (parseInt(timFormat) <= 2))) {
            var returnTime = "";
            switch (parseInt(timFormat)) {
                case 1:
                    var items = inp.split(":");
                    if (items != null && items.length >= 2) {
                        var hrs = parseInt(items[0]) % 12;
                        var ampm = parseInt(parseInt(items[0]) / 12);
                        if (hrs == 0) { // 00/12
                            hrs = "12";
                        }
                        if (ampm == 0) {
                            ampm = "am";
                        } else {
                            ampm = "pm";
                        }
                        returnTime = [hrs, items[1]].join(':');
                        returnTime = [returnTime, ampm].join(' ');
                    } else {
                        var items = inp.split(":");
                        if (items != null && items.length >= 2) {
                            returnTime = [items[0], items[1]].join(':');
                        } else {
                            returnTime = inp;
                        }
                    }
                    break;
                case 2:
                default :
                    var items = inp.split(":");
                    if (items != null && items.length >= 2) {
                        returnTime = [items[0], items[1]].join(':');
                    } else {
                        returnTime = inp;
                    }
                   break;
            }
            return returnTime;
        }
    }
    return inp; /* Invalid case so return what you got */
}