(function () {

    var enableDebugMode = 0;
    var replies_page_number = 1;
    "use strict";

    /**
     *  Displays selected Forum Topic details
     *  @id id of the selected Forum Topic
     */
    gotoDetail = function (id) {
        localStorage.setItem('forum_id', id);
        slideview("forum-topics.html", 'script', 'left');
    }


    /**
     *  Fetches the next page of forum replies
     */
    getNextForumRepliesList = function () {
        if (enableDebugMode)
            alert("Page Number = " + replies_page_number + ", Num of replies = " + localStorage.getItem('numOfForumTopicReplies'));
        if (parseInt(localStorage.getItem('numOfForumTopicReplies')) > (15 * replies_page_number)) {
            replies_page_number++;
            getTopicsReplies(replies_page_number);
        }
    }


    /**
     *  Fetches the Forums List from server
     */
    getForums = function () {
        var actions = null;
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (data.data.content.length <= 0) {
                    $('#row').html('<p style="text-align:center">'+localizeString("Forum_Label_NoData")+'/p>');
                } else {
                    displayForumsList(data.data.content);
                }
            } else {
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/forums_new", actions, successCallback, errorCallback);
    }


    /**
     * Display list of forums.
     * @forumList list of forums recieved from server
     */
    function displayForumsList(forumList) {
        if (enableDebugMode)
            alert("Forum List = " + JSON.stringify(forumList));
        var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
            ];
        for (i = 0; i < forumList.length; i++) {
            var div1 = $('<div class="col-lg-4 col-sm-6"></div>').attr('onClick', "gotoDetail('" + forumList[i].id + "')");
            var div2 = $('<div class="card-01"></div>');
            var aside1 = $('<aside class="card-side card-side-img pull-left"></aside>');
            var d = new Date(forumList[i].createdOn);
            var div3 = $('<div class="blog-date"></div>').append(d.getDate()).append($('<br/>')).append(monthNames[d.getMonth()]);
            var img = $('<img alt="" width="35" height="35">').attr({
                'src': 'images/default-listing.png',
                "id": "image" + i
            });
            aside1.append(div3, img);
            var div4 = $('<div class="card-main"></div>');
            var div5 = $('<div class="card-inner-01"></div>');
            var p = $('<p class="card-heading" style="max-width:190px; text-overflow:ellipsis!important; white-space:nowrap!important; overflow:hidden!important;">' + forumList[i].title + '</p>');
            var p2 = $('<p></p>');
            var div6 = $('<div class="blog-post-meta"></div>');
            var span3 = $('<span></span>');
            var ielem1 = $('<i class="fa fa-comments"> ' + forumList[i].topicCounts + localizeString("ForumTopics_Label_Topic") + ' </i>');

            span3.append(ielem1);
            div6.append("</br>", span3);

            //div6.append(span3);
            div5.append(p, p2, div6);
            div4.append(div5);
            div2.append(aside1, div4);
            div1.append(div2);
            $('.row').append(div1);
        }
    }


    /**
     * Forum Topics Details
     * @forum_id forum id for which topic details need to be fetched
     */
    getTopics = function (forum_id) {
        var actions = {
            "forumId": forum_id
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                var topictext = localizeString("ForumTopics_Title");
                if (data.data.content[0].topicList.length <= 1) {
                    topictext = localizeString("ForumTopics_Label_Topic");
                }
                $('#topiccount').html(data.data.content[0].topicList.length + ' ' + topictext);

                if (data.data.content.length > 0) {
                    displayForumTopics(data.data);
                }
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        };

        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/forums_new", actions, successCallback, errorCallback);
    }


    /**
     * Display the Forum Topic list
     */
    function displayForumTopics(forumData) {
        $('#row').html('');
        $('#detailstr').html('');
        $('#seprationline').html('');

        /* Display Forum header information first */
        var td1 = $('<td colspan="2" class="text_p"></td>');
        var p1 = $('<p class="text-bold-01" style="font-size:20px; margin-bottom:5px;" id="title"></p>').text(forumData.content[0].title);
        var p2 = $('<p>' + forumData.content[0].description + '</p>');

        var span1 = $('<span style=" font-size:12px;"></span>');
        var iElem2 = $('<i class="fa fa-clock-o"> ' + localizeString("Label_CreatedOn") + ': ' + forumData.content[0].createdOn +'</i> </br>');
        span1.append(iElem2);

        td1.append(p1, span1, p2);
        $('#detailstr').append(td1);
        $('#detailstr').css("display", "block");

        var tr2 = $('<tr id="seprationline"></tr>');
        var td2 = $('<td colspan="2" align="right" class="text_p" style="border-top: #babab9 1px solid; padding-top:10px;"></td>');
        tr2.append(td2);
        tr2.insertAfter('#detailstr');

        /* Display Forum topic list next */
        for (i = 0; i < forumData.content[0].topicList.length; i++) {
            //var thecontent = forumData.content.topicList[i].description.replace(/<\/?[^>]+>/ig, "").replace(/ /g, "%20").replace(/(?:\r\n|\r|\n)/g, '');
            var coldiv = $('<div class="col-lg-6 col-sm-12"></div>');
            var outerlink = $('<a href="javascript:void();">').attr('onClick', "gotoForumTopicDetail('" + forumData.content[0].topicList[i].id + "', '" + forumData.content[0].topicList[i].postCounts + "')");
            var singlethreaddiv = $('<div class="single-thread"></div>');
            var postheaderdiv = $('<div class="post-header"></div>');

            var justdiv = $('<div></div>');
            var topiclink = $('<a class="topic-icon-placeholder link" href="javascript:void();"></a>');
            var avatar = null;
            if (forumData.content[0].topicList[i].photo != null && forumData.content[0].topicList[i].photo != ''
                && forumData.content[0].topicList[i].photo != undefined)
                avatar = $('<img src="data:image/jpeg;base64,' + forumData.content[0].topicList[i].photo + '" alt="No Image"/>');
            else
                avatar = $('<img src="images/avatar-001.jpg" alt="No Image"/>');
            topiclink.append(avatar);
            justdiv.append(topiclink);

            var headerscoldiv = $('<div class="col-xx-10 headers-col"></div>');
            var headertextp = $('<p class="header-text-content"></p>').text(forumData.content[0].topicList[i].author);
            var taglinep = $('<p class="tagline-text-content"> <i class="fa fa-clock-o"></i> ' + forumData.content[0].topicList[i].createdOn + '</p>');

            headerscoldiv.append(headertextp, taglinep);
            postheaderdiv.append(justdiv, headerscoldiv);

            var row12div = $('<div class="row-12"></div>');
            var header2 = $('<h2>' + forumData.content[0].topicList[i].title + '</h2></BR>');

            //var postdescdiv = $('<div class="post-description"></div>');
            var postdescdiv = $('<div class="blog-post-meta"></div>');
            var span3 = $('<span></span>');
            var postCount = forumData.content[0].topicList[i].postCounts + " " +localizeString("Label_Posts");
            var postdescp = $('<p style="word-break: break-all;"><i class="fa fa-comments-o"></i> ' + postCount + '</p>');
            var em = $('<em></em>').text('');;
            span3.append(postdescp, em);
            postdescdiv.append(span3);

            row12div.append(header2);
            row12div.append(postdescdiv);

            postheaderdiv.append(row12div);
            singlethreaddiv.append(postheaderdiv);
            outerlink.append(singlethreaddiv);
            coldiv.append(outerlink);
            $('#row').append(coldiv);
        }
    }

    /**
     * Go to Forum Topic Details Page
     */
    gotoForumTopicDetail = function (id, postCount) {
        localStorage.setItem('topic_id', id);
        localStorage.setItem('numOfForumTopicReplies', postCount);
        slideview("forum-topic-comments.html", 'script', 'left');
    }


    /**
     * Get Forum topic Details
     */
    getForumTopicDetails = function () {
        var actions = {
            "topicId": localStorage.getItem('topic_id')
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                displayTopicDetails(data.data);
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/forums_new/topic", actions, successCallback, errorCallback);
    }


    /**
     * Display the topic details for a specific topic
     * @topicDetails topic details fetched from the server.
     */
    displayTopicDetails = function (topicDetails) {
        if (enableDebugMode)
            alert("topicDetails = " + JSON.stringify(topicDetails));
        $('#topictitle').text(localizeString("Reply_Label_ReplyTo") + ": " + topicDetails.content[0].title);
        //.replace(/<\/?[^>]+>/ig, "").replace(/ /g, "%20").replace(/(?:\r\n|\r|\n)/g, '')
        $('#topiccontent').text(topicDetails.content[0].description.replace(/<\/?[^>]+>/ig, "").replace(/ /g, "%20").replace(/(?:\r\n|\r|\n)/g, '').replace(/%20/g, " "));
        $('#topiccount').html(topicDetails.content[0].postCounts + " " +localizeString("Reply_Label_Replies"));
        $('#replies').html("");
        displayForumTopicReplies(topicDetails.content[0].postList);
        $('#Reply_Button_Post').css('display', 'block');
    }


    /**
     * Add new topic to the forum
     * @topic_title_new new topic title
     * @topic_content_new new topic content
     */
    addTopic = function (topic_title_new, topic_content_new) {
        $("#titleerror").css("display", "none");
        $("#detailerror").css("display", "none");
        if (topic_title_new.trim() == "") {
            $("#titleerror").css("display", "block");
            $("#titleerror").html(localizeString("ValidationError_Tilte") + '<i class="form-help-icon icon">error</i>');
            if (topic_content_new.trim() == "") {
                $("#detailerror").css("display", "block");
                $("#detailerror").html(localizeString("ValidationError_TopicDetail") + '<i class="form-help-icon icon">error</i>');
                return false;
            }
            return false;
        }
        if (topic_content_new.trim() == "") {
            $("#detailerror").css("display", "block");
            $("#detailerror").html(localizeString("ValidationError_TopicDetail") + '<i class="form-help-icon icon">error</i>');
            return false;
        }

        $("#BlogDetail_Button_Close").click();
        var topic_title = topic_title;

        var actions = {
            "title": topic_title_new,
            "description": topic_content_new,
            "forumId": forum_id
        };
        $('#float-textarea').val('');
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Submit"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                var topictext = "Topics";
                if (data.data.length <= 1) {
                    topictext = 'Topic';
                }
                $('#float-textarea').text('');
                swal({
                    title: localizeString("Alert_Title_Success"),
                    type: "success",
                    text: data.message,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    getTopics(forum_id);
                });
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }

            window.plugins.spinnerDialog.hide();
        };

        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/forums_new/addtopic", actions, successCallback, errorCallback);
    }


    /**
     * Get Forum topic Replies
     * @pageNumber Current pagination page for replies
     */
    getTopicsReplies = function (pageNumber) {
        var topic_id = localStorage.topic_id;
        var actions = {
            "topicId": topic_id,
            "page_number": pageNumber
        };
        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                localStorage.setItem('numOfForumTopicReplies', data.data.content.comments_count);
                if (pageNumber == 1) {
                    $('#topiccount').html(data.data.content.comments_count + ' ' + localizeString("Reply_Label_Replies"));
                    $('#replies').html("");
                }

                if (data.data.content.comments.length > 0) {
                    displayForumTopicReplies(data.data.content.comments);
                }
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }

        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/forums_new/comments", actions, successCallback, errorCallback);
    }


    /**
     * Displays the Forum Topic Replies.
     * @replies list of replies fetched from the server
     */
    function displayForumTopicReplies(replies) {
        if (enableDebugMode)
            alert("replies List" + JSON.stringify(replies));
        for (i = 0; i < replies.length; i++) {
            if ((replies[i].photo != null) && (replies[i].photo != "") && (replies[i].photo != NaN) && (replies[i].photo != undefined)) {
                var divs = '<div class="seprate-div"><div class="post-header"><div><a class="topic-icon-placeholder link" href="#"><img src="data:image/jpeg;base64,' + replies[i].photo + '" alt=""/> </a></div><div class="col-xx-10 headers-col"><p class="header-text-content ">' + replies[i].author + ' <span>' + replies[i].createdOn + '</span></p></div></div><div class="row-12" style="word-break: break-all !important;"><div class="post-description" style="word-break: break-all !important;"><p style="word-break: break-all;">' + replies[i].details + '</p></div></div></div>';
                $('#replies').append(divs);
            } else {
                var divs = '<div class="seprate-div"><div class="post-header"><div><a class="topic-icon-placeholder link" href="#"><img src="images/avatar-001.jpg" alt=""/> </a></div><div class="col-xx-10 headers-col"><p class="header-text-content ">' + replies[i].author + ' <span>' + replies[i].createdOn + '</span></p></div></div><div class="row-12" style="word-break: break-all !important;"><div class="post-description" style="word-break: break-all !important;"><p style="word-break: break-all;">' + replies[i].details + '</p></div></div></div>';
                $('#replies').append(divs);
            }
        }
    }


    /**
     * Post New Reply to the Topic
     * @reply new reply
     */
    addTopicReply = function (reply) {
        var topic_id = localStorage.topic_id;

        $("#detailerror").css("display", "none");
        $("#detailerror").html('<i class="form-help-icon icon">error</i>');

        if (reply.trim() == "") {
            $("#detailerror").css("display", "block");
            $("#detailerror").html(localizeString("ValidationError_Comment") + '<i class="form-help-icon icon">error</i>');
            return false;
        }
        $("#Reply_Button_Close").click();
        var actions = {
            "comment": reply,
            "topicId": topic_id
        };
        $('#float-textarea').val('');

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Send"), localizeString("SpinDialog_Wait"), true);

        successCallback = function (data) {
            if (data.status == "success") {
                $('#float-textarea').text('');
                swal({
                    title: localizeString("Alert_Title_Success"),
                    type: "success",
                    text: data.message,
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    replies_page_number = 1;
                    getTopicsReplies(1); // call this with page number 1 so that all the replies gets updated
                });
            } else {
                var thisone = this;

                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
            window.plugins.spinnerDialog.hide();
        }
        errorCallback = function (data) {
        };
        postMessageToServerWithAuth("/forums_new/addcomment", actions, successCallback, errorCallback);
    }
}());