(function () {

    var enableDebugMode = 0;

    /**
     * Function fetches the localized string from Locale files and store it in local storage
     **/
    getLocalizedStrings = function() {
        //var localeArray = localStorage.getItem("LOCALIZED_STRINGS");

        //if(localeArray == null || localeArray == undefined) {
            /*Get the default value from config.js*/
            var defaultLocale = localStorage.getItem("DEAFULT_LOCALE");
            var suportedLocales = JSON.parse(localStorage.getItem("SUPPORTED_LOCALE"));
            var systemLocale = window.navigator.userLanguage || window.navigator.language;
            systemLocale = systemLocale.toLowerCase(); //Android fix

            /* For US load 'en-us' strings other 'en' strings*/
            if(systemLocale.substr(0,2).match("en")){
                if (!systemLocale.match("en-us")){
                   systemLocale = systemLocale.substr(0,2);
                }
            }
            //Thai langauage code 'th' or 'th-TH'
            if(systemLocale.substr(0,2).match("th") || systemLocale.substr(0,2).match("hi")){
                systemLocale = systemLocale.substr(0,2);
            }

            var appLocale = defaultLocale;

            /*Check whether current system locale is supported if yes make it current app Locale*/
            for (var j = 0; j < suportedLocales.length; j++) {
                if (suportedLocales[j].match(systemLocale)) {
                    appLocale = systemLocale;
                    break;
                }
            }

            /* Set up APP locale in the LS for further usage */
            localStorage.setItem("APP_LOCALE", appLocale);

            /* Fetch strings from locale files and store in in local storage */
            var myUrl = "bundle/" + appLocale + ".json";
            var localizedStrings = {};

            $.ajax({
                url: myUrl,
                dataType: 'json',
                async: false,
                data: null,
                success: function(data) {
                    $.each( data, function( key, val ) {
                        localizedStrings[key]=val;
                    });
                    localStorage.setItem("LOCALIZED_STRINGS", JSON.stringify(localizedStrings));
                }
            });
        //}
    }

    /**
     * Localize the elements with for the tag
     **/
    localize = function (tag) {
        var thelocalizedStrings = JSON.parse(localStorage.getItem("LOCALIZED_STRINGS"));
        var tagId = document.getElementById(tag);
        if (tagId != null)
            tagId.innerHTML = thelocalizedStrings[tag];
    }

    /**
     * Localize the parameter string
     **/
    localizeString = function (tag) {
        var thelocalizedStrings = JSON.parse(localStorage.getItem("LOCALIZED_STRINGS"));
        if (thelocalizedStrings != null)
            return thelocalizedStrings[tag];
        else
            return null;
    }

    /**
     * Localize the parameter array strings
     **/
    localizeArray = function (stringArray) {
        var thelocalizedStrings = JSON.parse(localStorage.getItem("LOCALIZED_STRINGS"));

        for (j = 0; j < stringArray.length; j++) {
            var tagId = stringArray[j];
            var element = document.getElementById(tagId);
            if (element != null)
                element.innerHTML = thelocalizedStrings[tagId];
        }
    }

    /**
     * Scan the current page for LocalizeElements and localize them all.
     **/
    localizePage = function () {
        var thelocalizedStrings = JSON.parse(localStorage.getItem("LOCALIZED_STRINGS"));
        var localElements = document.getElementsByName("LocalizeElement");
        var localMenuElements = document.getElementsByName("LocalizeMenuElement");
        var localMandatoryElements = document.getElementsByName("localizeMandatoryElements");

        for (i = 0; i < localElements.length; i++) {
            var tagId = localElements[i].id;
            var element = document.getElementById(tagId);
            if (element != null)
                element.innerHTML = thelocalizedStrings[tagId];
            else
                console.log("tagid1 = " + tagId + ", is null");
        }

        for (i = 0; i < localMenuElements.length; i++) {
            var tagId = localMenuElements[i].id;
            var element = document.getElementById(tagId);
            if (element != null)
                element.innerHTML += thelocalizedStrings[tagId];
            else
                console.log("tagid2 = " + tagId + ", is null");
        }

        for (i = 0; i < localMandatoryElements.length; i++) {
            var tagId = localMandatoryElements[i].id;
            var element = document.getElementById(tagId);
            if (element != null)
                element.innerHTML = thelocalizedStrings[tagId]+"*";
            else
                console.log("tagid3 = " + tagId + ", is null");
        }
    }

}());