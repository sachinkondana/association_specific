(function () {
    var enableDebugMode = 0;

    /* returned when there is no specific data received from the server */
    var noData = [];

    /* last updated time for the page content */
    var lastUpdatedOn;

    var themeFunctions = {
        "1": "renderMultipleParaThemeContents",
        "2": "renderImageDetailsGridViewContents",
        "3": "renderDetailedListViewContents", // Decorated List View
        "4": "renderSimpleListViewContents", // Simple List View
        "5": "renderImageGalleryViewContents",
        "6": "renderDropDownListViewContents",
        "7": "renderContactUsLayoutContents"
    };

    /* Template Id for the page content */
    var receivedTemplateId;

    fetchPage = function (pageId, lastUpdated, fetchSuccessCb) { /* might need another param isPublic, not Sure*/
        if (enableDebugMode)
            alert("PageId = " + pageId + " updated = " + lastUpdated);
        var actions = {
            "pageId": pageId,
            "lastUpdated": lastUpdated,
        };

        window.plugins.spinnerDialog.show(localizeString("SpinDialog_Update"), localizeString("SpinDialog_Wait"));

        successCallback = function (data) {
            window.plugins.spinnerDialog.hide();
            if (data.status == "success") {
                swal.close();
                if (enableDebugMode)
                    alert("Page = " + JSON.stringify(data.data));
                if (!("generic" in data.data)) { // if (data.data.length <= 0) {
                    return null;        // nothing recieved
                }
                if (data.data.generic.isUpdated == true) {
                    lastUpdatedOn = data.data.generic.lastUpdated;
                    receivedTemplateId = data.data.generic.templateId;

                    if (("specific" in data.data)) { //data.data.specific.length > 0
                        fetchSuccessCb(data.data.specific);
                        return;
                    } else {
                        fetchSuccessCb(noData);  // no specific data recieved, cases like details removed for specific cases
                        return;
                    }
                } else {
                    fetchSuccessCb(null);        // null indicates nothing has changed
                    return;
                }
            } else {
                window.plugins.spinnerDialog.hide();
                var thisone = this;
                swal({
                    title: localizeString("Alert_Title_Sorry"),
                    text: data.message,
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    confirmButtonText: localizeString("Alert_Button_Ok")
                },
                function () {
                    return;
                });
            }
        };
        errorCallback = function (data) {

        };
        postMessageToServerWithAuth("/page", actions, successCallback, errorCallback);
    }

    getLastUpdatedOn = function () {
        return lastUpdatedOn;
    }

    getTemplateRenderingFunction = function () {
        return themeFunctions.receivedTemplateId;
    }
}());
