(function () {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultGolfCoachesTemplateFunction = "renderImageDetailsGridViewContents";

    /* Page id for this page */
    var pageId = "18";

    var contentList = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "imageList" : [
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "John Doe",
                        "description" : ["Sr. Coach"]
                    }
                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Details Grid
     *  @param content: Must be as required for Image Details Grid
     */
    displayExistingContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("GolfCoachesTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultGolfCoachesTemplateFunction;
            localStorage.setItem("GolfCoachesTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("GolfCoachesTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("GolfCoachesTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getCoachesContent = function() {
        var lastUpd = localStorage.getItem("GolfCoachesLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("GolfCoachesLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentGolfCoachesContent = JSON.parse(localStorage.getItem("CurrentGolfCoachesContent"));
        if (currentGolfCoachesContent == null || currentGolfCoachesContent == undefined) {
            currentGolfCoachesContent = getStaticContent();
            localStorage.setItem("CurrentGolfCoachesContent", JSON.stringify(currentGolfCoachesContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentGolfCoachesContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchGolfCoachesSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchGolfCoachesSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentGolfCoachesContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("GolfCoachesLastUpdated", lastUpdateOn);
        }
    };

}());