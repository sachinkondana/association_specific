(function() {

    var enableDebugMode = 0;

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultAffiliatedClubTemplateFunction = "renderDropDownListViewContents";

    /* Page id for this page */
    var pageId = "5";

    var contentList = {
        "imageBase": "images/",
        "templateContent": [{
                "name": "Sherwood Country Club",
                "address": "",
                "telNo": "",
                "faxNo": "",
                "emailId": ""
            },
            {
                "name": "Brook Hollow Club",
                "address": "",
                "telNo": "",
                "faxNo": "",
                "emailId": ""
            },
            {
                "name": "Chicago Golf Club",
                "address": "",
                "telNo": "",
                "faxNo": "",
                "emailId": ""
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };


    /**
     *  Display Content based on Drop Down List View
     *  @param content: Must be as required for Drop Down List View
     */
    displayExistingContent = function(content) {
        /* call the rendering function for the selected template for rendered content */
        var themedContent = getThemedAffiliationContent(content);

        /* update the html with the rendered content */
        $("#user-list").html(themedContent);
    };


    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* call the rendering function for the selected template for rendered content */
        var themedContent = getThemedAffiliationContent(content);

        /* update the html with the rendered content */
        $("#user-list").html(themedContent);
    };


    /**
     * Generates themed content for club affiliations
     * based on the recieved content.
     */
    getThemedAffiliationContent = function(content) {
        var affiliationContent = '';
        if (content == null || content.templateContent == null ||
            content.templateContent.length <= 0) {
            affiliationContent = "<p> " + localizeString("Affiliation_Label_Nodata") + " </p>";
        } else {
            for (var i = 0; i < content.templateContent.length; i++) {
                if (content.templateContent[i].name != null && content.templateContent[i].name != undefined &&
                    content.templateContent[i].name.trim().length > 0) {
                    var letters = (content.templateContent[i].name).match(/\b(\w)/g);
                    var capletters = "";
                    if (letters != null && letters != undefined) {
                        capletters = letters.join('').toUpperCase();
                    } else {
                        var fletters = content.templateContent[i].name.split(" ");
                        for (var j = 0; j < fletters.length; j++)
                            capletters = capletters + fletters[j].substring(0, 1);
                    }
                    if (capletters.length > 4)
                        capletters = capletters.substring(0, 4);
                    affiliationContent = affiliationContent + getListDom(content.templateContent[i], content.imageBase, i);
                }
            }
            if (affiliationContent.trim() == '')
                affiliationContent = "<p> " + localizeString("Affiliation_Label_Nodata") + " </p>";
        }
        localStorage.setItem("AffiliationDetails", JSON.stringify(content.templateContent));
        localStorage.setItem("GOLF_IMG_PATH", content.imageBase);
        return affiliationContent;
    }

    function getListDom(listData, iImgUrl, id) {
        var courseName = listData['name'],
            clubName = listData['club_name'],
            city = listData['address'],
            country = listData['country'],
            logo = (listData['logo']) ? (iImgUrl + listData['logo']) : "./images/placeholder.png",
            golfCourseId = listData['id'];

        courseName = courseName.replace(/<(?:.|\n)*?>/gm, '');

        return '<div class="col-lg-4 col-md-6 course-row animated fadeIn" onclick="gotoAffiliationDetails(' + id + ')">\
                    <div class="course-row-left" style="background-image:url(' + logo + ');">\
                    </div>\
                    <div class="course-row-right">\
                        <div class="course-row-hdr clamp2">' + courseName + '</div>\
                        <div class="course-row-country clamp2">' + city + '</div>\
                    </div>\
                </div>';
    }


    /**
     * Go to details of specific affiliations
     * @index array index at which the affiliation is available
     */
    gotoAffiliationDetails = function(index) {
        localStorage.setItem("AffiliationDetailsIndex", index);
        slideview('affiliation-details.html', 'script', 'left');
    }


    /**
     * Render the Affilaition details specific to selected club
     */
    loadAffiliationDetails = function() {
        var clubDetails = JSON.parse(localStorage.getItem("AffiliationDetails"));
        var index = localStorage.getItem("AffiliationDetailsIndex");
        var CmsImgPath = localStorage.getItem("GOLF_IMG_PATH");
        // var td1 = $('<div></div>');
        var td1 = $('<div id="title">\
                    <div class="hdr">' + clubDetails[index].name + '</div>\
                    </div>');

        if ((clubDetails[index].userdefined != null) && (clubDetails[index].userdefined != undefined) &&
            (clubDetails[index].userdefined != "")) {
            var additionalDetails = clubDetails[index].userdefined.replace(/<br\s*\/?>/mg, "\n");
            additionalDetails = additionalDetails.replace(/<\/br\s*?>/mg, "");
            td1.append('<div class="dtl">' + additionalDetails + '</div>');
        }

        //var span1 = $('<span style="float:left; font-size:12px; margin-bottom:5px;"></span>');
        if (clubDetails[index].address != null && clubDetails[index].address != undefined && clubDetails[index].address.trim() != "") {
            var span1 = $('<span></span>');
            var address = "<strong>" + localizeString("AffiliationDetails_Label_Address") + "</strong>&nbsp;&nbsp;: " + clubDetails[index].address;
            span1.append(address, "<br/><br/>");
            td1.append(span1);
        }
        if (clubDetails[index].telNo != null && clubDetails[index].telNo != undefined && clubDetails[index].telNo.trim() != "") {
            var contactArr = clubDetails[index].telNo.trim().split(',');
            for (var telcnt = 0; telcnt < contactArr.length; telcnt++) {
                var span2 = $('<span onclick="gotocall(\'' + contactArr[telcnt] + '\');" target="_system"></span>');
                var ielem2 = $('<i class="fa fa-phone" aria-hidden="true"></i>');
                var phone = "&nbsp;&nbsp;: " + contactArr[telcnt];
                span2.append(ielem2, phone, "<br/><br/>");
                td1.append(span2);
            }
        }
        if (clubDetails[index].faxNo != null && clubDetails[index].faxNo != undefined && clubDetails[index].faxNo.trim() != "") {
            var span3 = $('<span></span>');
            var ielem3 = $('<i class="fa fa-fax" aria-hidden="true"></i>');
            var fax = "&nbsp;&nbsp;: " + clubDetails[index].faxNo;
            span3.append(ielem3, fax, "<br/><br/>");
            td1.append(span3);
        }
        if (clubDetails[index].website != null && clubDetails[index].website != undefined && clubDetails[index].website.trim() != "") {
            var webArr = clubDetails[index].website.trim().split(',');
            for (var webcnt = 0; webcnt < webArr.length; webcnt++) {
                var span4 = $('<span onclick="gotourl(\'' + webArr[webcnt].trim() + '\');" target="_system"></span>');
                var ielem4 = $('<i class="fa fa-globe" aria-hidden="true"></i>');
                var web = "&nbsp;&nbsp;: " + webArr[webcnt];
                span4.append(ielem4, web, "<br/><br/>");
                td1.append(span4);
            }
        }
        if (clubDetails[index].emailId != null && clubDetails[index].emailId != undefined && clubDetails[index].emailId.trim() != "") {
            var emailArr = clubDetails[index].emailId.trim().split(",");
            for (var emailcnt = 0; emailcnt < emailArr.length; emailcnt++) {
                var span5 = $('<span onclick="gotomail(\'' + emailArr[emailcnt].trim() + '\');" target="_system"></span>');
                var ielem5 = $('<i class="fa fa-envelope" aria-hidden="true"></i>');
                var emailId = "&nbsp;&nbsp;: " + emailArr[emailcnt];
                span5.append(ielem5, emailId, "<br/><br/>");
                td1.append(span5);
            }
        }
        if (clubDetails[index].facebookId != null && clubDetails[index].facebookId != undefined && clubDetails[index].facebookId.trim() != "") {
            var span6 = $('<span onclick="gotofacebook(\'' + clubDetails[index].facebookId.trim() + '\');" target="_system"></span>');
            var ielem6 = $('<i class="fa fa-facebook" aria-hidden="true"></i>');
            var facebookId = "&nbsp;&nbsp;: " + clubDetails[index].facebookId;
            span6.append(ielem6, facebookId, "<br/><br/>");
            td1.append(span6);
        }
        if (clubDetails[index].twitterHandle != null && clubDetails[index].twitterHandle != undefined && clubDetails[index].twitterHandle.trim() != "") {
            var span7 = $('<span onclick="gototwitter(\'' + clubDetails[index].twitterHandle.trim() + '\');" target="_system"></span>');
            var ielem7 = $('<i class="fa fa-twitter" aria-hidden="true"></i>');
            var twitterHandle = "&nbsp;&nbsp;: " + clubDetails[index].twitterHandle;
            span7.append(ielem7, twitterHandle, "<br/><br/>");
            td1.append(span7);
        }

        $('#buttontr').append(td1);
        $("#buttontr").css('display', 'block');

        //$("#userdefinedcontent").html("");

        if (clubDetails[index].logo && CmsImgPath) {
            var clubImg = CmsImgPath + clubDetails[index].image;
            $("#clubImg").attr("src", clubImg);
        }
    }


    /**
     *  Fetch contents and display
     */
    getAffiliationContent = function() {
        var lastUpd = localStorage.getItem("AffiliatedClubLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("AffiliatedClubLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentAffiliatedClubContent = JSON.parse(localStorage.getItem("CurrentAffiliatedClubContent"));
        if (currentAffiliatedClubContent == null || currentAffiliatedClubContent == undefined) {
            currentAffiliatedClubContent = getStaticContent();
            localStorage.setItem("CurrentAffiliatedClubContent", JSON.stringify(currentAffiliatedClubContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentAffiliatedClubContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchAffiliatedClubSuccess);
    };


    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchAffiliatedClubSuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentAffiliatedClubContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("AffiliatedClubLastUpdated", lastUpdateOn);
        }
    };
}());