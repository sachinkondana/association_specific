(function() {

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultClubRuleTemplateFunction = "renderDetailedListViewContents";

    /* Page id for this page */
    var pageId = "6";

    var listContent = {
        "imageBase": "images/",
        "templateContent": [{
            "title": "Golf Association",
            "description": "",
            "descriptionList": [
                "Team Golf may charge a service charge on all past due balances in the amount of one and one-half (1.5%) per month from the date of the statement until paid in full."
            ]
        }]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return listContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Detailed List View Theme
     *  @param content: Must be as required for Detailed List View Theme
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubRuleTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultClubRuleTemplateFunction;
            localStorage.setItem("ClubRuleTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubRuleTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("ClubRuleTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        if (enableDebugMode)
            alert("themedContent = " + JSON.stringify(themedContent));

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        /*if (isStaticContent == 1)
        {
            var content = getStaticContent();
            displayContent(content);
        }*/
        var lastUpd = localStorage.getItem("ClubRuleLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("ClubRuleLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentClubRuleContent = JSON.parse(localStorage.getItem("CurrentClubRuleContent"));
        if (currentClubRuleContent == null || currentClubRuleContent == undefined) {
            currentClubRuleContent = getStaticContent();
            localStorage.setItem("CurrentClubRuleContent", JSON.stringify(currentClubRuleContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentClubRuleContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchClubRuleSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchClubRuleSuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentClubRuleContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("ClubRuleLastUpdated", lastUpdateOn);
        }
    };
}());