(function () {

    var isStaticContent = 1;

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultClubRuleTemplateFunction = "renderDetailedListViewContents";

    /* Page id for this page */
    var pageId = "7";

    var listContent = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "title" : "Changing Room ",
                "descriptionList" :
                [
                    "1.1 Unless otherwise determined by the Management from time to time, the changing room facilities shall be opened daily at such hour as shall be posted at the respective facilities.",
                    "1.2 The Management may at any time, close the changing rooms or any part thereof for any reason whatsoever, but not all at one time.",
                    "1.3 No smoking, eating or drinking is allowed in the Locker Rooms/Changing Rooms.",
                    "1.4 Boys over four (4) years shall not be allowed in any of the Ladies\' Changing Room.",
                    "1.5 Girls of any age are not allowed in any of the Men\'s Changing Room.",
                    "1.6 Club locker key are available at the Changing Rooms Counter and must be returned to the counter failing which a fee shall be levied for the same.",
                    "1.7 Club towels are available at the Changing Rooms Counter and must be returned to the counter failing which a fee shall be levied for the same. ",
                    "1.8 Each Members and Guest is entitled to sign out a maximum of one (1) towel per day free of charge. Extra towels will be available at a fee to be determined by the Management.",
                    "1.9 Storage of foodstuff, weapons, banned publications, and/or banned drugs in the Changing Room (lockers) are prohibited.",
                    "1.10 The Management shall be entitled to charge a fee for replacement of lost or missing locker key.",
                    "1.11 All lockers are inspected on a daily basis at 8pm. Any item(s) found will be removed, and the Management shall not be liable in any manner for any loss/damages.",
                    "1.12 Members must produce their Membership Cards for verification and registration by the attendant at the Changing Room Counter before issuance of towels and Locker keys.",
                    "1.13 Articles deposited in the locker room or left in the changing rooms shall be entirely at the risk of the Members and the Management shall not be responsible for loss or damage to any articles to deposit therein.",
                    "1.14 The Club will retain in its custody, the duplicate locker keys and the use of the duplicate keys will only be made in the presence of the Member to whom the locker is allotted."                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return listContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Detailed List View Theme
     *  @param content: Must be as required for Detailed List View Theme
     */
    displayExistingContent = function(content) {

        if (enableDebugMode)
            alert("content1 = " + JSON.stringify(content));

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubRuleTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultClubRuleTemplateFunction;
            localStorage.setItem("ClubRuleTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubRuleTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("ClubRuleTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {

        var lastUpd = localStorage.getItem("ClubRuleLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("ClubRuleLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentClubRuleContent = JSON.parse(localStorage.getItem("CurrentClubRuleContent"));
        if (currentClubRuleContent == null || currentClubRuleContent == undefined) {
            currentClubRuleContent = getStaticContent();
            localStorage.setItem("CurrentClubRuleContent", JSON.stringify(currentClubRuleContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentClubRuleContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchClubRuleSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchClubRuleSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentClubRuleContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("ClubRuleLastUpdated", lastUpdateOn);
        }
    };
}());