(function () {

    var enableDebugMode = 0;

    /**
     * Template ID: DDLV-001: Drop Down List View
     *  Theme Consisting of List of item having description where description id hidden until clicked
     *          --------------------------------------------------------
     *          |                                                      |
     *          |    --------------------------------------------      |
     *          |    | 1. Title Data 1                          |      |
     *          |    --------------------------------------------      |
     *          |    | 2. Title Data 2                          |      |
     *          |    ---------------------------------- ---------      |
     *          |    |    Additional details of Data 2          |      |
     *          |    |    Additional details of Data 2          |      |
     *          |    --------------------------------------------      |
     *          |    | 3. Title Data 3                          |      |
     *          |    --------------------------------------------      |
     *          --------------------------------------------------------
     */

    var  mainLayout = '                                                                             \
        <ul id="accordion" class="accordion">                                                       \
            @listItems                                                                              \
        </ul>';

    var listItemsLayout = '                                                                         \
        <li style="margin-bottom:0px; margin-top: -1px;">                                           \
            <div class="link-01">                                                                   \
                <div> @title </div><i class="fa fa-angle-down mdi-chevron-down"></i></div>          \
            <ul class="submenu">                                                                    \
                <li>                                                                                \
                    <div style="word-wrap: break-word;">                                            \
                        @subItemsLayoutList                                                         \
                    </div>                                                                          \
                </li>                                                                               \
            </ul>                                                                                   \
        </li>';

    var subItemsLayout = '                                                                          \
        <span style="display:block;"><b> @subItemTitle </b> @subItemData </span>                    \
        ';

    /**
     *  Renders the content as per the Detailed List View Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *  [
     *      {
     *          “name” : “name”,
     *          “address” : “<title>”,
     *          “telNo” : “Telephone Number”,
     *          “faxNo” : “Fax Number”,
     *          “website” : “Website Link”,
     *          “emailId” : “E-mail ID”,
     *          “facebookId” : “Facebook Page ID”,
     *          “twitterHandle” : “Twitter Handle”,
     *       },
     *       {
     *          “name” : “name”,
     *          “address” : “<title>”,
     *          “telNo” : “Telephone Number”,
     *          “faxNo” : “Fax Number”,
     *          “website” : “Website Link”,
     *          “emailId” : “E-mail ID”,
     *          “facebookId” : “Facebook Page ID”,
     *          “twitterHandle” : “Twitter Handle”,
     *       },
     *  ]
     *  @returns rendered content as per theme
     */
    renderDropDownListViewContents = function(detailsList) {
        if (enableDebugMode)
            alert("Creating New Theme Layout: " + JSON.stringify(detailsList));

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';


        if (detailsList != null && detailsList != undefined && detailsList.templateContent != null && detailsList.templateContent != undefined)
        {
            contentLayout += mainLayout;

            var elementLayoutList= '';
            for (var i=0; i < detailsList.templateContent.length; i++)
            {
                if (detailsList.templateContent[i].name != "" && detailsList.templateContent[i].name != null) {
                    elementLayoutList += listItemsLayout;
                    elementLayoutList = elementLayoutList.replace("@title", detailsList.templateContent[i].name);

                    var subItems = '';
                    if (enableDebugMode)
                        alert("Data: " + JSON.stringify(detailsList.templateContent[i]));

                    listData = detailsList.templateContent[i];

                    for ( var prop in listData ) {

                        if (prop === "address")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Adr: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "telNo")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Tel No: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "faxNo")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Fax No: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "website")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Website: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "emailId")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "E-mail ID: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "facebookId")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Facebook ID: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                        else if (prop === "twitterHandle")
                        {
                            subItems += subItemsLayout;
                            subItems = subItems.replace("@subItemTitle", "Twitter Handle: ");
                            subItems = subItems.replace("@subItemData", listData[prop]);
                        }
                    }

                    elementLayoutList = elementLayoutList.replace("@subItemsLayoutList", subItems);
                }
            }

            contentLayout = contentLayout.replace("@listItems", elementLayoutList);
        }

        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        return contentLayout;
    };

}());