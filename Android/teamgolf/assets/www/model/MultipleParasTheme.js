(function() {

    var enableDebugMode = 0;

    /**
     * Multi Param Theme:
     *  Theme Consisting of an image extending full width and multiple params below it
     *          --------------------------------------------------------
     *          |               ******* IMAGE ********                 |
     *          |                                                      |
     *          |                       Para1                          |
     *          |                                                      |
     *          |                       Para2                          |
     *          |                                                      |
     *          |                       Para3                          |
     *          --------------------------------------------------------
     */
    var themeLayout = '                                                                             \
        <p style="text-align:center; padding-top:0px; margin:0px;">                                 \
            <img src="@image1" class="no-img-popup fitImage" width="100%" style="display:@dispst"> \
        </p>                                                                                        \
        <table width="100%" border="0" cellspacing="15" cellpadding="00">                           \
            <tr>                                                                                    \
                <td class="text_p">                                                                 \
                    @DataContent                                                                    \
                </td>                                                                               \
            </tr>                                                                                   \
        </table> ';

    /**
     *  Renders the content as per the set Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *      {
     *          "image": "<PATH TO IMAGE>",
     *          "paraContent" : [ <array of para with each element being one para> ]
     *      }
     *
     *  @returns rendered content as per theme
     */
    renderMultipleParaThemeContents = function(pageContent) {

        var displayLayout;
        if (pageContent.templateContent != undefined && pageContent.templateContent != null) {
            var imagePath = "";
            if (pageContent.templateContent.image != null && pageContent.templateContent.image != undefined &&
                pageContent.templateContent.image != "")
                imagePath = pageContent.imageBase + pageContent.templateContent.image;
            displayLayout = themeLayout.replace("@image1", imagePath);
            if (imagePath != "")
                displayLayout = displayLayout.replace("@dispst", "block");
            else
                displayLayout = displayLayout.replace("@dispst", "none");

            var data = "";
            for (var i = 0; i < pageContent.templateContent.paraContent.length; i++) {
                data += pageContent.templateContent.paraContent[i];
                data += "<BR/><BR/>";
            }

            displayLayout = displayLayout.replace("@DataContent", data);
        } else {
            var imagePath = "";
            if (pageContent.image != "" && pageContent.image != undefined && pageContent.image != null)
                imagePath = pageContent.image;
            displayLayout = themeLayout.replace("@image1", imagePath);
            if (imagePath != "")
                displayLayout = displayLayout.replace("@dispst", "block");
            else
                displayLayout = displayLayout.replace("@dispst", "none");
            var data = "";
            if (pageContent.paraContent !== null && pageContent.paraContent != undefined) {
                for (var i = 0; i < pageContent.paraContent.length; i++) {
                    data += pageContent.paraContent[i];
                    data += "<BR/><BR/>";
                }
            } else {
                data += '<div style="width:100%; margin:0 auto; text-align:center">                   \
                            <p style="font-weight:bold;" align="center"> Details Not Available </p>   \
                        </div> ';
            }

            displayLayout = displayLayout.replace("@DataContent", data);
        }

        if (enableDebugMode)
            alert("New Theme Layout: " + displayLayout);

        return displayLayout;
    };

}());