(function () {

    var enableDebugMode = 0;

    /**
     * Template ID: RIT-001: Report Issue Page
     *  Theme Consisting of details, form and additional Reporting details
     *          --------------------------------------------------------
     *          |                                                      |
     *          |       Details                                        |
     *          |                                                      |
     *          |       ----------------------------------------       |
     *          |       |                                      |       |
     *          |       |       ***** FORM  ******             |       |
     *          |       ----------------------------------------       |
     *          |                                                      |
     *          |       Report Details:                                |
     *          --------------------------------------------------------
     */
    var displayStyle = 'style="display: none;" ';

    // No Configurable Content in present design

    var messageDetailedLayout = '                                                                   \
        <div>                                                                                       \
            <table width="100%" border="0" cellspacing="15" cellpadding="00">                       \
                <tr><td>                                                                            \
                    <p align="justify"> @MessageContent </p>                                        \
                </td></tr>                                                                          \
            </table>                                                                                \
        </div> ';

    //Form Layout - Non modifiable
    var formLayout = '                                                                                  \
        <div>                                                                                           \
        <table width="100%" border="0" cellspacing="15" cellpadding="00">                               \
            <tr>                                                                                        \
                <td>                                                                                    \
                    <ul id="list-table">                                                                \
                        <li>                                                                            \
                            <h4 class="bold-match-color" style="margin-top:0px;">'+localizeString("Report_Label_IssueDetail")+':</h4> \
                        </li>                                                                           \
                        <li>                                                                            \
                            <fieldset>                                                                  \
                                <div class="form-group form-group-label control-highlight">             \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" for="float-text">'+localizeString("ClassifiedDetail_Label_Title")+'*'+'</label>    \
                                            <input class="form-control" id="float-text" type="text">    \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="namesuccess"> \
                                                <i class="form-help-icon icon">check</i>                \
                                            </span>                                                     \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="nameerror">  \
                                                <i class="form-help-icon icon">error</i>                \
                                            </span>                                                     \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                                <div class="form-group form-group-label control-highlight">             \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" for="float-textarea">'+localizeString("Label_Description")+'*'+'</label>  \
                                            <textarea class="form-control" id="float-textarea" rows="5"></textarea> \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="msgsuccess">  \
                                                <i class="form-help-icon icon">check</i>                \
                                            </span>                                                     \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="msgerror">  \
                                                <i class="form-help-icon icon">error</i>                \
                                            </span>                                                     \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                            </fieldset>                                                                 \
                        </li>                                                                           \
                        <li style="margin-top:0px;">                                                    \
                            <a href="javascript:void(0);" class="btn btn-alt" onclick="sendInfo();">'+localizeString("Account_Button_Submit")+'</a>  \
                        </li>                                                                           \
                    </ul>                                                                               \
                </td>                                                                                   \
            </tr>                                                                                       \
        </table>                                                                                        \
        </div> ';


    /**
     *  Renders the content as per the Report Issue Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *  {
     *      "message" : "message displayed as para before Form",
     *  }
     *  @returns rendered content as per theme
     */
    renderReportIssueLayoutContents = function (issueDetails) {

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';

        // Add Message details is available
        if (issueDetails.message != null && issueDetails.message != "")
        {
            contentLayout += messageDetailedLayout;
            contentLayout = contentLayout.replace("@MessageContent", issueDetails.message);
        }

        // Add the mandatory Form
        contentLayout += formLayout;

        return contentLayout;
    };

    sendInfo = function () {

        var username = localStorage.getItem("USER_NAME");
        var password = localStorage.getItem("PASSWORD");
        var communeid = localStorage.getItem("COMMUNE_ID");
        var serverurl = localStorage.getItem("SERVER_URL");
        var deviceid = localStorage.getItem("DEVICE_ID");
        var deviceos = localStorage.getItem("DEVICE_OS");
        var appid = localStorage.getItem("APP_ID");
        var title = document.getElementById("float-text").value.trim();
        //var email = document.getElementById("float-text-value").value.trim();
        var message = document.getElementById("float-textarea").value.trim();

        //var emailValid = emailValidation(email);
        if (title != "" && message != "") { //&& emailValid
            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
            var actions = null;

            if (localStorage.isLoggedIn != null && localStorage.isLoggedIn == "true") {
                actions = {
                    "title" : title,
                    "name": localStorage.first_name,
                    "email": localStorage.getItem("email"),
                    "phone": "",
                    "message": message,
                    "deviceid": deviceid,
                    "deviceos": deviceos,
                    "appid": appid,
                    "locale": localStorage.getItem("APP_LOCALE")
                };
            } else {
                actions = {
                    "title" : title,
                    "name": "",
                    "email": "",
                    "phone": "",
                    "message": message,
                    "deviceid": deviceid,
                    "deviceos": deviceos,
                    "appid": appid,
                    "locale": localStorage.getItem("APP_LOCALE")
                };
            }

            $.ajax({
                url: serverurl + "/contactus",
                type: "POST",
                data: actions,
                dataType: "json",
                headers: {
                    "Authorization": "Basic " + btoa(username + ":" + password),
                    "COMMUNE_ID": "" + communeid,
                    "compatibility_version": "" + localStorage.getItem("BackendCompatiabilityVersion")
                },
                success: function (data) {

                    window.plugins.spinnerDialog.hide();
                    if (data.status == "success") {
                        swal({
                            title: localizeString("Alert_Title_Success"),
                            text: data.message,
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function () {
                            document.getElementById("float-textarea").value = "";
                            document.getElementById("float-text").value = "";
                            return;
                        });
                    } else {
                        var thisone = this;
                        swal({
                            title: localizeString("Alert_Title_Sorry"),
                            text: data.message,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function () {
                            return;
                        });
                    }
                },
                error: function (jqXHR, exception) {
                    window.plugins.spinnerDialog.hide();

                    if (jqXHR.status === 0) {
                        //swal.close();
                        var thisone = this;

                        swal({
                            title: localizeString("Alert_Title_Connectivity"),
                            text: localizeString("Alert_Text_Connectivity"),
                            type: "info",
                            showCancelButton: true,
                            closeOnCancel: true,
                            allowOutsideClick: false,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_TryAgain"),
                            cancelButtonText:localizeString("Alert_Button_Cancel")
                        },
                        function () {
                            $.ajax(thisone);
                            return;
                        });
                    } else if (jqXHR.status === 401) {
                        resignin();
                    } else if (jqXHR.status === 426) {
                        swal({
                            title: localizeString("Alert_Title_Unsupported"),
                            text: jqXHR.responseText,
                            type: "warning",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            allowOutsideClick: false,
                            confirmButtonText: localizeString("Alert_Button_Ok")
                        },
                        function () {
                            return;
                        });
                    } else {
                        var thisone = this;
                        swal({
                            title: localizeString("Alert_Title_Something"),
                            text: localizeString("Alert_Text_Something"),
                            type: "info",
                            showCancelButton: true,
                            closeOnCancel: true,
                            allowOutsideClick: false,
                            closeOnConfirm: true,
                            showLoaderOnConfirm: true,
                            confirmButtonText: localizeString("Alert_Button_TryAgain"),
                            cancelButtonText:localizeString("Alert_Button_Cancel")
                        },
                        function () {
                            $.ajax(thisone);
                            return;
                        });
                    }
                }
            });
        } else {
            if (title == "" && message != "") { // && email != ""
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_TitleBlank")+'<i class="form-help-icon icon">error</i>');
            } else if (title != "" && message == "") { //&& email != ""
                $("#msgerror").css("display", "block");
                $("#msgerror").html(localizeString("ValidationError_IssueDescriptionBlank")+'<i class="form-help-icon icon">error</i>');
            } else {
                // window.plugins.toast.showLongBottom('Please enter the details', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)});
                $("#msgerror").css("display", "block");
                $("#msgerror").html(localizeString("ValidationError_IssueDescriptionBlank")+'<i class="form-help-icon icon">error</i>');

                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_TitleBlank")+'<i class="form-help-icon icon">error</i>');
            }
        }
    }

    emailValidation = function (email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    if ( /iPhone|iPad|iPod/i.test(navigator.userAgent) ) {
        $("input").on('focus', function(e) {
            $('#head').addClass('fixfixed');
            $('#copyright_div').css("display", "none");
        });

        $("input").on('blur', function(e) {
            $('#head').removeClass('fixfixed');
            $('#copyright_div').css("display", "block");
        });

        $("textarea").on('focus', function(e) {
            $('#head').addClass('fixfixed');
            $('#copyright_div').css("display", "none");
        });

        $("textarea").on('blur', function(e) {
            $('#head').removeClass('fixfixed');
            $('#copyright_div').css("display", "block");
        });
    }
}());