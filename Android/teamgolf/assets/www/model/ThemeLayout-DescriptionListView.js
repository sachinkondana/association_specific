(function () {

    var enableDebugMode = 0;

    /**
     *  This theme is a combination of Multiple Para Theme and Detailed List View Component which
     * will be rendered as a single para
     */

    /**
     * Detailed List View:
     *  Theme Consisting of Title followed by list consisting of detailed description in list format
     *          --------------------------------------------------------
     *          | Title1                                               |
     *          |   Description (can be multi para)                    |
     *          |   1. List 1 - Title                                  |
     *          |   * Sub-list - 1                                     |
     *          |      * Sub-list-Item 1                               |
     *          |      * Sub-list-Item 2                               |
     *          |   * Sub-list - 2                                     |
     *          |      * Sub-list-Item 1                               |
     *          |      * Sub-list-Item 2                               |
     *          |                                                      |
     *          |   2. List 2 - Title                                  |
     *          |   * Sub-list - 1                                     |
     *          |      * Sub-list-Item 1                               |
     *          |      * Sub-list-Item 2                               |
     *          |   * Sub-list - 2                                     |
     *          |      * Sub-list-Item 1                               |
     *          |      * Sub-list-Item 2                               |
     *          |                                                      |
     *          | Addition Details displayed in a para                 |
     *          --------------------------------------------------------
     */
    //style="margin-left: 0; padding:0"
    var  mainLayout = '                                                                             \
        <ol style="margin-left: 0; padding:10px">                                                   \
            @ListItems                                                                              \
        </ol>                                                                                       \
    ';

    //style="padding:0"
    var elementLayout = '                                                                           \
        <li style="padding-bottom:10px;"> <b> @title </b>                                           \
            <ul @displayStyle >                                                                     \
                @SubListItems                                                                       \
            </ul>                                                                                   \
        </li>';

    var subListItemLayout ='                                                                        \
        <li> @data </li>                                                                            \
        ';

    var displayStyle = 'style=" display:none;" ';

    /**
     *  Renders the content as per the Detailed List View Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *          {
     *              "image" : "<Path to image>"
     *              "mainTitle": "Title1",
     *              "mainDescription" : "Description1"
     *              "detailsList" : [
     *                  {
     *                      "title": "Title2",
     *                      "subList" : [ <list of items> ]
     *                  },
     *                  {
     *                      "title": "Title2",
     *                      "subList" : [ <list of items> ]
     *                  }
     *              ]
     *          }
     *  @returns rendered content as per theme
     */
    renderListViewContents = function(detailsList) {

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';

        contentLayout += mainLayout;

        var elementLayoutList= '';
        for (var i=0; i < detailsList.length; i++)
        {
            elementLayoutList += elementLayout;

            //Replace the List
            if (detailsList[i].title != null && detailsList[i].title != "")
            {
                elementLayoutList = elementLayoutList.replace("@title", detailsList[i].title);
            }
            else
            {
                //TODO: Return Error ?? Because this is a must field
                elementLayoutList = elementLayoutList.replace("@title", "");
            }

            var subListItemList = '';
            if (detailsList[i].sublist != null && detailsList[i].sublist.length > 0)
            {
                for (var j=0; j < detailsList[i].sublist.length; j++)
                {
                    if (detailsList[i].sublist[j] != null && detailsList[i].sublist[j] != "" ) {
                        subListItemList += subListItemLayout;
                        subListItemList = subListItemList.replace("@data", detailsList[i].sublist[j]);
                    }
                }
                elementLayoutList = elementLayoutList.replace("@SubListItems", subListItemList);
                elementLayoutList = elementLayoutList.replace("@displayStyle", "");
            }
            else
            {
                elementLayoutList = elementLayoutList.replace("@SubListItems", "");
                elementLayoutList = elementLayoutList.replace("@displayStyle", displayStyle);
            }
        }

        contentLayout = contentLayout.replace("@ListItems", elementLayoutList);


        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        return contentLayout;
    };

    /**
     *  Renders the content as per the Detailed List View Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *          {
     *              "image" : "<Path to image>"
     *              "mainTitle": "Title1",
     *              "mainDescription" : "Description1",
     *              "detailsList" : [
     *                  {
     *                      "title": "Title2",
     *                      "subList" : [ <list of items> ]
     *                  },
     *                  {
     *                      "title": "Title2",
     *                      "subList" : [ <list of items> ]
     *                  }
     *              ],
     *              “subDescription” : “Additional Description”
     *          }
     *  @returns rendered content as per theme
     */
    renderSimpleListViewContents = function(contents) {

        var paraContents = [];
        var k = 0;

        if (contents != null && contents.templateContent != null) {
            if (contents.templateContent.mainTitle != null && contents.templateContent.mainTitle.trim() != "")
            {
                paraContents[k++] = '<b>' + contents.templateContent.mainTitle + '</b>';
            }

            if (contents.templateContent.mainDescription != null && contents.templateContent.mainDescription.trim() != "")
            {
                paraContents[k++] = contents.templateContent.mainDescription;
            }

            if (contents.templateContent.detailsList != null && contents.templateContent.detailsList != undefined
                && contents.templateContent.detailsList.length > 0)
                paraContents[k++] = renderListViewContents(contents.templateContent.detailsList);

            if (contents.templateContent.subDescription != null && contents.templateContent.subDescription.trim() != "")
            {
                paraContents[k++] = contents.templateContent.subDescription;
            }

            var contentForParaLayout = {};
            contentForParaLayout["templateContent"] = {};
            if (contents.templateContent.image != null && contents.templateContent.image != "") {
                var imagePath = contents.imageBase + contents.templateContent.image;
                contentForParaLayout["imageBase"] = contents.imageBase;
                contentForParaLayout.templateContent.image = contents.templateContent.image;
            } else  {
                contentForParaLayout.templateContent.image = null;
            }
            contentForParaLayout.templateContent.paraContent = paraContents;

            if (enableDebugMode)
                alert("contentForParaLayout: " + JSON.stringify(contentForParaLayout));

            return renderMultipleParaThemeContents(contentForParaLayout);
        } else {
            var contentLayout = '<div style="width:100%; margin:0 auto; padding-top:10px; text-align:center"> \
                                    <p style="font-weight:bold;"> Details Not Available </p>                  \
                                </div> ';
            return contentLayout;
        }
    };

}());