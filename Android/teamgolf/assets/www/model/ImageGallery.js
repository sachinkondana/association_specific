(function () {

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultImageGalleryTemplateFunction = "renderImageGalleryViewContents";

    /* Page id for this page */
    var pageId = "9";

    var content = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "imagePath" : "gal1.jpg",
                "title" : "",
                "description" : ""
            },
            {
                "imagePath" : "gal2.jpg",
                "title" : "",
                "description" : ""
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return content;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    loadImagesAsync = function (imgindx, maxlen) {
        var image = document.getElementById("galimg_"+imgindx);
        var downloadingImage = new Image();
        downloadingImage.onload = function(){
            image.src = this.src;
            $("#galimg_"+imgindx).addClass("gly-popup");
            imgindx++;
            if  (imgindx < maxlen)
                loadImagesAsync(imgindx, maxlen);
        };
        downloadingImage.src = $("#galimg_" + imgindx).attr("data-src");
    }

    downloadImages = function (maxLen) {
        loadImagesAsync(0, maxLen);
    }

    /**
     *  Display Content based on Image Gallery View
     *  @param content: Must be as required for Image Gallery View
     */
    displayExistingContent = function(content) {

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ImageGalleryTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultImageGalleryTemplateFunction;
            localStorage.setItem("ImageGalleryTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);

        if (content.templateContent && content.templateContent.length > 0)
            setTimeout(downloadImages, 500, content.templateContent.length);

        if (typeof oldIE === 'undefined' && Object.keys)
            hljs.initHighlighting();
        baguetteBox.run('.baguetteBoxOne', {
            captions: function(element) {
                return element.getElementsByTagName('img')[0].alt;
            }
        });
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ImageGalleryTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("ImageGalleryTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);

        if (content.templateContent && content.templateContent.length > 0)
            setTimeout(downloadImages, 500, content.templateContent.length);

        if (typeof oldIE === 'undefined' && Object.keys)
            hljs.initHighlighting();
        baguetteBox.run('.baguetteBoxOne', {
            captions: function(element) {
                return element.getElementsByTagName('img')[0].alt;
            }
        });
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        var lastUpd = localStorage.getItem("ImageGalleryLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("ImageGalleryLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentImageGalleryContent = JSON.parse(localStorage.getItem("CurrentImageGalleryContent"));
        if (currentImageGalleryContent == null || currentImageGalleryContent == undefined) {
            currentImageGalleryContent = getStaticContent();
            localStorage.setItem("CurrentImageGalleryContent", JSON.stringify(currentImageGalleryContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentImageGalleryContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchImageGallerySuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchImageGallerySuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentImageGalleryContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("ImageGalleryLastUpdated", lastUpdateOn);
        }
    };
}());
