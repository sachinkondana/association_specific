(function () {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultClubStaffTemplateFunction = "renderImageDetailsGridViewContents";

    /* Page id for this page */
    var pageId = "13";

    var contentList = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "imageList" : [
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Toni Scott",
                        "description" : ["Marketing and Membership Director"]
                    },
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Micheal Davison",
                        "description" : ["Accountant"]
                    }
                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Details Grid
     *  @param content: Must be as required for Image Details Grid
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubStaffTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultClubStaffTemplateFunction;
            localStorage.setItem("ClubStaffTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ClubStaffTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("ClubStaffTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        var lastUpd = localStorage.getItem("ClubStaffLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("ClubStaffLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentClubStaffContent = JSON.parse(localStorage.getItem("CurrentClubStaffContent"));
        if (currentClubStaffContent == null || currentClubStaffContent == undefined) {
            currentClubStaffContent = getStaticContent();
            localStorage.setItem("CurrentClubStaffContent", JSON.stringify(currentClubStaffContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentClubStaffContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchClubStaffSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchClubStaffSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentClubStaffContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("ClubStaffLastUpdated", lastUpdateOn);
        }
    };

}());