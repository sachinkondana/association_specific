(function() {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultAboutUsTemplateFunction = "renderMultipleParaThemeContents";

    /* Page id for this page */
    var pageId = "3";

    var aboutContent = {
        "imageBase": "images/",
        "templateContent": {
            "image": "",
            "paraContent": [
                "We are based out of India and have been in operation since August, 2017. We own and operate Team Golf which has a 100 golf clubs.",
                "Team Golf has over 50 ownership clubs across world. All clubs have multiple dining options, tennis court, squash courts, spas, salon and business conference facilities."
            ]
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch content from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Multiple Para Theme
     *  @param content: Must be as required for Multiple Para Theme
     */
    displayExistingContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("AboutUsTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultAboutUsTemplateFunction;
            localStorage.setItem("AboutUsTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("AboutUsTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("AboutUsTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        /*if (isStaticContent == 1)
        {
            var content = getStaticContent();
            displayContent(content);
        }*/
        var lastUpd = localStorage.getItem("AboutUsLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("AboutUsLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentAboutUsContent = JSON.parse(localStorage.getItem("CurrentAboutUsContent"));
        if (currentAboutUsContent == null || currentAboutUsContent == undefined) {
            currentAboutUsContent = getStaticContent();
            localStorage.setItem("CurrentAboutUsContent", JSON.stringify(currentAboutUsContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentAboutUsContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchAboutUsSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchAboutUsSuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentAboutUsContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("AboutUsLastUpdated", lastUpdateOn);
        }
    };

}());