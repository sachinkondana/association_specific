(function () {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultAdministrationTemplateFunction = "renderImageDetailsGridViewContents";

    /* Page id for this page */
    var pageId = "17";

    var contentList = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "imageList" : [
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Mr. Jack Wong",
                        "description" : ["CEO"]
                    },{
                        "image" : "avatar-001.jpg",
                        "title" : "Mr. Vivesrarao @ Rao",
                        "description" : ["General Manager"]
                    }
                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Details Grid
     *  @param content: Must be as required for Image Details Grid
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("AdministrationTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultAdministrationTemplateFunction;
            localStorage.setItem("AdministrationTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("AdministrationTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("AdministrationTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html("");
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        /*if (isStaticContent == 1)
        {
            var content = getStaticContent();
            displayContent(content);
        }*/
        var lastUpd = localStorage.getItem("AdministrationLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("AdministrationLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentAdministrationContent = JSON.parse(localStorage.getItem("CurrentAdministrationContent"));
        if (currentAdministrationContent == null || currentAdministrationContent == undefined) {
            currentAdministrationContent = getStaticContent();
            localStorage.setItem("CurrentAdministrationContent", JSON.stringify(currentAdministrationContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentAdministrationContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchAdministrationSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchAdministrationSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentAdministrationContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("AdministrationLastUpdated", lastUpdateOn);
        }
    };

}());
