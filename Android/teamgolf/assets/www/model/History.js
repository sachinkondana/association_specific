(function() {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultHistoryTemplateFunction = "renderMultipleParaThemeContents";

    /* Page id for this page */
    var pageId = "4";

    var aboutContent = {
        "imageBase": "images/",
        "templateContent": {
            "image": "",
            "paraContent": [
                "Founded in 2017 with a course layout designed by Jonathan Greener, open champion of Australia. TeamGolf has maintained a wonderful nature setting for golfers while providing a tranquil haven for an exotic flora and fauna.",
                "Over 100 species of birds & animals have been identified in the wetlands surrounding the large natural lake while kangaroos abound amid flowering large eucalyptus trees and colourful shrubs. Surprisingly, this undulating natural course, which gives no hint of surrounding and sought after urban development, is less than 15 minutes from the state’s capital city of Sydney."
            ]
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on existing or default template
     *  @param content: Must be as required for the template type
     */
    displayExistingContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("HistoryTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultHistoryTemplateFunction;
            localStorage.setItem("HistoryTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("HistoryTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("HistoryTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display cached content and Fetch the updated content
     */
    getContent = function() {
        var lastUpd = localStorage.getItem("HistoryLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("HistoryLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentHistoryContent = JSON.parse(localStorage.getItem("CurrentHistoryContent"));
        if (currentHistoryContent == null || currentHistoryContent == undefined) {
            currentHistoryContent = getStaticContent();
            localStorage.setItem("CurrentHistoryContent", JSON.stringify(currentHistoryContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentHistoryContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchHistorySuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchHistorySuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentHistoryContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("HistoryLastUpdated", lastUpdateOn);
        }
    };

}());