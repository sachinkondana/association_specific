(function () {

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultPresentCommiteeTemplateFunction = "renderImageDetailsGridViewContents";

    /* Page id for this page */
    var pageId = "11";

    var contentList = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "mainTitle" : "Office Bearer’s",
                "imageList" : [
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Ronald Mendez",
                        "description" : ["Honorary Treasurer"]
                    },
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Richard Matthew",
                        "description" : ["Honorary Chair"]
                    }
                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Details Grid
     *  @param content: Must be as required for Image Details Grid
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/
        if (enableDebugMode)
            alert("updatedPageContent = " + JSON.stringify(content));

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PresentCommiteeTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultPresentCommiteeTemplateFunction;
            localStorage.setItem("PresentCommiteeTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PresentCommiteeTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("PresentCommiteeTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {

        var lastUpd = localStorage.getItem("PresentCommiteeLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("PresentCommiteeLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentPresentCommiteeContent = JSON.parse(localStorage.getItem("CurrentPresentCommiteeContent"));
        if (currentPresentCommiteeContent == null || currentPresentCommiteeContent == undefined || currentPresentCommiteeContent == "") {
            currentPresentCommiteeContent = getStaticContent();
            localStorage.setItem("CurrentPresentCommiteeContent", JSON.stringify(currentPresentCommiteeContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentPresentCommiteeContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchPresentCommiteeSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchPresentCommiteeSuccess = function (updatedPageContent) {
        if (enableDebugMode)
            alert("content = " + JSON.stringify(updatedPageContent));
        if (updatedPageContent == null || updatedPageContent.length == 0) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentPresentCommiteeContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("PresentCommiteeLastUpdated", lastUpdateOn);
        }
    };

}());
