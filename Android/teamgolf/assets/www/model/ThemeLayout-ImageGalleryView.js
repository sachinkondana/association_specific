(function() {

    var enableDebugMode = 0;

    /**
     * Template ID: IGV-001: Image Gallery View:
     *  Theme Consisting of Gallery of Images
     *          --------------------------------------------------------
     *          |                                                      |
     *          |    ----------- ----------- ----------- -----------   |
     *          |    | *Image* | | *Image* | | *Image* | | *Image* |   |
     *          |    ----------  ----------- ----------- -----------   |
     *          |                                                      |
     *          |    ----------- ----------- ----------- -----------   |
     *          |    | *Image* | | *Image* | | *Image* | | *Image* |   |
     *          |    ----------  ----------- ----------- -----------   |
     *          |                                                      |
     *          --------------------------------------------------------
     */
    var mainLayout = '                                                                             \
        <div class="baguetteBoxOne gallery" style="margin-top: 6px">                                \
            @imgItems                                                                               \
        </div>                                                                                      \
    ';

    //style="padding:0"
    var imgItemsLayout = '                                                                          \
        <a href="@imagePath">                                                                       \
            <img id="@imgid" src="images/loading.gif" data-src="@imagePath" alt="@imageTitle">      \
        </a>';

    /**
     *  Renders the content as per the Detailed List View Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *  [
     *      {
     *          “imagePath” : “<Path to Image>”,
     *          “title” : “<title>”,
     *          “description” : “<description>”
     *       },
     *      {
     *          “imagePath” : “<Path to Image>”,
     *          “title” : “<title>”,
     *          “description” : “<description>”
     *      }
     *  ]
     *  @returns rendered content as per theme
     */
    renderImageGalleryViewContents = function(imageList) {
        if (enableDebugMode)
            alert("imageList = " + JSON.stringify(imageList));

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';
        if (imageList.templateContent != null && imageList.templateContent != undefined) {
            contentLayout += mainLayout;

            var elementLayoutList = '';
            var imageBase = imageList.imageBase;
            for (var i = 0; i < imageList.templateContent.length; i++) {
                elementLayoutList += imgItemsLayout;

                var replaceString = "/@imagePath/g";
                if (imageList.templateContent[i].imagePath != null && imageList.templateContent[i].imagePath != "") {
                    var imagePath = imageBase + imageList.templateContent[i].imagePath;
                    elementLayoutList = elementLayoutList.replace(/@imagePath/g, imagePath);
                    elementLayoutList = elementLayoutList.replace("@imgid", "galimg_" + i);
                }

                if (imageList.templateContent[i].title != null) {
                    var title = imageList.templateContent[i].title;
                    if (imageList.templateContent[i].description != null) {
                        elementLayoutList = elementLayoutList.replace("@imageTitle", "<em>" + title + "</em><BR/><p>" + imageList.templateContent[i].description + "</p>");
                    } else {
                        elementLayoutList = elementLayoutList.replace("@imageTitle", title);
                    }
                } else {
                    if (imageList.templateContent[i].description != null) {
                        elementLayoutList = elementLayoutList.replace("@imageTitle", "<BR/><p>" + imageList.templateContent[i].description + "</p>");
                    } else {
                        elementLayoutList = elementLayoutList.replace("@imageTitle", "");
                    }
                }
            }

            contentLayout = contentLayout.replace("@imgItems", elementLayoutList);
        }

        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        if (contentLayout == '')
            contentLayout = '<p style="text-align:center"> Gallery images not available </p>';

        return contentLayout;
    };


}());