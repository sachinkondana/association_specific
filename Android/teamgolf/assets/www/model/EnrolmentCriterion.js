(function() {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultEnrolmentCriterionTemplateFunction = "renderMultipleParaThemeContents";

    /* Page id for this page */
    var pageId = "15";

    var aboutContent = {
        "imageBase": "images/",
        "templateContent": {
            "image": "",
            "paraContent": [
                "Membership of the Team Golf is open to all persons who have completed the age of 21 years.",
                "Each approved Member shall be bound by the Rules and Regulations and any Bye-Laws and conditions of Membership.",
                "All applications for membership will be scrutinized by the Committee constituted by the Governing Body for ascertaining their eligibility."
            ]
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Multiple Para Theme
     *  @param content: Must be as required for Multiple Para Theme
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("EnrolCriterionTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultEnrolmentCriterionTemplateFunction;
            localStorage.setItem("EnrolCriterionTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("EnrolCriterionTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("EnrolCriterionTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        /*if (isStaticContent == 1)
        {
            var content = getStaticContent();
            displayContent(content);
        }*/
        var lastUpd = localStorage.getItem("EnrolCriterionLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("EnrolCriterionLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentECContent = JSON.parse(localStorage.getItem("CurrentEnrolCriterionContent"));
        if (currentECContent == null || currentECContent == undefined) {
            currentECContent = getStaticContent();
            localStorage.setItem("CurrentEnrolCriterionContent", JSON.stringify(currentECContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentECContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchECSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchECSuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentEnrolCriterionContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("EnrolCriterionLastUpdated", lastUpdateOn);
        }
    };

}());