(function () {

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultMemberOptionsTemplateFunction = "renderSimpleListViewContents";

    /* Page id for this page */
    var pageId = "16";

    var content = {
        "imageBase": "images/",
        "templateContent":
        {
            "image" : "",
            "mainDescription" : "Membership Options",
            "description" : "Effective 1st January 2013, Golf Association & Country Club is no longer offering Golfing Life Membership and Sports Life Membership. We are offering Golfing Term Memberships ( Corporate / Individual ) for you to enjoy golfing at our newly renovated golf course with Clubhouse facilities. For more information, please call us at 03-90762233 ext:  210 / 212 or E-mail: kartini@slgcc.com.my.",
            "detailsList" : [
                "Type of Memberships"
            ],
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return content;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Gallery View
     *  @param content: Must be as required for Image Gallery View
     */
    displayExistingContent = function(content) {

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("MemberOptionsTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultMemberOptionsTemplateFunction;
            localStorage.setItem("MemberOptionsTemplate", existingTemplate);
        }

        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(recvContent) {

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("MemberOptionsTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("MemberOptionsTemplate", templateFunc);
        }

        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](recvContent);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getMemberShipOptionsContent = function() {
        var lastUpd = localStorage.getItem("MemberOptionsLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("MemberOptionsLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentMemberOptionsContent = JSON.parse(localStorage.getItem("CurrentMemberOptionsContent"));
        if (currentMemberOptionsContent == null || currentMemberOptionsContent == undefined) {
            currentMemberOptionsContent = getStaticContent();
            localStorage.setItem("CurrentMemberOptionsContent", JSON.stringify(currentMemberOptionsContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentMemberOptionsContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchMemberOptionsSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchMemberOptionsSuccess = function (updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentMemberOptionsContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("MemberOptionsLastUpdated", lastUpdateOn);
        }
    };
}());
