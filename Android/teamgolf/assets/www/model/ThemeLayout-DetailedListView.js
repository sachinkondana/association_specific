(function () {

    var enableDebugMode = 0;

    /**
     * Detailed List View:
     *  Theme Consisting of Title followed by list consisting of detailed description in list format
     *          --------------------------------------------------------
     *          | Title1                                               |
     *          |       Description                                    |
     *          |    O ------------------------                        |
     *          |    | |      Desc 1          |                        |
     *          |    | ------------------------                        |
     *          |    O |      Desc 2          |                        |
     *          |      ------------------------                        |
     *          |                                                      |
     *          |                                                      |
     *          | Title2                                               |
     *          |       Description                                    |
     *          |    O ------------------------                        |
     *          |    | |      Desc 1          |                        |
     *          |    | ------------------------                        |
     *          |    O |      Desc 2          |                        |
     *          |      ------------------------                        |
     *          |                                                      |
     *          | Addition Details displayed in a para                 |
     *          --------------------------------------------------------
     */
    var colorsSupported = ["red", "green", "blue"];
    var styleForLastElement = 'style="border-left: 0px solid #babab3" ';
    var titleStyling = ' style="display:none" ';

    var mainLayout = '                                                                              \
            <div class="rules-area">                                                                \
                <p @titleDisplayControl><b> @MasterTitle </b></p>                                   \
                <p @masterDescDisplayControl> @MasterDescription </p>                               \
                <ul style="float:left; padding-left:12px;">                                         \
                       @elementLayoutArray                                                          \
                </ul>                                                                               \
            </div>';

    var elementLayout = '                                                                           \
        <li @lastElementStyling>                                                                    \
            <div class="rules-details">                                                             \
                <div class="rules-dots @color">                                                     \
                    <i class="fa fa-circle"></i>                                                    \
                </div>                                                                              \
                <div class="rules-desc">                                                            \
                    <p> @description</p>                                                            \
                </div>                                                                              \
                <div class="clear"></div>                                                           \
            </div>                                                                                  \
        </li>';

    /**
     *  Renders the content as per the Detailed List View Layout and returns the rendered content
     *  @param content: JSON String of @inputData format
     *      [
     *          {
     *              "title": "Title1",
     *              "description" : "Description1"
     *              "elementContent" : [ <array of each description> ]
     *          },
     *          {
     *              "title": "Title2",
     *              "description" : "Description2"
     *              "elementContent" : [ <array of each description> ]
     *          }
     *      ]
     *  @returns rendered content as per theme
     */
    renderDetailedListViewContents = function(content) {

        if (enableDebugMode)
            alert("renderDetailedListViewContents = " + JSON.stringify(content));

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';

        if (content != null && content != undefined && content.templateContent != null && content.templateContent != undefined)
        {
            for (var i = 0; i < content.templateContent.length; i++)
            {
                contentLayout += mainLayout;
                // Master Title
                if (content.templateContent[i].title != null && content.templateContent[i].title.trim() != "")
                {
                    //display content
                    contentLayout = contentLayout.replace("@titleDisplayControl", "");
                    contentLayout = contentLayout.replace("@MasterTitle", content.templateContent[i].title);
                }
                else
                {
                    // display none
                    contentLayout = contentLayout.replace("@titleDisplayControl", titleStyling);
                    contentLayout = contentLayout.replace("@MasterTitle", "");
                }

                // Master Description
                //<p @masterDescDisplayControl> @MasterDescription </p>
                if (content.templateContent[i].description != null && content.templateContent[i].description.trim() != "")
                {
                    //display content
                    contentLayout = contentLayout.replace("@masterDescDisplayControl", "");
                    contentLayout = contentLayout.replace("@MasterDescription", content.templateContent[i].description);
                }
                else
                {
                    // display none
                    contentLayout = contentLayout.replace("@masterDescDisplayControl", titleStyling);
                    contentLayout = contentLayout.replace("@MasterDescription", "");
                }

                // List
                // Since the last element must be styled differently
                var descriptionListLayout = '';
                for (var j = 0; j < content.templateContent[i].descriptionList.length; j++)
                {
                    if (content.templateContent[i].descriptionList[j] != null && (content.templateContent[i].descriptionList[j]).trim() != "") {
                        descriptionListLayout += elementLayout;

                        // Verify the last elements styling
                        if (j == content.templateContent[i].descriptionList.length - 1)
                        {
                            descriptionListLayout = descriptionListLayout.replace("@lastElementStyling", styleForLastElement);
                        }
                        else
                        {
                            descriptionListLayout = descriptionListLayout.replace("@lastElementStyling", "");
                        }

                        // Change the color
                        descriptionListLayout = descriptionListLayout.replace("@color", colorsSupported[colourIndex]);
                        colourIndex = (colourIndex + 1) % colorsSupported.length;

                        //Change the description
                        descriptionListLayout = descriptionListLayout.replace("@description", content.templateContent[i].descriptionList[j]);
                    }
                }

                // Add the element description list
                contentLayout = contentLayout.replace("@elementLayoutArray", descriptionListLayout);
            }
        } else {
            contentLayout = '<div style="width:100%; margin:0 auto; padding-top:10px; text-align:center"> \
                                <p style="font-weight:bold;" align="center"> Details Not Available </p>   \
                            </div> ';
        }

        return contentLayout;
    };

}());
