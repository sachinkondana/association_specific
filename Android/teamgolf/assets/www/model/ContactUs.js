(function() {

    var isStaticContent = 1;

    /* Default template function name for this page */
    var defaultContactUsTemplateFunction = "renderContactUsLayoutContents";

    /* Page id for this page */
    var pageId = "8";

    var aboutContent = {
        "imageBase": "images/",
        "templateContent": {
            "imagePath": "contactmap.png",
            "message": "",
            "contactName": "Team Golf",
            "address": {
                "address1": "Cunningham Road",
                "address2": "",
                "city": "Bangalore",
                "state": "Karnataka",
                "pinCode": "430000",
                "country": "India"
            },
            "phone": "+91 9076 2233",
            "emailId": "test@mobicom.my",
            "facebookId": "GolfAssociation",
            "twitterId": "GolfAssociation",
            "instagramId": "GolfAssociation"
        }
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Multiple Para Theme
     *  @param content: Must be as required for Multiple Para Theme
     */
    displayExistingContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ContactUsTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultContactUsTemplateFunction;
            localStorage.setItem("ContactUsTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);

        if (localStorage.getItem("isLoggedIn") == "true") {
            document.getElementById("float-text").value = localStorage.getItem("first_name") + " " + localStorage.getItem("last_name");
            document.getElementById("float-text-value-number").value = localStorage.getItem("phone");
            document.getElementById("float-text-value").value = localStorage.getItem("email");
        }
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("ContactUsTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("ContactUsTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);

        if (localStorage.getItem("isLoggedIn") == "true") {
            document.getElementById("float-text").value = localStorage.getItem("first_name") + " " + localStorage.getItem("last_name");
            document.getElementById("float-text-value-number").value = localStorage.getItem("phone");
            document.getElementById("float-text-value").value = localStorage.getItem("email");
        }
    };

    validateContent = function() {
        // Validation
        $("#float-text").on('keyup', function() {
            if ($("#float-text").val().length > 0) {
                $("#nameerror").css("display", "none");
                $("#nameerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_NameBlank") + '<i class="form-help-icon icon">error</i>');
            }
        });

        $("#float-text-value").on('keyup', function() {
            if (emailValidation($("#float-text-value").val())) {
                $("#emailerror").css("display", "none");
                $("#emailerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#emailerror").css("display", "block");
                $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            }
        });

        $("#float-text-value-number").on('keyup', function() {
            if (phValidation($("#float-text-value-number").val())) {
                $("#pherror").css("display", "none");
                $("#pherror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#pherror").css("display", "block");
                $("#pherror").html(localizeString("ValidationError_InvalidPhone") + '<i class="form-help-icon icon">error</i>');
            }
        });

        $("#float-textarea").on('keyup', function() {
            if ($("#float-textarea").val().length > 0) {
                $("#msgerror").css("display", "none");
                $("#msgerror").html('<i class="form-help-icon icon">error</i>');
            } else {
                $("#msgerror").css("display", "block");
                $("#msgerror").html(localizeString("ValidationError_MessageBlank") + '<i class="form-help-icon icon">error</i>');
            }
        });
    }

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        var lastUpd = localStorage.getItem("ContactLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("ContactLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentContactContent = JSON.parse(localStorage.getItem("CurrentContactContent"));
        if (currentContactContent == null || currentContactContent == undefined) {
            currentContactContent = getStaticContent();
            localStorage.setItem("CurrentContactContent", JSON.stringify(currentContactContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentContactContent);

        /* Validate the content */
        validateContent();

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchContactSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchContactSuccess = function(updatedPageContent) {
        if (updatedPageContent == null) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentContactContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("ContactLastUpdated", lastUpdateOn);
        }

        /* Validate the content */
        validateContent();
    };

}());