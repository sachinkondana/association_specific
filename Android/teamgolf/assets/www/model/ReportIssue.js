(function () {

    var isStaticContent = 1;

    var aboutContent = {
        "message" : localizeString("Report_Label_Message"),
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return aboutContent;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Multiple Para Theme
     *  @param content: Must be as required for Multiple Para Theme
     */
    displayContent = function(content) {
        var themedContent = renderReportIssueLayoutContents(content);
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {
        if (isStaticContent == 1)
        {
            var content = getStaticContent();
            displayContent(content);

            // Validation
            $("#float-text").on('keyup', function () {
                if ($("#float-text").val().length > 0) {
                    $("#nameerror").css("display", "none");
                    $("#nameerror").html('<i class="form-help-icon icon">error</i>');
                } else {
                    $("#nameerror").css("display", "block");
                    $("#nameerror").html(LocalizeString("ValidationError_TitleBlank") + '<i class="form-help-icon icon">error</i>');
                }
            });

            $("#float-textarea").on('keyup', function () {
                if ($("#float-textarea").val().length > 0) {
                    $("#msgerror").css("display", "none");
                    $("#msgerror").html('<i class="form-help-icon icon">error</i>');
                } else {
                    $("#msgerror").css("display", "block");
                    $("#msgerror").html(localizeString("ValidationError_IssueDescriptionBlank") + '<i class="form-help-icon icon">error</i>');
                }
            });

        }
    };

}());
