(function() {

    var enableDebugMode = 0;

    /**
     * Template ID: CUT-001: Contact Us Page
     *  Theme Consisting of details, form and additional contact details
     *          --------------------------------------------------------
     *          |    -----------------------------------------------   |
     *          |    | *********Clickable Image ****************   |   |
     *          |    ----------  ----------- ----------- -----------   |
     *          |                                                      |
     *          |       Details                                        |
     *          |                                                      |
     *          |       ----------------------------------------       |
     *          |       |                                      |       |
     *          |       |       ***** FORM  ******             |       |
     *          |       ----------------------------------------       |
     *          |                                                      |
     *          |       Contact Details:                               |
     *          |       ---------------------------------------        |
     *          |       Name                                           |
     *          |       Address Line 1                                 |
     *          |       Address Line 2                                 |
     *          |       City, State PIN-CODE                           |
     *          |                                                      |
     *          |       Phone:                                         |
     *          |       Email                                          |
     *          |       Facebook:                                      |
     *          |       Twitter:                                       |
     *          |       Instagram:                                     |
     *          --------------------------------------------------------
     */
    var displayStyle = 'style="display: none;" ';

    // No Configurable Content in present design
    var locationImageLayout = '                                                                     \
        <div style="display:none" id="locationdiv" onClick="return onBtnClicked();">                                     \
            <img class="no-img-popup" style="height:154px; width:100%" src=@imagePath>              \
        </div>                                                                                      \
    ';

    var messageDetailedLayout = '                                                                   \
        <div>                                                                                       \
            <table width="100%" border="0" cellspacing="15" cellpadding="00">                       \
                <tr><td>                                                                            \
                    <p> @MessageContent </p>                                                        \
                </td></tr>                                                                          \
            </table>                                                                                \
        </div> ';

    //Form Layout - Non modifiable
    var formLayout = '                                                                                  \
        <div style="overflow-x:hidden !important">                                                      \
        <table width="100%" border="0" cellspacing="15" cellpadding="00">                               \
            <tr>                                                                                        \
                <td>                                                                                    \
                    <ul id="list-table">                                                                \
                        <li>                                                                            \
                            <h4 class="bold-match-color" style="margin-top:0px;">' + localizeString("Contact_Label_Send") + '</h4> \
                            <p class="text">' + localizeString("Contact_Label_SendDetail") + '</p>        \
                        </li>                                                                           \
                        <li>                                                                            \
                            <fieldset>                                                                  \
                                <div class="form-group form-group-label control-highlight">             \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" style="font-size:15px;" for="float-text">' + localizeString("Contact_Text_Name") + '*' + '</label>  \
                                            <input class="form-control" id="float-text" style="font-size:15px;" type="text">                                            \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="namesuccess"> \
                                                <i class="form-help-icon icon">check</i>                \
                                            </span>                                                     \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="nameerror">     \
                                                <i class="form-help-icon icon">error</i>                \
                                            </span>                                                     \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                                <div class="form-group form-group-label control-highlight">             \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" style="font-size:15px;" for="float-text-value">' + localizeString("Contact_Text_Email") + '*' + '</label> \
                                            <input class="form-control" style="font-size:15px;" id="float-text-value" type="email" value="">                                \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="emailsuccess">    \
                                                <i class="form-help-icon icon">check</i>                                                                                    \
                                            </span>                                                                                                                         \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="emailerror">        \
                                                <i class="form-help-icon icon">error</i>                                                                                    \
                                            </span>                                                                                                                         \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                                <div class="form-group form-group-label control-highlight">             \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" style="font-size:15px;" for="float-text-value" style="width:150px;">' + localizeString("Contact_Text_Phone") + '*' + '</label> \
                                            <input class="form-control" style="font-size:15px;" id="float-text-value-number" type="text" value="">    \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="phsuccess">   \
                                                <i class="form-help-icon icon">check</i>                \
                                            </span>                                                     \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="pherror">   \
                                                <i class="form-help-icon icon">error</i>                \
                                            </span>                                                     \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                                <div class="form-group form-group-label">                               \
                                    <div class="row">                                                   \
                                        <div class="col-lg-6 col-sm-12">                                \
                                            <label class="floating-label" style="font-size:15px;" for="float-textarea">' + localizeString("Contact_Text_Message") + '*' + '</label>  \
                                            <textarea class="form-control" style="font-size:15px;" id="float-textarea" rows="5"></textarea> \
                                            <span class="form-help form-help-msg text-green" style="margin-top:0px; margin-bottom:0px; display:none;" id="msgsuccess">  \
                                                <i class="form-help-icon icon">check</i>                \
                                            </span>                                                     \
                                            <span class="form-help form-help-msg text-red" style="margin-top:0px; margin-bottom:0px; display:none;" id="msgerror">  \
                                                <i class="form-help-icon icon">error</i>                \
                                            </span>                                                     \
                                        </div>                                                          \
                                    </div>                                                              \
                                </div>                                                                  \
                            </fieldset>                                                                 \
                        </li>                                                                           \
                        <li style="margin-top:0px;">                                                    \
                            <a href="javascript:void(0);" class="btn btn-alt" onclick="sendInfo();">' + localizeString("Contact_Button_Submit") + '</a>  \
                        </li>                                                                           \
                    </ul>                                                                               \
                </td>                                                                                   \
            </tr>                                                                                       \
        </table>                                                                                        \
        </div> ';

    var emailLayout = '                                                                                 \
        <a @displayStyleEmailId style="word-break:break-all; color:blue; font-size:15px;" href="javascript:void();" onclick="gotomail(\'@emailId\')" class="contact-mail" target="_system">  \
            <i class="fa fa-envelope"></i>Email: @emailId                                               \
        </a> </br>                                                                                      \
        ';

    var contactLayout = '                                                                               \
        <a @displayStylePhone style="color:blue; font-size:15px;" href="javascript:void();" onclick="gotocall(\'@phoneNumber\')" class="contact-call" target="_system">    \
            <i class="fa fa-phone"></i>' + localizeString("Contact_Text_Phone") + ' : @phoneNumber      \
        </a> </br>                                                                                      \
        ';

    // Contact Information
    var contactDetailsLayout = '                                                                    \
        <div>                                                                                       \
        <table width="100%" border="0" cellspacing="15" cellpadding="00">                           \
            <tr>                                                                                    \
                <td>                                                                                \
                    <ul id="list-table">                                                            \
                        <li>                                                                        \
                            <h4 class="cnt-head" style="margin-top:10px;">' + localizeString("Contact_Label_ContactInfo") + '</h4>  \
                            <p class="text_p" style="font-size:18px;">                              \
                                @clientAddress                                                      \
                            </p>                                                                    \
                            <p>                                                                     \
                                <a @displayStylePhone style="color:blue; font-size:16px;" href="javascript:void();" onclick="gotocall(\'@phoneNumber\')" class="contact-call" target="_system">                                                   \
                                    <i class="fa fa-phone"></i>' + localizeString("Contact_Text_Phone") + ' : @phoneNumber            \
                                </a> </br>                                                          \
                                @morePhonesToDisplay                                                \
                                <a @displayStyleEmailId style="word-break:break-all; color:blue; font-size:16px;" href="javascript:void();" onclick="gotomail(\'@emailId\')" class="contact-mail" target="_system">                                                   \
                                    <i class="fa fa-envelope"></i>' + localizeString("Forgot_Text_Email") + ': @emailId               \
                                </a> </br>                                                          \
                                @moreEmailIdsToDisplay                                              \
                                <div class="socialIcon"><a @displayStyleFbId href="javascript:void();" onclick="gotofacebook(\'@facebookId\')"  target="_system">                                                   \
                                <i class="fa fa-facebook-official fa-3x"></i>    \
                            </a> </div>                                                          \
                            <div class="socialIcon"><a @displayStyleTwitterHandle href="javascript:void();" onclick="gototwitter(\'@twitterHandle\')" target="_system">                                                   \
                                <i class="fa fa-twitter fa-3x"></i>            \
                            </a> </div>                                                          \
                            <div class="socialIcon"><a @displayStyleInstaHandle href="javascript:void();" onclick="gotoinstagram(\'@instaHandle\')" target="_system">                                                   \
                                <i class="fa fa-instagram fa-3x"></i>          \
                            </a> </div>                                                      \
                            </p>                                                                    \
                        </li>                                                                       \
                    </ul>                                                                           \
                </td>                                                                               \
            </tr>                                                                                   \
        </table>                                                                                    \
        </div> ';

    /**
     *  Renders the content as per the Contact Us Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *  {
     *      "message" : "message displayed as para before Form",
     *      "contactName" : "Contact Name",
     *      "address" : { "address1" : "Address Line 1",
     *                    "address2" : "Address Line 2",
     *                    "city"    :   "City Name",
     *                    "state"   :   "State Name",
     *                    "pinCode" :   "Pin Code",
     *                    "country" :   "Country"
     *                  },
     *      "phone" :   "Phone Number",
     *      "emailId" :   "E-mail",
     *      "facebookId"    :   "Facebook ID must be appended with facebook.com/",
     *      "twitterId"     :   "Twitter ID"
     *  }
     *  @returns rendered content as per theme
     */
    renderContactUsLayoutContents = function(contactDetails) {

        // Create a List of desc
        var colourIndex = 0;
        var contentLayout = '';
        var imagePath;

        if (contactDetails != null && contactDetails != undefined && contactDetails.templateContent != null && contactDetails.templateContent != undefined) {
            // Add Clickable location image
            contentLayout += locationImageLayout;

            if (contactDetails.templateContent.imagePath != null && contactDetails.templateContent.imagePath != undefined &&
                contactDetails.templateContent.imagePath != "") {
                imagePath = contactDetails.imageBase + contactDetails.templateContent.imagePath;
            } else {
                imagePath = "images/contactmap.png";
            }
            contentLayout = contentLayout.replace("@imagePath", imagePath);

            // Add Message details is available
            if (contactDetails.templateContent.message != null && contactDetails.templateContent.message != "") {
                contentLayout += messageDetailedLayout;
                contentLayout = contentLayout.replace("@MessageContent", contactDetails.templateContent.message);
            }

            // Add the mandatory Form
            contentLayout += formLayout;

            // Add Contact Details Section
            // TODO: Change it to DIV with word wrap instead of <BR>
            if (contactDetails.templateContent.contactName != null) {
                contentLayout += contactDetailsLayout;
                var clientAddress = '' + contactDetails.templateContent.contactName;
                if (contactDetails.templateContent.address != null) {
                    //@clientAddress
                    //Create Client Address
                    if (contactDetails.templateContent.address.address1 != null && contactDetails.templateContent.address.address1 != "")
                        clientAddress += "<BR>" + contactDetails.templateContent.address.address1;
                    if (contactDetails.templateContent.address.address2 != null && contactDetails.templateContent.address.address2 != "")
                        clientAddress += "<BR>" + contactDetails.templateContent.address.address2;
                    if (contactDetails.templateContent.address.city != null && contactDetails.templateContent.address.city != "")
                        clientAddress += "<BR>" + contactDetails.templateContent.address.city;
                    if (contactDetails.templateContent.address.state != null && contactDetails.templateContent.address.state != "")
                        clientAddress += " " + contactDetails.templateContent.address.state;
                    if (contactDetails.templateContent.address.pinCode != null && contactDetails.templateContent.address.pinCode != "")
                        clientAddress += " " + contactDetails.templateContent.address.pinCode;
                    if (contactDetails.templateContent.address.country != null && contactDetails.templateContent.address.country != "")
                        clientAddress += "<BR>" + contactDetails.templateContent.address.country;
                }

                contentLayout = contentLayout.replace("@clientAddress", clientAddress);
            }

            // Phone Number
            if (contactDetails.templateContent.phone != null && contactDetails.templateContent.phone != "") {
                var contactArr = contactDetails.templateContent.phone.split(",");
                contentLayout = contentLayout.replace("@phoneNumber", contactArr[0]);
                contentLayout = contentLayout.replace("@displayStylePhone", "");
                contentLayout = contentLayout.replace("@phoneNumber", contactArr[0]);
                var newContactLayout = "";
                if (contactArr.length > 1) {
                    for (var i = 1; i < contactArr.length; i++) {
                        newContactLayout += contactLayout;
                        newContactLayout = newContactLayout.replace("@phoneNumber", contactArr[i]);
                        newContactLayout = newContactLayout.replace("@displayStylePhone", "");
                        newContactLayout = newContactLayout.replace("@phoneNumber", contactArr[i]);
                    }
                }
                contentLayout = contentLayout.replace("@morePhonesToDisplay", newContactLayout);
            } else {
                contentLayout = contentLayout.replace("@phoneNumber", "");
                contentLayout = contentLayout.replace("@displayStylePhone", displayStyle);
                contentLayout = contentLayout.replace("@phoneNumber", "");
                contentLayout = contentLayout.replace("@morePhonesToDisplay", "");
            }

            // E-Mail ID
            if (contactDetails.templateContent.emailId != null && contactDetails.templateContent.emailId != "") {
                var emailArr = contactDetails.templateContent.emailId.split(",");
                contentLayout = contentLayout.replace("@emailId", emailArr[0].trim());
                contentLayout = contentLayout.replace("@displayStyleEmailId", "");
                contentLayout = contentLayout.replace("@emailId", emailArr[0].trim());
                var newEmailLayout = "";
                if (emailArr.length > 1) {
                    for (var i = 1; i < emailArr.length; i++) {
                        newEmailLayout += emailLayout;
                        newEmailLayout = newEmailLayout.replace("@emailId", emailArr[i].trim());
                        newEmailLayout = newEmailLayout.replace("@displayStyleEmailId", "");
                        newEmailLayout = newEmailLayout.replace("@emailId", emailArr[i].trim());
                    }
                }
                contentLayout = contentLayout.replace("@moreEmailIdsToDisplay", newEmailLayout);
            } else {
                contentLayout = contentLayout.replace("@emailId", "");
                contentLayout = contentLayout.replace("@displayStyleEmailId", displayStyle);
                contentLayout = contentLayout.replace("@emailId", "");
                contentLayout = contentLayout.replace("@moreEmailIdsToDisplay", "");
            }

            // Facebook ID
            if (contactDetails.templateContent.facebookId != null && contactDetails.templateContent.facebookId != "") {
                contentLayout = contentLayout.replace("@facebookId", contactDetails.templateContent.facebookId.trim());
                contentLayout = contentLayout.replace("@displayStyleFbId", 'style="display:inline;"');
                contentLayout = contentLayout.replace("@facebookId", contactDetails.templateContent.facebookId.trim());
            } else {
                contentLayout = contentLayout.replace("@facebookId", "");
                contentLayout = contentLayout.replace("@displayStyleFbId", displayStyle);
                contentLayout = contentLayout.replace("@facebookId", "");
            }

            // Twitter Handle
            if (contactDetails.templateContent.twitterId != null && contactDetails.templateContent.twitterId != "") {
                var twId = contactDetails.templateContent.twitterId.trim();
                contentLayout = contentLayout.replace("@twitterHandle", twId);
                contentLayout = contentLayout.replace("@displayStyleTwitterHandle", "");
                contentLayout = contentLayout.replace("@twitterHandle", twId);
            } else {
                contentLayout = contentLayout.replace("@twitterHandle", "");
                contentLayout = contentLayout.replace("@displayStyleTwitterHandle", displayStyle);
                contentLayout = contentLayout.replace("@twitterHandle", "");
            }

            // Instagram Handle
            if (contactDetails.templateContent.instagramId != null && contactDetails.templateContent.instagramId != "") {
                var inId = contactDetails.templateContent.instagramId.trim();
                contentLayout = contentLayout.replace("@instaHandle", inId);
                contentLayout = contentLayout.replace("@displayStyleInstaHandle", "");
                contentLayout = contentLayout.replace("@instaHandle", inId);
            } else {
                contentLayout = contentLayout.replace("@instaHandle", "");
                contentLayout = contentLayout.replace("@displayStyleInstaHandle", displayStyle);
                contentLayout = contentLayout.replace("@instaHandle", "");
            }
        } else {
            // Add Clickable location image
            contentLayout += locationImageLayout;
            imagePath = "images/contactmap.png";
            contentLayout = contentLayout.replace("@imagePath", imagePath);
            contentLayout += messageDetailedLayout;
            contentLayout = contentLayout.replace("@MessageContent", localizeString("Contact_Label_NoData"));
        }

        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        return contentLayout;
    };

    sendInfo = function() {
        var username = localStorage.getItem("USER_NAME");
        var password = localStorage.getItem("PASSWORD");
        var communeid = localStorage.getItem("COMMUNE_ID");
        var serverurl = localStorage.getItem("SERVER_URL");
        var deviceid = localStorage.getItem("DEVICE_ID");
        var deviceos = localStorage.getItem("DEVICE_OS");
        var appid = localStorage.getItem("APP_ID");
        var name = document.getElementById("float-text").value.trim();
        var email = document.getElementById("float-text-value").value.trim();
        var ph = document.getElementById("float-text-value-number").value.trim();
        var message = document.getElementById("float-textarea").value.trim();
        var emailValid = emailValidation(email);
        var phValid = phValidation(ph);

        $("#pherror").css("display", "none");
        $("#pherror").html('');
        $("#emailerror").css("display", "none");
        $("#emailerror").html('');
        $("#msgerror").css("display", "none");
        $("#msgerror").html('');
        $("#nameerror").css("display", "none");
        $("#nameerror").html('');

        if (name != "" && message != "" && emailValid && phValid) {
            window.plugins.spinnerDialog.show(localizeString("SpinDialog_Loading"), localizeString("SpinDialog_Wait"), true);
            var actions = {
                "name": name,
                "email": email,
                "phone": ph,
                "message": message,
                "deviceid": deviceid,
                "deviceos": deviceos,
                "appid": appid,
                "locale": localStorage.getItem("APP_LOCALE")
            };
            $.ajax({
                url: serverurl + "/contactus",
                type: "POST",
                data: actions,
                dataType: "json",
                headers: {
                    "Authorization": "Basic " + btoa(username + ":" + password),
                    "COMMUNE_ID": "" + communeid,
                    "compatibility_version": "" + localStorage.getItem("BackendCompatiabilityVersion")
                },
                success: function(data) {

                    window.plugins.spinnerDialog.hide();
                    if (data.status == "success") {
                        swal({
                                title: localizeString("Alert_Title_Success"),
                                text: data.message,
                                type: "success",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                document.getElementById("float-textarea").value = "";
                                return;
                            });
                    } else {
                        var thisone = this;

                        swal({
                                title: localizeString("Alert_Title_Sorry"),
                                text: data.message,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    }
                },
                error: function(jqXHR, exception) {
                    window.plugins.spinnerDialog.hide();

                    if (jqXHR.status === 0) {
                        //swal.close();
                        var thisone = this;

                        swal({
                                title: localizeString("Alert_Title_Connectivity"),
                                text: localizeString("Alert_Text_Connectivity"),
                                type: "info",
                                showCancelButton: true,
                                closeOnConfirm: true,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_TryAgain"),
                                cancelButtonText: localizeString("Alert_Button_Cancel")
                            },
                            function() {
                                $.ajax(thisone);
                                return;
                            });
                    } else if (jqXHR.status === 401) {
                        resignin();
                    } else if (jqXHR.status === 426) {
                        swal({
                                title: localizeString("Alert_Title_Unsupported"),
                                text: jqXHR.responseText,
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                allowOutsideClick: false,
                                confirmButtonText: localizeString("Alert_Button_Ok")
                            },
                            function() {
                                return;
                            });
                    } else {
                        var thisone = this;

                        swal({
                                title: localizeString("Alert_Title_Something"),
                                text: localizeString("Alert_Text_Something"),
                                type: "info",
                                showCancelButton: true,
                                closeOnConfirm: true,
                                allowOutsideClick: false,
                                showLoaderOnConfirm: true,
                                confirmButtonText: localizeString("Alert_Button_TryAgain"),
                                cancelButtonText: localizeString("Alert_Button_Cancel")
                            },
                            function() {
                                $.ajax(thisone);
                                return;
                            });
                    }
                }
            });
        } else {
            if (name == "") {
                $("#nameerror").css("display", "block");
                $("#nameerror").html(localizeString("ValidationError_FirstnameBlank") + '<i class="form-help-icon icon">error</i>');
            }
            if (email == "") {
                $("#emailerror").css("display", "block");
                $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            }
            if (ph == "") {
                $("#pherror").css("display", "block");
                $("#pherror").html(localizeString("ValidationError_InvalidPhone") + '<i class="form-help-icon icon">error</i>');
            }
            if (message == "") {
                $("#msgerror").css("display", "block");
                $("#msgerror").html(localizeString("ValidationError_MessageBlank") + '<i class="form-help-icon icon">error</i>');
            }
            if (email != "" && !emailValid) {
                $("#emailerror").css("display", "block");
                $("#emailerror").html(localizeString("ValidationError_InvalidEmail") + '<i class="form-help-icon icon">error</i>');
            }
            if (ph == "" && !phValid) {
                $("#pherror").css("display", "block");
                $("#pherror").html(localizeString("ValidationError_InvalidPhone") + '<i class="form-help-icon icon">error</i>');
            }
        }
    }

    emailValidation = function(email) {
        var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
        return re.test(email);
    }

    phValidation = function(ph) {
        return true;
        /*var re = /^\+?([0-9\(\) ]{7,15})$/;
        return re.test(ph);*/
    }
}());