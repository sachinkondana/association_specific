(function () {

    var enableDebugMode = 0;

    /**
     * Image Details Grid View:
     *  Theme Consisting of Image with details Grid View
     *          mainLayout
     *          --------------------------------------------------------
     *          | Title1                                               |        _____
     *          |    -----------                   ----------          |          |
     *          |    | *Image* |                   | *Image* |         |          |
     *          |    ----------                    -----------         |       Row Layout
     *          |      Desc1                         Desc2             |          |     _____
     *          |    Detail Desc-1                 Detail Desc-1       |          |       | columnsPerRowSubDetailsLayout
     *          |    Detail Desc-2                 Detail Desc-2       |        __|___  __|__
     *          |                                                      |
     *          | Title2                                               |
     *          |    -----------                   ----------          |
     *          |    | *Image* |                   | *Image* |         |
     *          |    ----------                    -----------         |
     *          |      Desc1                         Desc2             |
     *          |    Detail Desc                    Detail Desc        |
     *          |                                                      |
     *          |                                                      |
     *          --------------------------------------------------------
     */

    var displayStyle = ' style="display:none" ';
    var mainLayout = '                                                                              \
        <div style="display:table;width: 90%; margin: 0 auto; padding-top: 10px;">                  \
            <p @displayStyle style="font-weight:bold;"> @mainTitle </p>                             \
                @rowsDetailsArray                                                                   \
        </div> ';

    var columnsPerRowLayoutleft = '                                                                 \
            <div style="display:table-cell; text-align:center; width:47%; padding-bottom:15px; padding-right:3px;">    \
                <div class="logo-sponsor" style="margin: 0 auto">                                   \
                    <img src="@mainImage" alt="" />                                                 \
                </div>                                                                              \
                <div @displayStyle class="bold-match-color-02" style="padding-top: 10px;"> @mainTitle   \
                </div>                                                                              \
                       @subDetailsLayoutArray                                                       \
            </div> ';
    var columnsPerRowLayoutright = '                                                                \
            <div style="display:table-cell; text-align:center; width:47%; padding-bottom:15px; padding-left:3px;">    \
                <div class="logo-sponsor" style="margin: 0 auto">                                   \
                    <img src="@mainImage" alt="" />                                                 \
                </div>                                                                              \
                <div @displayStyle class="bold-match-color-02" style="padding-top: 10px;"> @mainTitle   \
                </div>                                                                              \
                       @subDetailsLayoutArray                                                       \
            </div> ';

    var columnsPerRowSubDetailsLayout ='                                                            \
         <div style="word-wrap: break-word;" class="text"> @subDetails </div> ';

     /**
      * For now we are fixing that there can be only 2 images per row in Image Grid
      * In future we can determine the best number per row based on the width of screen
      */
    var columnsPerRow = 2;

    var rowLayout = '<div style="display:table-row;font-weight: normal; font-size: larger;border-top-width: 10px;width:100%"> \
        @columnsPerRowLayoutArray                                                                   \
        </div> ';

    var columnsPerRowSubContactDetailsLayout ='                                                     \
        <div style="color:blue; word-wrap: break-word; font-size:12px;" onclick="gotocall(\'@phoneno\')" target="_system">  \
            <i class="fa fa-phone" aria-hidden="true"></i>: @phoneno                                \
        </div>';

    var columnsPerRowSubEmailDetailsLayout ='                                                       \
        <div style="color:blue; word-wrap: break-word!important; font-size:12px; word-break: break-all;" onclick="gotomail(\'@emailId\')" target="_system">  \
            <i class="fa fa-envelope" aria-hidden="true"></i>: @emailId                             \
        </div>';

    /**
     *  Renders the content as per the Image Details Grid View Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *      "imageBase": ""
     *      "templateContent": [
     *        {
     *          "mainTitle" : "Main Title",
     *          "imageList" :
     *              [
     *                  {
     *                      "image": "Image Path",
     *                      "title" : "Title"
     *                      "description" : [ <array of sub details> ]
     *                  },
     *                  {
     *                      "image": "Image Path",
     *                      "title" : "Title"
     *                      "description" : [ <array of sub details> ]
     *                  },
     *              ]
     *        },
     *        {
     *          "mainTitle" : "Main Title",
     *          "imageList" :
     *              [
     *                  {
     *                      "image": "Image Path",
     *                      "title" : "Title"
     *                      "description" : [ <array of sub details> ]
     *                  },
     *                  {
     *                      "image": "Image Path",
     *                      "title" : "Title"
     *                      "description" : [ <array of sub details> ]
     *                  },
     *              ]
     *        }
     *      ]
     *  @returns rendered content as per theme
     */
    renderImageDetailsGridViewContents = function(contents) {

        if (enableDebugMode)
            alert("content = " + JSON.stringify(contents));

        // Create a List of desc
        var columnsPerRowIndex = 0;
        var contentLayout = '';
        if (contents != null && contents != undefined && contents.templateContent != null
            && contents.templateContent != undefined)
        {
            for (var i=0; i < contents.templateContent.length; i++)
            {
                contentLayout += mainLayout;

                // Master Title
                if (contents.templateContent[i].mainTitle != null && contents.templateContent[i].mainTitle != "")
                {
                    //display content
                    contentLayout = contentLayout.replace("@displayStyle", "");
                    contentLayout = contentLayout.replace("@mainTitle", contents.templateContent[i].mainTitle);
                }
                else
                {
                    // display none
                    contentLayout = contentLayout.replace("@displayStyle", displayStyle);
                    contentLayout = contentLayout.replace("@mainTitle", "");
                }

                // List
                var rowLayoutList = '';
                for (var j=0; j < contents.templateContent[i].imageList.length; )
                {
                    rowLayoutList += rowLayout;

                    var columnsPerRowLayoutList = '';
                    for (var k=0; k < columnsPerRow && j < contents.templateContent[i].imageList.length; k++, j++)
                    {
                        if (k == 0)
                            columnsPerRowLayoutList += columnsPerRowLayoutleft;
                        else
                            columnsPerRowLayoutList += columnsPerRowLayoutright;

                        if (contents.templateContent[i].imageList[j].image != null && contents.templateContent[i].imageList[j].image != "")
                            var imagePath = contents.imageBase + contents.templateContent[i].imageList[j].image;
                        else
                            var imagePath = "images/avatar-001.jpg";
                        columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@mainImage", imagePath);

                        // If Title is not provided then we will consider the first description as Title
                        var l = 0;
                        if (contents.templateContent[i].imageList[j].title != null && contents.templateContent[i].imageList[j].title != "")
                        {
                            columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@displayStyle", "");
                            columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@mainTitle", contents.templateContent[i].imageList[j].title);
                        }
                        else
                        {
                            columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@displayStyle", "");
                            columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@mainTitle", contents.templateContent[i].imageList[j].description[0]);
                            l = 1;
                        }

                        var columnsPerRowSubDetailsLayoutList = '';
                        for (; l < contents.templateContent[i].imageList[j].description.length; l++)
                        {
                            if (contents.templateContent[i].imageList[j].description[l] != null
                                && contents.templateContent[i].imageList[j].description[l] != "") {
                                columnsPerRowSubDetailsLayoutList += columnsPerRowSubDetailsLayout;
                                columnsPerRowSubDetailsLayoutList = columnsPerRowSubDetailsLayoutList.replace("@subDetails", contents.templateContent[i].imageList[j].description[l]);
                            }
                        }

                        if (contents.templateContent[i].imageList[j].contact != null
                            && contents.templateContent[i].imageList[j].contact != undefined
                            && contents.templateContent[i].imageList[j].contact != "") {
                            columnsPerRowSubDetailsLayoutList += columnsPerRowSubContactDetailsLayout;
                            columnsPerRowSubDetailsLayoutList = columnsPerRowSubDetailsLayoutList.replace("@phoneno", contents.templateContent[i].imageList[j].contact);
                            columnsPerRowSubDetailsLayoutList = columnsPerRowSubDetailsLayoutList.replace("@phoneno", contents.templateContent[i].imageList[j].contact);
                        }

                        if (contents.templateContent[i].imageList[j].email != null
                            && contents.templateContent[i].imageList[j].email != undefined
                            && contents.templateContent[i].imageList[j].email != "") {
                            var allEmails = contents.templateContent[i].imageList[j].email.split(",");
                            for (var m = 0; m < allEmails.length; m++) {
                                columnsPerRowSubDetailsLayoutList += columnsPerRowSubEmailDetailsLayout;
                                columnsPerRowSubDetailsLayoutList = columnsPerRowSubDetailsLayoutList.replace("@emailId", allEmails[m]);
                                columnsPerRowSubDetailsLayoutList = columnsPerRowSubDetailsLayoutList.replace("@emailId", allEmails[m]);
                            }
                        }

                        columnsPerRowLayoutList = columnsPerRowLayoutList.replace("@subDetailsLayoutArray", columnsPerRowSubDetailsLayoutList);
                    }

                    rowLayoutList = rowLayoutList.replace("@columnsPerRowLayoutArray", columnsPerRowLayoutList);
                }

                contentLayout = contentLayout.replace("@rowsDetailsArray", rowLayoutList);
            }
        }
        else
        {
            contentLayout = '<div style="width:100%; margin:0 auto; padding-top:10px; text-align:center"> \
                                <p style="font-weight:bold;"> '+localizeString("Coaches_Label_Nodata")+' </p>                   \
                            </div> ';
        }

        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        return contentLayout;
    };

}());