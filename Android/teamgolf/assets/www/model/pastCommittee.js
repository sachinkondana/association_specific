(function () {

    var enableDebugMode = 0;

    /* Default template function name for this page */
    var defaultPastCommiteeTemplateFunction = "renderImageDetailsGridViewContents";

    /* Page id for this page */
    var pageId = "12";

    var contentList = {
        "imageBase": "images/",
        "templateContent":
        [
            {
                "imageList" : [
                    {
                        "image" : "avatar-001.jpg",
                        "title" : "Trevor Morillo",
                        "description" : ["General manager 2012-2016"]
                    },
                ]
            }
        ]
    };

    /**
     *  Fetch static contents in JSON
     */
    getStaticContent = function() {
        return contentList;
    };

    /**
     *  Fetch contents from server
     */
    getStaticContentFromServer = function() {

    };

    /**
     *  Display Content based on Image Details Grid
     *  @param content: Must be as required for Image Details Grid
     */
    displayExistingContent = function(content) {
        /*var themedContent = renderMultipleParaThemeContents(content);
        $("#content").html(themedContent);*/
        if (enableDebugMode)
            alert("updatedPageContent = " + JSON.stringify(content));

        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PastCommiteeTemplate");

        if (existingTemplate == null || existingTemplate == undefined) {
            existingTemplate = defaultPastCommiteeTemplateFunction;
            localStorage.setItem("PastCommiteeTemplate", existingTemplate);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Display Content based on Template Fetched from the server
     *  @param content: Must be as required for Fetched template type
     */
    displayUpdatedContent = function(content) {
        /* get existing template from local storage */
        var existingTemplate = localStorage.getItem("PastCommiteeTemplate");

        /* get updated template fetched from the server */
        var templateFunc = getTemplateRenderingFunction();
        if (templateFunc != null && templateFunc != undefined) {
            existingTemplate = templateFunc;
            localStorage.setItem("PastCommiteeTemplate", templateFunc);
        }
        /* call the rendering function for the selected template for rendered content */
        var themedContent = window[existingTemplate](content);

        /* update the html with the rendered content */
        $("#content").html(themedContent);
    };

    /**
     *  Fetch contents and display
     */
    getContent = function() {

        var lastUpd = localStorage.getItem("PastCommiteeLastUpdated");
        if (lastUpd == null || lastUpd == undefined) {
            lastUpd = getFormatedDateTime();
            localStorage.setItem("PastCommiteeLastUpdated", lastUpd);
        }

        /* get the cached content first */
        var currentPastCommiteeContent = JSON.parse(localStorage.getItem("CurrentPastCommiteeContent"));
        if (currentPastCommiteeContent == null || currentPastCommiteeContent == undefined || currentPastCommiteeContent == "") {
            currentPastCommiteeContent = getStaticContent();
            localStorage.setItem("CurrentPastCommiteeContent", JSON.stringify(currentPastCommiteeContent));
        }

        /* Display Cached content first */
        displayExistingContent(currentPastCommiteeContent);

        /* get the updated content for the page */
        fetchPage(pageId, lastUpd, pageFetchPastCommiteeSuccess);
    };

    /**
     * Success callback for the fetchPage.
     * Display the updated content.
     */
    pageFetchPastCommiteeSuccess = function (updatedPageContent) {
        if (enableDebugMode)
            alert("content = " + JSON.stringify(updatedPageContent));
        if (updatedPageContent == null || updatedPageContent.length == 0) /* content not updated */
            return;

        /* cache the updated content */
        localStorage.setItem("CurrentPastCommiteeContent", JSON.stringify(updatedPageContent));

        /* display the updated content */
        displayUpdatedContent(updatedPageContent);

        /* get the last updated on value */
        var lastUpdateOn = getLastUpdatedOn();

        /* update the cached lastupdate value if received value is valid */
        if (lastUpdateOn != null && lastUpdateOn != undefined) {
            localStorage.setItem("PastCommiteeLastUpdated", lastUpdateOn);
        }
    };

}());