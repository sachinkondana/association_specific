(function () {

    var enableDebugMode = 0;

    /**
     * Template ID: ConversationLayout-001: Conversation Layout
     *  Theme Consisting of member conversation
     *          --------------------------------------------------------
     *          --------------------------------------------------------
     */
    var mainLayout = '                                                                            \
        <div class="list-chat" style="margin-bottom:50px;">                                       \
            <ul class="chat" id="chat-item-list">                                                 \
                @ListOfItems                                                                      \
            </ul>                                                                                 \
        </div>';

    var requestorMessageLayout = '                                                                \
        <li>                                                                                      \
            <div class="queryMesg">                                                               \
                @requestMessage                                                                   \
            </div>                                                                                \
        </li>                                                                                     \
        <li>                                                                                      \
            <div style="min-width: 50%; float: right; text-align: right; font-size: 0.4em;">      \
                @requestor                                                                        \
            </div>                                                                                \
        </li>';

    var responserMessageLayout = '                                                                \
        <li>                                                                                      \
            <div class="ansMessage">                                                              \
                @responseMessage                                                                  \
            </div>                                                                                \
        </li>                                                                                     \
        <li>                                                                                      \
            <div style="min-width: 50%; float: left; text-align: left; font-size: 0.4em;">        \
                @responser                                                                        \
            </div>                                                                                \
        </li>';

    var responseMessenger = '<div class="msgTxtbox">                                                                \
            <div class="leftDiv"><input class="inputMsg" type="text" placeholder="Message" id="resp-msgarea"></div> \
            <div class="rightDiv disabled" onclick="return sendMessageToUser();" id="sendMessageDiv">               \
                <i class="fa fa-paper-plane-o" aria-hidden="true"> </i>                                             \
            </div>                                                                                                  \
            </div>';

    /*var itemListSeparator = '                                                                     \
        <div class="listHorizontalLine"></div>                                                    \
        ';

    var displayStyle = ' style="display:none;" ';*/

    /**
     *  Renders the content as per the Conversation Layout and returns the rendered content
     *  @param contents: JSON String of @inputData format
     *
     *  [
     *      {
     *          "category" : "request | response",
     *          "sender" : "SENDER",
     *          "message" : "Message"
     *      },
     *      {
     *          "category" : "request | response",
     *          "sender" : "SENDER",
     *          "message" : "Message"
     *      },
     *  ]
     *  @returns rendered content as per theme
     */
    renderConversationLayoutContents = function (conversationLists) {
        if (enableDebugMode)
            alert("List Items: " + JSON.stringify(conversationLists));

        var contentLayout = mainLayout;
        var conversationLayoutArray = '';

        for (var i = 0; i < conversationLists.length; i++) {
            if (conversationLists[i].category == "request") {
                conversationLayoutArray += requestorMessageLayout;
                conversationLayoutArray = conversationLayoutArray.replace("@requestMessage", conversationLists[i].message);
                conversationLayoutArray = conversationLayoutArray.replace("@requestor", conversationLists[i].sender);
            } else if (conversationLists[i].category == "response") {
                conversationLayoutArray += responserMessageLayout;
                conversationLayoutArray = conversationLayoutArray.replace("@responseMessage", conversationLists[i].message);
                conversationLayoutArray = conversationLayoutArray.replace("@responser", conversationLists[i].sender);
            }
        }

        contentLayout = contentLayout.replace("@ListOfItems", conversationLayoutArray);

        //contentLayout += responseMessenger;

        if (enableDebugMode)
            alert("New Theme Layout: " + contentLayout);

        return contentLayout;
    };

}());